import { Component, Input, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";

@Component({
  selector: "form-error-display",
  templateUrl: "./form-error-display.component.html",
  styleUrls: ["./form-error-display.component.scss"],
})
export class FormErrorDisplayComponent implements OnInit {
  @Input() formControlObject: FormControl;
  @Input() errorMessages: Record<string, string> = {};

  knownMessage = {
    required: "This column is required",
  };

  constructor() {}

  get formErrorDisplay() {
    if (!this.formControlObject.touched) return null;

    try {
      const firstError = Object.keys(this.formControlObject.errors)[0];
      return {
        ...this.knownMessage,
        ...this.errorMessages,
      }[firstError];
    } catch (e) {
      return null;
    }
  }

  ngOnInit(): void {
    console.log(this.formControlObject);
  }
}
