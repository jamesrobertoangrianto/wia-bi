import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { faArrowUp, faCartPlus, faEye } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class BIDashboardComponent implements OnInit, OnDestroy{
  warehouse: any;
  stock_level: any;
  bestselling: any;
  faEye=faEye
  faArrowUp=faArrowUp
  faCartPlus=faCartPlus
  tab_option = [
   
    {
      'label' : 'Value',
      'name' : 'stock_level_value',
      'is_active' : true
      
    },
    {
      'label' : 'Stock',
      'name' : 'stock_level',
      
    }
  ]

  ads_tab_option = [
   
    {
      'label' : 'Google Ads',
      'name' : 'GOOGLE_ADS',
      'is_active' : true
      
    },
    {
      'label' : 'Instagram Ads',
      'name' : 'INSTAGRAM_ADS',
      
    }
  ]

  day_option_best = [
   
    {
      'label' : '30 Day',
      'name' : '30',
      'is_active' : true
      
    },
    {
      'label' : '14 day',
      'name' : '14',
      
    },
    {
      'label' : '7 day',
      'name' : '7',
      
    }
  ]


  sales_option = [
   
    {
      'label' : 'All',
      'name' : 'all',
      'is_active' : true
      
    },
    
    {
      'label' : 'WEB',
      'name' : '1',
    },
    {
      'label' : 'CHAT',
      'name' : '33',
    },
    {
      'label' : 'STORE',
      'name' : '32',
    },
    {
      'label' : 'TKPD',
      'name' : '8',
    },
    {
      'label' : 'SHPE',
      'name' : '9',
    },
  ]
  
  day_option_view = [
   
    {
      'label' : '30 Day',
      'name' : '30',
      'is_active' : true
      
    },
    {
      'label' : '14 day',
      'name' : '14',
      
    },
    {
      'label' : '7 day',
      'name' : '7',
      
    }
  ]
  
  top_view: any;
  buffer: any;
  version: string;
  show_report: boolean;
  link: any;
  sales_order: any;
  total_trx: number;
  sales_order_summary: {};
  sales_order_summary_total_trx: number;
  sales_order_summary_total_revenue: number;
  selected_product_id: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private title:Title,
    private toastr: ToastrService,
    private webService : ManualOrderService,

  ) {
  }

  ngOnDestroy(): void {
   
  }

  public BIMenuSelection;
  dateStr: string


  async ngOnInit() {

    this.show_report = false
  }

  getReport(){
    this.show_report = true
    this.getLinkbuilder('GOOGLE_ADS')
    this.getBestSelling(30)
    this.getSalesOrderSummary('all')

    this.getWarehouse()
    this.getDashboard('stock_level_value')
    this.getTopView(30)
   
    this.getWarehouseBuffer(1)
    
  }

  
  getconvert(items,name){
    var  filteredResult = items.find((e) => e.type == name);
    if(filteredResult){
      return filteredResult.total

    }else{
      return '-'
    }
  }


  async getLinkbuilder(source) {
    try {
      //le.log(source)
      let response = await this.webService.getLinkbuilder('?source='+source)
     this.link = response.data
     
     this.link.forEach(item => {
        item.add_to_cart = this.getconvert(item.purchase,'add_to_cart')
        item.checkout = this.getconvert(item.purchase,'checkout_page')
        item.total_purchase = this.getconvert(item.purchase,'purchase')
        item.order = this.getconvert(item.purchase,'order')
       

     });
     //console.log(this.link)

    } catch (e) {
      console.log(e)
    } finally {
      
    }
    
  }
  changeStockLevel(e){
    this.stock_level = null
    this.getDashboard(e.name)
  }

  viewProductPerformance(id){
    this.selected_product_id = id
    
  }

  async getSalesOrderSummary(id){
    // console.log(this.params)
    try {
     
         let response = await this.webService.getSalesOrderSummary('?source='+id)
        console.log(response)
        this.sales_order = response.items
        this.total_trx = 0
        this.sales_order_summary_total_trx = 0
        this.sales_order_summary_total_revenue = 0

        this.sales_order.forEach(item => {
            this.sales_order_summary_total_trx += parseFloat(item.total_trx)
            this.sales_order_summary_total_revenue += parseFloat(item.total_revenue)

            item.per = (item.total_trx/80)*100
            item.total_revenue =  new Intl.NumberFormat("id-ID", {
              style: "currency",
              currency: "IDR"
            }).format(item.total_revenue);
        });
       
        } catch (e) {
          console.log(e)
        } finally {
           
        }
  }

  

  async getWarehouseBuffer(id){
    // console.log(this.params)
    try {
     
         let response = await this.webService.getWarehouseBuffer(id,'')
        console.log(response)
        this.buffer = response.data

        this.buffer.forEach(item => {
            item.insight = this.getInsight(item)
        });
       
        } catch (e) {
          console.log(e)
        } finally {
           
        }
  }
  

  getInsight(item){
    if(item.stock_status == 'understock'){
      var sold_forcast = item.sold.value * 3
      var current_forcast = item.current_stock * 3
      if(item.current_stock > sold_forcast){
        return 'reduce buffer'
      }else if(item.sold.value > current_forcast){
        return 'buy more stock'

      }

    }
    if(item.stock_status == 'overstock'){
      var sold_forcast = item.sold.value * 3
      var current_forcast = item.current_stock * 3

      if(sold_forcast < item.current_stock){
        return 'flush stock'
      }
    }
  }
  async getWarehouse(){
    // console.log(this.params)
    try {
      
         let response = await this.webService.getWarehouse('')
          this.warehouse = response.data
         // console.log(response)
       
        } catch (e) {
          console.log(e)
        } finally {
           
        }
  }

  



  async getBestSelling(day){
    try {
      
      let response = await this.webService.getDashboard('best_selling','?day='+day)
      this.bestselling = response.data
      console.log(response)
     } catch (e) {
       console.log(e)
     } finally {
        
     }
  }

  async getDashboard(id){
    // console.log(this.params)
    try {
      
         let response = await this.webService.getDashboard(id,'')
        this.stock_level = response.data
        console.log(this.stock_level)
        this.stock_level.forEach(item => {
           item.sold_per_stock = item.summary.total_order / item.count
           item.age_per_sold = item.summary.avg_age / item.summary.total_order
          
           item.sold_per_age = (item.sold_per_stock/item.age_per_sold) * 100

           if(item.summary.avg_age > 45 && item.sold_per_age < 45){
            item.sold_per_age = Math.round(item.sold_per_age)

           }else{
            item.sold_per_age = null
           }
           
        })
        } catch (e) {
          console.log(e)
        } finally {
           
        }
  }

  async getTopView(day){
     console.log(day)
    try {
      
        
      let response = await this.webService.getDashboard('top_view','?day='+day)

        this.top_view = response.data
         console.log(this.top_view)
          this.top_view.forEach(item => {
            item.rate = this.getRate(item)
            item.class = this.getClass(item)
          });
       
        } catch (e) {
          console.log(e)
        } finally {
           
        }
  }

  getRate(item){
    var rate =  (item.summary.stock_level / item.total) * 100
    return Math.round(rate)
  }

  getClass(item){
    var rate = (item.summary.stock_level / item.total) * 100
    if(rate < 1){
      return 'brown'
    }
    else if(rate > 10){
      return 'brown'
    }
  }


  isAdmin(){
   if( localStorage.getItem('isAdmin') == 'true'){
     return true
   }
  }


 
}
