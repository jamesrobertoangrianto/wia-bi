import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-placement',
  templateUrl: './placement.component.html',
  styleUrls: ['./placement.component.scss']
})
export class PlacementComponent implements OnInit {
  placement: any;
  selectedDevice: any;
  params: any;
  collection: any;
  selected_placement: any;

  constructor(

    private webService: ManualOrderService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {

    this.getPlacement()
  }




  async addPlacement() {
    let form = {


    }
    try {

      let response = await this.webService.addPlacement(form)




    } catch (e) {
      console.log(e)
    } finally {
      this.ngOnInit()
    }
  }


  updateProduct(item, e) {
    let form = {}
    form[e.id] = e.value
    console.log(form)

    this.updatePlacement(item, form)
  }


  updateStatus(item, e, state) {
    let form = {}
    form[e] = state
    console.log(form)
    //this.toast.sendMessage(state ? 'Enabled' : 'Disabled')

    this.updatePlacement(item, form)
  }


  updateDestination(item, type) {
    console.log(item.id)
    console.log(this.selected_placement)
    if (type == 'category') {
      let form = {
        'category_id': item.id
      }
      this.updatePlacement(this.selected_placement, form)
    }

    this.selected_placement = null


  }

  async updatePlacement(item, form) {


    try {
      this.spinner.show();
      let response = await this.webService.updatePlacement(item.id, form)

      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.ngOnInit()
    }
  }

  async addCategoryProduct(item) {

  }



  async searchCollection(e) {
    console.log(e)
    if (e) {
      try {
        let response = await this.webService.getCategoryCollection('?name=' + e);
        console.log(response)
        this.collection = response.data


      } catch (e) {
        console.error(e);
      } finally {
        this.spinner.hide();
      }
    }



  }



  async onChange(event: any, item, path) {

    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      formData.append('file', file);

      try {

        let res = await this.webService.uploadPhoto(formData, 'placement_banner')
        console.log(res)

        if (res.data) {
          let form = {}
          form[path] = res.path

          console.log(form)
          this.updatePlacement(item, form)
        }

      } catch (error) {
        console.log(error)
      }
      finally {



      }

    }
  }



  async getPlacement() {

    try {
      this.spinner.show();
      let response = await this.webService.getPlacement('')

      console.log(response)
      this.placement = response.data

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }





}
