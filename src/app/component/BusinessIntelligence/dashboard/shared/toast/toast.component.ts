

import { Component, OnInit, OnChanges } from '@angular/core';
import { faTimes, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
})

export class ToastComponent implements OnInit {
  msg: string
  actionName: string
  actionFn: VoidFunction
  show: boolean
  isFull : boolean = false
  faTimes=faTimesCircle

  timeout
  messages: any[];

  constructor(
    public toastService : ToastService
  ) { }

  
  ngOnInit() {
    this.messages =[]
    this.toastService.launchEvent.subscribe(
      (res)=>{
      
        if(res == 'launch') {
          clearTimeout(this.timeout)
         
          this.messages.push(
          {'msg' : this.toastService.messages}
          )

         
         

          this.messages.forEach(element => {
            this.timeout = setTimeout(()=>{
              this.dismiss(element)
              },4500)
          
            });
          

        }
      }
    )
  }

  // executeFn(){
  //   this.show=false
  //   clearTimeout(this.timeout)
  //   this.toastService.actionFn()
  //   this.toastService.destroy()
  // }

  dismiss(item){
    var index =  this.messages.indexOf(item);
       this.messages.splice(index, 1)
  }

}


