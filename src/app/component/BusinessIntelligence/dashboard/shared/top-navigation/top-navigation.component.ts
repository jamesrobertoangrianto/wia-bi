import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faBars, faBell, faBinoculars, faCartPlus, faChartBar, faChartLine, faHamburger, faHandHoldingHeart, faHome, faPoll, faPollH, faSearch, faSignOutAlt, faStore, faTable, faTabletAlt, faTshirt, faWallet, faWarehouse } from '@fortawesome/free-solid-svg-icons';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { interval, Subscription } from 'rxjs';

subscription: Subscription;
@Component({
  selector: 'app-top-navigation',
  templateUrl: './top-navigation.component.html',
  styleUrls: ['./top-navigation.component.scss']
})
export class TopNavigationComponent implements OnInit {
  faBell = faBell
  faBars = faBars
  faSearch = faSearch
  faHome = faPoll
  faSignOutAlt = faSignOutAlt
  user_account: any;
  notif: any;
  notif_count: any;
  showModal: any;
  top_bar_menu: any;
  total_order: any;
  version: string;
  showAddOrder: any;




  constructor(
    public webService: ManualOrderService,
    private router: Router


  ) { }

  ngOnInit(): void {

    this.version = '10.6'

    let customer = JSON.parse(localStorage.getItem("customer"))
    let isWarehouse = JSON.parse(localStorage.getItem("isWarehouse"))

    if (customer) {
      if (!isWarehouse) {
        this.user_account = customer


        if (!localStorage.getItem("account_role")) {
          this.logout()
        }

        this.run()


        const source = interval(300000);

        source.subscribe(val => this.getNotification('wait_for_approval'));
        source.subscribe(val => this.getNotification('approve'));
        //  source.subscribe(val => this.getTopNotif('order'));


      }

    }
  }

  run() {
    this.top_bar_menu = [

      {
        'name': 'Product',
        'link': '/product',

        'icon': faTshirt,

        'is_active': this.getAccess(),
      },
      // {
      //   'name': 'Affiliate',
      //   'link': '/referal',
      //   'id': 'withdrawal',
      //   'icon': faHandHoldingHeart,
      //   'notif_label': 'Withdrawal',
      //   'is_active': this.getAccess(),
      // },

      {
        'name': 'Warehouse - WIA HQ',
        'link': '/warehouse/view/1',
        'id': 'warehouse',
        'query': 'inventory',
        'icon': faWarehouse,
        'is_active': this.getAccess(),
      },
      {
        'name': '  Sales Pipeline',
        'link': '/business-order',
        'id': 'pipeline',
        'notif_label': 'Request',
        'icon': faPollH,
        'is_active': this.getAccess(),

      },



      {
        'name': 'Purchase Order - Wait Approval',
        'link': '/purchase',
        'id': 'wait_for_approval',
        'query': 'wait_for_approval',
        'icon': faCartPlus,
        'notif_label': 'Wait for Approval',

        'is_active': this.getAccessRole('FINANCE'),
        'count': this.getNotification('wait_for_approval')
      },

      {
        'name': 'Purchase Order - Approved',
        'link': '/purchase',
        'id': 'approve',
        'query': 'approve',
        'icon': faCartPlus,
        'notif_label': 'Ready to Order',

        'is_active': this.getAccessRole('BUSINESS'),
        'count': this.getNotification('approve')
      },






      {
        'name': 'Order Management',
        'link': '/order-management-v2',
        'query': 'processing',
        'id': 'order',
        'icon': faStore,
        'is_active': this.getAccess(),
        'notif_label': 'Paid',
        'count': 0,

      }
    ]

    // this.getTopNotif('order')
    this.getTopNotif('pipeline')
    this.getTopNotif('purchase')

    this.getAffiliateWithdrawal()
  }


  getAccess() {

    // if(this.user_account.manager == '23'){
    //   return true
    // }

    let expression = this.user_account.manager
    switch (expression) {
      case 'warehouse':
        // code block
        return false
        break;

      default:
        // code block
        return true
    }

  }


  getAccessRole(id) {

    // if(this.user_account.manager == '23'){
    //   return true
    // }

    if (localStorage.getItem("account_role") == 'SUPERADMIN') {
      return true
    }


    if (id == localStorage.getItem("account_role")) {
      return true
    } else {
      return false
    }



  }


  async getAffiliateWithdrawal() {
    // console.log(this.params)
    try {

      let response = await this.webService.getAffiliateWithdrawal('')
      // this.promotions = response.promotions

      let objIndex = this.top_bar_menu.findIndex(obj => obj.id == 'withdrawal');
      this.top_bar_menu[objIndex].count = response.data.length


    } catch (e) {
      console.log(e)
    } finally {

    }
  }




  async getNotification(id) {
    console.log('get notif')
    try {

      let response = await this.webService.getPurchaseCount(id)


      let objIndex = this.top_bar_menu.findIndex(obj => obj.id == id);
      this.top_bar_menu[objIndex].count = response.data

    } catch (e) {
      console.log(e)
    } finally {

    }
  }


  logout() {
    console.log('logout')
    localStorage.removeItem("session_token");
    localStorage.removeItem("isAdmin");
    localStorage.removeItem("customer");
    localStorage.removeItem("account_name");

    localStorage.removeItem("account_role");

    window.location.reload()


  }


  async getTopNotif(id) {

    try {

      let response = await this.webService.getTopNotif(id)


      let objIndex = this.top_bar_menu.findIndex(obj => obj.id == id);
      this.top_bar_menu[objIndex].count = response.count


    } catch (e) {
      console.log(e)
    } finally {

    }

  }


  playSound(url) {
    const audio = new Audio(url);
    audio.play();
  }


  openModal(id) {
    this.showModal = id
  }
  closeModal() {
    this.showModal = null
  }

  navigateTo(id) {
    this.router.navigate(
      [id]
    );

  }



}
