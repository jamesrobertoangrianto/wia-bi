import { Component, Input, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
  @Input() items : any
  @Input() start_date : any
  @Input() end_date : any
  chart: {};
  
  dates: any[];
  chart_data: any[];
  constructor(  private route: ActivatedRoute,) { }

  ngOnInit(): void {
  //  console.log(this.items)
  //  console.log(this.start_date)
  //  console.log(this.end_date)
   
   this.route.queryParamMap.subscribe(queryParams => {
    this.chart_data = []

    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.dates = this.getDates(new Date(this.start_date), new Date(this.end_date))
      if(this.items){
        this.dates.forEach(item => {
        
  
          this.chart_data.push( this._getTransactionItemByDate(formatDate(item,'yyyy-MM-dd',"en-US")) )
      });
  
      console.log('init')
   
      }
     
 }, 100);


   
  
     
  })
   
   
   
  }

   getDates (startDate, endDate) {
    const dates = []
    let currentDate = startDate
    const addDays = function (days) {
      const date = new Date(this.valueOf())
      date.setDate(date.getDate() + days)
      return date
    }
    while (currentDate <= endDate) {
      dates.push(currentDate)
      currentDate = addDays.call(currentDate, 1)
    }
    return dates
  }

  
  _getTransactionItemByDate(date){

   
    let total = 0
    let revenue = 0
    let transaction = 0
    let bar_height = 0
    this.items.forEach(item=>{
     if( date == formatDate(item.order_date,'yyyy-MM-dd',"en-US")){
        total+= parseFloat ( item.total_product) 
        revenue+= parseFloat ( item.grand_total) 
        transaction+= 1

     }        
    })

    bar_height = (200/40) * total
    
   
    let a = {
      'date' :  formatDate(date,'MM-dd',"en-US"),
      'transaction' : transaction,
      'product' : total,
      'revenue' : revenue,
      'bar_height' : bar_height +'px'
    }
    return a


    
  

  }


}
