import { Component, Input, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-product-performance',
  templateUrl: './product-performance.component.html',
  styleUrls: ['./product-performance.component.scss']
})
export class ProductPerformanceComponent implements OnInit {
  product: any;
  @Input() product_id: any
  sales_perfomance: any
  view_performance: { id: string; label: string; value: any; }[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private title:Title,
    private toastr: ToastrService,
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
      this.getProductPerformance(this.product_id)
  }


  async getProductPerformance(id){
    // console.log(this.params)
    try {
      
         let response = await this.webService.getProductPerformance(id)
          this.product = response.data
      
        } catch (e) {
          console.log(e)
        } finally {
            this.buildSales()
            this.buildView()
        }
  }

  getPerc(item){
    let total = 0
     this.sales_perfomance.forEach(item => {
      total+=7
    });

    let final =  Math.round((((item.value.count/item.id)*100)))
    return final>100?100:final
  }
  getPerc2(item){
    let total = 0
     this.view_performance.forEach(item => {
      total+=item.value?.total
    });
    return Math.round((((item.value.total/item.id)*100)/total)*100)
  }
  buildSales(){
    this.sales_perfomance = [
      {
        'id' : '7',
        'label' : 'last 7 day',
        'value' : this.getSalesPerformance('last_7'),
       // 'percentage' :  (this.getSalesPerformance('last_7') / 7)
      },
      {
        'id' : '14',
        'label' : 'last 14 day',
        'value' : this.getSalesPerformance('last_14'),
       // 'percentage' :  (this.getSalesPerformance('last_14') / 14)

      },
      {
        'id' : '21',
        'label' : 'last 21 day',
        'value' : this.getSalesPerformance('last_21'),
       // 'percentage' :  (this.getSalesPerformance('last_21') / 21)

      },
      {
        'id' : '30',
        'label' : 'last 30 day',
        'value' : this.getSalesPerformance('last_30'),
       // 'percentage' :  (this.getSalesPerformance('last_30') / 30)

      },
      {
        'id' : '45',
        'label' : 'last 45 day',
        'value' : this.getSalesPerformance('last_45'),
       // 'percentage' :  (this.getSalesPerformance('last_45') / 45)

      },
      {
        'id' : '60',
        'label' : 'last 60 day',
        'value' : this.getSalesPerformance('last_60'),
       // 'percentage' :  (this.getSalesPerformance('last_60') / 60)

      }
    ]

    console.log(this.sales_perfomance)
  }


  buildView(){
    this.view_performance = [
      {
        'id' : '7',
        'label' : 'last 7 day',
        'value' : this.getViewPerfomance('last_7'),
        
      },
      {
        'id' : '14',
        'label' : 'last 14 day',
        'value' : this.getViewPerfomance('last_14')
      },
      {
        'id' : '21',
        'label' : 'last 21 day',
        'value' : this.getViewPerfomance('last_21')
      },
      {
        'id' : '30',
        'label' : 'last 30 day',
        'value' : this.getViewPerfomance('last_30')
      },
      {
        'id' : '45',
        'label' : 'last 45 day',
        'value' : this.getViewPerfomance('last_45')
      },
      {
        'id' : '60',
        'label' : 'last 60 day',
        'value' : this.getViewPerfomance('last_60')
      }
    ]

    console.log(this.sales_perfomance)
  }

  getSalesPerformance(id){
  return  this.product.sales_performance[id][0]?this.product.sales_performance[id][0]:0
  }
  getViewPerfomance(id){
    return  this.product.events_performance[id][0]?this.product.events_performance[id][0]:0
    }



  async updateInventory(item,type){
    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    let currentDate = tomorrow.toJSON().slice(0, 10);

    let form ={
     
      'id' : item.id,
      'type' : type,
      'flashdeal_end_date' : currentDate,
     

   }
  
    try {
      //this.spinner.show();
         let response = await this.webService.updateInventory(item.id,form)
       
    //console.log(response)
       
        } catch (e) {
          console.log(e)
        } finally {
           // this.spinner.hide();
         
          this.ngOnInit()
            
        }
  }




}
