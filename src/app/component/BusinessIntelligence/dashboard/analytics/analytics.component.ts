import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';

import { faCartPlus, faChevronDown, faSearch, faTrash } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {
  source = ['INSTAGRAM','YOUTUBE','INSTAGRAM_ADS','GOOGLE_ADS','FACEBOOK','TWITTER','WHATSAPP']
  faTrash=faTrash
  linkForm = new FormGroup({
    link: new FormControl(null,{validators: [Validators.required]}),
    description: new FormControl(null,{validators: [Validators.required]}),
    campaign_id: new FormControl(null,{validators: [Validators.required]}),
    source: new FormControl(null,{validators: [Validators.required]}),
   
  
  
  })

  faChevronDown=faChevronDown
  faSearch=faSearch
  tab_menu_list = [
    {
      'id':'link_builder',
      'label' : 'Link Builder'
    },
    {
      'id':'conversion',
      'label' : 'Conversion'
    },
    {
      'id':'acquisition',
      'label' : 'Acquisition'
    },
    {
      'id':'behavior',
      'label' : 'Behavior'
    }
  
  ]

  sortListItems = [
    {
     
      'name' : 'source',
      'items' : [
        {
          'id' : 'youtube',
          'name' : 'Youtube',
          'label' : 'Youtube',
    
        },
        {
          'id' : 'instagram',
          'name' : 'Instagram',
          'label' : 'Instagram',
    
        },
        {
          'id' : 'GOOGLE_ADS',
          'name' : 'GOOGLE_ADS',
          'label' : 'GOOGLE_ADS',
    
        },
        {
          'id' : 'INSTAGRAM_ADS',
          'name' : 'INSTAGRAM_ADS',
          'label' : 'INSTAGRAM_ADS',
    
        },
       
        {
          'id' : 'WHATSAPP',
          'name' : 'WHATSAPP',
          'label' : 'WHATSAPP',
        },
        
        {
          'id' : 'FACEBOOK',
          'name' : 'FACEBOOK',
          'label' : 'FACEBOOK',
        },
        {
          'id' : 'TWITTER',
          'name' : 'TWITTER',
          'label' : 'TWITTER',
        },
       

       
      ]

    },
    
  ]
  conversion: any;
  products: any;
showModal: any;
  orders: any;
  total_purchase_count: number;
  total_purchase_value: number;
  total_order_value: number;
  total_order_count: number;
  total_payment_count: any;
  total_payment_value: any;
  total_checkout_value: any;
  total_checkout_count: any;
  total_product_value: any;
  total_product_count: any;
  total_cart_value: any;
  total_cart_count: any;
  params: string;
  start_date: any;
  end_date: any;
  tab_view: string;
  link: any;
  selected_item: any;
  selected_order: any;

  faCartPlus=faCartPlus
  constructor(
    private customerService : ManualOrderService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private serializer: UrlSerializer,
    private route: ActivatedRoute,


  ) { }

  ngOnInit(): void {
    



    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")

      this.start_date = queryParams.get("start_date")
      this.end_date = queryParams.get("end_date")
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))      

      if(this.tab_view == 'link_builder'){
        this.getLinkbuilder()

      }
      if(this.tab_view == 'conversion'){
        this.getConversion()


      }

    })

    
  }

  getPercentage(view,result){
    var total = (result/view) *100
    return Math.round(total) + '%'
  }

  updateDate(){


    this.router.navigate( [], {
      queryParams: { 
        start_date: this.start_date,
        end_date: this.end_date 
      },
      queryParamsHandling:'merge',
    }
  );
  }


  search(){
    this.router.navigate( ['/analytics'], {
          queryParams: { 
            start_date: this.start_date,
            end_date: this.end_date,
           
           },
          queryParamsHandling:'merge',
        }
      );
    
  }

  async getConversion() {
    try {
      this.spinner.show();
      let response = await this.customerService.getConversion(this.params)
      this.conversion = response.data

      this.total_payment_count = 0
      this.total_payment_value = 0

      this.total_order_count = 0
      this.total_order_value = 0

      this.total_purchase_count = 0
      this.total_purchase_value = 0

      this.total_checkout_count = 0
      this.total_checkout_value = 0

      this.total_product_count = 0
      this.total_product_value = 0

      this.total_cart_count = 0
      this.total_cart_value = 0

      this.conversion.forEach(item => {
        this.total_purchase_count +=item.purchase.count
        this.total_purchase_value +=item.purchase.value

        this.total_order_count +=item.order.count
        this.total_order_value +=item.order.value

        this.total_payment_count +=item.payment_page.count
        this.total_payment_value +=item.payment_page.value

        this.total_checkout_count +=item.checkout_page.count
        this.total_checkout_value +=item.checkout_page.value

        this.total_product_count +=item.product_view.count
        this.total_product_value +=item.product_view.value
        this.total_cart_count +=item.add_to_cart.count
        this.total_cart_value +=item.add_to_cart.value

      });
      console.log(response)
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
    
  }


  async getLinkbuilder() {
    try {
      this.spinner.show();
      let response = await this.customerService.getLinkbuilder(this.params)
     this.link = response.data
     console.log(response)

     this.link.forEach(item => {
        item.add_to_cart = this.getconvert(item.purchase,'add_to_cart')
        item.checkout = this.getconvert(item.purchase,'checkout_page')
        item.total_purchase = this.getconvert(item.purchase,'purchase')
        item.order = this.getconvert(item.purchase,'order')
       

     });
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
    
  }

  getconvert(items,name){
    var  filteredResult = items.find((e) => e.type == name);
    if(filteredResult){
      return filteredResult.total

    }else{
      return '-'
    }
  }

  async addLinkBuilder() {
    try {
      this.spinner.show();
      let response = await this.customerService.addLinkBuilder(this.linkForm.value)
     this.link = response.data
      console.log(response)
      this.ngOnInit()
     
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.showModal = null
    }
    
  }



  async removeLink(id) {
    try {
      this.spinner.show();
      let response = await this.customerService.deleteLinkbuilder(id)
    
      console.log(response)
      this.ngOnInit()
     
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.showModal = null
    }
    
  }


  async getProductByConversionType(item,type) {
   
    try {
      this.selected_item = item[type]
      this.showModal = 'detailModal'
     console.log(this.selected_item)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
    
  }


  async getCheckoutConversionType(item,type) {
   
    try {
      this.spinner.show();
      this.selected_order = item[type]
      this.showModal = 'trxModal'
      console.log(this.selected_order)
     
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
    
  }

  openInNewTab( id) {
    let newRelativeUrl = this.router.createUrlTree(['/order-management/view/'+id]);
    let baseUrl = window.location.href.replace(this.router.url, '');


   


    window.open(baseUrl + newRelativeUrl, '_blank');
  }

}
