import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronRight, faWarehouse } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.scss']
})
export class WarehouseComponent implements OnInit {
  faChevronRight=faChevronRight
  faWarehouse=faWarehouse
  faWarehousefaWarehouse
  warehouseForm = new FormGroup({
    name: new FormControl(null,{validators: [Validators.required]}),
    location: new FormControl(null,{validators: [Validators.required]}), 
    status: new FormControl(1),
  })
  params: string;
  warehouse: any;
showModal: any;


  constructor(

    private webService : ManualOrderService,
    
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router : Router,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {

    this.route.queryParamMap.subscribe(queryParams => {

      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))     

      this.getWarehouse()
      
     
     
    })
  }



  async getWarehouse(){
    // console.log(this.params)
    try {
      this.spinner.show();
         let response = await this.webService.getWarehouse(this.params)
          this.warehouse = response.data
          console.log(response)
       
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        }
  }


  async addWarehouse(){
    console.log(this.warehouseForm.value)
    try {
      this.spinner.show();
         let response = await this.webService.addWarehouse(this.warehouseForm.value)
       
          console.log(response)

      this._navigateTo(response.data.id)

          
       
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
            this.showModal = null
            this.ngOnInit()
            
        }
  }

  _navigateTo(id) {
    this.router.navigate(
      ['/warehouse/view/'+id]
    );
  }
}
