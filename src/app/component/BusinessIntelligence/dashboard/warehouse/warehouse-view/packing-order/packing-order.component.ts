import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { faBarcode, faCamera, faImage, faQrcode } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-packing-order',
  templateUrl: './packing-order.component.html',
  styleUrls: ['./packing-order.component.scss']
})
export class PackingOrderComponent implements OnInit {
  faCamera=faCamera
  faBarcode=faQrcode

  @Input() inventory: any
  @Output() onItemUpdate = new EventEmitter()
selected_image: any;
showModal: any;
  products: any;
selected_order: any;
selected_order_view: any;
  
  constructor(
    private webService : ManualOrderService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
  }


  itemUpdate(item,e){
    
    let form ={
      'serial_number' : e.value,
      'id' : item.id
   }
    
    this.onItemUpdate.emit(form)
  }
  

  statusUpdate(item){
    let form ={
       'status' : 'ready_to_ship',
       'id' : item.id
    }
    this.onItemUpdate.emit(form)
  }

  viewOrder(item){
      console.log(item)
      this.selected_order_view = item.sales_order
  }

  async onChange(event: any,item) {
    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      formData.append('file', file);
      this.spinner.show();
      try {
      
        let res = await this.webService.uploadPhoto(formData,'packing_photo')
        console.log(res)
        item.photo = res.data
        if(res.data){
          let form ={
            'image_url' : item.photo,
            'id' : item.id
          }
          this.onItemUpdate.emit(form)
        }
        
      } catch (error) {
       console.log(error)
      }
      finally{
       
       this.spinner.hide()
      
      }

    }
  }

  async addThisProduct(item){
    console.log(this.selected_order)
    item.product_id = item.product_id

    item.added = true
    let form={
      'product_id' : item.product_id,
      'parrent_id' : this.selected_order.product_id,
      'order_number' :  this.selected_order.order_number,
      'status' :'ready_to_ship',
      'order_details' : this.selected_order.order_details,
      'warehouse_id' : 1,
      'type' : 'BONUS'
     

    }

    console.log(form)



   this.findInventory(1,form)
   
  }

  async addBonus(order,product){
    console.log(order)
    console.log(product)
    this.selected_order = product
  }


  async searchChanges(e){
    
    console.log(e)
    if(e.length > 0){
     
     
    
    //  let res = await this.webService.getProductList(e,20,1, 0)
      let response = await this.webService.getInventory(1,'?status=check_in&search='+e)
      console.log(response)
      this.products = response.data

      console.log(this.products)
     
    }else{
      
    }
  }

  

  async findInventory(qty,form){
   

    
   
    try {
      this.spinner.show();
         let response = await this.webService.allocateInventory(qty,form)
          console.log(response)

       
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
         
            this.ngOnInit()
            this.onItemUpdate.emit(true)
            
        }
  }




}
