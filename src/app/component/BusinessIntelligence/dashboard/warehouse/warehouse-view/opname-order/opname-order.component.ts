import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-opname-order',
  templateUrl: './opname-order.component.html',
  styleUrls: ['./opname-order.component.scss']
})
export class OpnameOrderComponent implements OnInit {

  @Input() inventory: any
  @Output() onItemUpdate = new EventEmitter()
  showModal: any
  products: any;
  keyword: any;
  page: number;
  variant: any;
  constructor(
    private webService: ManualOrderService, private toast: ToastrService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    console.log(this.inventory)
  }



  async statusUpdate(item, status) {
    let currentDate = new Date().toJSON().slice(0, 10);
    item.is_adding = true
    let form = {
      'status': status,
      'product_id': item.product_id,
      'opname_check_date': currentDate,

    }

    try {

      let response = await this.webService.updateOpnameOrder(form)
      console.log(response)
      if (response.data) {

        item.total = item.total - 1
        if (item.total == 0) {
          var index = this.inventory.indexOf(item);
          this.inventory.splice(index, 1);

        }
      } else {
        var index = this.inventory.indexOf(item);
        this.inventory.splice(index, 1);
      }


    } catch (e) {
      console.log(e)
    } finally {
      // this.spinner.hide();
      item.is_adding = false
      //   this.ngOnInit()
      //  this.onItemUpdate.emit(true)

    }


  }



  async getProductVariant(item) {
    try {

      let response = await this.webService.getProductVariants(item.id)
      this.variant = response.data
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {

    }

  }


  async searchChanges(e) {
    if (!e) {
      this.products = []
    }
    if (e.length > 2) {
      try {
        this.keyword = e
        let response = await this.webService.getProduct('?search=' + e)
        console.log(response.product)
        this.page = 1
        this.products = response.data
      } catch (e) {
        console.log(e)
      } finally {

      }
    }

  }




  async loadMoreSearch() {
    console.log('load more search')
    this.page += 1;
    try {

      let response = await this.webService.getProduct('?search=' + this.keyword + '&page=' + this.page)
      console.log(response)

      this.products = this.products.concat(response.data)

    } catch (e) {
      console.log(e)
    } finally {

    }
  }



  async addThisProduct(item) {
    console.log(item)
    item.product_id = item.id

    item.added = true
    let form = {
      'status': 'stock_opname',
      'product_id': item.product_id,

    }


    try {

      let response = await this.webService.addOpnameOrder(form)
      console.log(response)
      if (response.data) {
        item.added = true


      }
      this.onItemUpdate.emit(true)
    } catch (e) {
      console.log(e)
    } finally {
      // this.spinner.hide();
      item.is_adding = false
      //   this.ngOnInit()
      //  this.onItemUpdate.emit(true)

    }

  }



}
