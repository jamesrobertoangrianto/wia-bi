import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpnameOrderComponent } from './opname-order.component';

describe('OpnameOrderComponent', () => {
  let component: OpnameOrderComponent;
  let fixture: ComponentFixture<OpnameOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpnameOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpnameOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
