import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-racking-order',
  templateUrl: './racking-order.component.html',
  styleUrls: ['./racking-order.component.scss']
})
export class RackingOrderComponent implements OnInit {
  @Input() inventory: any
  @Input() rack: any
  @Output() onItemUpdate = new EventEmitter()
  warehouse_id: string;

  constructor(
    private webService : ManualOrderService,
    private route: ActivatedRoute,

  ) { }


  ngOnInit(): void {
    console.log(this.rack)
    this.route.paramMap.subscribe(params=>{
      
      this.warehouse_id = params.get("id")
   

      
    })
  }

  async statusUpdate(item){
    item.is_adding = true
    let form ={
       'status' : 'check_in',
       'product_id' : item.product_id,
      'purchase_number' : item.purchase_number,
     
       'warehouse_rack_id' : item.warehouse_rack_id
    }

    try {
    
         let response = await this.webService.addCheckinOrder(form)
          console.log(response)
        if(response.data){
         
          item.total = item.total-1
          if(item.total ==0){
            var index = this.inventory.indexOf(item);
            this.inventory.splice(index, 1);
            
          }
        }else{
          var index = this.inventory.indexOf(item);
          this.inventory.splice(index, 1);
        }
      
       
        } catch (e) {
          console.log(e)
        } finally {
            // this.spinner.hide();
         item.is_adding = false
         //   this.ngOnInit()
       //  this.onItemUpdate.emit(true)
            
        }
   
    
  }


  async updateInventory(item){
    
  
  }


  itemUpdate(item,e){
    
    let form ={
      'serial_number' : e.value,
      'id' : item.id,
   }
    
    this.onItemUpdate.emit(form)
  }
}
