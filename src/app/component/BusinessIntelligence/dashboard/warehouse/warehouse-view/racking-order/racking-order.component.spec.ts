import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RackingOrderComponent } from './racking-order.component';

describe('RackingOrderComponent', () => {
  let component: RackingOrderComponent;
  let fixture: ComponentFixture<RackingOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RackingOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RackingOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
