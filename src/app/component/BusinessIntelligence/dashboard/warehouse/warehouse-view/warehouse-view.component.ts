import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faCamera, faChevronLeft, faImage } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { interval, Subscription } from 'rxjs';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-warehouse-view',
  templateUrl: './warehouse-view.component.html',
  styleUrls: ['./warehouse-view.component.scss']
})
export class WarehouseViewComponent implements OnInit {
  params: string;
  warehouse_id: string;
  faCamera = faCamera
  faChevronLeft = faChevronLeft


  sortListItems = [
    {

      'name': 'type',
      'items': [

        {
          'id': 'purchase',
          'name': 'purchase',
          'label': 'purchase',

        },
        {
          'id': 'consignment',
          'name': 'consignment',
          'label': 'consignment',

        },
        {
          'id': 'flashdeal',
          'name': 'flashdeal',
          'label': 'flashdeal',

        },
        {
          'id': 'bonus',
          'name': 'bonus',
          'label': 'bonus',

        },


      ]

    },

    {

      'name': 'status',
      'items': [

        {
          'id': 'status',
          'name': 'delivered',
          'label': 'Delivered',

        },
        {
          'id': 'status',
          'name': 'check_in',
          'label': 'checkin',

        },
        {
          'id': 'status',
          'name': 'wait_to_pack',
          'label': 'Packing',

        },
        {
          'id': 'status',
          'name': 'ready_to_ship',
          'label': 'Shiped',

        },
        {
          'id': 'missing',
          'name': 'missing',
          'label': 'missing',

        },

      ]

    },

  ]


  Inventorys = []
  brands = []



  inventoryForm = new FormGroup({
    name: new FormControl(null, { validators: [Validators.required] }),
    location: new FormControl(null, { validators: [Validators.required] }),
    status: new FormControl(1),
  })


  rackForm = new FormGroup({
    name: new FormControl(null, { validators: [Validators.required] }),
    location: new FormControl(null, { validators: [Validators.required] }),
    chamber: new FormControl(null, { validators: [Validators.required] }),
    size: new FormControl(null, { validators: [Validators.required] }),
    warehouse_id: new FormControl(null),

  })


  bufferForm = new FormGroup({
    brand: new FormControl(null, { validators: [Validators.required] }),
    brand_id: new FormControl(null, { validators: [Validators.required] }),
    description: new FormControl(null, { validators: [Validators.required] }),
    inventory_value: new FormControl(null, { validators: [Validators.required] }),
    warehouse_id: new FormControl(null, { validators: [Validators.required] }),

  })

  inventory: any;
  showModal: any;
  tab_view: any;
  rack: any;
  products: any;
  buffer: any;
  warehouse: any;
  packing: any;
  isWarehouse: any;
  tab_menu_list: { id: string; label: string; }[];
  checkin: any;
  showWarehouse: boolean;
  warehouse_list: any;
  selected_warehouse: any;
  opname: any;
  variant: any;
  page: any;
  keyword: string;


  constructor(


    private webService: ManualOrderService,

    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastService,
    private router: Router,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {
    this.isWarehouse = JSON.parse(localStorage.getItem("isWarehouse"))

    if (this.isWarehouse) {

      this.tab_menu_list = [


        {
          'id': 'stock_opname',
          'label': 'Opname Request'
        },
        {
          'id': 'delivered',
          'label': 'Checkin Request'
        },
        {
          'id': 'wait_to_pack',
          'label': 'Packing Request'
        },










      ]
    } else {
      this.tab_menu_list = [

        {
          'id': 'inventory',
          'label': 'Inventory'
        },
        {
          'id': 'rack',
          'label': 'Rack'
        },
        {
          'id': 'stock_buffer',
          'label': 'Stock Buffer'
        },
        {
          'id': 'stock_opname',
          'label': 'Opname Request'
        },
        {
          'id': 'delivered',
          'label': 'Checkin Request'
        },
        {
          'id': 'wait_to_pack',
          'label': 'Packing Request'
        },










      ]
    }

    this.route.paramMap.subscribe(params => {

      this.warehouse_id = params.get("id")
      this.getWarehouseId(this.warehouse_id, true)
      this.getWarehouseRack(this.warehouse_id)

      // const source = interval(18000);
      // source.subscribe(val => this.getWarehouseId(this.warehouse_id,false) );

    })


    this.route.queryParamMap.subscribe(queryParams => {
      this.tab_view = queryParams.get("tab_view")

      this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams }))


      this.run()








    })

  }


  run() {
    if (this.tab_view == 'inventory') {

      this.getInventory(this.warehouse_id)
    }

    if (this.tab_view == 'delivered') {
      this.getWarehouseRack(this.warehouse_id)

      this.getCheckinOrder()

    }
    if (this.tab_view == 'rack') {

      this.getWarehouseRack(this.warehouse_id)
    }
    if (this.tab_view == 'stock_opname') {

      this.getOpnameOrder(this.warehouse_id)
    }
    if (this.tab_view == 'stock_buffer') {
      this.getWarehouseBuffer(this.warehouse_id)

    }
    if (this.tab_view == 'wait_to_pack') {



      this.getPackingOrder()


    }
  }



  async getProductVariant(item) {
    try {

      let response = await this.webService.getProductVariants(item.id)
      this.variant = response.data
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {

    }

  }


  async getWarehouse() {
    // console.log(this.params)
    if (this.selected_warehouse) {
      this.selected_warehouse = null
    } else {
      try {
        this.spinner.show();
        let response = await this.webService.getWarehouse(this.params)
        console.log(response)
        this.warehouse_list = response.data
        console.log(response)
        this.showWarehouse = true
      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
      }
    }

  }


  selectWarehouse(item) {
    console.log(item)
    this.selected_warehouse = item
    this.showWarehouse = false
  }

  statusUpdate(item, status) {
    let form = {
      'status': status,
      'id': item.id
    }
    this.updateInventory(form)
  }

  transfer(item) {
    let form = {
      'status': 'delivered',
      'warehouse_rack_id': null,
      'warehouse_id': this.selected_warehouse.id,
      'id': item.id
    }
    item.transfer_status = true
    this.updateInventory(form)
  }





  async searchChanges(e) {
    if (!e) {
      this.products = []
    }
    if (e.length > 2) {
      try {
        this.keyword = e
        let response = await this.webService.getProduct('?search=' + e)
        console.log(response.product)
        this.page = 1
        this.products = response.data
      } catch (e) {
        console.log(e)
      } finally {

      }
    }

  }



  async loadMoreSearch() {
    console.log('load more search')
    this.page += 1;
    try {

      let response = await this.webService.getProduct('?search=' + this.keyword + '&page=' + this.page)
      console.log(response)

      this.products = this.products.concat(response.data)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }



  addThisProduct(item) {
    console.log(item)
    item.product_id = item.id
    item.cogs = item.price - ((item.price * item.margin) / 100)
    this.Inventorys.push(item)
    this.products = null
  }

  removeProduct(index) {
    this.Inventorys.splice(index, 1);
  }


  addThisBrand(item) {
    console.log(item.id)
    this.bufferForm.get('brand').setValue(item.name)
    this.bufferForm.get('brand_id').setValue(item.id)
    this.bufferForm.get('warehouse_id').setValue(this.warehouse_id)
    console.log(this.bufferForm.value)

    this.brands = []
  }

  async getInventory(id) {
    // console.log(this.params)
    this.inventory = []
    try {
      this.spinner.show();
      let response = await this.webService.getInventory(id, this.params)
      console.log(response)
      this.inventory = response.data
      this.page = 1
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }



  async loadMore() {
    console.log('load')
    this.page = this.page + 1
    if (this.params == '/') {
      this.params = '?page=' + this.page
    } else {
      this.params = this.params + '&page=' + this.page
    }






    try {
      this.spinner.show();

      let response = await this.webService.getInventory(this.warehouse_id, this.params)
      console.log(response)

      if (response.data) {
        this.inventory = this.inventory.concat(response.data)

      }

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async getPackingOrder() {
    // console.log(this.params)
    this.inventory = []
    try {
      this.spinner.show();
      let response = await this.webService.getPackingOrder(this.warehouse_id)
      console.log(response)
      console.log('fsafs')
      this.packing = response.data

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async getCheckinOrder() {
    // console.log(this.params)
    this.inventory = []
    try {
      this.spinner.show();
      let response = await this.webService.getCheckinOrder(this.warehouse_id)
      console.log(response)
      console.log('fsafs')
      this.checkin = response.data

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }



  async getWarehouseId(id, state) {
    // console.log(this.params)
    console.log('log')
    this.inventory = []
    try {
      if (state) {
        this.spinner.show();

      }
      let response = await this.webService.getWarehouseId(id)
      this.warehouse = response.data


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }






  async getWarehouseRack(id) {
    console.log('get rack')
    try {
      this.spinner.show();
      let response = await this.webService.getWarehouseRack(id, this.params)
      this.rack = response.data
      console.log(response)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async getOpnameOrder(id) {
    console.log('get rack')
    try {
      this.spinner.show();
      let response = await this.webService.getOpnameOrder(id)
      this.opname = response.data
      console.log(response)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async getWarehouseBuffer(id) {
    // console.log(this.params)
    try {
      this.spinner.show();
      let response = await this.webService.getWarehouseBuffer(id, this.params)
      console.log(response)
      this.buffer = response.data

      this.buffer.forEach(item => {
        item.insight = this.getInsight(item)
      });

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  getInsight(item) {
    if (item.stock_status == 'understock') {
      var sold_forcast = item.sold.value * 3
      var current_forcast = item.current_stock * 3
      if (item.current_stock > sold_forcast) {
        return 'reduce buffer'
      } else if (item.sold > current_forcast) {
        return 'buy more stock'

      }

    }
    if (item.stock_status == 'overstock') {
      var sold_forcast = item.sold.value * 3
      var current_forcast = item.current_stock * 3

      if (sold_forcast < item.current_stock) {
        return 'flush stock'
      }
    }
  }

  async getBrands(e) {
    console.log(e)
    let response = await this.webService.getBrands(e)
    this.brands = response.brands
    console.log(response)

  }

  _navigateTo(id) {
    this.router.navigate(
      [],
      {
        queryParams: {
          'tab_view': id
        }
      }
    );
  }
  receiveItem(item) {
    let currentDate = new Date().toJSON().slice(0, 10);

    let inventory = []
    for (let i = 0; i < item.qty; i++) {
      inventory.push(
        {
          'product_id': item.product_id,
          'brand': item.brand.name,
          'name': item.name,
          'created_at': currentDate,
          'warehouse_id': this.warehouse_id,
          'cogs': item.cogs,
          'type': item.type,
          'status': 'delivered',
        }
      )
    }


    this.addInventory(item, inventory)

  }

  async addInventory(item, form) {
    try {
      // this.spinner.show();
      let response = await this.webService.addInventory(form)
      console.log(response)
      item.isAdded = true
      console.log(form)
      console.log('masukkk')
      this.toast.sendMessage('Product Added!')

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

      //this.ngOnInit()

    }
  }

  async addWarehouseRack() {
    console.log(this.inventoryForm.value)
    this.rackForm.get('warehouse_id').setValue(this.warehouse_id)

    try {
      this.spinner.show();
      let response = await this.webService.addWarehouseRack(this.rackForm.value)
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.showModal = false
      this.ngOnInit()

    }
  }

  async addWarehouseBuffer() {
    console.log(this.bufferForm.value)

    try {
      this.spinner.show();
      let response = await this.webService.addWarehouseBuffer(this.bufferForm.value)
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.showModal = false
      this.ngOnInit()

    }
  }

  async updateWarehouseBuffer(e, id) {

    let form = {}
    form[e.id] = e.value
    try {
      this.spinner.show();
      let response = await this.webService.updateWarehouseBuffer(form, id)
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.showModal = false
      this.ngOnInit()

    }
  }

  onItemUpdate(e) {
    console.log(e)
    this.updateInventory(e)
  }

  async updateInventory(item) {

    try {
      this.spinner.show();
      let response = await this.webService.updateInventory(item.id, item)
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

      this.ngOnInit()

    }
  }


  async deleteInventory(id) {

    try {
      this.spinner.show();
      let response = await this.webService.deleteInventory(id)
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

      this.ngOnInit()

    }
  }





}
