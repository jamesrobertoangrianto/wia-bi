import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BISidebarComponent } from './bisidebar.component';

describe('BISidebarComponent', () => {
  let component: BISidebarComponent;
  let fixture: ComponentFixture<BISidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BISidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BISidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
