import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { Router } from "@angular/router";
import {
  faChartBar,
  faMoneyBillWaveAlt,
  faLaptopHouse,
  faListAlt,
  faWallet,
  faStoreAlt,
  faFileInvoice,
  faShoppingCart,
  faTag,
  faChartLine,
  faUniversity,
  faThLarge,
  faStore,
  faBox,
  faBoxOpen,
  faBriefcase,
  faTshirt,
  faTruck,
  faWarehouse,
  faSearch,
  faPollH,
  faUsers,
  faChartPie,
  faPercent,
  faReceipt,
  faBook,
  faTv,
  faHome,
  faMoneyBill,
  faCartPlus,
  faList,
  faHandHoldingHeart,
  faDollarSign,
  faTags,
  faBullhorn,
  faUserAlt,
  faJournalWhills,
  faBookReader,
  IconDefinition,
} from "@fortawesome/free-solid-svg-icons";
import { PAGE_PERMISSION_TYPE } from "src/app/app-routing.constant";


type PAGE_MENU = {
  name: string,
  link: string,
  icon: IconDefinition,
  is_active: boolean
}

@Component({
  selector: "app-bisidebar",
  templateUrl: "./bisidebar.component.html",
  styleUrls: ["./bisidebar.component.scss"],
})
export class BISidebarComponent implements OnInit {
  @Output() onClick = new EventEmitter();
  faHome = faHome;
  faUsers = faUsers;
  faTv = faTv;
  faBook = faBook;
  faReceipt = faReceipt;
  faPercent = faPercent;
  faChartPie = faChartPie;
  faSearch = faSearch;
  faWarehouse = faWarehouse;
  faTruck = faTruck;
  faTshirt = faTshirt;
  faPollH = faPollH;
  faBox = faBoxOpen;
  faStore = faStore;
  faThLarge = faThLarge;
  faMoneyBillWaveAlt = faMoneyBillWaveAlt;
  faUniversity = faUniversity;
  faChartLine = faChartLine;
  faChartBar = faChartBar;
  faTag = faTag;
  faShoppingCart = faShoppingCart;
  faFileInvoice = faFileInvoice;
  faStoreAlt = faStoreAlt;
  faWallet = faWallet;
  faListAlt = faListAlt;
  faLaptopHouse = faLaptopHouse;

  ///Operational menu
  finance_menu: Array<PAGE_MENU>
  operational_menu: Array<PAGE_MENU>
  marketing_menu: Array<PAGE_MENU>
  admin_menu: Array<PAGE_PERMISSION_TYPE>

  constructor(private router: Router) { }

  ngOnInit() {
    this.finance_menu = [
      {
        name: "Finance Report",
        link: "/finance-report",
        icon: faChartLine,
        is_active: this.isActive('financeReport'),
      },
      {
        name: "Cash Bank",
        link: "/cash-bank",
        icon: faWallet,
        is_active: this.isActive('cashBank'),
      },

      {
        name: "Purchase Order",
        link: "/purchase",
        icon: faCartPlus,
        is_active: this.isActive('purchaseOrder'),
      },

      {
        name: "Sales Order",
        link: "/sales-order-report",
        icon: faFileInvoice,
        is_active: this.isActive('salesOrder'),
      },

      {
        name: "Product Report",
        link: "/sales-order-product-report",
        icon: faList,
        is_active: this.isActive('product'),
      },

      {
        name: "Chart of Account",
        link: "/chart-account",
        icon: faBook,
        is_active: this.isActive('chartOfAccount'),
      },
      {
        name: "Journal Account",
        link: "/journal-account",
        icon: faJournalWhills,
        is_active: this.isActive('journalAccount'),
      },
      {
        name: "Journal Transacion",
        link: "/journal-transaction",
        icon: faBookReader,
        is_active: this.isActive('journalTransaction'),
      },
    ];

    this.operational_menu = [
      {
        name: "Order Management",
        link: "/order-management-v2",
        icon: faStore,
        is_active: this.isActive('orderManagement'),
      },
      {
        name: "Product",
        link: "/product",
        icon: faTshirt,
        is_active: this.isActive('product'),
      },
      {
        name: "Warehouse",
        link: "/warehouse",
        icon: faWarehouse,
        is_active: this.isActive('warehouse'),
      },
      {
        name: "Sales Pipeline",
        link: "/business-order",
        icon: faPollH,
        is_active: this.isActive('salesPipeline'),
      },
      {
        name: "Vendor Report",
        link: "/vendor-report",
        icon: faReceipt,
        is_active: this.isActive('vendorReport'),
      },
      {
        name: "Customer",
        link: "/customer",
        icon: faUsers,
        is_active: this.isActive('customer'),
      },
    ];

    this.marketing_menu = [
      {
        name: "Analytics",
        link: "/analytics",
        icon: faChartPie,
        is_active: this.isActive('analytics'),
      },
      {
        name: "Affiliate",
        link: "/referal",
        icon: faHandHoldingHeart,
        is_active: this.isActive('affiliate'),
      },
      {
        name: "Revenue Planning",
        link: "/revenue-planning",
        icon: faDollarSign,
        is_active: this.isActive('revenuePlanning'),
      },
      // {
      //   name: "Promotion",
      //   link: "/promotion",
      //   id: 'promotion',
      //   icon: faTags,
      //   is_active: this.isActive(16),
      // },

      {
        name: "Placement",
        link: "/placement",
        icon: faBullhorn,
        is_active: this.isActive('placement'),
      },
    ];
  }

  route(s) {
    this.router.navigate([s]);
  }

  isAdmin() {
    if (localStorage.getItem("role") == "SUPERADMIN") {
      return true;
    } else {
      return false;
    }
  }

  isActive(id: PAGE_PERMISSION_TYPE) {
    if (localStorage.getItem("account_role") == "SUPERADMIN") {
      const menu_id: Array<PAGE_PERMISSION_TYPE> = [
        'financeReport',
        'cashBank',
        'purchaseOrder',
        'salesOrder',
        'productReport',
        'chartOfAccount',
        'orderManagement',
        'product',
        'warehouse',
        'salesPipeline',
        'vendorReport',
        'customer',
        'analytics',
        'affiliate',
        'revenuePlanning',
        'promotion',
        'placement',
        'journalAccount',
        'journalTransaction'
      ];

      return menu_id.includes(id);
    }
    if (localStorage.getItem("account_role") == "FINANCE") {
      const menu_id: Array<PAGE_PERMISSION_TYPE> = [
        'financeReport',
        'cashBank',
        'purchaseOrder',
        'salesOrder',
        'productReport',
        'chartOfAccount',
        'orderManagement',
        'product',
        'warehouse',
        'salesPipeline',
        'vendorReport',
        'journalAccount',
        'journalTransaction'
      ]

      return menu_id.includes(id);
    }
    if (localStorage.getItem("account_role") == "BUSINESS") {
      const menu_id: Array<PAGE_PERMISSION_TYPE> = [
        'purchaseOrder', 'orderManagement', 'product',
        'warehouse', 'salesPipeline', 'vendorReport',
        'promotion', 'placement'
      ]

      return menu_id.includes(id);
    }
    if (localStorage.getItem("account_role") == "OPERATIONAL") {
      const menu_id: Array<PAGE_PERMISSION_TYPE> = [
        'orderManagement', 'product', 'warehouse',
        'salesPipeline', 'vendorReport', 'promotion',
        'placement'
      ]

      return menu_id.includes(id);
    }
  }

  navigateTo(id) {
    this.router.navigate([id]);
    this.onClick.emit(true);
  }
}
