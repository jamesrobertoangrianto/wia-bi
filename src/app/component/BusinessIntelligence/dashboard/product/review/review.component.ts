
import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { Location } from '@angular/common'
import { faChartLine, faCheckCircle, faChevronCircleDown, faChevronLeft, faExpandAlt, faHandshake, faHeadphonesAlt, faMapMarkedAlt, faPhoneAlt, faPlus, faPlusCircle, faThumbsUp, faTrafficLight, faTrashAlt, faVectorSquare } from '@fortawesome/free-solid-svg-icons';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  tab_view: string;
  params: string;
  review: any[];
  page: number;

  constructor(
    private financeService: ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private location: Location,
    private webService: ManualOrderService,
    private router: Router,
    private toast: ToastService,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {


    this.route.queryParamMap.subscribe(queryParams => {
      // this.query =   queryParams.get("search")
      this.tab_view = queryParams.get("tab_view")
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams }))

      this.getReview()
    })

  }


  async getReview() {
    this.review = []
    try {
      this.spinner.show();
      let response = await this.webService.getProductReviews(this.params)
      console.log(response)
      this.review = response.data
      this.page = 1

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }



  updateStatus(item, e, state) {
    let form = {}
    form[e] = state
    console.log(form)
    this.toast.sendMessage(state ? 'Enabled' : 'Disabled')

    this.updateProductReview(item, form)
  }


  updateReview(item, e) {
    let form = {}
    form[e.id] = e.value
    console.log(form)

    this.updateProductReview(item, form)
  }


  async updateProductReview(item, form) {
    try {



      let response = await this.webService.updateProductReview(item.id, form)
      console.log(response)
      if (response.data) {
        console.log('update')

      } else {

      }
    } catch (e) {
      console.log(e)
    } finally {




    }
  }


  async loadMore() {
    console.log('Loading more products...');

    this.page += 1;
    const query = this.params === '/' ? `?page=${this.page}` : `${this.params}&page=${this.page}`;

    try {
      this.spinner.show();

      const response = await this.webService.getProductReviews(query);
      console.log(response);

      if (response?.data?.length) {
        this.review = [...this.review, ...response.data];
      }
    } catch (e) {
      console.error('Error loading more products:', e);
    } finally {
      this.spinner.hide();
    }
  }






}
