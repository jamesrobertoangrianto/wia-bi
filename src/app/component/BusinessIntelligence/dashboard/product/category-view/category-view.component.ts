import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faChartLine, faCheckCircle, faChevronLeft, faExpandAlt, faHandshake, faHeadphonesAlt, faImage, faMapMarkedAlt, faPhoneAlt, faPrint, faThumbsUp, faTrafficLight, faTrashAlt, faVectorSquare } from '@fortawesome/free-solid-svg-icons';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.scss']
})
export class CategoryViewComponent implements OnInit {
  @Input() selected_category : any
  @Output() onClose = new EventEmitter()
  product: any[];
  constructor(
    private webService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    this.getCategoryProduct(3)
  }

  async getCategoryProduct(id){
    this.product =[]
    try {
    
         let response = await this.webService.getCategoryProduct(id,'')
        this.product = response.data.items
       console.log(response)
  
        } catch (e) {
          console.log(e)
        } finally {
         
        }
  }

  async searchProduct(e){
   
    let param = '?name='+e
    try {
    
         let response = await this.webService.getCategoryProduct(3,param)
        this.product = response.data.items
       console.log(response)
  
        } catch (e) {
          console.log(e)
        } finally {
         
        }
  }

 

}
