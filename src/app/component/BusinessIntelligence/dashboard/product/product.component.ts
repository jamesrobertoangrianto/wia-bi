
import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { Location } from '@angular/common'
import { faChartLine, faCheckCircle, faChevronCircleDown, faChevronLeft, faExpandAlt, faHandshake, faHeadphonesAlt, faMapMarkedAlt, faPhoneAlt, faPlus, faPlusCircle, faThumbsUp, faTrafficLight, faTrashAlt, faVectorSquare } from '@fortawesome/free-solid-svg-icons';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import pdfMake from 'pdfmake/build/pdfmake';
import { ToastService } from 'src/app/services/toast.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {


  tab_menu_list = [

    {
      'id': 'product',
      'label': 'Product',

      'class': null,
    },
    {
      'id': 'collection',
      'label': 'Collection',

      'class': null,
    },

    {
      'id': 'promotion',
      'label': 'Promotion',

      'class': null,
    },


    {
      'id': 'review',
      'label': 'Review',

      'class': null,
    },
    {
      'id': 'product_discount',
      'label': 'Voucher',

      'class': null,
    },

    {
      'id': 'special_price',
      'label': 'Special Price',

      'class': null,
    },












  ]
  sortListItems = [

  ]
  product: any[];
  params: string;
  selected_product: any;
  tab_view: string;
  category: any;
  faPlus = faPlusCircle
  selected_added_product = []
  is_select_all: boolean;
  brand: any[];
  is_show_collection: any;
  collection: any;
  selected_added_category: any;
  faTrashAlt = faTrashAlt
  faChevronCircleDown = faChevronCircleDown
  page: any;
  faPlusCircle = faPlusCircle
  selected_performace_id: any;
  is_show_promotion: any;
  selected_promotion: any
  bundling: any;
  search: string;
  searchTimeout: any; // Declare a variable to hold timeout reference



  constructor(
    private financeService: ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private location: Location,
    private webService: ManualOrderService,
    private router: Router,
    private toast: ToastService,
    private serializer: UrlSerializer,



  ) { }

  ngOnInit(): void {

    if (this.sortListItems.length == 0) {
      this.getCategoryCollection('')

    }

    this.selected_product = null
    this.route.queryParamMap.subscribe(queryParams => {
      // this.query =   queryParams.get("search")
      this.tab_view = queryParams.get("tab_view")
      this.search = queryParams.get("search")

      this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams }))
      this.is_select_all = false;
      this.selected_added_product = []

      if (this.tab_view == 'product') {

        if (this.search) {
          console.log('search')
          if (this.searchTimeout) {
            clearTimeout(this.searchTimeout); // Clear the previous timeout
          }
          // this.getProduct()
          this.searchTimeout = setTimeout(() => {
            this.getProduct();
          }, 300); // Waits 500ms before calling getProduct

        } else {
          this.getProduct()

        }
      }

    })

  }

  async getBundling() {

    try {
      this.spinner.show();
      let response = await this.webService.getBundling('')

      console.log(response)
      this.bundling = response.data

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  selectPromotion(item) {
    this.selected_promotion.id = item.bundling_group
    this.selected_promotion = item

  }


  async getProduct() {
    this.product = []
    try {
      this.spinner.show();
      let response = await this.webService.getProduct(this.params)
      console.log(response)
      this.product = response.data
      this.page = 1

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async loadMore() {
    console.log('Loading more products...');

    this.page += 1;
    const query = this.params === '/' ? `?page=${this.page}` : `${this.params}&page=${this.page}`;

    try {
      this.spinner.show();

      const response = await this.webService.getProduct(query);
      console.log(response);

      if (response?.data?.length) {
        this.product = [...this.product, ...response.data];
      }
    } catch (e) {
      console.error('Error loading more products:', e);
    } finally {
      this.spinner.hide();
    }
  }



  async getProductVariant(item) {
    console.log(item)
    if (item.variant) {
      item.variant = null
    } else {
      try {
        this.spinner.show();
        let response = await this.webService.getProductVariants(item.id)
        if (response.data) {
          item.variant = response.data
        }

      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
      }
    }

  }

  async getCategoryType() {
    this.category = []
    try {
      this.spinner.show();
      let response = await this.webService.getCategory(this.params)
      console.log(response)
      this.category = response.data
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }



  async addVariant(item) {
    try {
      let form = {
        'parrent_id': item.id,
        'type': 'VARIANT_PRODUCT'
      }

      let response = await this.webService.addProduct(form)
      console.log(response)
    } catch (e) {
      console.log(e)
    } finally {
      let response = await this.webService.getProductVariants(item.id)
      if (response.data) {
        item.variant = response.data
        item.total_variant = item.variant.length
        this.toast.sendMessage('Variant Added!')

      }

    }
  }



  async addProduct() {
    let form = {
      "name": null,
      "url_key": null,
      "price": 0
    }
    try {
      this.spinner.show();
      let response = await this.webService.addProduct(form)
      console.log(response)
      this.product.unshift(response.data)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      //this.getProduct()
    }
  }


  addToCollection() {
    this.is_show_collection = true
    this.getCategoryCollection('')
  }


  updateProduct(item, e) {
    let form = {}
    form[e.id] = e.value
    console.log(form)

    this.updateProductID(item, form)
  }

  updateStatus(item, e, state) {
    let form = {}
    form[e] = state
    console.log(form)
    this.toast.sendMessage(state ? 'Enabled' : 'Disabled')

    this.updateProductID(item, form)
  }




  async updateProductID(item, form) {
    try {



      let response = await this.webService.updateProductID(item.id, form)
      console.log(response)
      if (response.data) {
        console.log('update')

      } else {

      }
    } catch (e) {
      console.log(e)
    } finally {




    }
  }


  async searchCollection(e) {
    console.log(e)
    if (e) {
      try {
        let response = await this.webService.getCategoryCollection('?name=' + e);
        this.collection = response.data


      } catch (e) {
        console.error(e);
      } finally {
        this.spinner.hide();
      }
    }



  }




  async getCategoryCollection(key) {

    try {
      let response = await this.webService.getCategoryCollection('');
      this.collection = response.data
      const categories = {
        BRAND: { name: 'brand_ids', label: 'Brand' },
        CATEGORY: { name: 'category_ids', label: 'Category' },
        ACTIVITY: { name: 'activity_ids', label: 'Activity' }
      };

      Object.entries(categories).forEach(([key, { name, label }]) => {
        if (response.data[key]) {
          this.sortListItems.push({
            name,
            label,
            items: response.data[key].map(({ id, name }) => ({ id, label: name, name: id }))
          });
        }
      });

    } catch (e) {
      console.error(e);
    } finally {
      this.spinner.hide();
    }
  }


  async removeProductVariant(item) {

    try {
      let res = await this.webService.deleteProductID(item.id);
      console.log(res)

      if (res.data) {
        this.ngOnInit()
      }

    } catch (e) {
      console.error(e);
    } finally {

    }
  }

  async removeProduct(item) {

    try {
      let res = await this.webService.deleteProductID(item.id);
      console.log(res)

      if (res.data) {
        const index = this.product.indexOf(item);
        if (index > -1) {
          this.product.splice(index, 1);
        }
      }

    } catch (e) {
      console.error(e);
    } finally {

    }
  }

  async addCategoryProduct(item) {
    try {
      await this.webService.addCategoryProduct(item.id, this.selected_added_product);
    } catch (e) {
      console.error(e);
    } finally {
      this.is_select_all = false;
      this.selected_added_product = []
      this.toggleCollection();
      this.ngOnInit();
    }
  }

  async addProductToPromotion(item) {

    try {

      let form = {
        'product_ids': this.selected_added_product,
        'type': item.type,
        'id': item.id
      }

      let res = await this.webService.addProductPromotion(form);

      if (res.data) {
        this.toast.sendMessage('Added to ' + item.title)

      }
    } catch (e) {
      console.error(e);
    } finally {
      this.is_select_all = false;
      this.selected_added_product = []
      this.togglePromotion();
      this.ngOnInit();
    }
  }

  selectAllSelected() {
    this.is_select_all = !this.is_select_all;
    this.selected_added_product = this.is_select_all
      ? this.product.map(item => item.id)
      : [];

    this.product.forEach(item => (item.checked = this.is_select_all));
  }

  selectAddedProduct(check, id) {
    if (check) {
      this.selected_added_product = [...new Set([...this.selected_added_product, id])];
    } else {
      this.selected_added_product = this.selected_added_product.filter(item => item !== id);
    }

    console.log(this.selected_added_product)
  }

  toggleCollection() {
    this.is_show_collection = !this.is_show_collection;
  }

  togglePromotion() {

    this.is_show_promotion = !this.is_show_promotion;
    if (this.is_show_promotion) {
      this.getBundling()
    }
  }



  closeModal() {
    //this.selected_product = null;
    this.ngOnInit()
  }



}
