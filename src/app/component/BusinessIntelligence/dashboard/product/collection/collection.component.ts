


import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { Location } from '@angular/common'
import { faChartLine, faCheckCircle, faChevronLeft, faExpandAlt, faHandshake, faHeadphonesAlt, faMapMarkedAlt, faMinusCircle, faPhoneAlt, faPlusCircle, faPlusSquare, faThumbsUp, faTrafficLight, faTrashAlt, faVectorSquare } from '@fortawesome/free-solid-svg-icons';

import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})

export class CollectionComponent implements OnInit {
  faChevronLeft = faChevronLeft
  selected_category: any;
  collection: any;
  faTrashAlt = faTrashAlt
  params: string;
  tab_view: string;
  category_id: string;
  faPlusCircle = faPlusCircle
  faMinusCircle = faMinusCircle
  faCheckCircle = faCheckCircle
  faPlusSquare = faPlusSquare
  category: any;
  product: any;
  is_search: boolean;
  show_details: any;
  selected_product: any;
  isLoading: boolean;
  page: number;
  keyword: any;




  constructor(
    private financeService: ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private location: Location,
    private webService: ManualOrderService,
    private router: Router,
    private toast: ToastService,
    private serializer: UrlSerializer,

  ) { }

  ngOnInit(): void {

    this.getCategoryCollection('')

    this.route.queryParamMap.subscribe(queryParams => {
      // this.query =   queryParams.get("search")
      this.tab_view = queryParams.get("tab_view")
      this.category_id = queryParams.get("category_id")

      this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams }))

      //  if(this.tab_view == 'collection'){
      //    this.getCategoryCollection('')
      //  }


      this.getCategoryProduct(this.category_id)

    })


  }


  test() {
    console.log('test')
    this.toast.sendMessage('fsjhfhs')
  }

  async getCategoryCollection(key) {

    try {

      let response = await this.webService.getCategoryCollection('?name=' + key)
      console.log(response)
      this.collection = response.data
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  async getCategoryChild(item) {
    console.log('ge child')
    try {

      let response = await this.webService.getCategoryChild(item.id)
      console.log(response)
      if (response.data) {
        item.child = response.data
      }
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  searchProduct(e) {
    console.log(e)

    if (e) {
      this.keyword = e
      this.getProduct()
    } else {
      this.keyword = null
      this.getCategoryProduct(this.category_id)
    }
  }

  async getCategoryProduct(id) {
    this.page = 1
    this.isLoading = true
    this.category = []
    this.product = []
    this.is_search = false
    this.show_details = false


    try {

      let response = await this.webService.getCategoryProduct(id, '?page=' + this.page)
      console.log(response)

      if (response.data.category) {
        this.show_details = true
        this.category = response.data.category
        this.product = response.data.items

        this.product.forEach(item => {
          //console.log(item.category_ids)
          item.is_added = item.category_ids.includes(parseFloat(this.category_id))
        });

      }


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

      this.isLoading = false

    }
  }






  async loadMore() {
    console.log('loadmore')
    this.page = this.page + 1

    this.is_search = false



    try {

      let response = await this.webService.getCategoryProduct(this.category_id, '?page=' + this.page)
      console.log(response)

      if (response.data.category) {


        this.product = this.product.concat(response.data.items)

        this.product.forEach(item => {
          //console.log(item.category_ids)
          item.is_added = item.category_ids.includes(parseFloat(this.category_id))
        });

      }


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

      this.isLoading = false

    }
  }



  async getProduct() {
    this.page = 1
    this.is_search = true
    try {

      let response = await this.webService.getProduct('?search=' + this.keyword + '&page=' + this.page)
      console.log(response)

      this.product = response.data
      this.product.forEach(item => {
        //console.log(item.category_ids)
        item.is_added = item.category_ids.includes(parseFloat(this.category_id))
      });
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async onChange(event: any) {

    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      formData.append('file', file);

      try {

        let res = await this.webService.uploadPhoto(formData, 'collection_cover')
        console.log(res)

        if (res.data) {
          let form = {
            'url_path': res.data,
            'url_key': res.path,



          }

          console.log(this.category)
          this.addCategoryGallery(this.category, form)
        }

      } catch (error) {
        console.log(error)
      }
      finally {



      }

    }
  }




  async addCategoryGallery(item, form) {
    try {

      let response = await this.webService.addCategoryGallery(item.id, form)
      console.log(response)
    } catch (e) {
      console.log(e)
    } finally {
      this.ngOnInit()

    }
  }

  async loadMoreSearch() {
    console.log('load more search')
    this.page = this.page + 1
    this.is_search = true
    try {

      let response = await this.webService.getProduct('?search=' + this.keyword + '&page=' + this.page)
      console.log(response)

      this.product = this.product.concat(response.data)
      this.product.forEach(item => {

        item.is_added = item.category_ids.includes(parseFloat(this.category_id))
      });
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }



  async addThisProduct(state, item) {
    if (state) {
      item.is_added = state
      this.addCategoryProduct([item.id])
    } else {
      item.is_added = false
      this.deleteCategoryProduct([item.id])
      if (!this.is_search) {
        var index = this.product.indexOf(item);
        this.product.splice(index, 1);
        this.toast.sendMessage('Product Removed!')
      }

    }

  }


  async addCategory(type, parrent_id?) {
    const form = {
      type,
      name: '-',
      ...(parrent_id && { parrent_id }) // Add parrent_id only if it's provided
    };

    try {
      this.category = []; // Reset category only if a request is made
      const response = await this.webService.addCategory(form);
      console.log(response);

      if (response.data.id) {
        this.router.navigate(
          [],
          { queryParams: { category_id: response.data.id }, queryParamsHandling: 'merge' },

        );
      }



    } catch (e) {
      console.error(e);
    } finally {
      this.spinner.hide();

    }
  }



  async deleteProductGallery(item) {
    try {
      console.log(item)

      let response = await this.webService.deleteProductGallery(item.id)
      console.log(response)
    } catch (e) {
      console.log(e)
    } finally {

      this.ngOnInit()
    }
  }

  updateCategoryField(item, e) {
    let form = {}
    form[e.id] = e.value
    console.log(form)

    this.updateCategory(item, form)
  }


  async updateCategory(item, form) {
    try {



      let response = await this.webService.updateCategory(item.id, form)
      console.log(response)
      if (response.data) {
        console.log('update')
        this.getCategoryCollection('')


      } else {


      }
    } catch (e) {
      console.log(e)
    } finally {



    }
  }


  async deleteCategory(item) {
    try {



      let response = await this.webService.deleteCategory(item.id)

      if (response.data) {
        this.toast.sendMessage('Collection Deleted!')

        this.ngOnInit()

      } else {


      }
    } catch (e) {
      console.log(e)
    } finally {



    }
  }





  async addCategoryProduct(ids) {
    let form = ids
    console.log(form)

    try {

      let response = await this.webService.addCategoryProduct(this.category_id, form)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }

  }


  async deleteCategoryProduct(ids) {
    let form = ids
    console.log(form)

    try {

      let response = await this.webService.deleteCategoryProduct(this.category_id, form)
      console.log(response)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }

  }






  navigateTo(item) {

    this.router.navigate(
      [],
      { queryParams: { category_id: item.id }, queryParamsHandling: 'merge' },

    );

  }


}

