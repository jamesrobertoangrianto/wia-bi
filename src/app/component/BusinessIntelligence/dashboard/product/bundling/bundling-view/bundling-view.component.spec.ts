import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BundlingViewComponent } from './bundling-view.component';

describe('BundlingViewComponent', () => {
  let component: BundlingViewComponent;
  let fixture: ComponentFixture<BundlingViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BundlingViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BundlingViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
