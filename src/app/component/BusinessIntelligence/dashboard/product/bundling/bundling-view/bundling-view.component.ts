

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { Location } from '@angular/common'
import { faChartLine, faCheckCircle, faChevronLeft, faExpandAlt, faHandshake, faHeadphonesAlt, faMapMarkedAlt, faMinusCircle, faPhoneAlt, faPlusCircle, faPlusSquare, faThumbsUp, faTrafficLight, faTrashAlt, faVectorSquare } from '@fortawesome/free-solid-svg-icons';
import { ToastService } from 'src/app/services/toast.service';


@Component({
  selector: 'app-bundling-view',
  templateUrl: './bundling-view.component.html',
  styleUrls: ['./bundling-view.component.scss']
})
export class BundlingViewComponent implements OnInit {
  @Input() selected_promotion: any
  @Output() onClose = new EventEmitter()

  searchTimeout: any; // Declare a variable to hold timeout reference
  faChevronLeft = faChevronLeft
  selected_category: any;
  collection: any;
  faTrashAlt = faTrashAlt
  params: string;
  tab_view: string;
  category_id: string;
  faPlusCircle = faPlusCircle
  faMinusCircle = faMinusCircle
  faCheckCircle = faCheckCircle
  faPlusSquare = faPlusSquare
  category: any;
  product: any;
  is_search: boolean;
  show_details: any;
  selected_product: any;
  isLoading: boolean;
  page: number;
  keyword: any;
  promotion: any[];

  constructor(
    private financeService: ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private location: Location,
    private webService: ManualOrderService,
    private router: Router,
    private toast: ToastService,
    private serializer: UrlSerializer,

  ) { }

  ngOnInit(): void {
    this.getBundlingitem(this.selected_promotion.id)

  }

  closeModal() {
    this.onClose.emit(true)
  }



  async getBundlingitem(id) {
    this.page = 1
    this.isLoading = true
    this.promotion = []
    this.product = []
    this.is_search = false
    this.show_details = false


    try {

      let response = await this.webService.getBundlingitem(id)
      console.log(response)

      if (response.data) {
        this.show_details = true
        // this.category = response.data.category
        this.product = response.data.product


        this.product.forEach(item => {
          item.is_added = true
        });

      }


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

      this.isLoading = false

    }
  }

  changeValueType(item, type) {
    item.value_type = type
    let form = {
      'value_type': type
    }
    this.updateBundling(item, form)

  }


  searchProduct(e: string) {
    console.log(e);
    this.product = []
    if (this.searchTimeout) {
      clearTimeout(this.searchTimeout); // Clear the previous timeout
    }

    if (e) {
      this.keyword = e;
      this.searchTimeout = setTimeout(() => {
        this.getProduct();
      }, 500); // Waits 500ms before calling getProduct
    }
    else {
      this.keyword = null;
      this.getBundlingitem(this.selected_promotion.id);
    }


  }


  async loadMoreSearch() {
    console.log('load more search')
    this.page = this.page + 1
    this.is_search = true
    try {

      let response = await this.webService.getProduct('?search=' + this.keyword + '&page=' + this.page)
      console.log(response)

      this.product = this.product.concat(response.data)
      this.product.forEach(item => {

        item.is_added = item.category_ids.includes(parseFloat(this.category_id))
      });
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async onChange(event: any, item) {

    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      formData.append('file', file);

      try {

        let res = await this.webService.uploadPhoto(formData, 'bundling_cover')
        console.log(res)

        if (res.data) {
          let form = {
            'image_path': res.data,
            'image_key': res.path,



          }
          this.selected_promotion.image_path = res.data

          this.updateBundling(item, form)
        }

      } catch (error) {
        console.log(error)
      }
      finally {



      }

    }
  }


  async getProduct() {
    this.product = []
    this.page = 1
    this.is_search = true
    try {

      let response = await this.webService.getProduct('?search=' + this.keyword + '&page=' + this.page)
      console.log(response)

      this.product = response.data
      this.product.forEach(item => {
        //console.log(item.category_ids)
        item.is_added = item.category_ids.includes(parseFloat(this.category_id))
      });
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  updateProduct(item, e) {
    let form = {}
    form[e.id] = e.value
    console.log(form)

    this.updateBundling(item, form)
  }

  updateCustomPrice(item, e) {
    let form = {
      'value': e.value
    }

    console.log(form)
    console.log(item)
    item.bundling_price = e.value
    item.value = e.value

    this.updateBundlingItem(item.id, form)
  }


  async updateBundlingItem(id, form) {


    try {
      this.spinner.show();
      let response = await this.webService.updateBundlingItem(id, form)

      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      //this.ngOnInit()
    }
  }


  async updateBundling(item, form) {


    try {
      this.spinner.show();
      let response = await this.webService.updateBundling(item.id, form)

      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.ngOnInit()
    }
  }



  async addThisProduct(state, item) {
    if (state) {
      item.is_added = state
      console.log(item)
      let form = {
        'product_id': item.id
      }
      console.log(form)
      this.addBundlingItem(form)
    } else {
      item.is_added = false
      console.log(item.id)
      this.deleteBundlingItem(item.id)
      if (!this.is_search) {
        var index = this.product.indexOf(item);
        this.product.splice(index, 1);
        this.toast.sendMessage('Product Removed!')
      }

    }

  }




  async addBundlingItem(form) {

    try {

      let response = await this.webService.addBundlingItem(this.selected_promotion.id, form)
      console.log(response)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }

  }



  async deleteBundlingItem(id) {

    try {

      let response = await this.webService.deleteBundlingItem(id)
      console.log(response)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }

  }



}
