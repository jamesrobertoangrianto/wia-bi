import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-bundling',
  templateUrl: './bundling.component.html',
  styleUrls: ['./bundling.component.scss']
})
export class BundlingComponent implements OnInit {
  bundling: any;
  selected_promotion: any

  constructor(
    private webService: ManualOrderService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router: Router,

  ) { }

  ngOnInit(): void {
    this.selected_promotion = null
    this.getBundling()
  }


  async getBundling() {

    try {
      this.spinner.show();
      let response = await this.webService.getBundling('')

      console.log(response)
      this.bundling = response.data

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }



  async addBundling(type) {
    let form = {
      'type': type

    }
    try {

      let response = await this.webService.addBundling(form)
      this.bundling = response.data
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.ngOnInit()
    }
  }

  updateStatus(item, e, state) {
    let form = {}
    form[e] = state
    console.log(form)
    //this.toast.sendMessage(state ? 'Enabled' : 'Disabled')

    this.updateBundling(item, form)
  }


  async updateBundling(item, form) {


    try {
      this.spinner.show();
      let response = await this.webService.updateBundling(item.id, form)

      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.ngOnInit()
    }
  }


}
