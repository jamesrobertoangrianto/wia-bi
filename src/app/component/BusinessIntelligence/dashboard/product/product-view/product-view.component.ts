import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faChartLine, faCheckCircle, faChevronLeft, faExpandAlt, faHandshake, faHeadphonesAlt, faImage, faMapMarkedAlt, faPhoneAlt, faPrint, faThumbsUp, faTrafficLight, faTrashAlt, faVectorSquare } from '@fortawesome/free-solid-svg-icons';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { ToastService } from 'src/app/services/toast.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit {

  htmlContent = '';

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    enableToolbar: false,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    uploadUrl: 'https://wia.id/wia-module/checkoutv4',
    toolbarHiddenButtons: [
      [

        'subscript',
        'superscript',


        'fontName'
      ],
      [


        'insertImage',
        'insertVideo',
        'insertHorizontalRule',

      ]
    ]




  };




  @Input() selected_product: any
  @Output() onClose = new EventEmitter()

  faTrashAlt = faTrashAlt
  product: any[];
  spinner: any;
  show_editor: any;

  in_the_box: any;
  tech_spec: any;
  video: any;
  description: any;
  selected_child_product: any;
  show_detail: boolean;
  size_chart: any;


  constructor(
    private webService: ManualOrderService,
    private toast: ToastService,
  ) {

  }

  ngOnInit(): void {
    this.getProductId(this.selected_product.id)
    this.show_editor = false
    this.show_detail = false
  }

  async getProductId(id) {
    this.product = []



    try {

      let response = await this.webService.getProductId(id)
      this.selected_product = response.data
      console.log(this.selected_product)

      let in_the_box = this.selected_product.product_details.IN_THE_BOX
      let tech_spec = this.selected_product.product_details.TECH_SPEC
      let video = this.selected_product.product_details.VIDEO
      let description = this.selected_product.product_details.DESCRIPTION
      let size_chart = this.selected_product.product_details.SIZE_CHART

      if (description) {
        this.description = description


      }


      if (in_the_box) {
        this.in_the_box = in_the_box
        this.in_the_box.description = JSON.parse(this.in_the_box.description)

      }

      if (size_chart) {
        this.size_chart = size_chart
        this.size_chart.description = JSON.parse(this.size_chart.description)

      }


      if (tech_spec) {
        this.tech_spec = tech_spec
        this.tech_spec.description = JSON.parse(this.tech_spec.description)

      }


      if (video) {
        this.video = video
        this.video.description = JSON.parse(this.video.description)

      }





    } catch (e) {
      console.log(e)
    } finally {
      this.show_detail = true

      this.spinner.hide();
    }
  }

  updateProduct(item, e) {
    let form = {}
    form[e.id] = e.value
    console.log(form)

    this.updateProductID(item, form)
  }

  angularEditorLogo(e) {
    console.log(e)
  }
  addDescription() {

    if (!this.description) {
      console.log('sini')
      // If in_the_box doesn't exist, initialize it with an empty description
      const form = {
        'attribute_name': 'DESCRIPTION',
        'description': '-',
      };
      this.addProductAttribute(form);
      this.show_editor = true
    }
  }


  async updateDescription(e) {

    const form = {
      'attribute_name': 'DESCRIPTION',
      'description': e,
    };

    try {
      const response = await this.webService.updateProductAttribute(this.description.id, form);
      console.log(response);  // Keep this for debugging or replace with more meaningful output
      if (response.data) {
        this.toast.sendMessage('Updated!')
        this.show_editor = false
      }
    } catch (error) {
      console.error('Error updating InTheBox:', error);
    }
  }

  saveDesc() {
    // Create a temporary DOM element to parse and clean the HTML
    const tempDiv = document.createElement('div');
    tempDiv.innerHTML = this.description.description;

    // Remove all 'style' and 'class' attributes from elements
    tempDiv.querySelectorAll('[style], [class]').forEach((el) => {
      el.removeAttribute('style');
      el.removeAttribute('class');
    });

    // Save the cleaned HTML
    this.description.description = tempDiv.innerHTML;

    // Submit the updated description
    this.updateDescription(this.description.description);
  }


  addInTheBox() {
    if (!this.in_the_box) {
      // If in_the_box doesn't exist, initialize it with an empty description
      const form = {
        'attribute_name': 'IN_THE_BOX',
        'description': JSON.stringify(['']),
      };
      this.addProductAttribute(form);
    } else {
      // If in_the_box exists, add an empty string to its description
      this.in_the_box.description.push('');
    }
  }

  async updateInTheBox(item, e) {
    const index = this.in_the_box?.description?.indexOf(item);
    if (index > -1) {
      // Update the value of the item in the description
      this.in_the_box.description[index] = e.value;

      const form = {
        'attribute_name': 'IN_THE_BOX',
        'description': JSON.stringify(this.in_the_box.description),
      };

      try {
        const response = await this.webService.updateProductAttribute(this.in_the_box.id, form);
        console.log(response);  // Keep this for debugging or replace with more meaningful output
      } catch (error) {
        console.error('Error updating InTheBox:', error);
      }
    }
  }

  async deleteInTheBox(item) {
    const index = this.in_the_box?.description?.indexOf(item);
    if (index > -1) {
      // Remove the item from the description array
      this.in_the_box.description.splice(index, 1);

      const form = {
        'attribute_name': 'IN_THE_BOX',
        'description': JSON.stringify(this.in_the_box.description),
      };

      try {
        const response = await this.webService.updateProductAttribute(this.in_the_box.id, form);
        console.log(response);  // Keep this for debugging or replace with more meaningful output
      } catch (error) {
        console.error('Error deleting InTheBox:', error);
      }
    }
  }






  addTechSpec() {
    if (!this.tech_spec) {

      let form = {
        'attribute_name': 'TECH_SPEC',
        'description': JSON.stringify([
          {
            'key': '',
            'value': ''
          }
        ])
      }
      this.addProductAttribute(form)

    } else {

      console.log('herssse')
      this.tech_spec.description.push({
        'key': '',
        'value': ''
      })
    }

  }


  addSizeChart() {
    if (!this.size_chart) {

      let form = {
        'attribute_name': ' SIZE_CHART',
        'description': JSON.stringify([
          {
            'key': '',
            'value': ''
          }
        ])
      }
      this.addProductAttribute(form)

    } else {

      console.log('herssse')
      this.size_chart.description.push({
        'key': '',
        'value': ''
      })
    }

  }


  async updateSize(item, e) {


    var index = this.size_chart.description.indexOf(item);
    this.size_chart.description[index][e.id] = e.value

    let form = {
      'attribute_name': 'SIZE_CHART',
      'description': JSON.stringify(this.size_chart.description),
    }

    console.log(form)

    let response = await this.webService.updateProductAttribute(this.size_chart.id, form)


  }



  async deleteSize(item) {

    var index = this.size_chart.description.indexOf(item);
    this.size_chart.description.splice(index, 1);
    let form = {
      'attribute_name': 'SIZE_CHART',
      'description': JSON.stringify(this.size_chart.description),

    }

    let response = await this.webService.updateProductAttribute(this.size_chart.id, form)



  }






  async updateTechSpec(item, e) {


    var index = this.tech_spec.description.indexOf(item);
    this.tech_spec.description[index][e.id] = e.value

    let form = {
      'attribute_name': 'TECH_SPEC',
      'description': JSON.stringify(this.tech_spec.description),
    }

    console.log(form)

    let response = await this.webService.updateProductAttribute(this.tech_spec.id, form)


  }



  async deleteTechSpec(item) {

    var index = this.tech_spec.description.indexOf(item);
    this.tech_spec.description.splice(index, 1);
    let form = {
      'attribute_name': 'TECH_SPEC',
      'description': JSON.stringify(this.tech_spec.description),

    }

    let response = await this.webService.updateProductAttribute(this.tech_spec.id, form)



  }



  addVideo() {
    if (!this.video) {

      let form = {
        'attribute_name': 'VIDEO',
        'description': JSON.stringify([
          {
            'key': '',
            'value': ''
          }
        ])
      }
      this.addProductAttribute(form)

    } else {

      console.log('herssse')
      this.video.description.push({
        'key': '',
        'value': ''
      })
    }

  }



  async updateVideo(item, e) {


    var index = this.video.description.indexOf(item);
    this.video.description[index][e.id] = e.value

    let form = {
      'attribute_name': 'VIDEO',
      'description': JSON.stringify(this.video.description),
    }

    console.log(form)

    let response = await this.webService.updateProductAttribute(this.video.id, form)


  }



  async deleteVideo(item) {

    var index = this.video.description.indexOf(item);
    this.video.description.splice(index, 1);
    let form = {
      'attribute_name': 'VIDEO',
      'description': JSON.stringify(this.video.description),

    }

    let response = await this.webService.updateProductAttribute(this.video.id, form)



  }


  async addProductAttribute(form) {
    try {


      let response = await this.webService.addProductAttribute(this.selected_product.id, form)
      console.log(response)
    } catch (e) {
      console.log(e)
    } finally {
      this.getProductId(this.selected_product.id)



    }
  }






  async duplicateProduct() {
    try {

      this.show_detail = false
      let response = await this.webService.duplicateProduct(this.selected_product.id)
      console.log(response)
      if (response.data) {
        this.selected_product = response.data
        this.toast.sendMessage('Duplicate Success!')
        this.getProductId(this.selected_product.id)

      }
    } catch (e) {
      console.log(e)
    } finally {




    }
  }





  updateStatus(item, e, state) {
    let form = {}
    form[e] = state
    console.log(form)

    this.updateProductID(item, form)
  }

  setCoverImage(item) {
    console.log(item)
    let form = {
      'feature_image': item.url_path
    }


    this.updateProductID(this.selected_product, form)
  }


  setVariantCoverImage(item, cover) {
    console.log(item)
    let form = {
      'feature_image': cover.url_path
    }


    this.updateProductID(item, form)
    item.showCoverPick = false
  }

  deleteCoverImage(item) {
    console.log(item)
    let form = {
      'feature_image': null
    }


    this.updateProductID(item, form)
  }

  selectActivity(e) {

  }

  async updateProductID(item, form) {
    try {

      console.log(this.selected_product.id)

      let response = await this.webService.updateProductID(item.id, form)
      console.log(response)
      if (response.data) {
        console.log('update')
        this.toast.sendMessage('Updated!')

      } else {
        this.getProductId(this.selected_product.id)

      }
    } catch (e) {
      console.log(e)
    } finally {

      this.getProductId(this.selected_product.id)


    }
  }


  async updateProductAttribute(item, form) {
    try {

      console.log(this.selected_product.id)

      let response = await this.webService.updateProductAttribute(item.id, form)
      console.log(response)
      if (response.data) {
        console.log('update')


      } else {
        //this.getProductId(this.selected_product.id)

      }
    } catch (e) {
      console.log(e)
    } finally {

      //  this.getProductId(this.selected_product.id)


    }
  }

  async deleteProductAttribute(item) {
    try {

      console.log(this.selected_product.id)

      let response = await this.webService.deleteProductAttribute(item.id)
      console.log(response)
      if (response.data) {



      } else {
        //this.getProductId(this.selected_product.id)

      }
    } catch (e) {
      console.log(e)
    } finally {

      this.getProductId(this.selected_product.id)


    }
  }


  async deleteProductID(item) {
    try {

      console.log(this.selected_product.id)

      let response = await this.webService.deleteProductID(item.id)
      console.log(response)
      if (response.data) {
        console.log('update')


      } else {
        //  this.getSalesOrderId(this.selected_order.id)

      }
    } catch (e) {
      console.log(e)
    } finally {

      this.getProductId(this.selected_product.id)


    }
  }



  async addVariant() {
    try {
      let form = {
        'parrent_id': this.selected_product.id,
        'type': 'VARIANT_PRODUCT'
      }

      let response = await this.webService.addProduct(form)
      console.log(response)
    } catch (e) {
      console.log(e)
    } finally {
      this.getProductId(this.selected_product.id)


    }
  }



  async addProductGallery(item, form) {
    try {

      let response = await this.webService.addProductGallery(item.id, form)
      console.log(response)
    } catch (e) {
      console.log(e)
    } finally {
      this.getProductId(this.selected_product.id)


    }
  }

  async deleteProductGallery(item) {
    try {
      console.log(item)

      let response = await this.webService.deleteProductGallery(item.id)
      console.log(response)
    } catch (e) {
      console.log(e)
    } finally {
      this.getProductId(this.selected_product.id)


    }
  }


  async onChange(event: any, item) {

    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      formData.append('file', file);

      try {

        let res = await this.webService.uploadPhoto(formData, 'product_cover')
        console.log(res)

        if (res.data) {
          let form = {
            'url_path': res.data,
            'url_key': res.path,



          }

          this.addProductGallery(item, form)
        }

      } catch (error) {
        console.log(error)
      }
      finally {



      }

    }
  }

  async onChangeVariant(event: any, item) {

    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      formData.append('file', file);

      try {

        let res = await this.webService.uploadPhoto(formData, 'product_cover')
        console.log(res)

        if (res.data) {
          let form = {
            'feature_image': res.data,


          }

          this.updateProductID(item, form)
        }

      } catch (error) {
        console.log(error)
      }
      finally {



      }

    }
  }








  closeModal() {
    this.onClose.emit(true)
  }

}
