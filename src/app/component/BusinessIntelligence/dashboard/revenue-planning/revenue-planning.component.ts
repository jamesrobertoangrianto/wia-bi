import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { faChartLine, faPlusSquare, faArrowUp,faArrowDown} from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-revenue-planning',
  templateUrl: './revenue-planning.component.html',
  styleUrls: ['./revenue-planning.component.scss']
})
export class RevenuePlanningComponent implements OnInit {
  display_revenue_plannings: any;
  openRevenueForm: boolean;
  faArrowUp=faArrowUp
  faArrowDown=faArrowDown
  faPlusSquare=faPlusSquare
  faChartLine=faChartLine
  stements: any;
  brands: any;
  selectedBrand: any;

  range: { begin: any; end: any; };


  months= 
      [ 
          {
            value : 0,
            name : 'January'
          },
          {
            value : 1,
            name : 'February'
          },
          {
            value : 2,
            name : 'March'
          },
          {
            value : 3,
            name : 'April'
          },
          {
            value : 4,
            name : 'May'
          },
          {
            value : 5,
            name : 'June'
          },
          {
            value : 6,
            name : 'July'
          },
          {
            value : 7,
            name : 'August'
          },
          {
            value : 8,
            name : 'September'
          },
          {
            value : 9,
            name : 'October'
          },
          {
            value : 10,
            name : 'November'
          },
          {
            value : 11,
            name : 'December'
          },
          
      ]
      years=
      [ 
          {
            value : 2021,
            name : '2021'
          },
          {
            value : 2022,
            name : '2022'
          },
          {
            value : 2023,
            name : '2023'
          },
          {
            value : 2024,
            name : '2024'
          },
          {
            value : 2025,
            name : '2025'
          },
          {
            value : 2026,
            name : '2026'
          },
      ]

  revenueForm = new FormGroup({
    month: new FormControl(null,{validators: [Validators.required]}),
    year: new FormControl(null,{validators: [Validators.required]}),
    brand: new FormControl(null,{validators: [Validators.required]}),
    category: new FormControl(null,{validators: [Validators.required]}),

    brand_id: new FormControl(null,{validators: [Validators.required]}),
   
    status: new FormControl('ACTIVE'),
    target_revenue: new FormControl(null,{validators: [Validators.required]}),

  
  
  })


  start_date: any;
  end_date: any;
  addSalary: any;
  addExpense: any;
  selectedBrandItems: any;
  sales_order: any;
  addExtraExpense: boolean;
  addExtraSalary: boolean;
  actual: any;
  planning: any;
  revenue_plannings: any;
  filterState: boolean;
  updateAdsState: boolean;
  isUpdateAdsState: boolean;
  inventory: any;
  transaction_history: any;
  current_month: any;
  selectedAsset: any;
  selectedMonth: any;
  weekly_view: boolean;
  table_view: string;
  month: string;
  year: string;
  current_year: any;
  showModal: any;
  fetch_products: any;
  total_actual: any;
  total_planning: any;
  total_content_creator: number;
  total_active_lifestyle: number;
  total_traveling: number;
  total_actual_content_creator: number;
  total_actual_traveling: number;
  total_actual_active_lifestyle: number;
  

  constructor(
    private marketingService : ManualOrderService,
    private FinanceService : ManualOrderService,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private activatedRoute: ActivatedRoute,

  ) {
    this.range={
      'begin': null,
      'end': null
    }
  
   }

  ngOnInit(): void {

    let date = new Date()
  
   
    
    // this.getInventoryItemByBrand('fjallraven')
    this.route.queryParamMap.subscribe(queryParams => {

      
      this.month = queryParams.get("month")
      this.year = queryParams.get("year")
      
      
      this.current_month = this.month
      this.current_year = this.year
   
      if(!this.current_month){
        this.current_month = date.getMonth()
      }

      if(!this.current_year){
        this.current_year = date.getFullYear()
      }



      this.selectedMonth = this.current_month


      this.addSalary = 0
      this.addExpense = 0
      this.actual ={}
      this.planning ={}
      this.transaction_history ={}

      this.getSalesOrdersByInvoiceStatus()
     
    })


  }

   dates() {
    var dayOfWeek = 5;//friday
    var date = new Date(2013, 10, 13);
    var diff = date.getDay() - dayOfWeek;
    if (diff > 0) {
        date.setDate(date.getDate() + 6);
    }
    else if (diff < 0) {
        date.setDate(date.getDate() + ((-1) * diff))
    }
    console.log(date);
}

  changeMonth(value){
   
    this.router.navigate( ['/revenue-planning'], {
          queryParams: { month: value },
          queryParamsHandling:'merge',
        }
      );
    
  }

  changeYear(value){
    console.log(value)
 
    this.router.navigate( ['/revenue-planning'], {
          queryParams: { year: value },
          queryParamsHandling:'merge',
        }
      );
    
  }




  selectBrand(item){
    this.selectedBrand = true
  
    if(item){
      this.revenueForm.controls['brand'].setValue(item.name)
     
      this.revenueForm.controls['brand_id'].setValue(item.id)
     
      this.brands=null

     
    }
    
  }


  async addRevenue(start?, end?) {
    console.log(this.revenueForm.value)
   
    let dateStart = start == null ? new Date(this.range.begin) : new Date(start)
    let dateEnd = end == null ? new Date(this.range.end) : new Date(end)
  
    this.start_date= formatDate(dateStart, 'yyyy-MM-dd', 'en_US') 
    this.end_date= formatDate(dateEnd, 'yyyy-MM-dd', 'en_US') 


    
    try {
      this.spinner.show();
       let response = await this.marketingService.addRevenue(this.revenueForm.value)
      console.log(response)
      this.revenueForm.reset()


      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
        this.closeModal()
       this.ngOnInit()
      }
   
  }

  async getBrands(e) {
    
    var name = e.target.value
    if(name)
    try {
    this.spinner.show();
     let response = await this.FinanceService.getBrands(name)
     this.brands = response.brands
      console.log(response)
   
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
    else
    this.brands=null
  }

  getValuePerTrx(value,trx){
    return value/trx
  }





  async removeItem(item) {
    this.spinner.show();
    
    try {
    
     let response = await this.marketingService.deleteRevenuePlanning(item.revenue_planning_id)
    
      
      
     

    } catch (e) {
      console.log(e)
    } finally { 
      this.ngOnInit()
    }
  }



  async getRevenuePlanningData(month,year) {
    this.display_revenue_plannings = []
    this.revenue_plannings = []
    this.selectedMonth = parseFloat(month)
  
    try {
    this.spinner.show();
     let response = await this.marketingService.getRevenuePlanning(month,year)

      this.revenue_plannings = response.revenue_plannings
      this.revenue_plannings.forEach(item => {
        item.actual_revenue = this._getActualRevenue(item.brand)

      });
      this.total_actual = 0
      this.total_planning = 0
      this.total_content_creator = 0
      this.total_traveling = 0
      this.total_active_lifestyle = 0

      this.total_actual_content_creator = 0
      this.total_actual_traveling = 0
      this.total_actual_active_lifestyle = 0

      console.log('here')
      console.log(this.revenue_plannings)
      this.revenue_plannings.forEach(item => {
        this.total_actual += item.actual_revenue
        this.total_planning += parseFloat(item.target_revenue)
        if(item.category == 'content_creator'){
          this.total_content_creator +=parseFloat(item.target_revenue)
          this.total_actual_content_creator += this._getActualRevenue(item.brand)

        }
        if(item.category == 'traveling'){
          this.total_traveling +=parseFloat(item.target_revenue)
          this.total_actual_traveling += this._getActualRevenue(item.brand)
       
        }
        if(item.category == 'active_lifestyle'){
           this.total_active_lifestyle +=parseFloat(item.target_revenue)
           this.total_actual_active_lifestyle += this._getActualRevenue(item.brand)
       

        }
      });
     

    } catch (e) {
      console.log(e)
    } finally { 
      this.spinner.hide();
    }
  }

  _getActualRevenue(brand){
    if(this.fetch_products){
      let total = 0
      this.fetch_products.forEach(item => {
        if(brand == 'Life Behind Bars (LBB)'){
          brand = 'lbb'
        }
        if(item.product_name.toLowerCase().includes(brand.toLowerCase())){
          total += item.sell_price_x_qty
        }

       // console.log(brand)
 
      });
      return total
    }
  }
  async getSalesOrdersByInvoiceStatus() {


    if( !this.start_date &&  !this.end_date){
     var date = new Date();
     var firstDay = new Date( parseInt(this.current_year) , parseInt(this.current_month) - 0,0);
     var lastDay = new Date( parseInt(this.current_year) , parseInt(this.current_month) + 1, 0);
     this.start_date = formatDate(firstDay, 'yyyy-MM-dd', 'en_US') 
     this.end_date = formatDate(lastDay, 'yyyy-MM-dd', 'en_US') 
 
    }
    
 
 
    
 
     try {
       this.spinner.show();
       let response = await this.marketingService.getSalesOrdersByInvoiceStatus(status?status:'all',this.start_date,this.end_date,0)
      // console.log(response)
    //  this.sales_orders = response.sales_orders
        this.fetch_products =  this._buildProductView(response.sales_orders)
       console.log(this.fetch_products)
    //  console.log('heress')
     } catch (e) {
       console.log(e)
     } finally {
      
       
       this.spinner.hide();
       this.getRevenuePlanningData(this.current_month ,this.current_year)

     }
   }
 
  _getCatName(name){
    if(name =='content_creator'){
      return 
    }
    return name
  }

   _buildProductView(sales_orders){
   
    var products = []

    sales_orders.forEach(item => {
      item.products.forEach(product => {
       
        products.push({
          'product_name' : product.name,
          'brand' : product.brand,
          'sell_price' : product.sell_price,
          'sell_price_x_qty' : product.sell_price * product.qty,

        
        
    
        })
      });
    });

    return products
 
  }




  
openModal(modal){
  this.showModal = modal
}
closeModal(){
  this.showModal = null
}
  //TRANSACTION







  


}
