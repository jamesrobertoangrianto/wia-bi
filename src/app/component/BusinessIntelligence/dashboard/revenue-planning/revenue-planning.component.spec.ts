import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevenuePlanningComponent } from './revenue-planning.component';

describe('RevenuePlanningComponent', () => {
  let component: RevenuePlanningComponent;
  let fixture: ComponentFixture<RevenuePlanningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevenuePlanningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevenuePlanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
