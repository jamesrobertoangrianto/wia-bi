import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common'
import { faChevronLeft, faExpandAlt } from '@fortawesome/free-solid-svg-icons';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-purchase-order-list',
  templateUrl: './purchase-order-list.component.html',
  styleUrls: ['./purchase-order-list.component.scss']
})
export class PurchaseOrderListComponent implements OnInit {

  purchaseForm = new FormGroup({
    purchase_vendor_id: new FormControl(null,{validators: [Validators.required]}),
    purchase_company_id: new FormControl(1,{validators: [Validators.required]}), 
    
    is_cbd: new FormControl(0,{validators: [Validators.required]}), 
    
    status: new FormControl(null), 
    //term_of_payment: new FormControl(null), 
    term_of_payment: new FormControl(null,{validators: [Validators.required]}), 


    // chamber: new FormControl(null,{validators: [Validators.required]}), 
    // size: new FormControl(null,{validators: [Validators.required]}), 
    // warehouse_id: new FormControl(null), 

  })
 
  vendorForm = new FormGroup({
    name: new FormControl(null,{validators: [Validators.required]}),
    npwp: new FormControl(null,{validators: [Validators.required]}), 
    pic: new FormControl(null,{validators: [Validators.required]}), 
    term_of_payment: new FormControl(null,{validators: [Validators.required]}), 
    credit: new FormControl(null,{validators: [Validators.required]}), 
    purchase_company_id: new FormControl(null,{validators: [Validators.required]}), 


  })

  tab_option = [
   
    {
      'label' : 'WIA',
      'name' : 1,
      'id' : 1,
      'is_active' : true
      
    },
    {
      'label' : 'PDB',
      'name' : 2,
      'id' : 2,
    
      
    }
  ]



  status: string;
  purchase_orders: any;
  payment_status: any;
  purchase_order: any;
  query: any;
  faExpandAlt=faExpandAlt
  tab_menu_list: { id: string; label: string; class: any; }[];
  purchase_menu_list: { id: string; label: string; class: any; }[];
  vendor_list: any[];
  purchase_order2: any;
showModal: any;
  vendor: any;
  
  constructor(
     
    private financeService : ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private location: Location,
    private webService : ManualOrderService,
    private router : Router,

  ) { 
   
  }

  ngOnInit(): void {

  
    this.route.queryParamMap.subscribe(queryParams => {
      this.query =   queryParams.get("search")
      this.status = queryParams.get("tab_view")



     // this.payment_status = queryParams.get("payment_status")
     if(this.query ){
      this.search()
     }else{
      if(this.status == 'UN_PAID' || this.status == 'OVERDUE'){
        this.getPurchaseOrdersByPaymentStatus(this.status)
      }else{
        this.getPurchaseOrdersByStatus(this.status)
      }
     
     }
     
     this.getPurchase()

     this.getPurchaseVendor()
    })


    this.getUnPaidPurchaseOrders()

    this.tab_menu_list = [
      {
        'id': 'all',
        'label': 'All Order',
  
        'class': null,
      },
      {
      'id': 'ON_PROCESS',
      'label': 'Wait for Delivery',

      'class': null,
    },
    {
      'id': 'UN_PAID',
      'label': 'Wait for Payment',

      'class': null,
    },
    {
      'id': 'OVERDUE',
      'label': 'Overdue Payment',

      'class': null,
    },







  ]



  this.purchase_menu_list = [
    {
      'id': 'all_purchase',
      'label': 'All Purchase',

      'class': null,
    },
    {
    'id': 'vendor',
    'label': 'Vendor',

    'class': null,
  },
  






]

    
  
    
  }

  async search(){
    console.log('msk')
    console.log(this.query)
   
    
    try {
      this.spinner.show();
       let response = await this.financeService.getPurchaseOrdersByQuery(this.query)
        //console.log(response)
        this.purchase_orders = response.purchase_orders
      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
      }
  
  }

  getDate(date){
    if(date){
      return new Date(date.replace(/-/g, "/"));
    }
    //console.log(date)
   
   
  
  }

  async getPurchaseOrdersByStatus(status) {
    console.log(status)

    try {
    this.spinner.show();
     let response = await this.financeService.getPurchaseOrdersByStatus(status?status:'all')
      console.log(response)
    this.purchase_orders = response.purchase_orders
   
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }
  async getPurchaseOrdersByPaymentStatus(status) {
    console.log(status)
    if(status == 'OVERDUE'){
      status = 'UN_PAID'
      var isOverdue = true
    }
    try {
    this.spinner.show();
     let response = await this.financeService.getPurchaseOrdersByPaymentStatus(status)
      console.log(response)
     this.purchase_orders = response.purchase_orders

     if(isOverdue){
      this.purchase_orders = this.purchase_orders.filter(item =>
        {
          var today = new Date()
          var receive = new Date(item.due_date)
          if(receive.getTime() < today.getTime() ){
           return true
          }
        });
  
     }

   
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async getUnPaidPurchaseOrders() {
   
    try {
    this.spinner.show();
     let response = await this.financeService.getPurchaseOrdersByPaymentStatus('UN_PAID')
    this.purchase_order = response.purchase_orders

    console.log(this.purchase_order)
     let total_payment = 0
    let overdue = 0

    this.purchase_order.forEach(item => {
      total_payment += parseFloat(item.purchase_amount)
      var today = new Date()
      var receive = new Date(item.due_date)
      if(receive.getTime() < today.getTime() ){
        overdue += parseFloat(item.purchase_amount)
      }
      
    });

    console.log(total_payment)

    this.purchase_order.total_payment = total_payment
    this.purchase_order.overdue = overdue
   
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  _getPurchaseOrderId(id){
    return parseFloat(id)+1000
  }

  _getDeliveryStatus(status){
   if(status == 'ON_PROCESS'){
    return 'Wait for delivery'
   }else{
     return 'Purchase Receive'
   }
  }



  _getPaymentStatus(item){
    if(item.payment_status == 'UN_PAID'){
      var today = new Date()
      var receive = new Date(item.due_date)
      if(receive.getTime() < today.getTime() ){
        return 'Overdue'
      }
      else{
        return 'Wait for payment'
      }
    }else{
      return item.payment_status
    }
   
  }

  _getPaymentStatusClass(item){
    if(item.payment_status == 'UN_PAID'){
      var today = new Date()
      var receive = new Date(item.due_date)
      if(receive.getTime() < today.getTime() ){
        return 'overdue'
      }
      else{
        return 'shipment'
      }
    }else{
      return 'item.payment_status'
    }
   
  }





  async getPurchase(){
    try {
      this.spinner.show();
         let response = await this.webService.getPurchase('')
          console.log(response)
      console.log('get pur')
          this.purchase_order2 = response.data
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        
           //this.ngOnInit()
            
        }
  }



  async getPurchaseVendor(){
    try {
      this.spinner.show();
         let response = await this.webService.getPurchaseVendor('')
          console.log(response)

          this.vendor = response.data
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        
           //this.ngOnInit()
            
        }
  }



  async addPurchase(){
    console.log(this.purchaseForm.value)
    this.purchaseForm.get('status').setValue('DRAFT')
    try {
      this.spinner.show();
         let response = await this.webService.addPurchase(this.purchaseForm.value)
         this._navigateTo(response.data.id)
          console.log(response)
          

       
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        
           //this.ngOnInit()
            
        }
  }

  _navigateTo(id) {
    this.router.navigate(
      ['/purchase-order/id/'+id]
    );
  }


  async addPurchaseVendor(){
    console.log(this.vendorForm.value)
    try {
      this.spinner.show();
         let response = await this.webService.addPurchaseVendor(this.vendorForm.value)
         
          console.log(response)
      if(response.data){
        this.vendorForm.reset()

      }
       
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        
           this.ngOnInit()
            
        }
  }

  
  selectForm(key,value){
    console.log(value)
    this.purchaseForm.get(key).setValue(value)
  }


  async searchVendor($event) {

    this.vendor_list = []
    try {
     
      let response = await this.webService.getPurchaseVendor('')


      if (response.data) {
        this.vendor_list = response.data.map(item => {

          return {
            id: item.id,
            label: item.name,
            name: item.name,
            
          };
        });

      }

    } catch (error) {
      //this.appService.openToast(error)
    } finally {
      //this.appService.hideLoading()

    }


  }








}
