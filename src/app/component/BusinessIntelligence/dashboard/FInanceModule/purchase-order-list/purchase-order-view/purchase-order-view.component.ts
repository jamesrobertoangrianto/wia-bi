import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-purchase-order-view',
  templateUrl: './purchase-order-view.component.html',
  styleUrls: ['./purchase-order-view.component.scss']
})
export class PurchaseOrderViewComponent implements OnInit {
  faChevronLeft = faChevronLeft
  purchase_id: string;
  purchase: any;
  purchase_order_number: any;
  Inventorys = []
  receivePurchaseForm = new FormGroup({

    is_cbd: new FormControl(null),
    payment_amount: new FormControl(null),
    delivery_number: new FormControl(null),
    purchase_id: new FormControl(null),

  })

  sendPaymentForm = new FormGroup({
    is_cbd: new FormControl(null),
    purchase_discount: new FormControl(null),
    account_payable: new FormControl(null),
    payment_amount: new FormControl(null),
    payment_method: new FormControl(null),
    account_id: new FormControl(null),
    purchase_id: new FormControl(null),

  })

  accounts: any;
  showModal: any;
  journal_transactions: any;
  journal_transactions_credit: number;
  journal_transactions_debit: number;
  isCBD: boolean;
  rack: any;
  inventory: any;
  warehouse_id: number;

  constructor(
    private toast: ToastrService,
    private financeService: ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.isCBD = false
    this.route.paramMap.subscribe(params => {
      this.purchase_id = params.get("id")
      this.warehouse_id = 1
      this.getPurchaseOrderById(this.purchase_id)
      this.getChartAccoutsByType()
      this.getWarehouseRack(this.warehouse_id)
      this.getInventory(this.warehouse_id, this.purchase_id)
    })
  }

  openModal(id) {
    this.showModal = id
  }


  async getWarehouseRack(id) {
    // console.log(this.params)
    try {
      this.spinner.show();
      let response = await this.financeService.getWarehouseRack(id, '')
      console.log(response)
      this.rack = response.data

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  receiveItem(item) {
    let currentDate = new Date().toJSON().slice(0, 10);

    let inventory = []
    for (let i = 0; i < item.order_qty; i++) {
      inventory.push(
        {
          'purchase_number': item.purchase_order_id,
          'product_id': item.product_id,
          'brand': item.brand,
          'type': 'PURCHASE',
          'name': item.name,
          'created_at': currentDate,
          'warehouse_id': 1,
          // 'warehouse_rack_id' : item.warehouse_rack_id,
          'cogs': item.order_price,
          'status': 'delivered',

        }
      )
    }


    console.log(inventory)
    this.addInventory(item, inventory)

  }

  _getTotalAllocate(product_id) {
    if (this.inventory) {
      let total = 0
      this.inventory.forEach(item => {
        if (item.product_id == product_id) {
          total += 1
        }
      });
      return total
    } else return false

  }

  async addInventory(item, form) {
    try {
      this.spinner.show();
      let response = await this.financeService.addInventory(form)
      console.log(response)
      item.isAdded = true
      this.toast.info('Success')
      //let res = await this.financeService.getProductStock(item.product_id)
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

      this.ngOnInit()

    }
  }

  async getInventory(warehouse_id, id) {
    // console.log(this.params)

    try {
      this.spinner.show();
      let response = await this.financeService.getInventory(warehouse_id, '?purchase_number=' + id)
      console.log(response)
      this.inventory = response.data

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this._getInventoryReceiveAmount()
    }
  }







  async getChartAccoutsByType() {

    try {
      this.spinner.show();
      let response = await this.financeService.getChartAccoutsByType()
      console.log(response)
      this.accounts = response.accounts

      console.log(this.accounts)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  navBack() {
    this.location.back()
  }

  async addPurchasePayment() {
    console.log(this.sendPaymentForm.value)

    try {
      this.spinner.show();
      let response = await this.financeService.addPurchasePayment(this.sendPaymentForm.value)
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.showModal = null
      this.getPurchaseOrderById(this.purchase_id)

    }
  }



  async addPurchaseReceive() {
    console.log(this.receivePurchaseForm.value)

    try {
      this.spinner.show();
      let response = await this.financeService.addPurchaseReceive(this.receivePurchaseForm.value)
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.showModal = null
      this.getPurchaseOrderById(this.purchase_id)

    }
  }

  calculatePayment(event) {
    var discount = event.target.value
    console.log(discount)
    this.sendPaymentForm.get('payment_amount').patchValue(parseFloat(this.purchase.purchase_amount) - parseFloat(discount))
    this.sendPaymentForm.get('account_payable').patchValue(parseFloat(this.purchase.purchase_amount))
    if (!discount) {
      this.sendPaymentForm.get('payment_amount').patchValue(parseFloat(this.purchase.purchase_amount))
      this.sendPaymentForm.get('account_payable').patchValue(parseFloat(this.purchase.purchase_amount))
    }
    // this.sendPaymentForm.get('payment_amount').patchValue(total)
  }

  async updatePurchaseItemById(item, method) {
    console.log(item)
    if (method == 'REMOVE') {
      item.isRemove = true
    }

    try {
      this.spinner.show();
      let response = await this.financeService.updatePurchaseItemById(item)
      console.log(response)

      var index = this.purchase.items.indexOf(item);
      this.purchase.items[index] = response.item
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      if (method == 'REMOVE') {
        item.isRemove = true
        this.purchase.items.splice(index, 1);
      }

    }
  }


  async getPurchaseOrderById(id) {


    try {
      this.spinner.show();
      let response = await this.financeService.getPurchaseOrderById(id)
      console.log(response)
      this.purchase = response.purchase_order
      this.purchase.items = response.items

      console.log(this.purchase.term_of_payment)
      if (this.purchase.term_of_payment == 0) {
        this.isCBD = true
        console.log(this.isCBD)
        this.sendPaymentForm.get('is_cbd').patchValue(this.isCBD)
        this.receivePurchaseForm.get('is_cbd').patchValue(this.isCBD)
      }
      this.purchase_order_number = parseFloat(this.purchase.purchase_order_id) + 1000

      this.sendPaymentForm.get('purchase_id').patchValue(this.purchase_order_number)
      this.sendPaymentForm.get('payment_amount').patchValue(this.purchase.purchase_amount)
      this.receivePurchaseForm.get('purchase_id').patchValue(this.purchase_order_number)
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }



  _getInventoryReceiveAmount() {
    let total = 0
    if (this.inventory) {
      this.inventory.forEach(item => {

        total += parseFloat(item.cogs)


      });
      this.receivePurchaseForm.get('payment_amount').patchValue(total)

      return total
    }

  }
}
