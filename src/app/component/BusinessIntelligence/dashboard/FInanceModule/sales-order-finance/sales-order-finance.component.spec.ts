import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesOrderFinanceComponent } from './sales-order-finance.component';

describe('SalesOrderFinanceComponent', () => {
  let component: SalesOrderFinanceComponent;
  let fixture: ComponentFixture<SalesOrderFinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesOrderFinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesOrderFinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
