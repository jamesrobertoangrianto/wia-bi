import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalAccountTransactionComponent } from './journal-account-transaction.component';

describe('JournalAccountTransactionComponent', () => {
  let component: JournalAccountTransactionComponent;
  let fixture: ComponentFixture<JournalAccountTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JournalAccountTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalAccountTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
