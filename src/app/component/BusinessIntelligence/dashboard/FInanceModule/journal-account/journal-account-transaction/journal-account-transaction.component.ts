import { Component, OnInit } from "@angular/core";
import { Location, formatDate } from "@angular/common";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { JournalAccountService } from "src/app/services/journal-account/journal-account.service";
import { ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";

const today = new Date();
@Component({
  selector: "app-journal-account-transaction",
  templateUrl: "./journal-account-transaction.component.html",
  styleUrls: ["./journal-account-transaction.component.scss"],
})
export class JournalAccountTransactionComponent implements OnInit {
  journalDetail: {
    account: Record<any, any>;
    items: Record<any, any>;
    summary: Record<any, any>;
  } = {
    account: {},
    items: [],
    summary: {},
  };

  state = {
    isOpenModal: false,
  };

  get journalAccountId() {
    return this.activatedRoute.snapshot.params["id"];
  }

  faChevronLeft = faChevronLeft;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private svc: JournalAccountService,
    private toast: ToastrService
  ) {}

  searchParams = {
    start_date: today.getTime(),
    end_date: today.getTime(),
  };

  handleFinishChangeStartDate = () => {
    // if the start date is beyond the end date,
    // set beyond date to the same date of the start
    if (
      new Date(this.searchParams.start_date).getTime() >
      new Date(this.searchParams.end_date).getTime()
    ) {
      this.searchParams.end_date = this.searchParams.start_date;
    }
    this.doAPICall(this.journalAccountId);
  };

  handleFinishChangeEndDate = () => {
    this.doAPICall(this.journalAccountId);
  };

  handleGoBack = () => {
    this.location.back();
  };

  doAPICall = (id) => {
    this.svc.getJournalAccountTransaction(id, this.searchParams).then((res) => {
      if (res.message_code !== "success") {
        //handle failure here
        return;
      }

      if (!res.data.account.id) {
        this.toast.error(
          "You are following an invalid Journal Account. Please check the ID and try again. You will be redirected back shortly",
          "No such Journal Account",
          {
            timeOut: 5000,
            positionClass: "toast-top-center",
          }
        );
        setTimeout(() => {
          this.location.back();
        }, 5000);
        return;
      }
      this.journalDetail = res.data;
    });
  };

  ngOnInit(): void {
    this.doAPICall(this.journalAccountId);
  }
}
