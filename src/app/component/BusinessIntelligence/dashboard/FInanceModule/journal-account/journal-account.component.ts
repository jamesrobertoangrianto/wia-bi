import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { JournalAccountService } from "src/app/services/journal-account/journal-account.service";

@Component({
  selector: "app-journal-account",
  templateUrl: "./journal-account.component.html",
  styleUrls: ["./journal-account.component.scss"],
})
export class JournalAccountComponent implements OnInit {
  journal_accounts: Array<Object> = [];
  state = {
    isOpenModal: false,
    isFetching: false,
  };

  editState: Record<string, Record<string, any>> = {};
  handlePartialEdit = (id, fieldName, event) => {
    const { value } = event;

    this.svc
      .updateJournalAccount(id, {
        [fieldName]: value,
      })
      .then((res) => {
        if (res.message_code !== "success") {
          this.toast.error("We are unable to save changes");
        }
      });
  };

  form = new FormGroup({
    name: new FormControl(null, {
      validators: [Validators.required],
    }),
    description: new FormControl(null, {
      validators: [Validators.required],
    }),
    type: new FormControl(null, {
      validators: [Validators.required],
    }),
    primary_account: new FormControl(),
  });

  journalType = [
    {
      label: "Cash & Bank",
      type: "CASH_BANK",
    },
    {
      label: "Inventory",
      type: "INVENTORY",
    },
    {
      label: "Expenses",
      type: "EXPENSES",
    },
    {
      label: "Contra Expenses",
      type: "CONTRA_EXPENSES",
    },
    {
      label: "Cost of Sales",
      type: "COST_OF_SALES",
    },
    {
      label: "Income",
      type: "INCOME",
    },
    {
      label: "Equity",
      type: "EQUITY",
    },
    {
      label: "Liabilities",
      type: "LIABILITIES",
    },
    {
      label: "Fix Assets",
      type: "FIX_ASSETS",
    },
    {
      label: "Current Assets",
      type: "CURRENT_ASSETS",
    },
    {
      label: "Account Payable",
      type: "ACCOUNT_PAYABLE",
    },
    {
      label: "Account Receivable",
      type: "ACCOUNT_RECEIVABLE",
    },
  ];

  constructor(
    private svc: JournalAccountService,
    private toast: ToastrService,
    private router: Router
  ) {}

  handleViewAccountDetail = (id) => {
    this.router.navigate(["journal-account", "view", id]);
  };

  handleChangeAccountType = (id, event) => {
    this.handlePartialEdit(id, "type", event.target);
  };

  handleCloseCreateNewAccount = () => {
    this.state.isOpenModal = false;
  };

  handleOpenCreateNewAccount = () => {
    this.state.isOpenModal = true;
  };

  handleSubmitCreateNewJournalAccount = () => {
    const value = this.form.value;
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.svc.addJournalAccount(value).then((res) => {
        if (res.message_code === "success") {
          this.toast.success("Journal has been created successfully");
          this.handleCloseCreateNewAccount();
          //call API to the latest list
          this.fetchAPI();
          this.form.reset();
        } else {
        }
      });
    }
  };

  fetchAPI = () => {
    this.state.isFetching = true;
    this.svc
      .getJournalAccount()
      .then((res) => {
        this.journal_accounts = res.data;
      })
      .finally(() => {
        this.state.isFetching = false;
      });
  };

  ngOnInit(): void {
    this.fetchAPI();
  }
}
