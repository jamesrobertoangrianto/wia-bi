import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { matRangeDatepickerRangeValue } from 'mat-range-datepicker';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.scss']
})
export class ExpenseComponent implements OnInit {
  expenses: any;
  selectedItem: any;
  range: matRangeDatepickerRangeValue<any>
  expenseForm = new FormGroup({


    customer_id: new FormControl(1),
    amount: new FormControl(null),
    note: new FormControl(null),
    account_id: new FormControl(null),
    expenses_account: new FormControl(null),
    status: new FormControl('PAID'),

    type: new FormControl(null),
    cost_type: new FormControl(null),


  })
  totalExpense: number;
  totalPayable: number;
  enableCloseStatement: boolean;
  accounts: any;
  expenses_accounts: any;
  journal_transactions: any;
  journal_transactions_credit: number;
  journal_transactions_debit: number;
  showModal: any;


  constructor(
    private customerService: ManualOrderService,
    private financeService: ManualOrderService,
    private spinner: NgxSpinnerService,
    private manualOrderSvc: ManualOrderService,
    private toast: ToastrService,
  ) {

    this.range = {
      'begin': null,
      'end': null
    }

  }

  months =
    [
      {
        value: 0,
        name: 'January'
      },
      {
        value: 1,
        name: 'February'
      },
      {
        value: 2,
        name: 'March'
      },
      {
        value: 3,
        name: 'April'
      },
      {
        value: 4,
        name: 'May'
      },
      {
        value: 5,
        name: 'June'
      },
      {
        value: 6,
        name: 'July'
      },
      {
        value: 7,
        name: 'August'
      },
      {
        value: 8,
        name: 'September'
      },
      {
        value: 9,
        name: 'October'
      },
      {
        value: 10,
        name: 'November'
      },
      {
        value: 11,
        name: 'December'
      },

    ]

  ngOnInit(): void {
    this.enableCloseStatement = true

    this.getExpense(true)
  }


  closeDetails() {
    this.selectedItem = null
  }

  openForm() {
    this.selectedItem = true
  }





  async getExpense(is_current_month) {

    if (is_current_month == true) {
      var date = new Date();
      var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      var from = formatDate(firstDay, 'yyyy-MM-dd', 'en_US')
      var to = formatDate(lastDay, 'yyyy-MM-dd', 'en_US')
    }
    else {
      let dateStart = new Date(this.range.begin)
      let dateEnd = new Date(this.range.end)
      var from = formatDate(dateStart, 'yyyy-MM-dd', 'en_US')
      var to = formatDate(dateEnd, 'yyyy-MM-dd', 'en_US')

    }

    try {
      this.spinner.show();
      console.log(from)
      let response = await this.customerService.getExpense(from, to)

      console.log(response)
      this.expenses = response.expenses
      let total = 0
      this.expenses.forEach(item => {
        total += parseFloat(item.data.debit)
        //console.log(item.data.debit)
      });

      this.totalExpense = total

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }





  openModal(id) {
    this.showModal = id
  }




  getTotalExpense() {
    if (this.expenses) {
      var total = 0
      this.expenses.forEach(item => {
        if (item.status !== 'DECLINED') {
          total += parseFloat(item.amount)
        }


      });
      return total
    }


  }


}
