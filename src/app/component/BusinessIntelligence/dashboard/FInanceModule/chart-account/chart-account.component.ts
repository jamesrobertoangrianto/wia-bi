import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-chart-account',
  templateUrl: './chart-account.component.html',
  styleUrls: ['./chart-account.component.scss']
})
export class ChartAccountComponent implements OnInit {
  accounts: any;
  isShowModal: boolean;

  constructor(
    private financeService : ManualOrderService,
    private spinner: NgxSpinnerService
  ) { }


  accountType = [
    {
      'type_name' : 'Cash & Bank',
      'type' : 'cash_bank'
      
    },
    {
      'type_name' : 'Inventory',
      'type' : 'inventory'
      
    },
    {
      'type_name' : 'Expenses',
      'type' : 'expenses'
      
    },
    {
      'type_name' : 'Contra Expenses',
      'type' : 'contra_expense'
      
    },
    {
      'type_name' : 'Cost of Sales',
      'type' : 'cost_of_sales'
      
    },
    {
      'type_name' : 'Income',
      'type' : 'income'
      
    },
    {
      'type_name' : 'Equity',
      'type' : 'equity'
      
    },
    {
      'type_name' : 'Liabilities',
      'type' : 'liabilities'
      
    },
    {
      'type_name' : 'Fix Assets',
      'type' : 'fix_assets'
    },
    {
      'type_name' : 'Current Assets',
      'type' : 'current_assets'
    },

    {
      'type_name' : 'Account Payable',
      'type' : 'account_payable'
    },

    {
      'type_name' : 'Account Receivable',
      'type' : 'account_receiveable'
    },
    
  ]

  accountForm=new FormGroup({
    

    account_name: new FormControl(null),
    account_number: new FormControl(null),
    account_type: new FormControl(null),
    account_type_name: new FormControl(null),
    account_description: new FormControl(null),
    account_balance: new FormControl(null),
  
  })
  
  ngOnInit(): void {
    this.getChartAccounts()
  }

  closeDetails(){
    this.isShowModal = false
  }

  openModal(){
    this.isShowModal = true
  }


  async  getChartAccounts() {
  
    try {
    this.spinner.show();
     let response = await this.financeService.getChartAccounts()
      console.log(response)
      this.accounts = response.accounts
   
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


           
  async  addChartAccount() {
 
    try {
    this.spinner.show();
     let response = await this.financeService.addChartAccount(this.accountForm.value)
      console.log(response)
      this.accounts = this.accounts.concat(response.account)
      
   
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.isShowModal = false
    }
  }

}
