import { Component, OnInit } from "@angular/core";
import { ManualOrderService } from "src/app/services/manual-order/manual-order.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";
import {
  faChevronLeft,
  faFileDownload,
} from "@fortawesome/free-solid-svg-icons";

import { formatDate } from "@angular/common";

@Component({
  selector: "app-account-transaction",
  templateUrl: "./account-transaction.component.html",
  styleUrls: ["./account-transaction.component.scss"],
})
export class AccountTransactionComponent implements OnInit {
  transactions: any;
  account: any;
  account_id: string;

  faChevronLeft = faChevronLeft;
  faFileDownload = faFileDownload;
  month: any;
  year: any;
  current_month: any;
  current_year: any;
  total_credit: any;
  total_debit: any;
  constructor(
    private financeService: ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {}

  get containsEntries() {
    return (this.account?.transactions || []).length > 0;
  }

  handleDownloadCSV = () => {
    const csvRow = ["id", "date", "description", "debit", "credit"].join(",");
    const dataRows = this.account.transactions.map((item) => {
      return [
        item.account_id,
        item.create_date,
        item.account_description,
        item.debit,
        item.credit,
      ].join(",");
    });

    const csvContent =
      "data:text/csv;charset=utf-8," + [csvRow, ...dataRows].join("\n");
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute(
      "download",
      `AccountTransaction_${this.account_id}_${this.current_year}-${
        Number(this.current_month) + 1
      }.csv`
    );
    link.click();
  };

  years = [
    {
      value: 2022,
      name: "2022",
    },
    {
      value: 2023,
      name: "2023",
    },
    {
      value: 2024,
      name: "2024",
    },
    {
      value: 2025,
      name: "2025",
    },
    {
      value: 2026,
      name: "2026",
    },
  ];

  months = [
    {
      value: 0,
      name: "January",
    },
    {
      value: 1,
      name: "February",
    },
    {
      value: 2,
      name: "March",
    },
    {
      value: 3,
      name: "April",
    },
    {
      value: 4,
      name: "May",
    },
    {
      value: 5,
      name: "June",
    },
    {
      value: 6,
      name: "July",
    },
    {
      value: 7,
      name: "August",
    },
    {
      value: 8,
      name: "September",
    },
    {
      value: 9,
      name: "October",
    },
    {
      value: 10,
      name: "November",
    },
    {
      value: 11,
      name: "December",
    },
  ];

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.account_id = params.get("id");
    });

    this.route.queryParamMap.subscribe((queryParams) => {
      var date = new Date();
      this.month = queryParams.get("month");
      this.year = queryParams.get("year");

      this.current_month = this.month;
      this.current_year = this.year;
      console.log(this.month);
      console.log("this.current_month");

      if (!this.current_month) {
        this.current_month = date.getMonth();
      }
      if (!this.current_year) {
        this.current_year = date.getFullYear();
      }

      this.getChartAccoutsById(this.account_id);
    });
  }

  navBack() {
    this.location.back();
  }

  changeMonth(value) {
    console.log(value);
    console.log("value");
    this.router.navigate([], {
      queryParams: { month: value },
      queryParamsHandling: "merge",
    });
  }
  changeYear(value) {
    console.log(value);
    console.log("value");
    this.router.navigate([], {
      queryParams: { year: value },
      queryParamsHandling: "merge",
    });
  }

  async deleteItem(item) {
    var a = confirm("Confirm?");
    //var a = true

    if (a) {
      console.log(item);

      try {
        this.spinner.show();
        let response = await this.financeService.deletJournalTransactionById(
          item.id
        );
        console.log(response);
      } catch (e) {
        console.log(e);
      } finally {
        this.spinner.hide();

        var index = this.account.transactions.indexOf(item);
        this.account.transactions.splice(index, 1);

        this._calculateTotal();
      }
    }

    try {
      this.spinner.show();
      let response = await this.financeService.deletJournalTransactionById(
        item.id
      );
      console.log(response);
    } catch (e) {
      console.log(e);
    } finally {
      this.spinner.hide();

      var index = this.account.transactions.indexOf(item);
      this.account.transactions.splice(index, 1);

      this._calculateTotal();
    }
  }

  goToDetails(id, type) {
    console.log(id);
    console.log(type);
    if (type == "sales_order") {
      type = "sales-order";
    }
    if (type == "purchase_order") {
      type = "purchase-order";
    }
    this.router.navigate(["/" + type + "/view/" + id], { queryParams: {} });
    console.log(this.current_month);
    console.log(this.current_year);
  }

  _calculateTotal() {
    this.total_credit = 0;
    this.total_debit = 0;
    this.account.transactions.forEach((element) => {
      this.total_credit += parseFloat(element.credit);
      this.total_debit += parseFloat(element.debit);
    });
  }
  async getChartAccoutsById(id) {
    var date = new Date();
    var firstDay = new Date(
      parseInt(this.current_year),
      parseInt(this.current_month) - 0,
      0
    );
    var lastDay = new Date(
      parseInt(this.current_year),
      parseInt(this.current_month) + 1,
      0
    );
    console.log(date.getFullYear());
    var from = formatDate(firstDay, "yyyy-MM-dd", "en_US");
    var to = formatDate(lastDay, "yyyy-MM-dd", "en_US");
    console.log(from);
    console.log(to);
    try {
      this.spinner.show();
      let response = await this.financeService.getChartAccoutsById(
        id,
        from,
        to
      );
      console.log(response);
      this.account = response;

      // this.account.transactions.forEach(item => {
      //   if(item.reference_type == 'sales_order'){
      //     console.log(item)
      //  // this.deleteItem(item)
      //   }

      // });

      this._calculateTotal();
    } catch (e) {
      console.log(e);
    } finally {
      this.spinner.hide();
    }
  }
}
