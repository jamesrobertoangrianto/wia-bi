import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesOrderProductComponent } from './sales-order-product.component';

describe('SalesOrderProductComponent', () => {
  let component: SalesOrderProductComponent;
  let fixture: ComponentFixture<SalesOrderProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesOrderProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesOrderProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
