import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { Location } from '@angular/common'
import { faCheckCircle, faChevronLeft, faDownload, faExpandAlt, faFileExport, faFilePdf, faHeadphonesAlt } from '@fortawesome/free-solid-svg-icons';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
@Component({
  selector: 'app-sales-order-product',
  templateUrl: './sales-order-product.component.html',
  styleUrls: ['./sales-order-product.component.scss']
})
export class SalesOrderProductComponent implements OnInit {
  faHeadphonesAlt = faHeadphonesAlt
  faCheckCircle = faCheckCircle
  faDownload = faDownload
  selected_order: any


  tab_menu_list = [
    {
      'id': null,
      'label': 'All Order',

      'class': null,
    },
    {
      'id': 'wait_for_payment',
      'label': 'Wait For Payment',

      'class': null,
    },
    {
      'id': 'processing',
      'label': 'Processing',

      'class': null,
    },
    {
      'id': 'shipment',
      'label': 'Shipment',

      'class': null,
    },







  ]

  sortListItems = [
    {

      'name': 'sales_channel',
      'label': 'Sales Channel',

      'items': [


        {
          'label': 'WIA WEB',
          'name': 'wia_web',
          'is_active': true

        },
        {
          'label': 'ONLINE CHAT',
          'name': 'online_chat',
        },
        {
          'label': 'WIA STORE',
          'name': 'wia_store',
        },
        {
          'label': 'TOKOPEDIA WIA',
          'name': 'tokopedia_wia',
        },
        {
          'label': 'TOKOPEDIA DRONESTORE',
          'name': 'tokopedia_ds',
        },
        {
          'label': 'TOKOPEDIA KINTO',
          'name': 'tokopedia_kinto',
        },
        {
          'label': 'BLI BLI WIA',
          'name': 'bli_bli_wia',
        },
        {
          'label': 'BLI BLI DRONESTORE',
          'name': 'bli_bli_ds',
        },
        {
          'label': 'SHOPEE WI',
          'name': 'shopee_wia',
        },
        {
          'label': 'ETHIX KG',
          'name': 'ethix_kg',
        },
        {
          'label': 'ETHIX CP',
          'name': 'ethix_cp',
        },
        {
          'label': 'RETAILER',
          'name': 'retailer',
        },
        {
          'label': 'TIKTOK WIA',
          'name': 'tik_tok_wia',
        },
        {
          'label': 'B2B',
          'name': 'b2b',
        },


      ]

    },

    {

      'name': 'payment_type',
      'label': 'Payment',

      'items': [

        {
          'label': 'EDC',
          'name': 'EDC',


        },

        {
          'label': 'QRIS',
          'name': 'QRIS',


        },
        {
          'label': 'Credit Card',
          'name': 'credit_card',


        },
        {
          'label': 'Bank Transfer / VA',
          'name': 'bank_transfer',
        },
        {
          'label': 'Shopee',
          'name': 'shopee',
        },
        {
          'label': 'Gopay',
          'name': 'gopay',
        },




      ]

    },

    {

      'name': 'bank',
      'label': 'bank',

      'items': [


        {
          'label': 'BCA',
          'name': 'bank_transfer',
        },
        {
          'label': 'Mandiri',
          'name': 'shopee',
        },
        {
          'label': 'BRI',
          'name': 'gopay',
        },
        {
          'label': 'Bank Lainnya',
          'name': 'gopay',
        },




      ]

    },

    {

      'name': 'payment_installment',
      'label': 'Installment',

      'items': [


        {
          'label': '3',
          'name': '3',


        },
        {
          'label': '6',
          'name': '6',
        },
        {
          'label': '12',
          'name': '12',
        },
        {
          'label': '24',
          'name': '24',
        },




      ]

    },

    {

      'name': 'payment_fee',
      'label': 'Trx Fee',

      'items': [


        {
          'label': 'Not Set',
          'name': '0',


        },




      ]

    },







  ]
  query: string;
  status: string;
  params: string;
  sales_order: any;
  showAddOrder: boolean;
  page: number;
  total_revenue: number;
  total_profit: number;
  total_expense: number;
  total_cogs: any;
  total_bonus: any;
  total_payment_fee: any;
  total_discount: any;
  is_show_report: boolean;
  end_date: any;
  start_date: any;
  total_product: number;
  constructor(
    private financeService: ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private location: Location,
    private webService: ManualOrderService,
    private router: Router,
    private toast: ToastrService,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {




    this.showAddOrder = false
    this.route.queryParamMap.subscribe(queryParams => {
      this.query = queryParams.get("search")
      this.start_date = queryParams.get("start_date")
      this.end_date = queryParams.get("end_date")

      this.query = queryParams.get("search")

      this.status = queryParams.get("tab_view")
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams }))




      this.getSalesOrder()


    })

  }

  getPotential(item) {
    return item.profit + item.credit_note
  }
  selectOrder(item) {
    this.selected_order = item
    this.selected_order.id = item.sales_order_id
  }

  changeDate() {
    console.log('date')
    console.log(this.start_date)
    console.log(this.end_date)
    this.router.navigate(
      [],
      {
        queryParams: {
          'start_date': this.start_date,
          'end_date': this.end_date

        },
        queryParamsHandling: 'merge'
      }
    );
  }

  closeModal(e) {

    if (e) {
      this.ngOnInit()

    } else { this.selected_order = null }
  }

  async generateReport(is_download?) {
    this.sales_order = []

    if (this.params == '/') {
      this.params = '?generate_report=1'
    } else {
      this.params = this.params + '&generate_report=1'
    }

    try {
      this.spinner.show();
      let response = await this.webService.getSalesOrderProductReport(this.params)

      this.sales_order = response.data

      if (is_download) {
        this.export(this.sales_order)
      }
      console.log(this.sales_order)
      this.getSummary(this.sales_order)
      this.is_show_report = true



    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  export(items) {

    console.log(this.sales_order)

    let header = ['order number', 'sales channel', 'date', 'brand', 'product', 'qty', 'cogs', 'sell price']

    const rows = [
      header,
      ...items.map(item => {
        //  return [item]
        return [item.sales_order_id, item.sales_order?.sales_channel, item.sales_order?.order_date, item.brand, item.name, item.order_qty, item.cogs, item.final_price];
      })
    ];




    let csvContent = "data:text/csv;charset=utf-8,";

    rows.forEach(function (rowArray) {
      let row = rowArray.join(",");
      csvContent += row + "\r\n";
    });


    var encodedUri = encodeURI(csvContent);

    // var csvContent = "data:text/csv;charset=utf-8,%EF%BB%BF" + encodeURI(csvContent);

    window.open(encodedUri);


  }



  async getSalesOrder() {
    this.is_show_report = false
    this.sales_order = []
    try {
      this.spinner.show();
      let response = await this.webService.getSalesOrderProductReport(this.params)
      console.log(response)
      this.sales_order = response.data
      this.page = 1


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async loadMore() {
    this.is_show_report = false
    console.log('load')
    this.page = this.page + 1
    if (this.params == '/') {
      this.params = '?page=' + this.page
    } else {
      this.params = this.params + '&page=' + this.page
    }






    try {
      this.spinner.show();

      let response = await this.webService.getSalesOrderProductReport(this.params)
      console.log(response)
      if (response.data) {
        this.sales_order = this.sales_order.concat(response.data)

      }

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  getSummary(sales_order) {
    this.total_product = 0
    this.total_revenue = 0
    this.total_profit = 0
    this.total_expense = 0
    this.total_cogs = 0
    this.total_bonus = 0
    this.total_discount = 0
    this.total_payment_fee = 0

    sales_order.forEach(item => {
      this.total_revenue += (parseFloat(item.final_price) * item.order_qty)
      this.total_discount += (parseFloat(item.discount_amount) * item.order_qty)

      this.total_profit += parseFloat(item.profit)
      this.total_product += parseFloat(item.order_qty)
      this.total_cogs += parseFloat(item.cogs)
      this.total_bonus += parseFloat(item.bonus)

      this.total_payment_fee += (item.payment_fee * item.order_qty)


      // this.total_expense += item.total_expense

    });
  }

  updateTrxFee(item, e) {
    let form = {}
    form[e.id] = e.value
    item.payment_fee = e.value
    item.profit = item.profit - item.payment_fee
    this.updateSalesOrderItem(item, form)
  }

  updateCreditNote(item, e) {
    let form = {}
    form[e.id] = e.value
    item.credit_note = parseFloat(e.value)
    this.updateSalesOrderItem(item, form)
  }



  async updateSalesOrderItem(item, form) {
    try {

      let response = await this.webService.updateSalesOrderItem(item.id, form)

      if (response.data) {
        this.toast.success('updated')



      } else {
        this.ngOnInit()
      }
    } catch (e) {
      console.log(e)
    } finally {



    }
  }








}

