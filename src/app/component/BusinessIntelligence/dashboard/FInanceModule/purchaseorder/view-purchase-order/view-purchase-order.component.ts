import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faCheckCircle, faChevronLeft, faTrash } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { Location } from '@angular/common'
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { FormControl, FormGroup } from '@angular/forms';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-view-purchase-order',
  templateUrl: './view-purchase-order.component.html',
  styleUrls: ['./view-purchase-order.component.scss']
})
export class ViewPurchaseOrderComponent implements OnInit {
  purchase_id: string;
  @Input() selected_purchase: any
  params: string;
  faChevronLeft = faChevronLeft
  purchase: any;
  keyword: any;
  faTrash = faTrash
  products: any;
  purchase_item: any;
  faCheckCircle = faCheckCircle
  inventory: any;
  role: string;
  selected_product_id: any


  receivePurchaseForm = new FormGroup({

    is_cbd: new FormControl(null),
    payment_amount: new FormControl(null),
    delivery_number: new FormControl(null),
    purchase_id: new FormControl(null),

  })

  sendPaymentForm = new FormGroup({
    is_cbd: new FormControl(null),
    purchase_discount: new FormControl(null),
    account_payable: new FormControl(null),
    payment_amount: new FormControl(null),
    payment_method: new FormControl(null),
    account_id: new FormControl(null),
    purchase_id: new FormControl(null),

  })
  journal_transactions: any;
  journal_transactions_credit: number;
  journal_transactions_debit: number;
  showModal: string;
  variant: any;
  page: any;

  constructor(

    private webService: ManualOrderService,

    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router: Router,
    private location: Location,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {


    this.route.paramMap.subscribe(params => {

      this.purchase_id = params.get("id")
      this.getPurchaseId(this.purchase_id)
      this.getPurchaseItem(this.purchase_id)


    })


    // this.route.queryParamMap.subscribe(queryParams => {


    //   this.params = this.serializer.serialize(this.router.createUrlTree([''],
    //     { queryParams: this.route.snapshot.queryParams}))   

    //     this.getPurchaseId(this.purchase_id)




    // })
  }


  navBack() {
    this.location.back()
  }

  canApprove() {
    let role = localStorage.getItem("account_role")
    switch (role) {
      case 'SUPERADMIN':
        return true

      case 'FINANCE':
        return true

      default:
        return false
    }
  }


  canApply() {
    let role = localStorage.getItem("account_role")
    switch (role) {
      case 'SUPERADMIN':
        return true

      case 'BUSINESS':
        return true

      default:
        return false
    }
  }



  async getPurchaseId(id) {
    // console.log(this.params)

    try {
      this.spinner.show();
      let response = await this.webService.getPurchaseId(id)
      console.log(response)
      this.purchase = response.data


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  isAllowEdit() {

    switch (this.purchase.status) {
      case 'DRAFT':
        return true

      case 'WAIT_FOR_APPROVAL':
        return true



      default:
        return false
    }

  }


  receiveItem(item) {
    let currentDate = new Date().toJSON().slice(0, 10);

    let inventory = []
    for (let i = 0; i < item.qty; i++) {
      inventory.push(
        {
          'purchase_number': this.purchase_id,
          'product_id': item.product_id,
          'brand': item.brand,
          'type': 'PURCHASE',
          'name': item.name,
          'created_at': currentDate,
          'warehouse_id': 1,
          // 'warehouse_rack_id' : item.warehouse_rack_id,
          'cogs': item.cogs,
          'status': 'delivered',

        }
      )
    }


    console.log(inventory)
    this.addInventory(item, inventory)

  }

  async addInventory(item, form) {
    try {
      this.spinner.show();
      let response = await this.webService.addInventory(form)
      console.log(response)

      this.toast.info('Success')

      let form2 = {
        'is_receive': true
      }
      let res = await this.webService.updatePurchaseItem(form2, item.id)
      item.is_receive = true
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

      this.ngOnInit()

    }
  }


  async getPurchaseItem(id) {
    // console.log(this.params)

    try {
      this.spinner.show();
      let response = await this.webService.getPurchaseItem(id)
      this.purchase_item = response.data
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }




  isAdded(id) {

    if (this.purchase_item.some(item => item.product_id === id)) {
      return true
    } else {
      return false
    }

  }



  isAllReceive() {

    if (this.purchase_item.some(item => item.is_receive === false || !item.is_receive

    )) {
      return false
    } else {
      return true
    }

  }


  async getInventory(warehouse_id, id) {
    // console.log(this.params)

    try {
      this.spinner.show();
      let response = await this.webService.getInventory(warehouse_id, '?purchase_number=' + id)
      console.log(response)
      this.inventory = response.data

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

    }
  }

  async getProductVariant(item) {
    try {

      let response = await this.webService.getProductVariants(item.id)
      this.variant = response.data
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {

    }

  }


  async addPurchaseItem(item) {
    console.log(item)
    console.log(this.isAdded(item.product_id))


    console.log(item)
    let form = {
      'name': item.name,
      'brand': item.brand?.name,
      'product_id': item.id,
      'cogs': null,
      'qty': null,
      'purchase_number': this.purchase.id,
      'purchase_vendor_id': this.purchase.purchase_vendor_id,
      'purchase_company_id': this.purchase.purchase_company_id

    }

    if (this.isAdded(item.product_id)) {
      this.toast.info('added')
    } else {


      try {
        this.spinner.show();
        let response = await this.webService.addPurchaseItem(form)
        console.log(response)



      } catch (e) {
        console.log(e)
      } finally {
        this.clearSearch()
        this.ngOnInit()
        this.spinner.hide();
      }
    }

  }



  async updatePurchaseItem(item, e) {

    // console.log(item)
    // console.log(e)
    // item[e.id]=e.value
    // console.log(item)

    let form = {}
    form[e.id] = e.value

    console.log(form)

    try {
      this.spinner.show();
      let response = await this.webService.updatePurchaseItem(form, item.id)
      console.log(response)
      if (response.data) {
        this.ngOnInit()
      }



    } catch (e) {
      console.log(e)
    } finally {
      this.clearSearch()
      this.spinner.hide();
    }
  }



  updateStatus(value) {

    let form = {
      'id': this.purchase_id,
      'status': value,



    }

    if (value == 'APPROVE') {
      let currentDate = new Date().toJSON().slice(0, 10);

      form['approve_by'] = this.webService.getAccountName()
      form['approve_date'] = currentDate
    }

    if (value == 'WAIT_FOR_PAYMENT') {
      let currentDate = new Date().toJSON().slice(0, 10);


      form['receive_date'] = currentDate
    }
    this.updatePurchase(form)
  }

  updatePaymentStatus(value) {
    let form = {
      'id': this.purchase_id,
      'payment_status': value
    }
    this.updatePurchase(form)

  }

  updateDO(value) {
    console.log(value)
    let form = {
      'id': this.purchase_id,
      'delivery_number': value.value
    }
    this.updatePurchase(form)
  }

  addNote(value) {
    console.log(value)
    let form = {
      'id': this.purchase_id,
      'note': value.value
    }
    this.updatePurchase(form)
  }

  updateTop(value) {
    console.log(value)
    let form = {
      'id': this.purchase_id,
      'payment_date': value.value
    }
    this.updatePurchase(form)
  }


  async updatePurchase(form) {



    try {
      this.spinner.show();
      let response = await this.webService.updatePurchase(form, form.id)
      console.log(response)

      if (response.data) {
        if (form.payment_status) {
          this.addPurchasePayment()
        }

        if (form.status == 'WAIT_FOR_PAYMENT') {
          this.addPurchaseReceive()
        }

      }



    } catch (e) {
      console.log(e)
    } finally {
      this.ngOnInit()
      this.spinner.hide();
    }
  }


  async addPurchaseReceive() {

    let form = {
      "is_cbd": this.purchase.is_cbd,
      "payment_amount": this.purchase.total_receive,
      "delivery_number": this.purchase_id,
      "purchase_id": this.purchase_id
    }
    if (this.purchase) {
      console.log(form)
      try {
        this.spinner.show();
        let response = await this.webService.addPurchaseReceive(form)
        console.log(response)


      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
        // this.showModal = null
        // this.getPurchaseOrderById(this.purchase_id)

      }
    }

  }


  _getInventoryReceiveAmount() {
    let total = 0
    if (this.inventory) {
      this.inventory.forEach(item => {

        total += parseFloat(item.cogs)


      });


      return total
    }

  }

  async addPurchasePayment() {
    console.log(this.sendPaymentForm.value)

    let form = {
      "is_cbd": this.purchase.is_cbd,
      "purchase_discount": '',
      "account_payable": null,
      "payment_amount": this.purchase.total_receive,
      "payment_method": 'transfer',
      "account_id": 28,
      "purchase_id": this.purchase_id
    }



    try {
      this.spinner.show();
      let response = await this.webService.addPurchasePayment(form)
      console.log(response)


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();


    }
  }


  generatePdfs(item) {

    var title = 'Purchase Order #'

    const documentDefinition = {
      pageSize: 'A4',

      pageMargins: [15, 15, 15, 15],

      content: [






        {
          image: this._getImage(),
          width: 100,
          margin: [0, 0, 0, 15],

        },
        {
          text: title + this.purchase_id, margin: [0, 0, 0, 5], fontSize: 13,
          bold: true
        },

        { text: 'Status :' + this.purchase.status, margin: [0, 0, 0, 15], fontSize: 11 },
        { text: 'Purchase Request :' + this.purchase.create_by, margin: [0, 0, 0, 0], fontSize: 11 },
        { text: 'PO Date :' + this.purchase.approve_date, margin: [0, 0, 0, 15], fontSize: 11 },



        {
          margin: [0, 10, 0, 10],
          fontSize: 11,
          columns: [

            {
              // star-sized columns fill the remaining space
              // if there's more than one star-column, available width is divided equally
              width: '50%',
              text: 'To: \n' + this.purchase.vendor.name + '\n',


            },

            {
              // star-sized columns fill the remaining space
              // if there's more than one star-column, available width is divided equally
              width: '50%',
              text: 'From: \n Wearinasia (WIA.ID)\n Dalton Utara no. 50,\nScientia Square. Gading Serpong. Tangerang\nT: O21 - 2222 7825',

            },

          ],

          // optional space between columns
          columnGap: 10
        },

        { text: 'Purchase Item', margin: [0, 0, 0, 5], fontSize: 11 },




        this.getPackingItems(item),
        this.getSubtotalItems(item),





      ],



      defaultStyle: {
        fontSize: 11,
        color: 'black'
      }

    };

    pdfMake.createPdf(documentDefinition).download()
    this.toast.success('success')
  }

  getPackingItems(item) {
    console.log(item)
    return {
      margin: [0, 10, 0, 10],
      table: {
        widths: [300, '*', '*', '*'],
        body: [
          [{
            text: 'Product',
            style: 'tableHeader'
          },
          {
            text: 'SKU',
            style: 'tableHeader'
          },
          {
            text: 'Qty',
            style: 'tableHeader'
          },
          {
            text: 'Price',
            style: 'tableHeader'
          },


          ],
          ...this.purchase_item.map(ed => {
            return [ed.name, ed.product_id, parseFloat(ed.qty), 'Rp' + parseFloat(ed.cogs)];
          },
          ),
        ]
      }
    };

  }


  getSubtotalItems(item) {
    // if(type=='PRINT_INVOICE'){

    //   return [

    //     {text: 'Subtotal : Rp '+ parseFloat(item.subtotal), margin: [ 0, 5, 0, 0], 	fontSize: 11},
    //     {text: 'Discount : Rp '+parseFloat (item.discount),margin: [ 0, 5, 0, 0], 	fontSize: 11},
    //     {text: 'Shipping : Rp '+parseFloat (item.shipping_cost),margin: [ 0, 5, 0, 0], 	fontSize: 11},
    //     {text: 'Total : Rp '+ parseFloat(item.grand_total), margin: [ 0, 5, 0, 0], 	fontSize: 11},
    //   ];
    // }


  }



  async deletePurchaseItem(item) {



    try {
      this.spinner.show();
      let response = await this.webService.deletePurchaseItem(item.id)
      console.log(response)



    } catch (e) {
      console.log(e)
    } finally {
      this.clearSearch()
      this.ngOnInit()
    }
  }



  clearSearch() {
    this.keyword = null
    this.products = null
    this.page = null
  }


  async searchChanges(e) {
    if (!e) {
      this.products = []
    }
    if (e.length > 2) {
      this.keyword = e
      try {

        let response = await this.webService.getProduct('?search=' + e)
        console.log(response)
        this.page = 1
        this.products = response.data
      } catch (e) {
        console.log(e)
      } finally {

      }
    }

  }



  async loadMoreSearch() {
    console.log('load more search')
    this.page += 1;
    try {

      let response = await this.webService.getProduct('?search=' + this.keyword + '&page=' + this.page)
      console.log(response)

      this.products = this.products.concat(response.data)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }





  _getImage() {
    return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAACZ8AAAGYCAYAAAAnJA5pAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAdWlJREFUeNrs3buWFEm6L3hruoU90uTWttbe2tE66wlwnoAs7WgE2tEq0UbL5AmAJyCQtghIs45EIM3WyH4CosWRKrc2Wk8YGVEkSV7i4uZun/vvt5av6it4mF/MzP3vn/0pwXg16y07Xm1H63/912v/eVr/58cd/93L9bZxudr+ce1fX9zxvwMAAAAAAAAAgBD+pAkIbBMoa9f//vGN/zyaxfqfn2/8+4VDDQAAAAAAAABAbYTPiKBN3wNlOWDWpB8rl03Bplpa3v557V9fOj0AAAAAAAAAABiC8Bk1adJVyCxvUw2Z7WoTSvu8/ucyfV/SEwAAAAAAAAAAihE+Yyi5ilkOmbWr7e/rfx5plk7cDKQtkgppAAAAAAAAAAB0TPiMvuRgWZuuKprlfx5rkl4t01UI7fP6n0tNAgAAAAAAAADAIYTPKEXYrG7LJIwGAAAAAAAAAMABhM/oUg6Ynay2p0nYLJrN8pwf1/8EAAAAAAAAAIB7CZ9xiE11sxw2O1n/e+K7TN+DaB/W/x4AAAAAAAAAAOAgOWA2W23vV9u/bJPYvqy209XWOP0BAAAAAAAAANhQ+Yxt5MDZZjnNE80xaXl5znfpqiLaUnMAAAAAAAAAAAA3qXBm27YimuVWAQAAAAAAAACA1K62t6vt9yRcZdt+yyFFVfEAAAAAAAAAACbEsptkTbqqcvZs/a9hX5erbZ6ulua80BwAAAAAAAAAADBOuVKVZTVtJZflnCXLcgIAAAAAAAAAwCg0q+18tX1NwlG2fra8hOurpKoeAAAAAAAAAACE1K62t0kQyjbs9ildVdwDAAAAAAAAAAAqN0tXgR/BJ1tN29dkSU4AAAAAAAAAAKhODvTMkqU1bTGW5DxPQmgAAAAAAAAAADCoHOA5T1eBHsEmW7QQWl4WtnEZAwAAAAAAAABAf4TObGPahNAAAAAAAAAAAKAwoTObEBoAAAAAAAAAALA1oTObEBoAAAAAAAAAALA1oTPblLdX62sAAAAAAAAAAADYwWy1fU0CSLZpbzl4eZ6E0AAAAAAAAAAA4EFtEjqz2W5u+ZqYuT0AAAAAAAAAAMDPmtX2KQkZ2Wz3bV/SVUATAAAAAAAAAAAmLy8n+CoJFdlsu2zv01VgEwAAAAAAAACAnvxZE1TlNF2FaFpNATv5H+lqGc7/Y7VdrLb/T5MAAAAAAAAAAJT1J01QhePV9nb9T+Awy9X2fLUtNAUAAAAAAAAAAGNliU2bzVKcAAAAAAAAAACwk5PV9jUJCNlsJbff09VytgAAAAAAAAAAdMyym/3L1c7yEpsnmgJ6s1htL1bbhaYAAAAAAAAAAOjGnzVBr3Lg7NNqO9YU0Ktmtf2vdBW4XWgOAAAAAAAAAIDDqXzWD9XOoB65+tnzpAoaAAAAAAAAAMBBVD4rT7UzqMt/JFXQAAAAAAAAAAAOpvJZOaqdQf0W6aoK2lJTAAAAAAAAAADsRvisjDZdBc8aTQHVu1xtL1bbXFMAAAAAAAAAAGxP+Kx756vtTDNAOB/SVRW0S00BAAAAAAAAAPAw4bPuNKvt/Wo71hQQ1nK1/braLjQFAAAAAAAAAMD9/qwJOnGy2v7vZJlNiO5otf2v1fbfq+2/NAcAAAAAAAAAwN1UPjvcq9V2qhlgdCzDCQAAAAAAAABwD+Gz/eUKSZ+SZTZhzJbJMpwAAAAAAAAAALey7OZ+2tX2/yTLbMLY5ZDp/1xt/28SQAMAAAAAAAAA+IHw2e7yEpv/udr+TVPAJORr/SRdBdH+t+YAAAAAAAAAALhi2c3dvF1tM80Ak7VIV8twXmoKAAAAAAAAAGDqhM+2kysefVptx5oCJm+ZrgJoluEEAAAAAAAAACZN+OxhOXCWg2dHmgJYy5XPnq+2D5oCAAAAAAAAAJiqP2uCe52stvdJ8Az40b+ttv+52v57tf2X5gAAAAAAAAAApkjls7vNVttbzQA8YJ6uqqABAAAAAAAAAEyK8NntcuhsphmALeXlN3MA7VJTAAAAAAAAAABTIXz2o7y85qskeAbs7mK1PUkCaAAAAAAAAADARAiffZeDZ59W27GmAPaUA2jP1/8EAAAAAAAAABg14bMrgmdAV3Lls1wBTQANAAAAAAAAABg14bOrwNn71dZoCqAjAmgAAAAAAAAAwOhNPXyWg2e54tmRUwHoWA6g5SU4P2gKAAAAAAAAAGCMphw+EzwD+pADaHPNAAAAAAAAAACMzVTDZ4JnQJ8E0AAAAAAAAACA0Zli+EzwDBiCABoAAAAAAAAAMCpTC58JngFDEkADAAAAAAAAAEZjSuEzwTOgBgJoAAAAAAAAAMAoTCV8JngG1EQADQAAAAAAAAAIbwrhM8EzoEYCaAAAAAAAAABAaGMPnwmeATX7dbV90AwAAAAAAAAAQERjDp8JngG1u1xtT1bbhaYAAAAAAAAAAKIZa/gsB86+rLbGIQYqJ4AGAAAAAAAAAIQ0xvBZDp7limfHDi8QhAAaAAAAAAAAABDO2MJngmdAVMvV9ku6CqIBAAAAAAAAAFRvbOGzt6tt5rACQeXKZ7kCmgAaAAAAAAAAAFC9MYXPBM+AMVikqwAaAAAAAAAAAEDVxhI+m6Wr8BnAGMxX23PNAAAAAAAAAADU7M8j+A0nq+0/HUpgRI5X23+vtv/SFAAAAAAAAABAraJXPssBjU+r7cihBEYoVz+bawYAAAAAAAAAoEaRw2c5cPY1CZ4B43W52p6stgtNAQAAAAAAAADUJmr4LAfOcsWzY4cQGLkcQPvb+p8AAAAAAAAAANWIGj57u9pmDh8wEbny2S+aAQAAAAAAAACoyZ8D7vPpavu/HDpgQv5jtTWr7aOmAAAAAAAAAABqES181q62/3TYgAnKywz/M11VQQMAAAAAAAAAGFykZTePVtvX9T8BpiovvymABgAAAAAAAAAMLlL47Eu6qvwDMGXLdBVAu9QUAAAAAAAAAMCQHgXZz1dJ8Awga1bbe80AAAAAAAAAAAztzwH28WS1vXaoAP7QpKvKlQtNAQAAAAAAAAAMpfZlN5t0tdzmkUMF8JMnSQANAAAAAAAAABhI7eGzHDyz3CbA7S5X29/W/wQAAAAAAAAA6NWjivftPAmeAdwnV4V8qxkAAAAAAAAAgCH8udL9apNABcA2/sdq++/V9l+aAgAAAAAAAADoU43LbuZKPnm5zcbhAdhKXnbzyWq70BQAAAAAAAAAQF9qDJ+9X20nDg3ATnLw7BfNAAAAAAAAAAD0pbZlN3Po7NxhAdjZf6SrQPFCUwAAAAAAAAAAfaip8llebvPr+p8A7CdXP7P8JgAAAAAAAABQXE3hM8ttAhzO8psAAAAAAAAAQC9qWXbTcpsA3bD8JgAAAAAAAADQixoqn1luE6B7lt8EAAAAAAAAAIqqIXxmuU2A7ll+EwAAAAAAAAAoauhlNy23CVCG5TcBAAAAAAAAgKKGrHyWl9n8stoahwGgmL+ttqVmAAAAAAAAAAC69mjAv/ssCZ4BlPZWEwAAAAAAAAAAJQy17ObxaptrfoDimtX2z9V2oSkAAAAAAAAAgC4NtexmXm7zWPMD9OIyXS2/eakpAAAAAAAAAICuDFH57HS1zTQ9QG/+bb39b00BAAAAAAAAAHSl78pnR6vt6/qfAPTrl2T5TQAAAAAAAACgI496/vvOkuAZwFBeaQIAAAAAAAAAoCt9Vj47Xm1fNDnAoH5dbR80AwAAAAAAAABwqD7DZ59WW6vJAQa1XG1/0wwAAAAAAAAAwKH+3NPf0662c80NMLi89HEOHi80BQAAAAAAAABwiL4qn31dbY3mBqjCZbqqfnapKQAAAAAAAACAffVR+Wy23gCow7+tt/+tKQAAAAAAAACAfZWufJaXd/uSVD0DqFGufrbUDAAAAEBBx+nqOXHNLpIK8QAAjM/Rejxes8v1eBwIrHT47Hy1nWlmgCrNV9tzzRDayWp775wbjUjjpl9X2weHzDVp/oC5LwPLD1B/D7Cfr1fbC4fL+I0iFjf+/XK1/fPav17e8b+jX59WW1v5Pj4Jfp7s28bm6dP2rwD7GP3aZBxjKR9xE1GbfgwdPb7x3x067s5yWOi/bxl7C/XXdy58CjCve+JQjdJstb3V10/DXwr+2blD+00TA1Td4b/UmYa2CDjJ4W5PA+1rflghfBb7GGYfHTIguNNg424P32H4OcZifS3+I31/QbbQjExY7qPyS+kn+ilgAM8C3SvPHS4qdbQeE+f+PD+zbVKZFcnaPcbim2pWxt8wXWeB9tNHOQcqGT47TfWXUweYulfpqoIRMV2uJ2ptkP1t1pNg5ZNvf0hwHGh/W4dsFO2ycMiA4KK8rMr9fK6OOXfIoJrx2smN//xivf1j/U/jJKYkz0VzNQwBNKBPs1QmIFNCLrRx7pBR2fzy8Xps21S+r3eNv5fr7fO18bdxCIxv/h2lr8/3qBfuQ4cpFT5T9QwgTmfaJg/WI/ucYgVe2iR8dte1GMnxetKwdOj+0ASaSKXkgQ4wjr4z0n03PyOZO2xQ9fj2+JbxUp5vfTCHYSLXgAAa0KdngfY1v/OcGc8zcD/drq+b45H8pma9tdf+s+W1MfgiefYM0UXKC236+tcO2/4eFfpzVT0DiONME4QWbenDxw7ZaNqlddh+EC1A+NkhA4KL9sHbsb4TQo5383z5y2r7fbW9TVcPoz3zZKw2ATTnOGBsHH/+QXxNuqq493U9Hn2VxhM8u+83z9bj7q/rbQq/G8Z6PUd7Z6KvP1CJ8JmqZwCxtMmLsMjyF/iXwc43fnYScJ8FCWO3xweHDAisCTqmeObQQVibr6Dzi7AcRHsfdAwPD8kvd78mL3mBsiK+Qzx2b6THMeendX98lmJV/C4x988FbzYfgwiigb6+9D3HPP8AJcJnqp4BGATQr0ghkjxGaB2yHxwHHTsZhP8o0nmdA6uWjgIii1q5d5am/eIAxiSPhXMAbfMSzLXNmOT56afk5S5Q7h4zC7rvnqFTSrMeU+bAWf7YodUkt947NkG0L0lFYtDXl+HD0QM8KnAiGXwBxHOSPCyPLNryeSbPP19/UScQXkZ8P6cjPexQ9QyI7CjFDkDPHEIY3T0pvwTLLwo/meswsnNbAA0o4TT4WF7YhS7lseP79VhScZft5fHJZmlOH4JAfU4C38+8Lz9A1+EzHSNAXGeaIKxoQZKnDtkPIi9f2Tp8Idvhs0MGBDZLsZ87+GAPxj0m/JSE0BgPATSghOgVRU4dQjoeN1pd4rCxyuZDkBxGazQJVCH6+2bP7vbUZfhM1TOA2GYG52FFW0Iv6jKTpSbIbeD9f+wQhmwHlc+AyKI/d4i89ACwnTy+F0JjTHPWL/ouoCNjqCZiOS6ME+uUxypCaFDHfS76NThL3mHupcvw2YmDABCe6mdxfQw4ACV+O/gyL16AMAdVLx02IHC/04zgd/hwD6Yz1s8vFt8mz0yJL5/HM80AGAd/m4+4H7Kr4yR01pd8fX41BofBjCGkne8d3n3tocvwmcACwDgG5gbkMVl6M6YxtEM78WMY7fd/dNkBgY0ltHWcLGEGU5tn5xdglukiOgE04BBNGs8zJNXP2NbRuv/8kjxDHWoMfq4poNe+fizzBdmnPXQVPpslJSwBxsID8ZiiVTMy2R5PO0w9SGjJTYB+NCMbP6h+BtOSXzy+SlfVLnzwRWQCaMC+xvQSN89LfEzCQ87TVfhJvznsGPxsfRxazQHFjel+17hv7K6r8JmUP8B4eBEWV6RQSZME149H0gZTH4BHKr+cA6oXbpWAMWoVZkkABaYoj529/CK6HEB7pRmAHYxx+SrP0HlovHdmzleNJl19BPI+eScB+kZ9fTFdhM/a5IEJwNgeBsw0Q0ifg+3v1NdMH8v46ThN90FKk2I9sFD1DDA+rYuKwzDde1p++XWuKQjeh73VDMCWZml8z45OkmARP4/xNpVuG81R7XX7xVwc9PU73DPcz3fQRfhM1TOA8ZHmjilasOTxxI/XmH7/VIOEbbD9/ZwA4vYzY3yx43kKTFuuhiG8Q2Qz5zCwpTE+a/YBNzfnrEJNca7dTUhQgBT09dvMedjSoeGzRoMDjFKuZNRqhnCiLak39XNsTIGtqQYJnwbbX5XPgKjORvq7muSZCkxdvgfkF5VefBH5HBZAA+4z5qohPuBmE2SynGM8bbpaHvVEU0An19Oxvp5Dw2czTQgwWioxxPQx2OS8nehxGtukdqrHMdLvzsHUS7dIIOi9tjHmBkYsP6RXeYHIZs5hYKLj3TxPEVwxhlPtLK48dsnBwVfGMaCvv+c+MXOIt/PIiQTAHWbJ1zoRRats1E70OI2tUli+VxxP7Bgep1gPJRZuj0BQY//CsJ1gHwrcPrYU3iF6f+YcBm5q0vjDWSqiTNNs3e+Zy43D6fp4NpoCdjaFcJa+fkuHhM/GXCoXgO+TKGKJVt3o6USP0xgfvLUTO4bRfu/HBBBPk6ZRScBDLCDLLy8tX0j0c1gADZjaOLdN3pVOzdv1pr8b3zjmS1LNEHY1heqPx2m6hTR2ckj4TNUzgPFzr48pUvWzaNWjutCkcT6Uejyx4xgpOJkDqQu3RiCgqYSyTpIXF8D3+4EAGpEJoAEbU1qmysck0zmnPyUf7I/9GOdlOM81BWztmd/Jxr7hsyZJ/gJMgft9TJ+D7W87seMz1mtqascx0u9duC0CQc0m8jun9GIO2O7e555AZJvKIZYig2mb0gcWsyR0O4U5Ww6etZpiEs6S6nawbf/X6OvZeHRA4wIwDU81QTiLYPs7tYpZY/29eeDdTuQYRgsQWnITiGiWpvVQR7UE4LpXSXCH2Jp09ZLeeQzTdTah35rnLT7gHvfx1adNzyyp5goPmVo1sFOH/H6PnEgAbDHINsCOZbnaLgLt79QezrR+W3jRAoQLt0UgoKmFsZrkhRXwXZ6DW36TMZzHXtbDNLVpOpVQNs4cdn0Zo3Ps+MO910c7sd8sI/WAfcJnUxw0AkzdTBOEswi0r82ExhZ5HDXmMOfTCR3HKHIQdemWCAS8z07x4a7qZ8B1+T54rhkIzkt7mKYpjmubZElGfRhjHZM7D0Bfv+nrZw793fYJn0n0AUyPe3880ZbZaydyXMYezsqT8LFXSmxSrIcNC7dDwNgz1HiocfiBa86SF13Et3l5r8InTEMz4evdxyTj67uMw3A+wO3XxGyiv9378ns82uNEMkkEmJ48qG40QyiL1XYZaH9VzPIb/b4yPiaAWPKYczbh32+5HuCmV5qAEcjvFd4nlQJgCqYcwDpJnqGPRV7+XNCI65bJ6hKwMeUxfat/uNuu4bOTNP5qFgDczpdb8SyCDdjGrpnIoHTsQcLHgfb1Mql8BsQzm/jv99wFuG2u1GoGRuKtvh6M50fOM/T4XiWFWPjRfLU9SbE+9gd9nd/fu13DZ081GcBkmXDFE6niUX7JOvZgVjuR8671+6qxcBsEApr6AxwV54HbvNUEjOx8nmkGGKVZ8iGF+1tseS52qhm4Zr7anifBM7h+n2z09T4cvc0u4TMPQAGmLQ8mlBKNZRFw0DpmUwnxNyOefERbgtiSm0A0s+ThTWbpTeC2MfZMMzAiOYB2rhlgdFQCuZrP6LPjjrcE/rlunq6CZ4C+/iZ9/S12CZ8JngHwTBOEslxtF4H29/HIj0c7oXOv9buqsHAbBIw1Q2qSJfYA90jGL4etveSH8cjjVx8uX/FiPqbcJ/kYio15EjyDm5rkeZW+/h67hM8suQmAIHI8i0D72o54gn+cpvXwYqzjxkgByeV6A4jUV7aa4Q8eYgG3zZfcJxmbWRJAg7EQkja3iezUMeOaF0nwDG6jUv93TfLO/Cfbhs8suQnApjP1BVss0ZbdG+skf2rjKMdxeB/c/oBghK1+7nMazQDc4MU+YzRLAmgQXZMsP6XPjiu/AxeoYCOHzl5rBrj1Xikv9CPPMm/YNnzmRALAxDmmxWq7DLS/Y116c2oVZPNEZGxB1TbY/n52+wOC9RszzfATD7GAm2bJclCM99x+7/yG0NcwP7dJoxlCeKX/YS0Hz+aaAcxFt9Tq63+0bfjMkpsAbAgkx7Nwfg1qjEGsKR7LaONhlc+ASE41wa1mmgAwJ2di5/an5KUWROSjCeP5qBrHiTXBM9DX70PlzGu2CZ8poQfAzQmZpTdj+Rjs/GpG1v5THUeNrYpdG2hfBc+AaFTWvZ2KcMBtfCTMmOXnTQJoEMvMNXsnL+rrJzRAXjXm1yR4BvdpkwpfdzkxDvruL1ueTABwszO90AxhLAIOZMc02Xs80fOuXQ+6L0fwW6JVr7PkZln5/vROM0BnZskDrPv8ljwE5zB53vZiQuPPjb9fG8ON7UHwyYjG2XCbTQDtifMcQvAhyd02H5MYz9epST72Waz/+fnG/OFyy/bbzOX/eu3fR5rfX67HG951wf2EqR/u619riu3CZ76mA+C2vuFcM4SxXE+gjgOdX/MRtX874XMv//YxVOGKVr1O5bOy/pnihXqhZl5W3e943Z+677CvywmdP3f9zqNr19LTNI5K3mMZZ8N9/V8OoOVKJEvNAdWPVbl/vjPXDFWaUtWz5Xqs/I909Z5g24DZIWPVfH/4+/qfNY6/Bc9gO02ySuJDcjhP+Cxtt+ymkwmAm46TChXRLALta+taGY3HfkfvlsnLGSCOJnlZtQ0BPTjMJoB3vtp+WW3/vtqep9gvmh47rExAnk9/SeMIjMJYqYTysNZ9rEr544Sxv//+sB7z/m295X/9ej0uLl1ZdLH+u57fGH/PUx1VTQXPQF/fpSZ5vvnNQ+GzMZamB6C7iTNxRFqGL9oSh66T8f/+SL9DBQwgkjNNsJVZ8uEHdCm/bJqnqxdh+aXTwvgUqn4+8CkJbkCt1+dMM2zFi/v6bJYxH5vlanuRrsJev67HvMuKxt/Pr+3bUM8wL5LgGejr9fVFPBQ+U/UMgLtYljmWaIGU1nUyCmOo/NYE+w2fE0AMU/jSvEszTQBFLNLVy6e8LYONs30wzJTGDAJoUJ9TTbDTWF6/XZexhQTyOHZT5SxXHLusfH/zu4ocQMtBtJc9jsMFz2A3Yw3qlmqrZuqN8FD4TLAAgLu0miCcSAG0MYxBjlwno7hXnLjOAYqYJQ+wduELSihrka4qoc0D7bMgDlOyCaCZY0M9LA2/G2G9ejQjGkflkFmudPa3YOPY6/t/nr4vC7os+HdtgmeXLgHYmhULdjP5Z3ePHpjQeYgBgH5iPCJVRGpH0N6tU+6bx/a/N4JnQCTCVLuPvWeaAYrKL6LyS6/X5htQbV/4SX8IVcjXYaMZdiKsV4+xVODOYapfAo1dHzJPVyG0l6n7gJjgGew339TX7z4+mvSHto8eOKEAYAoTtan44PzqlQqy4ziOkcbEltwEIvUNjWbYmRdW0I9cPSLCS7y/OlRM1NskgAbGpfE07l3VeDyC3zBPV8Gz5QiPz3nqtpLbIgmewT58NLq7HDyb9HvzRyPvfAEwUeO7ZbAJafTzq3XK/THgPg58DCN9qbJwugFBeIC1f7+k8jD0IwfQav94p3GYmDABNBi2/2k1w16E9uoQPRgwT1fVesdsU5H40NDYPAmewb59veIj+5n0UqUqnwFwCH1FPB+cX70NzhunW/hjGWm/l+mqhDxAhD7SGHJ/gnvQn/zCq+YXVe6lTF0OoJ1qBujdmSY4qO9uNMOgon/Ms0jjD57d/L25Cto+7zTmE2sr6NJME+ytmfJc/a7wWeQKFQD0P2kmjkjL8h2nuA9kfBXyo6f2u7iF0wwIwsuqw8xSrKqcEFkOnr3QDFC1V+kqhAb0Y/LLSZkPhdcGH5v+OtExef7dL3f4/8yT4BkcwoePh5lspdNHI+x8ATBh424fnF+9sCRt/OMY7WOMj04zIMi91cuqw6nyAv2Zp6sKs8bZUK9ZEkCDPq83H0Ic5kQbDurvgfc9fxQx5eUjz9N2lYlfJ8Ez0NcP34bNFH/4XeEzL0wB2JY+I54Pzq/ivFj/WWt/i1o4xYAg/aMHWId7pgmgVy81AVRvlgTQoA8qoRzuKFnObEhN0P1epKuPIqYut8GTdHcALYfOVC4GfX0t85PJUfkMgEPpM+KJtPRmxBCX4Nntoi29GSn4uEjT/vIRiMMSM91okhdW0KeaP94R6IXvct8ogAblnKSJVvEowIv94bRB99vHEN9dpNsDaDl4Ntc8cPA98lgz6Ov3dVv47MhJBcBEJm1TFanyWcRxiWqA47hPRAoRWnITiNIPNJqhM6qfQX8uK55DeYYLP5qtti9JMBNKEJjqTp4X+XiVbS2SFQ9uuhlAEzyDbnjW1J1JVjq9LXzmoQUAu9J3xLJcb1G0wdrXw6O77xNRXgA0KVZAYuH0AgLwsqr78VGjGaA3/9AEEGru+SkJoEGXmuTjY/OjcfQPEb1z6G6VA2i/JsEz6LKvn2kGff0hbgufGUACsCuVnuKJVP0s0nKNTarrRfCisvaJEsyLNB5epquHLQC194+RwtnzFCOobxlTmO64GrifABp0K9rL0whLFLbJxyR9i9gnXCbBqofG6NoHujELtr+vg8xJ2imdRLeFz/7u2gJgjw6UWD4H2tfWvu4lB5Jq+zIuSlA1UuBx4XYGBBDxZVWEr8tzoM9LdejHUhNAOAJo0I1oy0YtVtt5+r4cn3kSkS00AdCTSEtu5ndfL4LM0ye1lKnKZwB0oUke5kXzIdj+RqmWUlNo6WOFxznKODPSePij2xlQuYgvq5YpxtfT0doWIltqAggpB9C+Jh9NwiGiffDwZv3PCOP5WfJMnftZ+h3oqz9qAvb174K07WT6+pvhs8ZAB4A9eZAXT6QAWpSKWW1lxzd/5VnTkoxNgHvFcbDx8MKtDKhctJdVmwdXyyBjJdUSAOB+eRzyKXluBfuKtNT79TH8myD3pxOnWG8i9gMLhw3oQaTqXNeXI34dZJ9Pp3Ii/WUEHe/eXr16lY6Pu/3Jy+UyPX/+vIrfd3Jykn77rdvn0BcXF+nFixfDjxBXxy0fv67l35Z/467atk2fPn3qdF/+9Kc/jfK6e//+fTo66u7d0+fPn9P5+blhQR1ak6Fw8tKbJ4HOrwj7WMvL9euhs4+VjfHaVFcg7qchVKBr+CLFWEYCmLZI4ahl+rFCwpsA/UKz3scPTjUAuNMmgPak8vko1KZNMSuhbMb2i1T/M8WzFKNK21j6AgB+dJxirQRzva/fBNFmle9zDvedT/Hkyj/6X1PZTk9P/1VC0zRV/L63b992/tt+//33Kn7b+fl5kWN3dHS01/60bdv5vozxmivRToccN1vn23tjtJCDykjnWO0Pu15V1FZvKz7Otd8rPgW6JsY4YTnX7jAqbbCxxm3X99cA+/3Jqabfcn70wrhk3GPsNvj5GWkeM+T2e1IBrfb72tiuzejeB7u+b4aLTpznBBu7OzeY6rMZ89bhvE2x3xNGefY4m8LJdHPZzcdTupIWi0WZO3hbx1ggVz7rWq5Y1XW1uH08ftz9qZornl1eKh5SUteV+DZOT081bh08vIsnf+27DDZJsn/b+XzjONfUwZ04jp1R5Qao3bNg+3tbuf4Iy/XkvqtxukFRKmXAeK7lL2kiL3/gQHl8Gak6fH5GcnnLf7YMsO+/Od16sdQEAD+NjSONi+e33MsXQe7vz6ZwQj26ZTA5GTlslJfJ7FqJYNSuckCsy6UNrysRatvpLrj6XSUCfh8/ftTFlJypNk2xc+fZs2cauJLDrAlCWgTa15pD8rnTrSmA+eGBfz+0ttLjGOmh6vWlVQFqHRvOAu3vPN0e1p4H2X8vrKAsH1vBuLxNAmgwtvHlyzv+83cB9v0kebbeh6UxKMAPolV3ebfjGKAm7RTu6dfDZ0dTHNyUqH42dDjr29lbsPra0OG6Ur/twwfFQ4rOVH8rN1fNwbbZbKaR6+k8ieWj86ub7r+ifbmt0tlnx3K7YU6g68HABahdtAH6XRXOcp86D9LeKjNBOY0mgNERQIO7RauEskh3B4vmQX6D+xG3eawJgIIiVXdZpruLadxW/bRGo/9w9Hr4bJLp6RLVrmpYmvLp06fF/uwc/ipVVW2rkVaB8FtebjNXwqPQTHV1vpQOh6l+Vo1GE4SzCHZ+1TpeqelBwLs7Bt81eVrpcWwDXQ+fE0DdIj1QyeOh+yaEEZbezJP0E6cdTGK8f92lQwMHEUCD252kWB823FfdbJliBNBUMmYM1yIQ6/7SBNrflw/Miz8EafNR39MnHz4rUfksK1l57CGllqWs5feVqCyn6ln5Y1Y6sJjPyVwBjcE5CPHkQdki0P62le5XTS97F3cc55pS1scVDrKbYONhgxegZrM0npdVad2HRvha6cypB5Obh/iSEg6XA2ivNAP8IFIQapkeDpdFWHozWrU5+nOqCYCJ9/XbhMsiLL05+r7+5rKbk5MrXpUIoJWsPPaQPoJhQy29mcNFJQJGnz8rHlK09/qtn/7r7My7lgooAx3TR+fYQWoKUi3T3S+ganvQ1tqfvd22tCpAVVOASI8F0naVECJUP2uC9WcQxXHyoRWMXX6x/1YzwDdtivVx3jbPuxbp7mU5zaOmYxF4fm0sCnSpSbGeH83Tw+9DlkHu86Pu66+HzyYbGCix9OaQlcH6CL6VqD42ZLuqfFb2mPW1DG0fFdbYasCCiXfRS90+7X0sazvOtS29GWksHCkwCkzPcYr1smrbUNk8xQj+emEF3XtW8b4tHR7ozCwJoEHt/d5tXm/5v4tQESXPo1qnIDcc6Z+AjkWr5rLts7sIlU6bVOd7zk48uvFDJ6nU0ptjC2j9cFUUqkD2kBLBuouLi28V8Cg0U33W31w1B89ms5lGH77TJJ5cRWkZaH/byvanphDVx0DHubbjGGnALzUP1Cxa+Gle6H87ZH9mTA7dqX1ZjKVDBJ3K17sX/ExZk2ItB5XH59u+3PmQYnxM8sxpWFTUJctb/RPQ4Rw32ruQbee9u4wLhjTaD0eFz9JV+Gi5XHb+5w6xNGWfobAhqruV+DtLVL7j+/nYdxisryU+eXAiRDyLQPtaU9grD9SPAx3Hmo5zU9H4s6alUx+SJ09RH5QB41d7SOOmedotuPEmyO+aORWhM6cVjxOXDg8U60c/BZojwpTHkbuMz/PzlA9BjkHjVCzmMvj1KYAGdHEviTTOfVP4fz+Edqx9/SZ8djz1q6xE9bMhwll9VlvrO1yXl24ssaSiJTcL9l4DVCHLgbehqg7yBw/nYoqUxG3ty+3DmfTwA5TajvOJ47j70MXtCqjYabD93bUc/zLFCOz7Ige6m9vWfD0tHSIoOkcUQGOKIo0j87h814/zfEzC5+D7n8+Nt/onYCJ9/TLt/hxuHuS3nY3x5NqEzybfSZWofpXDUn0vTdlnIKzvgE+JMF9ebjNXvqNQ7zVQFTLVzwZ3rAlCWgQ7x2oZu0RZcrPW4/zYcdxZ9IdkwLhFWiLmYs9+McILq2gV6KBWtb/Y80ALysrPHgTQmJJZsPP93R7/n33nAOZV47EcybX6JVmBBthdDnc0gfb35Z73+Q9BjsXo5hmb8NnkO6gSlc++NWzP1c/6DITlKmQ5YNeXp0+7fy+s6lnB0e9sVqRS3bbXXd/BT37wf2qCkHLFrEWg/VUx65bhTMDj3NqPIscZYJApQIr1AGvfEFmeRC4D/D5f5MBhziuac9zlnw4TFCeAxpREq4Qy3/P/+y7A72uSj0lKGUt4v1n3T2+TZVqB7UUKNx+yXHaUD0dHt5TbI9fY+uy9vCwSQBtzJbKsz3Bdib/r82fFQ4rNVAeuPqb62aBUPosr0tKbjys512uZ3C/T9g9PajrORxXcM9pA5/1FsrwSUK9oD7DmB/z/I7ywOk4+NIR9zVKMJTBUPoP++tRPyfMuxn+eRzrHDxmPz9fzAfOraRrb+CmPW78mITTgYfkeESns9OGA/nqRYrxHGd3Sm5vw2WPXW5mlN/sMZ/UZdNsoUY3sNqWCdaUq3k1+pnp83GtVvFtH3ANWXsPXoIFFuim29uGngXjU4zz0hOepaxTg8ClAihV0mh/4/38d5Hd6YQW7O09XL++MDYGbYx0BNMYs2pfch47nI1REad1zjKF2MEvfQ2itQwyMoK9/eeD/P0Jf34ztnq3y2fXRRoEgUl76r68Qzpgrn5UI1l1cXKTlcunEL9F7VVB1LAfPhrgm+MakOK5IVZWaCs61mkJLnwMf56E/gog0uI9UnRCY2BQg2P4e+gDq0MppfZklX7/DtvJHVO9TnC+PFw4ZDHKfEEBjrOf2LND+5nH4soM/wzxrusa8JNJs3VflINq5+SAQtK9f6Otj2oTPTJjSVRgpL7/ZtT4CWjnklrch9BHwKdGGqp4V6r2Ojr5VHavB2dmZAwL7DeqiaAf8u49S3Mpn+/zvSx/HowGPY5Rx8GXykhGodAqQ4pXtX3bw57wL8ntnTlF40Gm6ekEX6V5myU0iuhzBbxBAY6z9YCRdjMOXqa5nY/eN5a0yUmZOOHZNuvqoIo9xv6y2V/oumLSTYP1JF1XLonw4epJGFBR+dG3SRB5xfOh+zNHHcph9Lu/Z9+8rVT2uxDKrrGaqp/XMVfO5M+S1MXEaPq5IX349do5fDV9GcJyHas9ILxgXbk9ApWZpeg+wNvflZYDfa+lNuF2+b21CZ69SvGejnx1CAsp98Hwk949PweaTMJbx4kXq7vlIlI9JTp2iRc6j5YR+7/H6PMohtN/TVbXfWRJGgymJVK0l35+7Cuy8CfKbR1P9LIfPGtfbd58/d//spo/KYE+fDrfyV+lwT4k/P1e4U/ms0Ez1WV1z1RqWAIVgIn35NeSD3scVtcPnERznx47jg6TmgVpFGnAvU7dh3pcBfnOTVD+Dm9dDfuGWX7y9SnGfi35wOAnqeRpPAO29PpYRmAXrC7t8idxVReTSfExiLNV1/5Wfqb9NP4bRztOwq1MA5bQT7usvUoyq4bOx3H+Fz26ONj6UGW9EDGhtK1clK7nkZ4nKaoJnhe6Ms9lgy7/eJYc/a9unidDocV2mWEu4DNUB1vSF84ee/39jas820Llu8ALUKFpp+K7DYh9SjCXEvLBiao7W47x8jzpPVy/Uvq63tyl+tSLBM6IbSwAtre8pM4eUwCKNE0ssnxWhIkqTVFos4Z0m+GPcnM+vXBUpV/X8fT1m3gTS8n+nQhrEFumj0an29Udj6esfud5unNGXl+niovt37iUrk+Xg2dHRsGHIkuG3En+2JTcLzVSf1TlXVf1ssEkxcUW6SbYDnd+1nOPLtP9XmjUt1TNEmx4HuldNbTmA2uUHcv+yHbydO5VGIdoDrA8B/sxS4yUvDKarnWAfk1+a5Zdn79f9drSg7Jjma3AXATQY3nGK9VFeiZfHUe5DXnB0L0o1nCE06XsgLY+nv6zH2F/WY+zc752v+758D1EtDeq/nqMo8ZFnlA9HR9HXP0oeQP6kRDCpZDhryCU3N0pUJ/s2+ylUVU3lszLHasgKfPfJFdmAnQdjUQzRCZ6M5FjVdpzbkf99Bw1d3JaACjXB7qXzVOZh05sgv98LKzBfg9o8X29jsHkRD5FEGx/OC/yZlylGAK1NPvYu4Y0m2MkmsDpLV8G03PdtqqVtPv54tf7vZQ9AX1/LfTlKXx/to4Bb5fCZRPINJZbezMGcUtXJagj8lNqHEn9urmy3XC6d6F33XhVXF8vXngBa7/6uCUK7SDG+BNgMyPoeyzyu6PcfUr1smeqqpvV4wsfxIapbADU6C7a/pV4s5HHTIsDvnyXPf2AM5oHmarDtOT2WANrmRTxEEG15p/zSblnoz46y/OKZ09a4qvJ7SrvaTtd94aZaWg6kna/vN+aj0L9ZoH1dpHIVKaOEjZ9FP+Esu3mLHE7Ky2927eSk+7F8rgqWg21DK7UfJSqqqXpWYFR5dFTk/O6SpTcHmWwQW6Sv6due/75abnhdLPVV03E+Gfl5c8hxNngBahzreVn1XZQXVqdOXQjPRwmM0TyNJ4A2SwJoxDlXIz2/LfnSeJFiLL8ovBPv3OLq+etm+c5cHS2H0s6Tymigr/9ZyWdryxTnw9Em8kmXw2d/de39rET1sxJBqpqWOSyxLyUCTSWWVZ187zWbdV7Zr+sAaM3LgkKlPgfa1z6X3qzpRrIY2XHOHclxj8cxysRrkQAqnAIkD7Cum6cYX8w/c+pCaMtkyU3GK/elAmjQn0hfauf+b1H474gQQDpKsSrYRPE6qX7Wp/zsN4fRcgjt93V/KVgJ+vplKr80ZpSwcei+PofPGtfezz5/7v5dbIngSxeBtvl83knQp+twXamgkMpnBXqvAlXFcgC062P17Jl3LT1PholN5bPbPa3od38c4XFuR3jOHDwsdTsCapwCBNrXZU/9XYSHWE2KVbEO+NFLTcDIzVfbL2kcIYBZunqh7vkYNTpJsd4L9tH/fQhy77G8S/fycX+hGQaxCVRuqqK9N1+FzrQpVoXBPlYUKL0qgr4+WXbz7rOvQOWzEktTdlEZLAftugj5dF2l7OnTpyGO6+Rnqqvjns/tzmeUL1+md++67WtyhbYS+8qtlE0ex8T7Isi+Nqm/h2ZtRb970dGfU1Pn+HRkf8/Yjg/Api+MNKjuKxQ2D9IeXlhB3PmZcSFTkJ9DPEnjCaB9SgJoGA9G6P8ug4zn8zxMOKfMXG6hGQaXz+1NEC0HuL1jgv1Fq8byuqe/512Atghd6VT47K6R5uVlurjo/p17l9W8cpCti6UOcyCrq6Uou/x9JSqflahoN/mZaoGqZzkMuVwuO6vKd10OoAFbi7ROcR8PXpqKJr15kLIc4XFue/g7jlKchxfLFONrHGBiU4BIU/vU30ukfL+OEAxpkwr4ENGbZEkopmNMAbQ89xRAoyZNilUNft7jvSDKclyWdylD9bN6bIIXeWnOr+t/rR+F3fr6mb7+zr8rgrAfjj5yw75bV4Gs67pcmrKLSmM5YJfDPV0tb9hVtbIcquu6Slym8lnHvVfTFAkJvnnz5tZ/3cnd+jcf+sMut81A+/q4h7+jrej3Lir9szoZ4kzoOI7pGgQmMgVIsb6073v5nHdB2uXMqQyhLFfbuWZgYgTQwDgw6zMQlvvbRYA2ibZsaqR+RwCtPvlcz1XQvq7/6dyHh8309ff29fMg84eQ1R8fJWUr71QiqNTl0pRdBL02AbtcZaqLSm9dBZG6XsJz8xvzRocz1bOzIsfp+rWXq591KQcbVT+DnSbdUR70tj38HTUt1dhlQn6Z6lpi9fGEjuNDlGwFahPtS46XfT9GSDEqVuYJtxfg4F4Gtcvz1F8qm6/uaxNAaxxWBnSU4n1I0vfYOsrHJL6wLyMv+7bQDNXev2bpKoSWl+ZsNQmMoo+4GGCsr68vyLKb953t66pgXesioNVVZbDrIZ8uqp91tRRolxXibvutpE7OwRIhwXfvfrzn3wyjdeHZM5Wpe9JoglGIcvM86mHSWcukNg9OFh3/mYuKjuVUjuOYrj9gIlOAFOvrydy3LQf4e984lkDH97K5ZmDCcl+eK6CNJYD2JSkGwHCifYAwxMvheYrxMcks+ZiklF9H0ueM/V72ab21mgNC9w9DPENb6OvLET57QInAUhfhsy5CPzlYd73aWVfLjHbx+0os5fj5s+IhnY7uVudgF0HDm16/fv1zz9Px0pv5/CqxrCs/aTTBKES6ebaF/+xaBnqLAn/mx4qO43HB+0cT6N4keAZUNwVIHmBtYx6kfVRLgBieawL49gHWWAJoeSz1KQmgMYxIS24u03DPRSJURIlWxS5an/M8jWPZ57FrkxAa3BSp+kq+z84H+rvfBGmj02gnoPDZA0oElrpYLrOLymA3g3W58lkXld4O/X1N03zbuqbyWccz1QJLbuYlNm87B/O52fWSqb/95l0LbHv7DLSvJZdTrGkCWyIotkh1PVRpJ3AcHxyGuv0AtU0BAu3rcsAxzJAPz3aaeicvrKB2L1KML7Khr/5VAA3216ZYHwoP+VJ4HqSNzpzWxVys+xwBtDj3t9yvvk8KIjBtxynW+4+h+/oI9/hwS7kJnz2gRGCpi6Upu6gMdlulsy6W3jx030os5Sh41vFIbnWMSwQE76tw1nX1s9lsVqRyG4zQZYrzcDcPrktd2E8r+p2lOrVFRb/x8QSO41DHGWCvKUCK9RD33cT//m09c2pDtfLY/LVmgB9sAmjzEfyWTQCtdVjpyW/BrvUhr/NlivMxiXtIOQJo8eSXy19X26tkWVr09REM2dfme3uE9y+5r59FOqjCZw+deTeWpuzKIQGtHF7rIvhzW9Csi6U3D61c1kVVt5ssudlx71Wgalg+H++71nJVtK6dnp46mLCdj4H2tS3wZ+bJai1fJF+kcg89Po78OJb8c7u2TKpcAHWJFlIaOrCxSDHC+/nhfOP0hurk8f6vmgHuvD7ycmjzEfyWTQBt5rBSWJNiVbz9kIYP/ET5mMTyLmUJoMWUX/x91b8yMUfBzvk8ll8OvA9Rlt4M9UxW+GwLXVQDu+mQpSm7qHp21xKbXVUIO6R6WRe/r9Tv4ipcWKI63bt398/n8vnadQDt2TMf+sO23UagfS1R2aqmB3QfJ3Kcm9R94K9kZbzOhy5uO0Bl9+RZoP3Nk4YaXg5EeYjlhRXUx0tOeNhYAmjZ2+QFOcZ7172sYB8WKcZHgT4mKS8H0H5J41j2eUqO1v2rZa6ZimjVVmoIeV+kGO8+20j3MeGzLXRRDeyns+SAgNUhwbWHflNXld72rV6W26XrpRCXy+W3jY5mqgWqnuXjs02wrOulN0sF6WCE8gAsysuPtsCf+bii31cylLRMdT1I6fpYRrrhK9kK1GQWbH9rCX3VULFh2+NrSRKox/Pk5Sbscr3MR/JbBNAoJVollEWqJ/QV5WMS947y8jk5lmWfp6ZdbV9W27mmYOQiVVupKfSl0mnHhM+2Ge3eUSXsEPsuTZmDWV1VPrtLF2G7ffdR1bPKZ6qr8282634u81DVsz96o4uLzpfBLRGmg7F2h0H2s0ndf/FXS2jpMpV/EVXT0puPK//zSjJ4AWoSacC8SPWENnK/PY8wzUuxAtowZmMK0kCf182LkfwWATRKOEmxPjSoKfA1TzE+JvGCo7/5Xe5zfk0q1EZ0lq5CaKqgMUZ5/Njo60fd14cZzwmfbanE0pv7VFzqIpyVq0zdF+DpIqy1b0iui6puN33+rHhIl+ds15Xpvt3Zd1hOs+vqZ/k83ScIChP0MdC+dvkCtaalGj+M5O8Y4jjmY9gGOX8XCaAesxTrZVVtXyxGqZZw5lSHwQmewf5er6+hMXi73mCK47xlquu51GWK8XFgtOp20eVz4m/Jh6MR5ef8n1wvjFCkqmc19q0R5uFh+nrhsy2VWHpzn6Up913O8rqHgnQ5mNZFpbddw2c51HR83H3ovERwcLIz1bPu56o5eLbLsqj5f991JcISvwtGKNLNtMsKVzVVAukjTX2R6vrSo63sz+ll2Ol2A1Qk0lf0eVIxr3CfIoyhmmB9JYyN4Bkcbp7GE0CbJQE0utGmWJVQalz6KsrHJKqf9Ss/O80V0J6kepaJZTtHSdCbcWlSrOc581RfpTF9fYeEz7ZUIsC0T2Wwfaql3bRNkK6L6me7BuVKLLlZYsnUyc5UC1UI23bJzR96ph0qpW17XZWo6AYjkyfSF0H2tcsOpaalGj+M7O/ZxtMRHscHhy9uN0BF/WmkJSne2a+DeGEFwxA8g+7MkwAaXPcs2P6+rnCfLlKM56HHycckQ1ikqypoue/xIjJeP5uroHkxSHTRqqvUGPRaphjVLJtUV7GMWwmfbXvWPbBU5T52XZoyB3+6CP9sE6TrYqnK/Nt2CfR0UdXtphIV6yY7U33W/Vw1X1P7BDu7Xnozn6ez2cxBhu0m1BHkzue4oz+nreQ39VmRrKb1qrtq/5Mg5+4yxQl5AhOYAgTb33nF+7UM0H65r2yc9tCbPLZ/kgTPoES/+2saRwhglgTQ2F+TYi0tN6/4uo1SEeWZ037Q8zeH0F4mIbRI2iSARmz53D0JtL854LWsdN98ONoR4bMdDF39rIuqZ7mi2TaVwLqofDbE7+vjmE1ypto0RcJZ+4bIchi062P7228+9IctREr0dtGptBX9nj4HvzV95XHcwQOAJsV5mW7gAtQi33tngfZ3nuoOeEV5iDVz6kMv8scGvxj7QdE5bQ53jiWA9iV5Mc74x3U1B7zmQe4nM/eKQeVz5DwJoUWTnz0LoBG5r4907tbc19ccjLuuTZW/6xI+20GJKlpPn26/mlQXlcG2rWiWA2pdVHrbdp+7qup2XYlqdZPtvQoEz/I5dsjymV1XP8vnX4kAJIzMItDEuYtymk8ra/u+5GNcUwfaDvz/73W46TYDVOI02P7WHu6aB2lHX+RAeXlJsRw8W2oKKCrPaccSQPNinLGP6xap/irwUcbzp079wd0MoRnz6WdBX391L1xUvo9vHPfDCZ/tMgJeLLaqGrZTj3Z8vPXSlF1VPtvWu3eHP7/fdp93qZC2y/Gio7tYgapgh4bH8rmcA4ZdKrG0KIyxOwyyn20HE8a2kt+Sb3Z9P4SrKQR1aAjwsesLYPehcaB9vQhw/8x9+TxAW0areAeR5PtADsK80BTQ6xhBAI0pmgU7VyJUCbb0Jru6HkJ7nuoPWE7dpp+FKHIAowm0vy8D7OPcOO9wwmc7GmrpzS6CZzmos0tYp4vfum1Fs10qwG2rRKW6Sc5UZ7OtA5I73cEPqHr2x6z0Xbfz0nyddV2BD0Yo0s21PeD/e1zR4H0xwN/5YSTHcTMRCzHMTEryA5VMAVKsB1hRXgRFWXpT9TPo3qba2UJTQO82AbQxvPQXQGOM47llivGyN+/nhwD72SQfk9Rovh4LPklxwg1TlPvZt5oBfX3nLoP0oZcpzoej1b7zEj7b0VBLb3ax5OYuVc++zcwvLjqpLLVNuE7ls4p7rwJVz7qqWtZFgK2P3wsjE+nmekjn2Vb0O4YI/OUH88tKfn+T9g9BHKc4D+al5oFaRPpaPsqDoc0YahlgP49TrCWrofbrPle7eJF8ZABDGlsA7ev6n3CbNtj58c6+Tno+N8WxYa6C9u/r8eFSk1RnlgQ4qV+TYj23mQeaD0fp689q3THhs11HBgNVPusinPX58+dBfu9D4bpdlh7dZb+7XiJ1kjPV1XmXj0/XDl1ycyMH2LoOoJWq9DZRJk/jPa5RHtgekv6vaanGxcT+3i6PZaRJ2CIBDC9a8OhNsPa1XA9MQx7XPVlv5sVQh8s0ngBafnD5KQmgMY5x3OtA+/ohSL/euj+E6JPyuZ8/Uvhl/a+90KzHW9cQlYtWRSXSs7tFkPlCkyp9fit8tqMcdskVwTo9Ox5YmjL/d10EgHatfJZ1UentoeBcF0uKlthvVjPVZ8+KXENdhji7XnozB89KnJNTvWVqgtFaBNnPJu1fMeukorYe6uFDTZ3pvmHAp0HO1WUax0sQIL5oD7DmAfc3wkuFWbKkF+w7dt+EzhaaA6ojgMbYHaVYFXuijI2vi1IRxfIuceQ+KVdBy9XQfl1fF0vNMjjLb6Kv726OHO2eFiUsV2Vf/0gntsdVUqD62X1hly6qnu0TPOvqt+Ywz33huS6WFO3jGE2u91odt1wFrGsvX77s/Fh3sYTndWdnZ04AuF+khO8+neiJtv5jYhD5OB7y/5tyWwMTngKkWA+wolQeuO5yvd8RnLokYOvrep6uKlcInUGMa/ZJoP74obGbABqRx2/vArZxlEpts+Rjkohy3/Q8/VgRzceqw8h967lmoEInwe7vbwK28YcUIxyfz4Wmtp0SPttDiapa9wWwHlq2chv7LLn5bTZ+edlJkKt0uO66EtXpJjlTPe1+rprPp32DkPfpOtCWqw12fV7CyCxSnC8T9+lELbm5vm2nel6e5QnVrjfmSGUslWwFajALtr9vgrazpTdhPHOizcvB/M+lJoEw8lx3U10muk0AbeawEmz8dpFiBrYvA9073Bdi21REyyG0f1+PN+fGnL3KVSoazUCF52UU+X4V8YOPSB+OVlf9zLKbeyhRVeu+oMuQlc+yLsJ2d4XrSixvqOpZRzPVAktuzufzbwG0ruXzu+s/97ffVKaGh263QfZzn060ltDSMg3/ddvHwMfysesJYLchcKB9XQa+d16kGF+vN8kLK7jt+s0vATdVzuYp3nJhwHebF/nR5QDaW/325M1SrJDEm8BtbelN+rYJPW4+fMhbDlG/Tp4plmb5TWrS6ut78zLIflY3/hc+21PX1ZvuWpoyB8/yf3eIXAXskGUJuwhz3RWgK7Hk5r5V3rh2p5rNvlX/6ryXeVOmnylRUS0HI0u0wYQoPzh+UW62uRPdZQmKpqIBfA1fVywqOpa7DhraQPdLLy2BoVVZqv0eL4O3t+pnEFN+2Zxf8i01BYzGWAJomQDatEUat10Gv+4WKc7HJCcujVHKY9H83Dh/FJE/iPhTuqqQ9nw9V14Yr3amTXGeMTN+kULF0fv6ZYoR7j2qbfxv2c09lQg43VYFrIslNw8Njx0aXrvv95VY2rDEso6Tm6kWqHqWj0sX59FdSgTbcgiPgwYWjFukm+1Jof9t8eFGBftwUdFYsV0PprcddB8HOT8tuQnUINoDrOiTvnmQ8XIbqD+FPpztMB4F4sgv61+P5LcIoE3TcYoVjngzgjaP8htUP5uOi/U88zxdBdJydbQ/rf913l6m78G0vC012U5zABhak2IFivNzu+jviVU63UMOn/3T9brHFVMg4HRbFbAuwlldLJvZxe+9+ftyVanbqr0dNLq6uCiyrOOkZqqrY1IiFFiq6tn1Y9/1kquW3oR7LQNNUh8X+t/2MUCvwaKiNtm2g4o2EQMYUpNivayap3F86DAPsp8mRfBdDp690gwwSrl6zPOR/Ja37lWTE228Nh9Bm0d5qd6mWBWu6d5ivZ2n78G06+G0zXLym6pp+dy2qs3P11GrGdDX7+TlCNo8j1eWAfazqo8QLLu5p1zBqesqTjeX2LxrKc5d5CBWF4GcLiq93Qw0lQg4dRG0m3zvVSBwla+VroNht3n3rtsQcr4GVT/b//ajCSYhSmgmdzhHO/xvte2N7rWifXnc8f+uhnulBzrA0KJ9xftmJO0e5XfMkkpPcPOaaDUDjNI8jSeAdpquQmiMX3XLLW1xnS1H0O6RqjGr2sR98vW4SN+rpv2arpbwvL6U5+sUY/m5knyUhb5+e4s0nuqKUaqfVbP8+l+SgMDecjWw09PTTv/MHMjaVBm7bZnKna/ujgI/XVQ+y0G6XO1sE9q7rdJbDfs56d6rUNiqdNWzP2au83l69erVDyHOg0eUv/327c9lZ//QBJOQk8mnQfa1TQ8/FGpTPS9XP1fUdjV1rnlw9GLL4x1iOOk2EtI80MSzZktNUMcUIMWrFrkc0TXwIUj750nia5dLWBdbjp/6vve8TXGDjXnf/+bUgtGO9TfXeXSz9T+fO6yjNgu2v2OaS78M0v6bZ2neBbPPPOLmR7Ptanu6/ufxhNoiX0dN8iyL4c6/SHPnMfX1eW4QIcQ9W49LqrhH5Q7iX7bdt5OTk3917dWrV3/8+W/fvj34z5vNZp393vfv33e6P7///nunbZf/vKHOhbZtOz8Xhvgdp6ennf+OfFyOjo56+w3n5+ed/4Z8fN3zdt7OjQcnI8o5uc2SE68q2t+msuP8PlDbHAc6L2duIT8518dBr06DjTHbET5AjNDuX10qofutT+4/xgMV+KSP0cbBrps8V/t9JM/o3gZ/ptO6hd7ra6BzcYxjyk9B2v7UpUIBzfrc+pK886pda94aWqRrbIx9/Xv3qO1ZdvMAJapsXa92VlPls6yLpTc31c5yFbQuq1OVOh5TU2LJzXxc8vKvfSlRpezZs2dOjt35kmpC3WGgCVYX/5s+LFN9X1F9DnQsW9cPwPZTgED7mvvGxcjaP0oltybFqpBHDLmaXuTlx39L9X0wAnRnvtqepHE835qlqxe6ltEen5NgfdHLER6DKNVdLBlIqTl6HtPn5Tn/tr7Gx/xeyItChtCmWFUGx7hayJsg+1lFX/+XpETkQXKwpouQ2EZeljJvOZh1aDjr4uLijyUuu/qteUnDg2ZDq7Z6/vz5t+VFu9ZFOG7SM9XVscnnXonjUuJ4P3Tu54BjV/JSpC9fvuz0epqAC00wGfnmG+FlZL4pNPeMe5qKBvE1BpLyPr2qZF9yaff5Pf/94yDXTr5PCuoCg04BUqyXVW9Gehzyg7kIJfx/S0LTdC8vARX1C/ej9fj4V4cRRivP2Z6kcQS32vXvGEugju/jsyguRzqWnK/HA7XfI5r1fWDhsqGQZbqqupO32XqO24zsN+bfc5y8+6Jf0UKPr0d4DBbre1zt97Sj9f13XsPOWCKuomUK89KUXSwdeH0Jz662r1+/Hrxfx8fH//r06VPn7dbn0o5phMtuljgmY5KvSfc8JfG5c9I1hiUOZxXtZ61hvlqWcvh9JOPac7ePW507dtCbKGXjN/f+sVbrOAp0HBqXTch+q/Zw1ytz30mw7KY2jjyOPk7jWYLzy40xlfus53F9ba9GfCzOgxyD9y4benY6ov4z+r2sNW/V1/ewvR35/exfrqOHWXbzQCWWenz69Okfy1Me4t27d1X+3lwFq+tKWLnSVZ9LO46u92qa3quTRWPpzd0vS00wGcsUp4rqfZ3r00r2seYvQWvZr/yg/PieiXSYYaTbBzDkFCDFWkbxQxpvlY78u+ZB9vXMpUMB0ZfneesQwuhtKqAtR/Bb8lzaEpzjEG1c9mbExyLKWD5a5WviyxWQ/pbG9Qz0xGGlRzN9fVV9fYTnFm0aeIWnTfhs4frdT16Gr+ul+LoIZ+UgVg5kda2LpS3PzrqfF338+NHJWNkxGZsc0MvLb7L9bUgTTEqUCeTJA4OyGtQ8Jvsc4Fi2Qc7FfI8U0gWG9Fuw/X058uPxLsh+5v7Xy2pKjIteRH5ckFRFhSnI87dfRjKP2wTQjh3WsI5SvA9JliM+HssU59noby4fBhjr52Xqn6dxvDNqkhAn7tm3WaRxv++ItHz4oOeNymddjJw7rn52dHRU3T5d/3MPrTDWxe/r6/dOYqa6Oh4nJ8L621D9bKcJN9PyOch+3lUx6zjV8yK15jR1TZ3tXVXsngY5Fw1cgKH7w1mg/V1MYHyZf+OFc4cJm6fYH8bmL/oahxFGLz8Uf5LGFUAjplmK9UHAmwkckzfOHXhwvP8kjSOA1jqcuF//5N0Ejom+fgub8Nln1/D+uqgGFmmfFotFXbP+QlXeJtN7zWZFAoFjlCsSHh/7KHALS00wOZGCNLdNDmtK4C4c672P433LcVY3VHPbAAYUrXrVm4kclyi/U7UESnkefP8tvwnTMKYAmgfCcUUajy3TNFZeWqQYz8SjVc1jXDbLWEcPoD12KNHX/zQ+nk/kHhZlDnA61F+s8lkHaqy6VXKfalvisrYwXLje6zfvDrRX55aaYJrdYZD9fLrlfzbU4LX266emQcDNh2VtoOvF4AUY0lmwceVUqkXm3xnhIXyTfOlNues98hK7bfIyF6ZiTAE0YvY3TaD9fTOhYxPlt565jBjQGAJoKlTQxzkW6TzT19dnsKXcNuGzhev4MDUF0HIVsEOXxrxPbWGv2sJwoWaqbZuaptEQO1Apbiv/1ASTFKWSU3vj39dULSvCeKymfbz5lVmUJTcjhAyBEU8BkpdVtcqT+ChBO1/kUMrr4OOkV0klIZhSv/1LmkaVB4zDDrlOpnSNRPmtTfIxCcPKz0Yjf3QifIa+Pmb/19VvjfLh6GyIv/gvrt9u5GUuT07q+MCxdBhruVx+C7jVsvygymcH9F6qeO0lB9Bev36tIe6fPDA9+WXpqyD7epK+v9ytqTpBhDT1cn2N1zAIaB/497UycAEGnQIE29+/rrZzh63KsVSThKnpXn6Im5ff/BR0//N1ceq+BZOyWTJ4pinoqZ+JVGXzMg247NNAlinGxz65IsrCJcWA8gu2/CFxG3T/W9cQhRwFG1cuJzgOzr85Qgg19/XzIXfgX7b9t6Zp/lWL4+Pj4r/31atXVfzWL1++VHH827bt/LdN6ZyN5uvXr+57929tYqq+BjlHr4fk3layT78HOs6vKjqWzbUHsO6R43Ae4BieO0wEFeleaYs1ntJv1b1FDHK9D359NC6Nn3wyTtfGIx9Hv03GBq7NaT2PscXfjFcY2nHg6+ckWFu35q2eMdj09X14dO1fL13P+8vVwPI2tLzcZq5KVlotS12qerY/Vc/2l5cqraXSYaVcmNMVZamo9o5/7brZchhQ4bGMclO+dI8EBjTTBHR8PllekFJepBhLWdzlrUMIk5MroM01AwVFq4SC+SE85CJw32npTUp5pgno0Fnff6HwWYdqCEJ9+PCht9+ag25DqyUEF26menT0belI9ie8d6dLTTBpnwNNDptr/6xBpA5tUdG1/vjGPyO0HcBgQ1hNQJfTyhTva2/iWK62N4H3v01e6MIUPU/fl+GErp0kwX/MDxmfqGP+vzp0FJDnkI1mIPL48Xr47EL7H6aGIFSf+zB02C6H31Q+2/NOc3LyLYDG/tq2/VYBjZ/oS6Yt0k25TXUt3fDBsd578JxSnGU4pOaBocySl1V070wTUNB58PnlK/ddmKR5EkDDuIsYVNOjBhdBx/yNQ0cBqp4Rvq+/Hj77p/Y/TA1BqD73YeiwneDZATPVM3NV7Vh0ssB0RVpSMFfKelrRdROtamAtIao8eD5NcV7sGbwAQ/FVOyU0KU4AnJheBN73PD710ACmaZ4E0OhWmwQdME9kvN5pAvi2Sk+rGYje16t81qGhK3H1vRRmX0t83sWSm3vOVFXs6owKcrcSZCbKzfmkosF8xA5tUdG+RHmpl8faS7cIYIgpQLp6iAUl+DKX0mPOeeD9P3X/hcnK9y4BNLoiIEQpwg7U4EPAfXbdoK8niiZ9X0GoOOGzjg0ZiOr7785Bt4uL4U4blc/27L1+0391JQfPZrOZhviRvoQoN+eakqMRJ9jLiq53Vc8A7iccREl5QtRoBgrK1c8uA+//K4cQJmu+2n4Jfg9jeHmcdaIZMF9kxJb6SibuSF/PWPr66+GzSzf3ww1d+axvQ4XtcuhtuVw64XadqTbNt2pddEeY7+fLUxM4B5LqTru4DHzdKEGqvYAAU4B0FQ6CkpxjlB4vR15+s3WNwKTl+f6T5L0LxlnUfY5Z3oUa+ktwH4Yycjik6eMveuTm3nHvOFAoKv+dQ1QhG2rpTVXP9uy9VOnqXA705aVMuboVJQ/TWN+mNcH2Xal9d10AlJwCaAJ64IscSpsHH0vl6mdeJsB0CaBhnEXtTjUBA1tqAvT1EP88uxk++6zdDzdEMGqoMFYOvOXlN/s25PKmoe8qqnRpVxMEerpNa4KtRR575QfoHp5vR1APGIolVOhDDtXMNAOFvQh+jZw5hDBpAmjsY5aElzFvZBr+qQmYqN4qUmFc2ce48i+3TII4UA5G9V1hasgwVq5+1vfvVflsjzvK6hgdHR1N/ljkNjg+Pu52ZHBy8q0CmqVgBZj5fmvQBNt3oyPY/5nD6P4IVPtQodEM9CR/kTPXDBSUn1m+THFDXLmiyEdzJZj8feyX1fZ+tR1rDrYcX0EfmvX80XgeQF/POOWQyEnpvl74rIApVT7LPn/+3Gv4bKilPsP3XgWqc+Vj8euvv4Zri69fv34Li3Xdvi9evJj6aaYPYSN/yZs7plZTPHjNRP/qOYeqZg7lw12mJgAG4Ot1+nS83swJKOn1+t7WBN3/vPzmLw4jTNoyXVVA+5QE0Lhf6xxhgPnjXDMwkL9qAiaoSd6h0a+z0n39o1smP0o/HygvQ9lnGCwHgIZY+vL639+nHHZjx5lq23Ze7St78+ZNyPZ49+5d539mqcpywXjRxHWW3pxGGwlVPWyZLEsM9C8P/lvNQM98sUtp+eFX5K++8r351GEE97J0FUDzHI37+JCEvrVJ4JHhNJqACTrTBAxwr21L/gWPbvnPTHo60OcymEOHsXLw7eKiv9NG5bM9ZqrPup+r5iUmoy5/+vr1687/zBw8y8tvTlh+cLZ0tXHNQhM83KWN5No3dhz/cQbiEQJiCLN0VcYfSo+tIo+vzlwnQBJA435NUmUe88gI1yndEXxkajZLIMKo+vrbwmfKSnWg78pnQytRSeo2OfCUN3YYATdNkWVRo1Y9y3Jgcj6fd/7nnp1NOqS+cLVxQ36A6oZ9T5eWxvOQWZW7+xlbA33LD7BmmoGBqOpEH3L1s6grN+R79CuHEEjfA2hzTcENxvIMee4JyT8sB6W+rLa3mqITjfOOCTpx3jPgudeU+sNVPiskVwLrIyRVSxirr7Cdqmd7zBYKBM9Khbf6VCIwmYN+eYnTifqHq43bugdNMIm20TlrH6Auwj8MyRJR9GG52t4E3v9ZsjQycCUH0J4nATSMp6hrnMLdcvDsU/r+0ZcA2uFUf2KKLLnJKPv628JnC+3djT4CWbWEsfoK2w29xGjImWqBJTfzeZcDaNGvzxLLxZZo7yhN6mrjFipiTaNtVLm7p8vUBMAQUwBNwICa5IUV/ThPsT+gVf0MuE4AjY1Zspwfw7L05t2uB8+uX7MCaIeJ+AxDIR8O0errGWtff1v4LCdKltr8cB8/ln+vXFMYa0phuzAz1dnsWzWurkVecrP07yjV5gEsXHE4LybdNo71HUM1TQD0rGjpdNiSACR9eRF43/PLy3OHELhGAA3jKGrQJJWobjNLPwfPrv9375Ml9PYdEx8H3O9Lh44DCPkytE31zs49uuM/X2jzw00tjFU6bCd4tsdMtUAVrlIVw4ZQqoJbiaVOK+crD+6bhBlT3D7OGtsEVZW7O7oaTQD0zAMsatCmmC8QiDmunge/ZzcOI3BNDqC91AyTdZwsy4x5ZY1m6aq62X3hshzY+2QeNJlzzTsx9tUkAV9GfP+9K3ymSkMHcqilZEintjBW6bCdJTd3nKkeH6e27X6u+u7du1Fdo/P5vPu79W+Tm5stXHHcd/vWBD/56D4wCcukmjDQryZ5WUU9vLCiL7n6WdQPO/ILTMtvAjedp6sQGsZPMJQ2CchvzNL2y2puluUULNlOkwpV3unBPx0+9PUEV+SjB5XPCitZDay2MFYO8pQMoKl8tmPvVSAAVSqsNaQSS28eHR1NrfqZcBH33r41wSTGWarcOfeB4Z1pAiqSX7hYdoa+xqEvgl8rrcMI3DBPAmhTU2z5IzC/3Fu+Jt/ucS2/T5bh3MbbwPuu8hn6esag8yX07gqfLZNKDZ0oGZiqMYxVKmy3XC6/bWzZexUKP5UIag0tn1clQpMTq362cNXxwETsUjP8MMYa6+TU0ps/EswFep0CJF9XU985OdMM9GQefF761iEE7ri3CaBNx6kmoDJT/5jk1YFjtNx+X82J7pTbpQ28/wuHEPdVRnIvbrr8Ax+5cZaVl93M1aJK/Lk1hrFKVT5T9WzHmeppmbnq2KqebZQI1ZVa9rTG21wSLGKL27gmmMT4ythRewDDPizwAIvaWE6CPkWuftakq2X2AG6ar7Zfk2dvU/BME1CZKX/glENnpx21Yf6z8lKcrVPqD3mpt8hLzy8cQvakoiQ1mnX5h90XPlOtoSMlglMll7c8RKlQXG1LjFY/U332rMh5PNbqc6V+W4njYKBNUG7i3425OlgOoy4d4j/ujV4OAH0S8qFGTVKRj37Hoq+D38cbhxG4RX658MQcc9RO9AFUaopBiRwWm3X8Z7bpKoD21rX+LXiW2yLyx3NW/2Df+4C+nhp1GmZQ+awHJYJTpZa37GQ2PKGwXZUz1ZOT1Pz/7N09e9zWlQDgQ8pFtgr3F2T8C0J16Tzs0i1VptKw3MpSmUpUmUp0mUqjcitRv0CjLp2ZLp3HXbYK3WWrLK4xE8n6IOcDwOAA7/s85yHj0B7MvQDuBXBw7qT58WuIS25+6NWrV43/N8vSp2UJ1IEz0WajoUETjKYtDNjOjcABLgHCDSz6S2IkXXoeeV+GWFfGAPickmArAc18CbpWrjOnI/q+bSSefaj8t39Yfc50hPvTEBLPioVTAztQ4ZQ+j/WNjX1f3fH/LVcXNafafD9tVFXqczJWSVJqMjmuLFvaxtKljV7939zE2dnZoLdl6AmAV1dXkhxNtGnPrTlFPSUYwXd8E+3epHFuBPiUh1X02TTqG1lLTUFH1x1l+c3XiY+X8/DyDvB56wS0ITy4571JWI6P/l9vLgb+HU9W88eujsXZKsp5vVR9mI9gPyrLmL4YwPdYrvoNth3rZ5qBHnvc1VhUBoJ/CSGEEJ+J18ZjtnDpmPn5InsMxt7PPzjcB3+uuNRN9OwGlnmp6HsMuZpThnHr7QjPjW+TzyVP9MlBYqqNzaOTKC/2fT+iecR04P350lxRJIjJgI/Bkx6cU/8Rw62GNk0+Nx/K/f2p61b3DYS4JxopHnJ8z/9vySAAvuSdJmAL3t4fTxuMva8XdnWgQ6qekUGp5KRCC126iLxL001iPC+tALtZV0BTeSW/k9U8CVx3Hu4YLAkvpz3YjtlqW8qLCC8GcG6Yrr7P2xhWUp1nHDiHYj+9w33JZ4vIe7MGABNt+uNm5HOKZYxnuamxJ6Z6eQPoyvoGNWTYVz1Ypeu593eJt/9ZDLvCCLC/cn9FAlp+s5Cgj331kNcofUg8+1iZA5YXEcqqM/9Y/XzSw+38UpuWbf0+hpd0VlzHeO7v4/yJfXUnxxv8zUJbA/CRpYk2O16g+e6+69CZOwNdUU2KTJ5pAjp2mfya9aUuBO4hAS0/lVDIYmgvk5RErj4mnn2p3UsltJLQtU5GK/PcaU/uB5yutme9fS8iR6LcLr5zKsBYz8DtXYV9k+Qz1RsA+JiqZ+zine8+CssYb3LqIlQNBrojmYdMJjG8N9/pv4vE216OFxUDgfusE9AWmiKd81DlklyGkkCRJfHsc9bJaM9W36Eke/2w+v0y6qo102gnKe1k9d8un/Fi9Zn/ijrp7FkMN+FsbWGsZcdrulPNQCKP9/0PfLXB35QEA2/bAfChV5qAHYx5TnE9wu/7ZIT97KWNcfhNSKDIpjyUG1pFiLIPTnQtyZQHVgvNQIfK/jaPvEsUvwwvNwCbzXXPVueMmeZINS+CTE5X16GL5N+hJE0NqYL4JL78os+6r8r9kJ/i7nsjJ/HLJJnfxPt7DlO7fzzXBOzgsSYg4ZhSkpx3fp65SfLZejCSmQlAxDAf4NLtvjO2OcUYKwWWSm9jTD5bOMxHYRYe6mQ8Ns8G9p08rCKjdYWPpaagQ08j7zLFZZufrb4DwH0uPrheod/KfGiqGUjoceS99zXExLP7TD/6yW4W4Z4vu13LmZORUbnnvPMzzeMN/06FGwDWLLnJPt74zs4TA7UMiblANyZhKTbykjhJ18oLMJkrFZQXOrwQDGyqJKDNNYP5ELRkFjkrcJfr57ElntEcL4Kw63UcZDTdZ6zfNPlMogEAa5aVYx9jnFMs9LV+BmiQh1VkNgsPfejeVfK52gtdCGzhYnXeo59UQmEI8/ls2/vaNQh7XEd42ZhdWHKTzJ7t+i9umny2dHIFIOq3xiUks4+b1X40pu+7HGlfjy1R9Z3DG+jITBOQWHnoo3Ifh5C5YsHUuR/Y4Zx3oRl6KetS0LCW6WWoMn96qcvY0TJyV1DmsOeeiWZgjPPV4y3+1tKbAEg8w360ncWI+3lhvwZo3Cw8rCI/1fs4hPJSSOZKQC+c/4EtzUMCWh890wQkl6V6X9lGiWfso4yht5qBHah6xmjH+m2SzzxQA8CSmzThnWNmFJYxnsq5Y6voBxyOpB2G4DTqSk7QteeRtypxufkrYQHY1jwkoPVJmf9MNAOuS1t3GRLP2E95aWWhGdiB+x2Meqz/aou/XUb9YO1UWwOMkiU3acpiRMfMQl+PYu4oMRfowjTZObXMG/+q2zpVbgxlqYz02DyJA83Py1J0r5Nu/5PVvNOxA2xjvvqpgmI/5mqZxszvdFmnfht5lqc/XUUfXzotSWczuxN7uFldM8DQx/rC0rLd+k2iMWqympdslRfw1ZYf8l3IFgcYK4lnNGUZ40hoX+jqnx+OPXF+BGhEtrL9lqjo3q8TjbuzyF2FitzztjJPnybd/pI88lA3AluaR30f5m1IQDuUSeRJLFqPl5e6rVMnq/lJlmP02+hfZcUnIfGM/ZR7GI80A3ucxzOdg4z1h3EeuV4c3erZ1/EOOyEA4+RtN5q0GMF3VA2r7uehJx6U73ejq4GWTSLXDax5SDwzX7/fTJdxIJmTY8sLPE90IbCDct16Zo52MNkqobgP3L1sq46UuXzfHp5fxftqj7CLMk4uNQN7nBeN9dwn0zhVEuUm2/wL2yafWXINYJzKhFtyBU0aQ2LWQjePoh3MjYEuzJJt7ytddrA5e6Zx91tdxgGPlcw32p+FykXAbiSgmc9vup+4D3wY2eYnfUyILy8ZzO1K7LjvOPexj0z3OMo18UKXGeub3q+Pd/gAN5EBxse5n6aVie2Qb3behLek1oaeaPhOFwN9u9DvwRi40GUHk+kmVrYlKRiWy8Tz9XLsvNCFwB5zNQlo3ZpFrqRhlVBcS23qcU+3qyQRPbU7seU+M9cM7GHrClHG+tFaxoArne6SfHbtwgRgdEy8acNiwN/Nkpvj6OcIlc+Anl3k94AbWIcfl5aJtlf1Mw7pIvnYMNWFwI5KgsvXocKL+c6nyrO/uS47qEwvgU+ivy+TXEXupdbpznPnPYz1GOu/qNyTPt/0j493/BA7JMB4LEIFJ9ox5AQtCUnvlfPHUG9o34SbWED7Hifa1ltjYC9kuol1ugo41LVu5nOW6mfAvvO2s5CA1rZpsrnOXJf1og8y3Wt63PO2LOe5pd2KO/aRS83AniaR68Ughab60QeZxqZnm/7hrslnll8DGA/nfNqyGOj3KhN3N29/6Y3vBbCT8qBqmmh73cDqh3my7VX9jEPKXBGjjBFPdCGwBwlo7XucbHtVMTaf39Y0+p1gWc5vD2P4KzOw23XAhWagAc+Sba+xXj9saxIb3p/eNfnsxgUJwCgov0qblgOdT6j4Mp420ddA27Il5TzXZb2ZY2Waw88i19KyDO+aN/O5qzzomOhGYM/zoAS0dkyiv0sSfk62KhxDli0xoO/XrevznOtV1i7Ccy+asdWShD2wMOfrjWznoI3G+uM9PkBWJIDBD5qY7A7NO936iSEuT6nCHdC2cgNrlmxMX+q23shWvVj1Jg7pKvG8rowVlt8Emri+LZWB5pqiUbNk22v1i/4o11WZXng8jxwvk1xGnYSmWve4xzuJZzQ91md6mc5Y36/zUaZzURnrJ/f90T7JZ5bTABg+ica0bYjLFqqGNY52WehSoGXZknHcwOrfOLVMtL2PdRkHlnnJnXITeKoLgYbOhXPN0JhMVYzLvNH9LNdXu8r04lS5Tvra/j5K6wp4xjnGOtZb6ap/sj2Dv3esP7aDAnDHhdhSM9DBfjakZPYhVvhqytAqwr3RpUDLMiXjlDnjXJf1TqZlZSaRrzoIw1Lm8VeJt/+lLgQaIgGtGWVek6kSiheQ+yfbMqjZEjAercJ93PHM9b8Oq1jQrI0qQfWI+V0/z02Zzkv3jvXHe36ACSnAcDnH05XFgL6LhKQvU/kMYHOzyHUDS9Wz/o69mR6mqH7GoT2PvA8gy5hxqQuBhkhAG9+8Rn/3U6b782Uucp7wekkVtOErL5g8DImGGOs989Uv+7q30um+yWfL8OANYIiWLrro0JASthw3X1Yu8IfydtlNqAwJtMvDKpoaezPNTaaRK+mSYR4zmZff/NYxBDToIvk58ZBOI9dyyPOQlOE6q7m5SMb537oK2tIuN7i5fenXp5qCFpTrrkwJt9mqaY5trM80D7tzrD9u4ANkSQIMj3M7XVoM5HuUybvS3Xd7Y58FuFfGh1VL3dZbz5Nt7zNdxoFdJ57rlbeQX+hCoOF5ngS07WVLwHEfuL9uI1cCWrmOnSSeAz5MeP3E5y1W/elFcYz1NSsW9H/OncWd962bSD6TKQngohb2UeYRQ0jaWujKjeaNQ2B5VaBNbmDR9Dwr0xylvDl8ots4sMyJFueRb8kroN/mIQFtG/cuR9QzN+FFyr7LlhyY+WWS8lzkMuqlOBd2vbR9WCqdnYXcBYz1a+VYkIhprG/SF1fsOG7oA2SCAwzHdSi1TveGcEEvIel+Q1iu8jbcgALaU25gZUoaWDonpvAq2TEw02X04NyW+V7ni5DECTRrHhLQNvUk2faqetZ/2RIEh/AySZkLnq1CcmYei6irnV1pCpznjPUJx51MCYKzLx0DTSWfSVQAGA4JxRzCu4Fc4DL8dtLPwEEu3s0b2cM8ciV/f6vL6IHLyPvSxCTyJT8AOeYTJRHDc6C7PU60raUvVULJIVPiQLYXqu6yiDqZ6SJU0er7uewiVDujO9kqPM51WQrZVpb47D2HppLPbkPWJMAQzE3QOZDsN7sW4Qbspt7YfoAvypR042FVLpluYk3CsoH0Q+YqP89WxxJAkxYhAe0us2Tn3rm+1FctzkOG1v5fhyS0Prpa9c1cU9CRqbGellwnG2M++8LFcYMfcGXnBUjvlSbgwJOrrCQkbW5h+wE+qyTbTJKN2+4B5DFPtr2qn9GXeV/ma5SXuhBoQVmCTgLa5z1Otr0KSpjPt6Vc104H2gfrJDTLcR7W9aovnhqP6Fi2exXG+lyyvTg6+/gfNpl85q1ngNwWIamCw3qX/Phh8zlj1vYqN5aWuhBoiRtYtKmMX/NE2zsNVZvoh4vI+0CrHEeqCAJtXRtLQPulSeRKtlmE+xvZZLv+GvLLJOW66uHqPLiwa3Z+7irt/sg5jAON9Zmur25Comw2V8m295MXL44b/oDn9gmAtJzDObSsSexLk/itZa0Ut9B1QEsmke9hlbEvn2xVjlU/ow9uk18rv6jiRDcCLShzwYfmhP+WbZlBL5Lks4xc906zVfbe9bq4JEKtl32UkNt+W0v445Bmxnpadhv5Xhw9/fAfNJ18tgzrKgNknbybtHNoy8j5xpJjZzxtZnlVoC3ZHlZZqj3v+JtprjULSTP0Q3n7OGtyxSThGAPkUeYVZyEBrcxXzpP1m1WMcvIySX+PqVIt15KczZuHKnM4p+0iWxITAxnrj1v4AJVzAPJx7qYvMt78kpC0vazLVy50HdCCbA+r3MDK7TvHBuzkIvG2P4mP3kYGaHhuOPYEtFnkSpj3Ikle5b7pMtmxMbbzYblWLslSJRHtKiwNuYvSZuV51X+GZD6M9bua67K0FsnOe784NtpIPlvaoQHSDWQLzUBPvEu4zd4W3f3co58B6uSaTDewlO3PbR65loNRsYm+KDd/rxJv/wtdCLRo7Alo2ao7XdllU8uUPFiuc2cj7adlFU+jTkJ7GBLRNhlHyrXqo1WbXYZlTDHW78O9u9yy9d+/x/rjlj5ABR2APJyz6ZNr2zsa2SrGvdNlQEuyJdfMdVlqt8nmL5MqprqNHl07Z30IVo6jmS4EWp5jnI1wrni+mq9kmstL6MgtW/Lgt7rs58TcdSLa16vfrx2LPyvt8OGSpe6109drqUyVpBch2XUI58ZMY8S/x/q2ks+W4YY0QJZJyEIz0MOJVRYSksbRzxm3F8hhGrkeVmVb5oXPy/YGpQdW9EW5+fs08faX6mcnuhFo+TxZkgfmI/rO2eYpltwcxnGW6Rg7DS+TfKhcT5cEwlLhqywr+TDeJ6MtR/b9j1Y/5yERj357nGx7VT0z1ndtEvULGa0lnxUq6QD0n3M1fZQpoUtC0jjabxmSLYB2KNvPIZQ37xeJtjdbRRGGbR55X+AqiWeWsgW6MJYEtDI/mZoDcgDZkggf67I7j8t1Mta6Mlr5vTy3GUJC2s1qPLiITyu/QZaxfpZoe5eOr8FI+eLoccs7t7XjAfprEW440E8SksbjnX0SGLFJrN4KSzTumTsOR7YHVqqf0ScXibf9SeRaMgbIfa6cD/w7Zkvo9SLJcJTrsptE2zsLL5Nsc91d7kNexvuEtFIh7CzeJ6XNo5/L6i1W2/Z0tb3rym7r8WCpe0lolmx7VTgd1niwSLS90zLWf9XyhzxfHZRKugP0j6pn9HlSVWLS8+2UkNRMG75IsJ2WVwXaoOoZhzSP/izBVx6c3bfMykSX0bPrlXI9nbWK2MuoHwQCtK0kHPwYw626mOlFkjLXch9rWL5bjel92Lc2SYQ7DclH+1isfn7uOD6J9y8XTD66dvrmC32x6XXg8qN+W67O6x9u0ybXc5BVtsqNCkMNb6yf9mwcusv0qIMNuQwl3QH6Zh6539YGAAAAAAAAAA6si+Szkj39fXhLFaBPSrnmpWYAAAAAAAAAAHb1oIPP+GcVP0WuEsQAQ1aWB1FmHQAAAAAAAADYy1GHn1Wqn51qcoCDuo266tmtpgAAAAAAAAAA9nHc4Wc91dwAB1eqnkk8AwAAAAAAAAD2dtTx570Oy28CHMoy6qpnAAAAAAAAAAB76zr5bBL18psnmh6gc2dVLDQDAAAAAAAAANCEBx1/Xlnq7T+qmGp6gE5dV/EnzQAAAAAAAAAANOXoAJ9Zqp6V6mcTzQ/QiZL4+zDqZTcBAAAAAAAAABpxfIDPLEkQF5oeoDPfhcQzAAAAAAAAAKBhRwf87NdVnOsCgFbdRF31DAAAAAAAAACgUYdMPptEvfzmiW4AaM1ZFQvNAAAAAAAAAAA07cEBP7ssv/l/VfxeNwC04qqKP2sGAAAAAAAAAKANRz3YhrdVTHUFQKOWUS+3easpAAAAAAAAAIA29CH57DTq5TcBaM6jKq41AwAAAAAAAADQlgc92Ia/R50EN9UdAI0oSWfPNQMAAAAAAAAA0KajHm1LqX52qksA9lKW2fw6LLcJAAAAAAAAALSsT8lnlt8E2J/lNgEAAAAAAACATjzo0bZYfhNgP/Mq/qQZAAAAAAAAAIAuHPVwm96GBDSAbS2reBiW2wQAAAAAAAAAOtLH5LNJ1MtvnugegI2dVbHQDAAAAAAAAABAVx70cJtK1Z7/reJc9wBs5HnUS24CAAAAAAAAAHTmqMfb9jokoAHc5ybq5TYBAAAAAAAAADrV5+SzsuxmWX5zopsAPqtUiiyJZ0tNAQAAAAAAAAB07ajn23cadQIaAJ+6CMttAgAAAAAAAAAH8qDn2/f3Kn6q4ve6CuAX5lU81wwAAAAAAAAAwKEcJdnO11Wc6y6An91UcRb1spsAAAAAAAAAAAeRJfnsJOrlNye6DBi5knBWEs9uNAUAAAAAAAAAcEhHibb1tIq3USeiAYzVoyquNQMAAAAAAAAAcGgPEm3r36v437D8JjBeV1V8pxkAAAAAAAAAgD54kGx7yzJzpfLZ73QdMDKLKv6gGQAAAAAAAACAvjhKut1l+c2p7gNGYlnFwypuNQUAAAAAAAAA0BdZk89K9bPvq5joQmDgSsLZWdSVHwEAAAAAAAAAeuMo8bafRl0B7UQ3AgP2qIprzQAAAAAAAAAA9M1R8u0/r+K1bgQG6mkVV5oBAAAAAAAAAOijB8m3/29V/FTF73UlMDDzKv6oGQAAAAAAAACAvnowgO/wlyomUS/DCTAEi6iX2wQAAAAAAAAA6K2jAX2XsvzmuS4Fkrup4qyKW00BAAAAAAAAAPTZkJLPTqp4GyqgAXmVhLOvQ+IZAAAAAAAAAJDA0cC+jwQ0IKuScFYqnt1oCgAAAAAAAAAgg6MBfqeSeFYS0E50L5CExDMAAAAAAAAAIJ2jgX4vCWhAJhdVzDUDAAAAAAAAAJDJ0YC/mwQ0IAOJZwAAAAAAAABASkcD/37nVbzWzUBPSTwDAAAAAAAAANJ6MPDv97cqfow6CQ2gTySeAQAAAAAAAACpHY3ke86qeKm7gZ64quKpZgAAAAAAAAAAMjsa0XedhQQ04PDmUVc9AwAAAAAAAABI7Whk33cWEtCAw5mHxDMAAAAAAAAAYCCORvidZyEBDejePCSeAQAAAAAAAAAD8mCE3/mmih+rONf9QEeuqvhvzQAAAAAAAAAADMnRiL/7LFRAA9pXqp3NNQMAAAAAAAAAMDRHI//+pfpZSUA7sSsALZB4BgAAAAAAAAAM1pEmiNMq3oYENKBZEs8AAAAAAAAAgEGTfFaTgAY05baKpyHxDAAAAAAAAAAYOMln75UEtNdVTDQFsKOSeHZWxY2mAAAAAAAAAACGTvLZL5XKZ6UC2qmmALYk8QwAAAAAAAAAGBXJZ58qCWilAtpUUwAbKglnJfHsVlMAAAAAAAAAAGMh+ezLXlYx0wzAPRZVPAqJZwAAAAAAAADAyDzQBF/0poqfqvi9pgC+YB514tk/NQUAAAAAAAAAMDaSz+72lyp+jHoJzl9pDuADT6v4o2YAAAAAAAAAAMbKspubOa3ibRUnmgJGryyveVHFtaYAAAAAAAAAAMZM8tnmSuJZSUA71RQwWsuol9m80RQAAAAAAAAAwNhJPtveyypmmgFGZxF14tmtpgAAAAAAAAAAkHy2q1nUSWjAOFxV8VQzAAAAAAAAAAC8J/lsd2X5zddVTDQFDFapcnZRxbWmAAAAAAAAAAD4Jcln+zmJOgFtqilgcG6iTjy70RQAAAAAAAAAAJ96oAn28s8qXkWdxDfVHDAY8yr+UMVSUwAAAAAAAAAAfJ7KZ82ZRl0F7URTQFplmc2nUSefAQAAAAAAAABwB8lnzSqJZy+rONcUkE5ZXvNRqHYGAAAAAAAAALARy242qyzD+T9V/FTF76r4lSaBFJ5HvczmraYAAAAAAAAAANiMymftOY26CtqppoDeWlZxUcVCUwAAAAAAAAAAbEfls/b8vYo/R53gN9Uc0DvzqJfZ/JumAAAAAAAAAADYnspn3VAFDfqjLK1Zqp1dawoAAAAAAAAAgN2pfNYNVdCgH0rC2VkVN5oCAAAAAAAAAGA/Kp91TxU06N6yiqeh2hkAAAAAAAAAAANwWcW/hBCtx4sqTpxyAAAAAAAAAACapfLZYU2iroI21RTQuLK0Zql2ttAUAAAAAAAAAAAM1ayKf4QKVUI0EeVYunRaAQAAAAAAAABgLMqygGV5QMlDQuwer6OuKAgAAAAAAAAAAKNzWsXbkEQkxDbxQ1i+FgAAAAAAAAAAfnYedUKNxCIh7l5i84nTBQAAAAAAAAAAfOoy6gQbiUZC/DLKsXHiFAEAAAAAAAAAAF9WEmxehGQjIUq8rGLitAAAAAAAAAAAAJubRJ14IwFJjDHeVjF1GgAAAAAAAAAAgN1NqngdkpGEpDMAAAAAAAAAAGAH06gTcyQoCUlnAAAAAAAAAADA1qahEpqQdAYAAAAAAAAAAOxoUsXLkLwkJJ0BAAAAAAAAAAA7mIQkNJEnyr46ddgCAAAAAAAAAEB/nFRxWcU/QoKT6FeUfbIknU0cpgAAAAAAAAAA0G+zKr4PSU/isPFDFU+iTowEAAAAAAAAAAASmYYlOUX38baKc4cfAAAAAAAAAADkN4l6Sc5SiUpylGhrac0XYWlNAAAAAAAAAAAYrFKRSjU00WSVs5nDCgAAAAAAAAAAxuOkiidVfB8SqMR2USroXYYqZwAAAAAAAAAAMHqTqJdMtCynuGtZzVIx79ThAgAAAAAAAAAwPkeagA2U5KLHUS/POdEco3ZbxXUVb1Y/AQAAAAAAAAAANlIS0VREG2eFs3O7PwAAAAAAAAAAayqfsY+SiFYSkv4rLL04NMuoK5u9quJGcwAAAAAAAAAA8DHJZzRlUsU06kS08vNEk6RTks3erX4uNQcAAAAAAAAAAHeRfEZb1lXRvok6GY3+KRXNFlW8Wf0EAAAAAAAAAICNST6jK9NVSEY7nEXUCWfvVr/fahIAAAAAAAAAAHYl+YxDmUZdHe2b1c+JJmlUSSxbVPHX1c+FJgEAAAAAAAAAoEmSz+iLk6iT0KZV/DYkpG1juYpS0exmFUvNAgAAAAAAAABAmySf0XfTqJPQSnzzwe9jVKqZrZPLfvzgd8tnAgAAAAAAAADQOclnZDWN99XSfr36uf7fmS0/iHWC2XoJTQAAAAAAAAAA6A3JZwzVdPVzEu8rpZXlPE9Wv3eZqPZhdbLy868f/H7zmb8BAAAAAAAAAIDe+38BBgBQ7DEXUNeOvgAAAABJRU5ErkJggg=='
  }


}
