import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { Location } from '@angular/common'
import { faChevronLeft, faExpandAlt } from '@fortawesome/free-solid-svg-icons';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-purchaseorder',
  templateUrl: './purchaseorder.component.html',
  styleUrls: ['./purchaseorder.component.scss']
})


export class PurchaseorderComponent implements OnInit {


  sortListItems = [
    
   
    
  ]


  purchaseForm = new FormGroup({
    purchase_vendor_id: new FormControl(null,{validators: [Validators.required]}),
    purchase_company_id: new FormControl(1,{validators: [Validators.required]}), 
    
    is_cbd: new FormControl(0,{validators: [Validators.required]}), 
    
    status: new FormControl(null), 
    create_by: new FormControl(null), 

    //term_of_payment: new FormControl(null), 
    //term_of_payment: new FormControl(null,{validators: [Validators.required]}), 


    // chamber: new FormControl(null,{validators: [Validators.required]}), 
    // size: new FormControl(null,{validators: [Validators.required]}), 
    // warehouse_id: new FormControl(null), 

  })
 
  vendorForm = new FormGroup({
    name: new FormControl(null,{validators: [Validators.required]}),
    npwp: new FormControl(null,{validators: [Validators.required]}), 
    pic: new FormControl(null,{validators: [Validators.required]}), 
    term_of_payment: new FormControl(null,{validators: [Validators.required]}), 
    credit: new FormControl(null,{validators: [Validators.required]}), 
    purchase_company_id: new FormControl(null,{validators: [Validators.required]}), 


  })

  tab_option = [
   
    {
      'label' : 'WIA',
      'name' : 1,
      'id' : 1,
      'is_active' : true
      
    },
    {
      'label' : 'PDB',
      'name' : 2,
      'id' : 2,
    
      
    }
  ]



  status: string;
  purchase_orders: any;
  payment_status: any;
  purchase_order: any;
  query: any;
  faExpandAlt=faExpandAlt
  tab_menu_list: { id: string; label: string; class: any; }[];
  purchase_menu_list: { id: string; label: string; class: any; }[];
  vendor_list: any[];
  purchase_order2: any;
showModal: any;
  vendor: any;
  params: string;
  summary: any;
  page: any;
  
  constructor(
     
    private financeService : ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private location: Location,
    private webService : ManualOrderService,
    private router : Router,
    private serializer: UrlSerializer,

  ) { 
   
  }

  ngOnInit(): void {
    this.getPurchaseVendor()
  
    this.route.queryParamMap.subscribe(queryParams => {
      this.query =   queryParams.get("search")
      this.status = queryParams.get("tab_view")
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams}))   
  


     
     this.getPurchase()

    
    })




  this.purchase_menu_list = [
    {
      'id': 'all_purchase',
      'label': 'All Purchase',

      'class': null,
    },
    {
      'id': 'wait_for_approval',
      'label': 'Wait for Approval',
      'class': null,
    },
    {
      'id': 'approve',
      'label': 'Approved',
      'class': null,
    },
    {
      'id': 'wait_for_delivery',
      'label': 'Wait for Delivery',
      'class': null,
    },
    {
      'id': 'wait_for_payment',
      'label': 'Wait for Payment',
      'class': null,
    },
    {
    'id': 'vendor',
    'label': 'Vendor',

    'class': null,
  },
  






]

    
  
    
  }







  async getPurchase(){
    try {
      this.spinner.show();
         let response = await this.webService.getPurchase(this.params)
          console.log(response)
      console.log('get pur')
          this.purchase_order2 = response.data
          this.summary = response.summary
      this.page = 1
          this.purchase_order2.forEach(item => {
            item.sold_perc = (item.total_sold/item.total_order)*100
          });
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        
           //this.ngOnInit()
            
        }
  }

  async loadMore(){
    console.log('load')
    this.page = this.page + 1
    if(this.params == '/'){
      this.params = '?page='+this.page
    }else{
      this.params = this.params+'&page='+this.page
    }
    


  


    try {
      this.spinner.show();
     
      let response = await this.webService.getPurchase(this.params)
       console.log(response)
       if(response.data){
        this.purchase_order2 = this.purchase_order2.concat(response.data)
         
          this.purchase_order2.forEach(item => {
            item.sold_perc = (item.total_sold/item.total_order)*100
          });

       }

        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide(); 
        }
  }


  async getPurchaseVendor(){
    try {
      this.spinner.show();
         let response = await this.webService.getPurchaseVendor('')
          console.log(response)

          this.vendor = response.data


          let vendor = response.data.map(item => {
            return {
              id: item.id,
              label: item.name,
              name: item.id
            };
          });
    
          
          this.sortListItems.push(
            {
           
              'name' : 'vendor',
              'items' : vendor
            },
          )


          
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        
           //this.ngOnInit()
            
        }
  }


  _getExpired(end_date) {
    var today = new Date()
    var expired = new Date(end_date)
    let time = Math.round((expired.getTime() - today.getTime()) / (1000 * 60 * 60 * 24));
    if (time < 1) {
      return 'Due'
    } else {
      return time + ' Day(s) Left'
    }

  }


  async addPurchase(){
    if(this.webService.getAccountName()){
      this.purchaseForm.get('status').setValue('DRAFT')
     this.purchaseForm.get('create_by').setValue(this.webService.getAccountName())
      console.log(this.purchaseForm.value)


  
      try {
        this.spinner.show();
           let response = await this.webService.addPurchase(this.purchaseForm.value)
           this._navigateTo(response.data.id)
            console.log(response)
            
  
         
          } catch (e) {
            console.log(e)
          } finally {
              //this.purchaseForm.reset()
              this.spinner.hide();
          
             this.ngOnInit()
              
          }
    }
    
  }

  _navigateTo(id) {
    this.router.navigate(
      ['/purchase/id/'+id]
    );
  }


  async addPurchaseVendor(){
    console.log(this.vendorForm.value)
    try {
      this.spinner.show();
         let response = await this.webService.addPurchaseVendor(this.vendorForm.value)
         
          console.log(response)
      if(response.data){
        this.vendorForm.reset()

      }
       
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        
           this.ngOnInit()
            
        }
  }

  
  selectForm(key,value){
    console.log(value)
    this.purchaseForm.get(key).setValue(value)
  }


  async searchVendor(e) {
    console.log(e)
    this.vendor_list = []
    try {
     
      let response = await this.webService.getPurchaseVendor('?search='+e)
      console.log(response)

      if (response.data) {
        this.vendor_list = response.data.map(item => {

          return {
            id: item.id,
            label: item.name,
            name: item.name,
            
          };
        });

      }

    } catch (error) {
      //this.appService.openToast(error)
    } finally {
      //this.appService.hideLoading()

    }


  }








}

