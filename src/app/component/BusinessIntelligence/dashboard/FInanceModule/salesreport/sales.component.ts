import { Component, OnInit, ViewChild } from '@angular/core';
import { BusinessIntelligenceService, trans } from 'src/app/services/business-intelligence/business-intelligence.service';
import { MatPaginator } from '@angular/material/paginator';
import { Transaction } from 'src/app/model/transaction';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { matRangeDatepickerRangeValue } from 'mat-range-datepicker';
import { faCheck, faChevronDown, faThumbsUp, faThumbsDown } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

import { formatDate } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {
  @ViewChild('paginator') paginator: MatPaginator;



  total_transaction: any
  revenue: any
  grossProfit: any
  totalProfit: any
  faChevronCircleDown = faChevronDown
  faThumbsUp = faThumbsUp
  faThumbsDown = faThumbsDown
  faCheck = faCheck



  range: matRangeDatepickerRangeValue<any>

  shownRange: any

  shownTransaction: Transaction[]
  totalProductSold: any;
  total_qty: number;
  filterResult: Transaction[];
  source: any[];
  showFilterMenu: any;
  filterName: any;
  selectedTransaction: any;
  selectedItem: any;
  selectedItemIndex: any;
  total_discount: number;
  total_bonus: number;
  total_transaction_fee: number;
  startDate: Date;
  endDate: Date;
  total_product_discount: number;
  filteredItems: Transaction[];
  selectedRangeTransaction: trans;



  columns = ['Date', 'Transaction']

  myOptions = {

    fontSize: 8,
    backgroundColor: 'black',
  };
  myData: any;
  enableCloseStatement: boolean;
  searchInput: any;
  profit: string;
  chat_data: any[];
  chart_data: any[];
  chart_start: string;
  chart_end: string;
  show_forecase: boolean;
  topSellingProduct: any[];
  topSellintestgProduct: {};
  total_cogs: number;



  constructor(
    private biSvc: BusinessIntelligenceService,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private manualOrderSvc: ManualOrderService,
    private toast: ToastrService,
    private route: ActivatedRoute,
    private router: Router,

  ) {
    this.total_discount = 0
    this.total_transaction_fee = 0
    this.total_bonus = 0
    this.total_product_discount = 0
    this.total_transaction = 0
    this.revenue = 0
    this.grossProfit = 0
    this.totalProfit = 0
    this.total_qty = 0
    this.range = {
      'begin': null,
      'end': null
    }
    this.shownRange = {
      'begin': null,
      'end': null
    }
  }



  manualOrderForm = new FormGroup({

    quote: new FormArray([], {
      validators: [Validators.required]
    }),
    profit: new FormControl(null),
    subtotal: new FormControl(null),
    extra_discount: new FormControl(null),
    total_transaction_fee: new FormControl(null),
    grand_total: new FormControl(null),
    quote_id: new FormControl(null),

  })

  isAdmin() {
    if (localStorage.getItem('isAdmin') == 'true') {
      return true
    }
    else if (this.activatedRoute.snapshot.queryParamMap.get('profit') == '1') {
      return true
    }


  }

  async ngOnInit() {

    var now = new Date();


    this.filterName = 'All'
    let today = new Date().getTime()
    let cutoff = new Date().setDate(new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate())
    this.showFilterMenu = false

    if (cutoff > today) {
      console.log("Past cutoff!")
      this.show_forecase = true
      let query = new Date()
      query.setMonth(query.getMonth() - 0)
      query.setDate(0)
      await this.query(query, new Date())


    } else {
      console.log("new cutoff!")
      let query = new Date().setDate(0)
      await this.query(query, today)
    }

    this.route.queryParamMap.subscribe(queryParams => {

      let source = queryParams.get("source")
      let keyword = queryParams.get("keyword")
      this.filterBySource(source)
      this.searchInput = keyword
      if (keyword) {

        this.filterByName(keyword)
      }


    })

  }




  async addStatement() {
    let date = new Date(this.endDate)
    var form = {
      'type': 'REVENUE',
      'name': 'SALES_ORDER',
      'amount': this.totalProfit,
      'statement_month': date.getMonth() + 1,
      'statement_year': date.getFullYear(),

    }

    // console.log(form)
    try {
      this.spinner.show();
      let response = await this.manualOrderSvc.addstatement(form)

      // console.log(response)
      this.toast.success('success close statement')

    } catch (e) {
      this.toast.error(e)
      console.log(e)
    } finally {
      this.spinner.hide();
      //  this.selectedItem = null
    }



  }


  async query(start?, end?) {

    if (this.range.begin) {
      this.show_forecase = false
    }
    this.searchInput = null
    this.filterName = 'All'
    let today = new Date()

    let dateStart = start == null ? new Date(this.range.begin) : new Date(start)
    let dateEnd = end == null ? new Date(this.range.end) : new Date(end)


    this.startDate = dateStart
    this.endDate = dateEnd

    if (dateStart.getTime() > new Date().getTime()) {
      this.toast.error("Start date cannot be later than today!")
      return
    }
    else if (dateEnd.getTime() > new Date().getTime()) {
      this.toast.error("End date cannot be later than today!")
      return
    }

    this.spinner.show();

    console.log(dateStart)
    // var lastDay = new Date(date.getDate());
    var from = formatDate(dateStart, 'yyyy-MM-dd', 'en_US')
    var to = formatDate(dateEnd, 'yyyy-MM-dd', 'en_US')


    try {
      this.selectedRangeTransaction = await await this.biSvc.getSales(from, to)
      console.log(this.selectedRangeTransaction)
      console.log('hi')

      if (dateEnd.getMonth() < today.getMonth()) {
        this.enableCloseStatement = true

      }



      this.grossProfit = 0
      this.totalProfit = 0
      this.total_qty = 0

      this.total_discount = 0
      this.total_transaction_fee = 0
      this.total_bonus = 0

      this.total_transaction = 0
      this.revenue = 0
      this.grossProfit = 0

      this.total_product_discount = 0


      this.total_transaction = this.selectedRangeTransaction.transactions.length

      this.source = []

      this.selectedRangeTransaction.transactions.forEach(
        (item) => {
          this.revenue += this.getRevenuePerItem(item)
          this.total_qty += parseInt(item.total_product)
          this.grossProfit += parseInt(item.gross_profit)
          this.totalProfit += this.getProfit(item)
          this.total_transaction_fee += this.getTransactionFee(item)
          this.total_bonus += this.getTotalBonus(item)
          this.total_discount += this.getTotalDiscount(item)
          this.total_product_discount += this.getTotalProductDiscount(item)

          if (!this.source.includes(item.source)) {
            this.source.push(item.source)
          }


        }
      )



      this.paginator.length = this.selectedRangeTransaction.transactions.length
      this.paginator.pageSize = 20
      this.shownTransaction = this.selectedRangeTransaction.transactions.slice(0, 20)
      this.paginator.firstPage()

      this.shownRange = {
        'begin': dateStart,
        'end': dateEnd
      }


      this.chart_data = this.selectedRangeTransaction.transactions
      this.chart_start = from
      this.chart_end = to


      this._setTopSelling(this.selectedRangeTransaction.transactions)





    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  _setTopSelling(items) {
    // this.topSellingProduct = []
    // items.forEach(item => {
    //  item.items.map(product => {

    //     this.topSellingProduct.push(product.product_id)

    //    }

    //  );
    // });

    let arr = [3, 2, 2, 1, 4, 5, 3, 2, 2, 1, 1];

    // Do some preprocessing first...
    this.topSellintestgProduct = arr.reduce((counts, num) => {
      counts[num] = (counts[num] || 0) + 1;
      return counts;
    }, {});

    console.log(this.topSellintestgProduct);


  }





  filterBySource(name) {
    this.filterName = name

    console.log(name)
    if (name) {


      this.filterResult = this.selectedRangeTransaction.transactions.filter(item => item.source === name);
      this.chart_data = this.selectedRangeTransaction.transactions.filter(item => item.source === name);

      console.log(this.chart_data)
      console.log('msk name')
    }


    else {
      this.filterResult = this.selectedRangeTransaction.transactions
      this.chart_data = this.selectedRangeTransaction.transactions
      console.log(this.chart_data)
    }

    this.grossProfit = 0
    this.totalProfit = 0
    this.total_qty = 0

    this.total_discount = 0
    this.total_transaction_fee = 0
    this.total_bonus = 0

    this.total_transaction = 0
    this.revenue = 0

    this.total_product_discount = 0
    this.total_transaction = this.filterResult.length

    this.filterResult.forEach(
      (item) => {
        this.revenue += this.getRevenuePerItem(item)
        this.total_qty += parseInt(item.total_product)
        this.grossProfit += parseInt(item.gross_profit)
        this.totalProfit += this.getProfit(item)
        this.total_transaction_fee += this.getTransactionFee(item)
        this.total_bonus += this.getTotalBonus(item)
        this.total_discount += this.getTotalDiscount(item)
        this.total_product_discount += this.getTotalProductDiscount(item)

      }
    )




    this.paginator.length = this.filterResult.length
    this.paginator.pageSize = 20
    this.shownTransaction = this.filterResult.slice(0, 20)
    this.paginator.firstPage()







  }



  searchKeyword() {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,

      queryParams: {
        keyword: this.searchInput


      },

      queryParamsHandling: 'merge',
      replaceUrl: true,

    });
    console.log(this.searchInput)
  }


  filterByName(name) {


    if (this.filterName == null) {
      this.filterResult = this.selectedRangeTransaction.transactions.filter(item =>
        (item.id.toLowerCase().includes(name.toLowerCase()) || this.getProductFilter(item, name))

      )
    }
    else {
      this.filterResult = this.selectedRangeTransaction.transactions.filter(item =>
        (item.id.toLowerCase().includes(name.toLowerCase()) || this.getProductFilter(item, name)) && item.source == this.filterName

      )
    }

    this.chart_data = this.filterResult



    // console.log(this.filterResult)



    this.grossProfit = 0
    this.totalProfit = 0
    this.total_qty = 0

    this.total_discount = 0
    this.total_transaction_fee = 0
    this.total_bonus = 0

    this.total_transaction = 0
    this.revenue = 0

    this.total_product_discount = 0
    this.total_transaction = this.filterResult.length

    this.filterResult.forEach(
      (item) => {
        this.revenue += this.getRevenuePerItem(item)
        this.total_qty += parseInt(item.total_product)
        this.grossProfit += parseInt(item.gross_profit)
        this.totalProfit += this.getProfit(item)
        this.total_transaction_fee += this.getTransactionFee(item)
        this.total_bonus += this.getTotalBonus(item)
        this.total_discount += this.getTotalDiscount(item)
        this.total_product_discount += this.getTotalProductDiscount(item)

      }
    )

    this.paginator.length = this.filterResult.length
    this.paginator.pageSize = 100
    this.shownTransaction = this.filterResult.slice(0, 100)
    this.paginator.firstPage()


  }

  getProductFilter(item, name) {
    var a = item.items.filter(product =>
      product.name.toLowerCase().includes(name.toLowerCase())
    )
    if (a.length > 0) {
      return true
    }
  }

  getMonhlyTarget(profit) {
    return (120000000 - profit) * -1
  }

  getForecast() {
    var now = new Date();


    var leftDay = this.endDate.getDate() - 0

    // console.log(this.startDate.getDate())
    var dailyTrans = this.totalProfit / leftDay
    var days = new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate()

    return dailyTrans * days
  }


  updatePage(e) {
    this.shownTransaction = this.selectedRangeTransaction.transactions.slice(
      (this.paginator.pageIndex) * this.paginator.pageSize,
      (this.paginator.pageIndex + 1) * this.paginator.pageSize,
    )
  }



  calcDiscount(item: Transaction) {
    return Math.abs(parseInt(item.voucher))
  }

  showFilter() {


    this.showFilterMenu = !this.showFilterMenu
  }




  getProductSold(item) {
    let products = item.items
    let total_product = 0
    products.forEach(item => {
      total_product += item.price
    })
  }

  getTransactionFee(item) {
    let products = item.items
    let total_transaction_fee = 0
    let voucher = 0


    if (this.getMidtrans(item.midtrans)) {

      return this.getMidtransTransactionFee(this.getMidtrans(item.midtrans)) / item.total_product

    }
    else {
      var discount = parseFloat((item.voucher < 0) ? item.voucher * -1 : item.voucher)

      products.forEach(item => {
        total_transaction_fee += ((item.transaction_fee / 100) * item.sell_price) * item.qty
        voucher += ((item.transaction_fee / 100) * (discount / item.qty))
      })
      return total_transaction_fee - (voucher ? voucher : 0)
    }

  }

  getRevenuePerItem(item) {
    let products = item.items
    let total_transaction_fee = 0
    let voucher = 0


    products.forEach(item => {
      total_transaction_fee += (item.sell_price * item.qty)

    })

    return total_transaction_fee

  }

  getMidtransTransactionFee(midtrans) {
    //console.log(midtrans)
    if (midtrans.payment_type == 'credit_card') {
      if (midtrans.installment_term) {
        return midtrans.gross_amount * (3 / 100)
      }
      else {
        return 0
      }


    }
    else {
      return 6000;
    }
  }

  getTotalBonus(item) {
    let products = item.items
    let bonus = 0
    products.forEach(item => {
      bonus += (item.bonus_value) * item.qty
    })


    return bonus

  }

  getTotalCashback(item) {
    let products = item.items
    let bonus = 0
    products.forEach(item => {
      bonus += (item.cashback) * item.qty
    })


    return bonus

  }

  getTotalDiscount(item) {

    var discount = parseFloat((item.voucher < 0) ? item.voucher * -1 : item.voucher)

    return discount ? discount : 0

  }

  getTotalProductDiscount(item) {
    let products = item.items
    let selisih = 0
    products.forEach(item => {
      selisih += (item.price - item.sell_price) * item.qty
    })

    return selisih

  }

  getVoucherTransactionFee(item) {
    var discount = parseFloat((item.voucher < 0) ? item.voucher * -1 : item.voucher)

    return discount
    //return item.voucher
  }

  addReview(product_id, order_id) {

    var link = 'https://wia.id/m/review/product/add/' + product_id + '?order_id=' + order_id
    console.log(link)

    window.open(link, "_blank");

  }



  getProfit(item) {
    //console.log(item)
    let products = item.items
    let purchase_order_item
    let order_price = 0
    let total_order_price = 0
    let sell_price = 0
    let bonus = 0
    let grandTotal = 0
    let voucher = 0


    voucher = parseFloat((item.voucher < 0) ? item.voucher * -1 : item.voucher)

    products.forEach(item => {
      if (item.purchase_order_item) {
        sell_price += parseFloat(item.sell_price) * item.qty

        //total_transaction_fee += ( (item.transaction_fee/100) * item.sell_price) * item.qty  
        purchase_order_item = this.getPurchaseOrder(item.purchase_order_item)

        if (purchase_order_item) {
          purchase_order_item.forEach(po => {
            order_price += (parseFloat(po.order_price) * (item.qty / purchase_order_item.length))

          });

        }


        grandTotal = sell_price - order_price - (voucher ? voucher : 0)


      }


    })

    if (purchase_order_item) {
      return (grandTotal - this.getTotalBonus(item)) + this.getTotalCashback(item)
    }
    else {
      return this.getGrossProfit(item)
    }








  }

  getGrossProfit(item) {
    // console.log(item)
    let products = item.items

    let gross_profit = 0
    let total_transaction_fee = 0
    let final_profit = 0
    let selisih = 0
    let bonus = 0



    var discount = parseFloat((item.voucher < 0) ? item.voucher * -1 : item.voucher)


    products.forEach(item => {
      gross_profit += (item.price * (item.margin / 100)) * item.qty
    })

    products.forEach(item => {
      total_transaction_fee += ((item.transaction_fee / 100) * item.sell_price) * item.qty
    })

    products.forEach(item => {
      selisih += (item.price - item.sell_price) * item.qty
    })

    final_profit = gross_profit - selisih - (discount ? discount : 0)

    return (final_profit - this.getTotalBonus(item) - this.getTransactionFee(item)) + this.getTotalCashback(item)


  }




  profitClass(item) {
    if (this.getProfit(item) < 0) {
      return 'non-profit'
    }
    else {
      let products = item.items
      let gross_profit = 0

      var profit = this.getProfit(item)


      products.forEach(item => {
        gross_profit += (item.price - (item.price * item.margin / 100)) / profit
      })
      var x = 100 - gross_profit
      if (x < 65) {
        return 'non-profit'
      } else {
        return 'profit-' + Math.ceil(x / 5) * 5;
      }

    }




  }

  cleanString(str) {
    str = str.replace('"[', '[');
    str = str.replace(']"', ']');
    return str;
  }

  getPurchaseOrder(purchase_order) {
    if (purchase_order) {
      var purchase_order
      var json = this.cleanString(purchase_order);

      //console.log(JSON.parse(json))
      return JSON.parse(json)
    }

  }

  getMidtrans(item) {
    if (item) {
      var json = this.cleanString(item);

      return JSON.parse(json)
    }


  }



  profitClass2(item) {
    let products = item.items
    let gross_profit = 0

    var profit = this.getProfit(item)


    products.forEach(item => {
      gross_profit += (item.price - (item.price * item.margin / 100)) / profit
    })
    var x = 100 - gross_profit

    return Math.ceil(x / 5) * 5;


  }

  editTransaction(i) {

    this.selectedTransaction = {
      index: i,
      transaction: this.selectedRangeTransaction.transactions[i]
    }


    let quoteArray = <FormArray>this.manualOrderForm.get('quote');

    this.selectedTransaction.transaction.items.forEach(
      (item) => {
        quoteArray.push(new FormGroup({
          // item_id: new FormControl(),
          // product_id: new FormControl(item.entity_id),
          name: new FormControl(item.name),
          // brand: new FormControl(),
          // request_quantity: new FormControl(1),
          // available_quantity: new FormControl(),
          // url_path: new FormControl(),
          // has_error: new FormControl(),
          margin: new FormControl(item.margin),
          transaction_fee: new FormControl(item.transaction_fee),
          // price: new FormControl(item.final_price),
          // custom_price: new FormControl(item.final_price),
          // featured_image: new FormControl(item.featured_image),
          // installment_term: new FormControl(item.installment),
          // product_custom_options_list: new FormControl(),
          // custom_options: new FormControl(),
        }))
      }
    )


    //console.log(this.selectedTransaction.items)
  }

  getQuote() {
    return this.manualOrderForm.get('quote').value
  }





  async updateOrder() {

    this.spinner.show();
    //console.log(this.selectedItem)

    var sub_total = 0
    this.selectedItem.items.forEach(item => {
      //console.log(item.price)
      sub_total += parseFloat(item.sell_price) * item.qty

    });



    this.selectedItem.grand_total = sub_total + parseFloat(this.selectedItem.shipping_cost) - parseFloat(this.selectedItem.voucher)
    this.selectedItem.sub_total = sub_total

    //console.log( this.selectedItem)
    try {


    } catch (e) {

      console.warn(e)

    } finally {
      this.spinner.hide();
      this.selectedItem = null
    }
  }




  toNumber(price) {
    return parseFloat(price)
  }

  calcProfit(item: Transaction) {



    return parseInt(item.gross_profit) - Math.abs(parseInt(item.voucher)) - Math.abs(parseInt(item.payment_fee))
  }

  printDate(date: Date) {
    if (date == null) return ''
    var monthNames = [
      "Jan", "Feb", "Mar",
      "Apr", "May", "Jun", "Jul",
      "Aug", "Sept", "Oct",
      "Nov", "Dec"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }

  rangeIsSelected() {
    return this.range.begin != null && this.range.end != null
  }


  viewDetails(item, i) {
    this.selectedItem = item
    this.selectedItemIndex = i

    console.log(item)

  }
  closeDetails() {
    this.selectedItem = null
  }


}
