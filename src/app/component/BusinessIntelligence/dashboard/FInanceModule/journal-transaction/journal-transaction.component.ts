import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { JournalAccountService } from "src/app/services/journal-account/journal-account.service";
import { ManualOrderService } from "src/app/services/manual-order/manual-order.service";

@Component({
  selector: "app-journal-transaction",
  templateUrl: "./journal-transaction.component.html",
  styleUrls: ["./journal-transaction.component.scss"],
})
export class JournalTransactionComponent implements OnInit, OnDestroy {
  constructor(
    private svc: JournalAccountService,
    private mnlSv: ManualOrderService
  ) {}

  data: { items: Array<any>; summary: Record<string, any> } = {
    summary: {},
    items: [],
  };

  form = new FormGroup({
    sales_order_id: new FormControl(""),
    purchase_id: new FormControl(""),
    warehouse_inventory_id: new FormControl(""),
    affiliate_transaction_id: new FormControl(""),
    reference_channel: new FormControl(""),
  });

  fetchAPI = () => {
    this.svc.getJournalTransaction(this.form.value).then((res) => {
      console.log(res);
      if (res.message_code === "success") {
        this.data = res.data;
      }
    });
  };

  timeoutFn: any;
  subscriber: Subscription = null;

  ngOnInit(): void {
    this.fetchAPI();

    this.subscriber = this.form.valueChanges.subscribe((res) => {
      clearTimeout(this.timeoutFn);
      this.timeoutFn = setTimeout(this.fetchAPI, 1000);
    });
  }

  ngOnDestroy(): void {
    this.subscriber.unsubscribe();
  }

  /*---*/
  searchTimeoutFn = null;
  referenceChannelOptionsResult = [];
  isLoadingReferenceChannelOptions = false;
  handleSearchReferenceChannel = (search) => {
    clearTimeout(this.searchTimeoutFn);
    this.isLoadingReferenceChannelOptions = true;
    //wrap this in setTimeout to prevent API call floods
    this.searchTimeoutFn = setTimeout(async () => {
      const [purchaseResult, salesOrderResult] = [
        await this.mnlSv.getPurchase(`?tab_view=all_purchase&search=${search}`),
        await this.mnlSv.getSalesOrderV2(`?search=${search}`),
      ];

      this.referenceChannelOptionsResult = [
        ...purchaseResult.data.map((item) => ({
          id: `${item.id}`,
          label: `#${item.id}`,
          prefix: "Purchase Order",
        })),
        ...salesOrderResult.data.map((item) => ({
          id: `${item.id}`,
          label: `#${item.id}`,
          prefix: "Sales Order",
        })),
      ];
      this.isLoadingReferenceChannelOptions = false;
    }, 1000);
  };
}
