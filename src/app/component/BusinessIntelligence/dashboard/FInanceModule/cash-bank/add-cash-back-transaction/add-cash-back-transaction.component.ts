import { Location } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import {
  faChevronLeft,
  faPaperPlane,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { ToastrService } from "ngx-toastr";
import { JournalAccountService } from "src/app/services/journal-account/journal-account.service";
import { ManualOrderService } from "src/app/services/manual-order/manual-order.service";

@Component({
  selector: "app-add-cash-back-transaction",
  templateUrl: "./add-cash-back-transaction.component.html",
  styleUrls: ["./add-cash-back-transaction.component.scss"],
})
export class AddCashBackTransactionComponent implements OnInit {
  isProcessing = false;
  faChevronLeft = faChevronLeft;
  faTrash = faTrash;
  faSend = faPaperPlane;

  constructor(
    private toastSvc: ToastrService,
    private activatedRoute: ActivatedRoute,
    private jrnlSvc: JournalAccountService,
    private location: Location,
    private mnlSv: ManualOrderService
  ) {}

  get transactionType() {
    return this.activatedRoute.snapshot.params.id;
  }

  get correspondingCashBankAccount() {
    return this.form.value.map((i) => i.value);
  }

  get actualTransactionType() {
    switch (this.transactionType) {
      case "make-payment":
        return "CREDIT";
      case "receive-payment":
        return "DEBIT";
    }
  }

  /*---*/
  private cashBankAccountOptions: { id: any; label: any }[] = [];
  cashBankAccountSearchState = "";
  get selectedCashBankAccountFullName() {
    try {
      const res = this.cashBankAccountOptions.find(
        (i) => this.selectedCashBankAccount.value === i.id
      );
      return res && res.label;
    } catch (e) {
      return "";
    }
  }
  get filteredCashBankAccountOptions() {
    return this.cashBankAccountOptions.filter((item) => {
      return item.label
        .toLowerCase()
        .includes(this.cashBankAccountSearchState.toLowerCase());
    });
  }

  handleBlurCashBankOptionField() {
    this.cashBankAccountSearchState = "";
  }
  handleSearchCashbackOptions(text) {
    this.cashBankAccountSearchState = text;
  }

  /*---*/
  private journalAccountOptions: { id: any; label: any }[] = [];
  journalAccountSearchState = "";
  get filteredJournalAccountOptions() {
    return this.journalAccountOptions.filter((item) => {
      return item.label
        ?.toLowerCase()
        .includes(this.journalAccountSearchState.toLowerCase());
    });
  }
  getJournalAccountNameFromId(id) {
    const res = this.journalAccountOptions.find((i) => i.id === id);
    return res && res.label;
  }

  /*---*/
  selectedCashBankAccount = new FormControl(null, {
    validators: [Validators.required],
  });

  form = new FormArray([]);
  get forms() {
    return this.form.controls as FormGroup[];
  }

  /*---*/
  searchTimeoutFn = null;
  referenceChannelOptionsResult = [];
  isLoadingReferenceChannelOptions = false;
  handleSearchReferenceChannel = (search) => {
    clearTimeout(this.searchTimeoutFn);
    this.isLoadingReferenceChannelOptions = true;
    this.searchTimeoutFn = setTimeout(async () => {
      const [purchaseResult, salesOrderResult] = [
        await this.mnlSv.getPurchase(`?tab_view=all_purchase&search=${search}`),
        await this.mnlSv.getSalesOrderV2(`?search=${search}`),
      ];

      this.referenceChannelOptionsResult = [
        ...purchaseResult.data.map((item) => ({
          id: `${item.id}`,
          label: `#${item.id}`,
          prefix: "Purchase Order",
        })),
        ...salesOrderResult.data.map((item) => ({
          id: `${item.id}`,
          label: `#${item.id}`,
          prefix: "Sales Order",
        })),
      ];
      this.isLoadingReferenceChannelOptions = false;
    }, 1000);
  };

  addEntry = () => {
    const form = new FormGroup({
      journal_account_id: new FormControl(null, {
        validators: [Validators.required],
      }),
      description: new FormControl(),
      value: new FormControl(null, {
        validators: [Validators.required, Validators.min(0)],
      }),
      reference_channel: new FormControl(),
    });

    this.form.push(form);
  };

  removeEntry = (index: number) => {
    this.form.removeAt(index);
  };

  get transactionTypeTitle() {
    switch (this.transactionType) {
      case "make-payment":
        //credit
        return "Make Payment";
      case "receive-payment":
        //debit
        return "Receive Payment";
      default:
        return "-";
    }
  }

  state = {
    showUnsupportedTransactionDialog: false,
  };

  handleGoBackToPreviousPage = () => {
    this.location.back();
  };

  ngOnInit(): void {
    if (!["make-payment", "receive-payment"].includes(this.transactionType)) {
      this.state.showUnsupportedTransactionDialog = true;
      return;
    }

    this.addEntry();
    this.jrnlSvc.getJournalAccount().then((res) => {
      const accounts = res.data;
      this.journalAccountOptions = accounts
        .filter((item) => item.type !== "CASH_BANK")
        .map((item) => {
          return {
            id: item.id,
            label: item.name ?? `(NO NAME) - #${item.id}`,
            prefix: item.type,
          };
        });
    });

    this.jrnlSvc.getJournalAccount().then((res) => {
      const accounts = res.data;
      this.cashBankAccountOptions = accounts
        .filter((item) => item.type === "CASH_BANK")
        .map((item) => {
          return {
            id: item.id,
            label: item.name ?? `(NO NAME) - #${item.id}`,
            prefix: item.type,
          };
        });
    });
  }

  get isRequestOverallValid() {
    return (
      this.form.valid &&
      this.form.value.length > 0 &&
      this.selectedCashBankAccount.valid
    );
  }

  handleSubmit() {
    if (!this.isRequestOverallValid) return;

    this.form.markAllAsTouched();
    this.selectedCashBankAccount.markAllAsTouched();

    this.isProcessing = true;
    this.jrnlSvc
      .addBankTransaction(
        this.selectedCashBankAccount.value,
        this.form.value,
        this.actualTransactionType
      )
      .then((res) => {
        console.log(res);
        if (res.message_code === "success") {
          this.toastSvc.success(
            "Your transaction has been submitted successfully. You can continue making another transaction or leave this page",
            "Request has been processed",
            {
              positionClass: "toast-top-center",
              timeOut: 5000,
            }
          );
          this.form.reset();
        } else {
          this.toastSvc.error(
            "Your transaction cannot be requested. Please try again",
            "We are unable to process your request",
            {
              positionClass: "toast-top-center",
              timeOut: 5000,
            }
          );
        }
      })
      .finally(() => {
        this.isProcessing = false;
      });
  }
}
