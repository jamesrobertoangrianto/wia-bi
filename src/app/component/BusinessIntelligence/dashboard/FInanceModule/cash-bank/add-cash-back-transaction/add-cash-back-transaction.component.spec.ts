import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCashBackTransactionComponent } from './add-cash-back-transaction.component';

describe('AddCashBackTransactionComponent', () => {
  let component: AddCashBackTransactionComponent;
  let fixture: ComponentFixture<AddCashBackTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCashBackTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCashBackTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
