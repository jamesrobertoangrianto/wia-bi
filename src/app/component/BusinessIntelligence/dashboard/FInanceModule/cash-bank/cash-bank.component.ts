import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { JournalAccountService } from "src/app/services/journal-account/journal-account.service";

@Component({
  selector: "app-cash-bank",
  templateUrl: "./cash-bank.component.html",
  styleUrls: ["./cash-bank.component.scss"],
})
export class CashBankComponent implements OnInit {
  accounts: any;
  total: number;
  showModal: any;

  constructor(
    private spinner: NgxSpinnerService,
    private jrnlSvc: JournalAccountService
  ) {}

  ngOnInit(): void {
    this.getChartAccoutsByType();
  }

  openModal(id) {
    this.showModal = id;
  }

  private async getChartAccoutsByType() {
    try {
      this.spinner.show();
      const resp = await this.jrnlSvc.getJournalAccount();
      const options = resp.data.filter((i) => i.type === "CASH_BANK") || [];
      console.log(options);

      this.accounts = options;

      this.total = options.reduce((accumulator, value) => {
        return accumulator + Number(value.balance);
      }, 0);
    } catch (e) {
      console.log(e);
    } finally {
      this.spinner.hide();
    }
  }
}
