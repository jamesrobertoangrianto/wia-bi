import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { Location } from "@angular/common";
import { ManualOrderService } from "src/app/services/manual-order/manual-order.service";

import {
  FormGroup,
  FormControl,
  FormArray,
  Validators,
  FormBuilder,
} from "@angular/forms";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-add-transaction",
  templateUrl: "./add-transaction.component.html",
  styleUrls: ["./add-transaction.component.scss"],
})
export class AddTransactionComponent implements OnInit {
  faChevronLeft = faChevronLeft;
  title: string;
  bank_accounts: any;
  accounts: any;
  journals: any[];
  type: string;
  bank_account_title: string;
  journal_account_title: string;
  payment_type: string;
  submit_button: string;
  isSubmitClick: boolean;

  constructor(
    private salesService: ManualOrderService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router: Router,
    private location: Location,
    private financeService: ManualOrderService
  ) { }

  manualOrderForm = new FormGroup({
    reference_number: new FormControl(""),
    reference_type: new FormControl(""),
    bank_account: new FormControl(null, {
      validators: [Validators.required],
    }),
    create_date: new FormControl(null, {
      validators: [Validators.required],
    }),
    accounts: new FormArray([], {
      validators: [Validators.required],
    }),
  });

  accountSearchKeywords: string = "";
  get filteredAccounts() {
    return this.accounts
      .filter((i) =>
        i.account_name
          .toLowerCase()
          .includes(this.accountSearchKeywords.toLowerCase())
      )
      .map((item) => {
        return {
          label: item.account_name,
          metadata: { ...item },
        };
      });
  }
  handleSearchAccount = (event) => {
    this.accountSearchKeywords = event;
  };
  handleChangeAccount = (event, index) => {
    const { metadata } = event;
    this.manualOrderForm.get(["accounts", index, "account"]).setValue(metadata);
  };
  getAccountName = (index) => {
    const value = this.manualOrderForm.get([
      "accounts",
      index,
      "account",
    ]).value;
    if (value) return value.account_name;
  };

  ngOnInit(): void {
    this.journals = [];
    console.log("haio");
    this.getBankAccounts();

    this.route.queryParamMap.subscribe((queryParams) => {
      this.payment_type = queryParams.get("payment_type");
      console.log(this.payment_type);
      if (this.payment_type == "expenses") {
        this.getChartAccoutsByExpenseType();
        this.manualOrderForm.get("reference_type").setValue(this.payment_type);
        this.submit_button = "Pay Expenses";
      } else if (this.payment_type == "account_payment") {
        this.manualOrderForm.get("reference_type").setValue(this.payment_type);
        this.getChartAccounts();
        this.submit_button = "Pay Account";
      } else {
        this.payment_type = "receive-payment";
        this.getChartAccounts();
        this.manualOrderForm.get("reference_type").setValue(this.payment_type);
        this.submit_button = "Receive Payment";
      }
    });
    this.route.paramMap.subscribe((params) => {
      let id = params.get("id");

      if (id === "make-payment") {
        this.type = "make_payment";
        this.title = "Make Payment";
        this.bank_account_title = "Pay From";
        this.journal_account_title = "Payment for";
      } else if (id === "receive-payment") {
        this.type = "receive_payment";
        this.title = "Receive Payment";
        this.bank_account_title = "Deposit Account";
        this.journal_account_title = "Receive Account";
      }
    });

    this.addMoreAccount();
    this.buildJournal();
  }

  buildJournal() {
    this.journals.push({
      account: "account",
      credit: "1000",
      debit: "00",
    });
  }

  async getBankAccounts() {
    try {
      this.spinner.show();
      let response = await this.financeService.getChartAccoutsByType();
      console.log(response);
      this.bank_accounts = response.accounts;
    } catch (e) {
      console.log(e);
    } finally {
      this.spinner.hide();
    }
  }

  async getChartAccounts() {
    try {
      this.spinner.show();
      let response = await this.financeService.getChartAccounts();
      console.log(response);
      this.accounts = response.accounts;
    } catch (e) {
      console.log(e);
    } finally {
      this.spinner.hide();
    }
  }

  async getChartAccoutsByExpenseType() {
    try {
      this.spinner.show();
      let response = await this.financeService.getChartAccoutsByExpenseType();
      console.log(response);
      this.accounts = response.accounts;
    } catch (e) {
      console.log(e);
    } finally {
      this.spinner.hide();
    }
  }

  addMoreAccount() {
    let quoteArray = <FormArray>this.manualOrderForm.get("accounts");
    quoteArray.push(
      new FormGroup({
        account: new FormControl(null),
        amount: new FormControl(null),
        send: new FormControl(null),
        account_description: new FormControl(null),
      })
    );
  }

  removeAccount(index) {
    let fArray = <FormArray>this.manualOrderForm.get("accounts");
    fArray.removeAt(index);
  }

  getQuote() {
    return this.manualOrderForm.get("accounts").value;
  }

  async submitForm() {
    // console.log(this.manualOrderForm.value)
    // if(this.manualOrderForm.valid){
    //   try {
    //     this.spinner.show();
    //      let response = await this.financeService.addExpense(this.manualOrderForm.value)
    //     console.log(response)
    //     } catch (e) {
    //       console.log(e)
    //     } finally {
    //       this.spinner.hide();

    //     }
    // }

    try {
      this.spinner.show();

      this.toast.success("success");
    } catch (e) {
      console.log(e);
    } finally {
      this.spinner.hide();
      this.manualOrderForm.reset();
      this.manualOrderForm.get("reference_type").setValue(this.payment_type);
    }
  }
}
