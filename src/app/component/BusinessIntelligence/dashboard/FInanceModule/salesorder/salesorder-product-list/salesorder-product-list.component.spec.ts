import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesorderProductListComponent } from './salesorder-product-list.component';

describe('SalesorderProductListComponent', () => {
  let component: SalesorderProductListComponent;
  let fixture: ComponentFixture<SalesorderProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesorderProductListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesorderProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
