import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-salesorder-product-list',
  templateUrl: './salesorder-product-list.component.html',
  styleUrls: ['./salesorder-product-list.component.scss']
})
export class SalesorderProductListComponent implements OnInit {
  status: string;
  start_date: string;
  end_date: string;
  month: any;
  asset: string;
  year: string;
  current_asset: string;
  current_month: any;
  current_year: any;
  sales_orders: any;
  products: any[];
  faSearch=faSearch
  fetch_products: any[];
  searchInput: any;
  salesOrderSummary: any;
  view: string;
  loss_profit: number;

  constructor(
    private financeService : ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  sourceList = [
    {
      label:'All',
      id:'0'
    },
    {
      label:'WIA_WEB',
      id:'1'
    },
    {
      label:'ONLINE_CHAT',
      id:'33'
    },
    {
      label:'OFFLINE_STORE',
      id:'32'
    },
    {
      label:'TOKPED_WIA',
      id:'8'
    },
    {
      label:'TOKPED_DS',
      id:'14'
    },
    {
      label:'TOKPED_KINTO',
      id:'37'
    },
    {
      label:'BLI_BLI_WIA',
      id:'11'
    },
    {
      label:'BLI_BLI_DS',
      id:'16'
    },
    {
      label:'SHOPEE_WIA',
      id:'9'
    },
   
   

    {
      label:'RETAILER',
      id:'31'
    },


    {
      label:'TIKTOK_WIA',
      id:'35'
    },
    {
      label:'TIKTOK_DM',
      id:'36'
    },
    {
      label:'B2B',
      id:'5'
    },
   
  ]


  tab_menu_list = [
    
    {
      'id':'all_product',
      'label' : 'All Order'
    },
    {
      'id':'bestseller',
      'label' : 'Bestseller'
    },
    {
      'id':'loss_profit',
      'label' : 'Loss Profit'
    },
    {
      'id':'most_profit',
      'label' : 'Most Profit'
    }
  
  
  ]
  ngOnInit(): void {
    this.salesOrderSummary = []
    var date = new Date();
    this.route.queryParamMap.subscribe(queryParams => {

     
      this.view = queryParams.get("tab_menu")

      this.start_date = queryParams.get("start_date")
      this.end_date = queryParams.get("end_date")
      this.month = queryParams.get("month")
      this.asset = queryParams.get("asset")
      this.year = queryParams.get("year")

      
      this.current_asset = this.asset
      this.current_month = this.month
      this.current_year = this.year


      if(!this.current_asset){
        this.current_asset ='0'
      }
      
      if(!this.view){
        this.view ='all_product'
      }


      if(!this.current_month){
        this.current_month = date.getMonth()
      }
     
      if(!this.current_year){
        this.current_year = date.getFullYear()
      }

     
      
      this.getSalesOrdersByInvoiceStatus(this.month,this.status,this.current_asset)
    })
  
  }

  async getSalesOrdersByInvoiceStatus(month,status,asset) {



   if( !this.start_date &&  !this.end_date){
    var date = new Date();
    var firstDay = new Date( parseInt(this.current_year) , parseInt(this.current_month) - 0,0);
    var lastDay = new Date( parseInt(this.current_year) , parseInt(this.current_month) + 1, 0);
    this.start_date = formatDate(firstDay, 'yyyy-MM-dd', 'en_US') 
    this.end_date = formatDate(lastDay, 'yyyy-MM-dd', 'en_US') 

   }
   


   

    try {
      this.spinner.show();
      let response = await this.financeService.getSalesOrdersByInvoiceStatus(status?status:'all',this.start_date,this.end_date,asset)
   //console.log(response)
   //  this.sales_orders = response.sales_orders
      this.fetch_products =  this._buildProductView(response.sales_orders)
      this.products = this.fetch_products
  
      if(this.view == 'bestseller'){
        this.products = this.sortByBestSelling(this.products)
      }

      if(this.view == 'loss_profit'){
      
        this.products = this.sortByLossProfit(this.products)
      }
      if(this.view == 'most_profit'){
        
        this.products = this.sortByMostProfit(this.products)
      }
      if(this.view == 'voucher_discount'){
      console.log('voucher')
        this.products = this.sortByVoucherDiscount(this.products)
      }
      console.log(this.products)

      this.buildSalesOrderSummary(this.products)
    
    } catch (e) {
      console.log(e)
    } finally {
     
      
      this.spinner.hide();
      
    }
  }


  search(){
    this.router.navigate( ['/sales-order/product-list'], {
          queryParams: { 
            start_date: this.start_date,
            end_date: this.end_date,
            asset: this.asset
           },
          queryParamsHandling:'merge',
        }
      );
    console.log(this.start_date)
    console.log(this.end_date)
    console.log(this.asset)
  }

  sortByBestSelling(arr){

    arr.sort((a,b) => {
      return (a.product_id.localeCompare(b.product_id))
    
     })
     
    let counts = arr.reduce((counts, num) => {
      counts[num.product_id] = (counts[num.product_id] || 0) + 1;
      return counts;
    }, {});

    console.log(counts);
   
    

    arr.sort((a,b) => {
     return (counts[b.product_id] - counts[a.product_id])
   
    })


  
    return arr
  }

  sortByVoucherDiscount(arr){

    arr.sort((a,b) => {
      if(a.coupon_code){
        return (a.coupon_code.localeCompare(b.coupon_code))
      }
     
    
     })
     
    let counts = arr.reduce((counts, num) => {
      
      if(!num.coupon_code){
        num.coupon_code = num.product_id + 1
      }
      
      counts[num.coupon_code] = (counts[num.coupon_code] || 0) + 1;
      
     
      return counts;
    }, {});

    

    

    arr.sort((a,b) => {
      if(counts[b.coupon_code]){
        return (counts[b.coupon_code] - counts[a.coupon_code])
      }
    
   
    })
    console.log(arr);

  
    return arr
  }

  getBonusItemName(item){
   
    if(item.bonus_item){
      if(this.isJsonString(item.bonus_item)){
           let name = JSON.parse(item.bonus_item)
           item.bonus_item = name.name
          return name.name
      }
      else{
        return item.bonus_item
      }
     
     
      
    }else{
      return null;
    }
   
  }

   isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
  sortByLossProfit(arr){
    console.log('filter')
    arr.sort((a,b) => {
      return (a.gross_profit - b.gross_profit)
    
     })
     
  
    return arr
  }

  sortByMostProfit(arr){
    console.log('filter')
    arr.sort((a,b) => {
      return (b.gross_profit - a.gross_profit)
    
     })
     
  
    return arr
  }


  _buildProductView(sales_orders){
   
    var products = []

    sales_orders.forEach(item => {
      item.products.forEach(product => {

       // console.log(product)
        
        let gross_revenue = (product.sell_price * product.qty) + (item.transaction_fee_income * product.qty) + ( (item.shipping_cost / item.total_product)* product.qty) 
        let gross_profit =  gross_revenue  - product.total_cogs - (product.total_discount_amount) -  (product.bonus_value* product.qty) - ( product.transaction_fee * product.qty) 
        
        products.push({
          //'item' : item,
          'date' :item.order_date,
          'id' : product.id,
          'order_id' :item.increment_id,
          'invoice_number' :item.invoice_number,
          'source' :item.customer_group,
          'image' :product.image,
          'customer' :item.shipping_address.firstname,
          'payment_method' :this._getPaymentMethod(item.midtrans),
          'product_name' : product.name,
          'brand' : 'brand',
          'cogs' : product.total_cogs,
          'po' : JSON.parse(product.purchase_order_item),
          'promo_discount_amount' : product.promo_discount_amount,
          'total_discount_amount' : product.total_discount_amount,
          'qty' : product.qty,
          'order_qty' : item.total_product,
          'product_id' : product.product_id,
          'sell_price' : product.sell_price,
          'sell_price_x_qty' : product.sell_price * product.qty,
          'current_total_transaction_fee' : item.transaction_fee,
          'transaction_fee_percentage' : ' ',
          'transaction_fee' :  product.transaction_fee,
          'transaction_fee_income' :item.transaction_fee_income,
          'transaction_fee_income_per_item' :item.transaction_fee_income / item.total_product,
          'bonus_value' :  product.bonus_value?product.bonus_value:0,
          'bonus_item' :  product.bonus_item,
          'coupon_code' :  product.promo_code,
          'shipping_cost' :  item.shipping_cost / item.total_product,
          'shipping_method' : item.shipping_method,
        
          'gross_profit' : product.total_cogs?gross_profit:0 ,
          'lost_profit' : this._checkLost(gross_profit),
          

    
        })
      });
    });


    return products
 
  }

  _checkLost(gross_profit){
    if(gross_profit<0){
      return true
    }
    else{
      return false
    }
  }


  _getProductCogs(items){

    //console.log(items)
    let cogs = 0
   let qty = items?.length
    if(items){
      items.forEach(item => {
       // console.log(item)
        cogs += parseFloat(item.order_price)
      });
  
    }
   if(qty){
    return cogs / qty

   }else{
    return 0
   }
   
 

 
  }


 
  changeMonth(value){
    
    this.router.navigate( ['/sales-order/product-list'], {
          queryParams: { month: value },
          queryParamsHandling:'merge',
        }
      );
    
  }
  changeAsset(value){
  
    // this.router.navigate( ['/sales-order/product-list'], {
    //       queryParams: { asset: value },
    //       queryParamsHandling:'merge',
    //     }
    //   );

    this.asset = value
    
  }
  changeYear(value){
    
    this.router.navigate( ['/sales-order/product-list'], {
          queryParams: { year: value },
          queryParamsHandling:'merge',
        }
      );
    
  }

  _getPaymentMethod(item){
    if(item){
      var payment = JSON.parse(item)
      if(payment){
        if(payment?.payment_type =='credit_card'){
          return payment.payment_type + '-' + payment.bank + '-' + payment.installment_term
        }
        else{
          return payment.payment_type
        }
      }
     
    
    }
   
  }

  updateDate(){
   

    this.router.navigate( ['/sales-order/product-list'], {
      queryParams: { 
        start_date: this.start_date,
        end_date: this.end_date 
      },
      queryParamsHandling:'merge',
    }
  );
  }
  
  searchKeyword(){
    
    
    this.products = this.fetch_products.filter(item =>
      item.product_name.toLowerCase().includes(this.searchInput) ||
      item.customer.toLowerCase().includes(this.searchInput) ||
      
      item.coupon_code?.toLowerCase().includes(this.searchInput) ||
      item.order_id.toLowerCase().includes(this.searchInput)
    
    )

    this.buildSalesOrderSummary(this.products)
  }

  buildSalesOrderSummary(products){
  

    let gross_profit = 0

    let revenue = 0
    let cogs = 0
    let qty = 0
    let bonus = 0
    let discount = 0
    let trx_fee_income = 0
    let trx_fee_expense = 0
    let total_shipping_cost = 0
    
    products.forEach(item => {
     
      gross_profit += item.gross_profit
     revenue +=  parseFloat(item.sell_price_x_qty)
     cogs +=  parseFloat(item.cogs?item.cogs:0)
     qty += parseFloat(item.qty)
     bonus +=  parseFloat(item.bonus_value)  * item.qty
     discount +=  parseFloat(item.total_discount_amount) 
     trx_fee_income +=  parseFloat(item.transaction_fee_income_per_item)  * item.qty
     trx_fee_expense +=  parseFloat(item.transaction_fee)  * item.qty
     total_shipping_cost +=  parseFloat(item.shipping_cost)  * item.qty
    });

    this.salesOrderSummary.gross_revenue = revenue
    this.salesOrderSummary.total_cogs = cogs
    this.salesOrderSummary.total_qty = qty
    this.salesOrderSummary.total_bonus = bonus
    this.salesOrderSummary.total_sales_discount = discount

    this.salesOrderSummary.total_shipping_cost = total_shipping_cost
    this.salesOrderSummary.total_trx_fee_income = trx_fee_income
    this.salesOrderSummary.total_trx_fee = trx_fee_expense
    this.salesOrderSummary.total_loss_profit =  this._getLossProfit()
    this.salesOrderSummary.gross_profit = gross_profit


    
  }

  _getLossProfit(){
    this.loss_profit = 0
    this.products.forEach(item => {
      if(item.gross_profit < 0){
        this.loss_profit += Math.abs(item.gross_profit) 
      }
     
    })

    return this.loss_profit
  }

  

  del(item){
    var index = this.products.indexOf(item);
    this.products.splice(index, 1);
    this.buildSalesOrderSummary(this.products)
  
  }


  async saveEdit(item){
  //  console.log(item)
 
     try {
       this.spinner.show();
        // console.log(item)
       let response = await this.financeService.updateSalesOrderTransactionFee(item)
       this.buildSalesOrderSummary(this.products)
      
       
       } catch (e) {
         console.log(e)
       } finally {
        this.toggleEdit(item)
         this.spinner.hide();
       }
    //this.buildSalesOrderSummary(this.products)
  
  }

  toggleEdit(item){
    item.edit = !item.edit
  }







}
