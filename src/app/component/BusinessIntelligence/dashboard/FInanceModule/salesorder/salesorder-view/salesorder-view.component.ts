import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { iif } from 'rxjs';

@Component({
  selector: 'app-salesorder-view',
  templateUrl: './salesorder-view.component.html',
  styleUrls: ['./salesorder-view.component.scss']
})
export class SalesorderViewComponent implements OnInit {
  @Output() onClose = new EventEmitter()
  @Input() selected_order: any
  faChevronLeft = faChevronLeft
  order: any;
  order_id: string;
  accounts: any;
  showModal: any;

  receivePaymentForm = new FormGroup({
    order_id: new FormControl(null),
    deposit_account_id: new FormControl(null, { validators: [Validators.required] }),
    receive_amount: new FormControl(null),
    revenue: new FormControl(null),
    split_payment: new FormControl(null),
    transaction_fee_income: new FormControl(null),
    create_date: new FormControl(null),
    invoice: new FormControl(null),

    cogs: new FormControl(null),
    bonus_value: new FormControl(null),
    inventory: new FormControl(null, { validators: [Validators.required] }),
    transaction_fee: new FormControl(null),
    tax: new FormControl(null),
    shipping_discount: new FormControl(null),

    shipping: new FormControl(null),
    sales_discount: new FormControl(null),
  })
  journal_transactions: any;
  journal_transactions_debit: number;
  journal_transactions_credit: number;
  inventory: number;

  total_cashbank: number;
  transaction_fee: any;
  bonus_value: any;
  total_profit: number;
  inventory_items: any;
  bonus: any;
  allowClose: boolean;

  constructor(
    private financeService: ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private location: Location,
    private toast: ToastrService,
  ) { }

  ngOnInit(): void {
    if (this.selected_order) {
      this.order_id = this.selected_order.entity_id

      this.getSalesOrdersByInvoiceStatus(this.order_id)
      this.getChartAccoutsByType()

    }

  }

  openModal(id) {
    this.showModal = id
  }





  async getSalesOrdersByInvoiceStatus(id) {


    try {
      this.spinner.show();
      let response = await this.financeService.getSalesOrderById(id)

      console.log(response)
      this.order = response.order
      this.getInventoryByOrder(this.order.increment_id)

      this.receivePaymentForm.get('create_date').patchValue(this.order.order_date)
      this.receivePaymentForm.get('invoice').patchValue(this.order.increment_id)
      this.receivePaymentForm.get('order_id').patchValue(this.order_id)
      this.receivePaymentForm.get('shipping').patchValue(this.order.shipping_cost)
      this.receivePaymentForm.get('revenue').patchValue(this.order.subtotal)

      this.receivePaymentForm.get('split_payment').patchValue(this.order.split_payment)
      this.receivePaymentForm.get('transaction_fee_income').patchValue(this.order.transaction_fee_income)




      this.receivePaymentForm.get('sales_discount').patchValue(Math.abs(this.order.discount ? this.order.discount : 0))




      this._getProductItems(this.order.products)
      if (this.order.midtrans) {
        //  console.log( JSON.parse(this.order.midtrans))
        this.order.midtrans = JSON.parse(this.order.midtrans)

      }
      if (this.order.products) {
        this._toJson(this.order.products)
      }

      this.total_cashbank = this.getTotalCashBank()


      this.selected_order.invoice_number = this.order.invoice_number
      this.selected_order.journal_entry = this.order.journal_entry
      this.selected_order.gross_profit = this.total_profit



      this.receivePaymentForm.get('receive_amount').patchValue(this.total_cashbank)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();




    }
  }



  async getInventoryByOrder(id) {
    // console.log(this.params)
    let params = ''
    try {
      this.spinner.show();
      let response = await this.financeService.getInventoryByOrder(id, params)
      console.log(response)
      this.inventory_items = response.data
      this.inventory = 0
      this.bonus_value = 0
      this.inventory_items.forEach(element => {
        if (element.type == 'BONUS') {
          this.bonus_value += element.cogs

        } else {
          this.inventory += element.cogs

        }
      });

      if (this.inventory) {
        this.receivePaymentForm.get('inventory').patchValue(this.inventory + this.bonus_value)
        this.receivePaymentForm.get('cogs').patchValue(this.inventory)
        this.receivePaymentForm.get('bonus_value').patchValue(this.bonus_value)
      }



      this.updateCogs()



    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  updateCogs() {

    this.order.products.forEach(item => {
      item.total_cogs = this.getTotalCogs(item.product_id)
      let form = {
        'id': item.id,
        'total_cogs': item.total_cogs
      }
      let response = this.financeService.updateSalesOrderTransactionFee(form)

    });

  }


  getTotalCogs(id) {
    let total = 0
    this.inventory_items.forEach(element => {
      if (element.product_id == id) {
        total += element.cogs

      }
    });

    return total
  }

  getTotalProfit() {
    let credit = parseFloat(this.order.subtotal) + parseFloat(this.order.shipping_cost) + parseFloat(this.order.split_payment ? this.order.split_payment : 0) + parseFloat(this.order.transaction_fee_income ? this.order.transaction_fee_income : 0)
    let debit = Math.abs(this.order.discount ? this.order.discount : 0) + parseFloat(this.transaction_fee ? this.transaction_fee : 0) + parseFloat(this.bonus_value ? this.bonus_value : 0)

    return credit - debit - this.inventory
  }


  getTotalCashBank() {
    let credit = parseFloat(this.order.subtotal) + parseFloat(this.order.shipping_cost) + parseFloat(this.order.split_payment ? this.order.split_payment : 0) + parseFloat(this.order.transaction_fee_income ? this.order.transaction_fee_income : 0)
    let debit = Math.abs(this.order.discount ? this.order.discount : 0) + this.transaction_fee
    return credit - debit
  }



  _getProductItems(items) {
    let inventory = 0
    let transaction_fee = 0
    let bonus_value = 0
    items.forEach(item => {


      transaction_fee += (parseFloat(item.transaction_fee) * item.qty)


    });







    this.transaction_fee = transaction_fee




    this.receivePaymentForm.get('transaction_fee').patchValue(transaction_fee)





  }

  async updateTransactionFee(item, e) {
    item[e.id] = e.value
    console.log(item)

    try {
      this.spinner.show();

      let response = await this.financeService.updateSalesOrderTransactionFee(item)


      this.getSalesOrdersByInvoiceStatus(this.order_id)
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.toast.success('success')
    }
  }


  _getInventoryItems() {
    //  let total = 0
    //   this.inventory_items.forEach(item => {
    //     console.log(item)
    //    // total += item.cogs
    //   });

    return 12300
  }



  async getChartAccoutsByType() {

    try {
      this.spinner.show();
      let response = await this.financeService.getChartAccoutsByType()

      this.accounts = response.accounts


    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async receivePayment() {

    this.receivePaymentForm.get('deposit_account_id').setValue('26')
    // console.log(this.receivePaymentForm.value)
    if (this.receivePaymentForm.valid) {
      try {
        this.spinner.show();
        let response = await this.financeService.addReceivePayment(this.receivePaymentForm.value)



      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
        this.showModal = null
        this.getSalesOrdersByInvoiceStatus(this.order_id)
        this.toast.success('Payment Received!')


        // this.selected_order.transaction_fee = this.transaction_fee
        // this.selected_order.bonus = this.bonus_value
        // this.selected_order.gross_profit = this.total_profit



        // this.onClose.emit(this.selected_order)

      }
    }
    else {
      this.toast.success('sales not complete')

    }

  }

  closeModal() {

    this.selected_order.transaction_fee = this.transaction_fee
    this.selected_order.bonus = this.bonus_value
    this.selected_order.gross_profit = this.getTotalProfit()
    this.onClose.emit(this.selected_order)
  }






  _toJson(items) {
    // return  this.order.midtrans =  JSON.parse(this.order.midtrans)
    items.forEach(item => {
      item.purchase_order_item = JSON.parse(item.purchase_order_item)
    });
  }


}
