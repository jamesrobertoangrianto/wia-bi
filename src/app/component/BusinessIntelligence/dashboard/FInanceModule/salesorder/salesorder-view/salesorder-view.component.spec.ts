import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesorderViewComponent } from './salesorder-view.component';

describe('SalesorderViewComponent', () => {
  let component: SalesorderViewComponent;
  let fixture: ComponentFixture<SalesorderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesorderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesorderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
