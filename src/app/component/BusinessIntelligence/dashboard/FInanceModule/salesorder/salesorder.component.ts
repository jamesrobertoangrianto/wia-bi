import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute,Router } from '@angular/router';


import { formatDate } from '@angular/common';
import { Location } from '@angular/common'
import { faArrowRight, faChevronCircleDown, faChevronDown, faChevronLeft, faChevronUp, faDotCircle, faSearch, faShip, faShippingFast, faTruck } from '@fortawesome/free-solid-svg-icons';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-salesorder',
  templateUrl: './salesorder.component.html',
  styleUrls: ['./salesorder.component.scss']
})
export class SalesorderComponent implements OnInit {
  faChevronDown= faChevronDown
  faArrowRight=faArrowRight
  faShip=faTruck
  faSearch=faSearch
  faDotCircle=faDotCircle
  faChevronUp= faChevronUp
  sales_orders: any;
  accounts: any;
  status: string;
  month: string;
  current_month: any;
  total: number;
  total_title: string;
  year: string;
  current_year: any;
  total_cogs: number;
  total_transaction_fee: number;
  total_sales_discount: number;
  total_bonus: number;
  total_discount: number;
  total_profit: number;
  loss_profit: number;
  total_shipping_cost: any;
  test: any[];
  shipping_cost: number;
  total_journal_result: number;
  excel: any[];
  product_view: any[];
  isShowProductView: boolean;
  table_view: string;
  searchInput: any;
  fetch_product_view: any[];
  fetch_sales_orders: any;
  asset: string;
  current_asset: string;

  start_date :any
  end_date :any
  month_view :any
  total_transaction_fee_income: number;
  total_trx: number;
  selected_order: any;
  showModal: boolean;

  constructor(
      
    private financeService : ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) { }

  years=
  [ 
      {
        value : 2022,
        name : '2022'
      },
      {
        value : 2023,
        name : '2023'
      },
      {
        value : 2024,
        name : '2024'
      },
      {
        value : 2025,
        name : '2025'
      },
      {
        value : 2026,
        name : '2026'
      },
      
  ]

  sourceList = [
    {
      label:'All',
      id:'0'
    },
    {
      label:'WIA_WEB',
      id:'1'
    },
    {
      label:'ONLINE_CHAT',
      id:'33'
    },
    {
      label:'OFFLINE_STORE',
      id:'32'
    },
    {
      label:'TOKPED_WIA',
      id:'8'
    },
    {
      label:'TOKPED_DS',
      id:'14'
    },
    {
      label:'TOKPED_KINTO',
      id:'37'
    },
    {
      label:'BLI_BLI_WIA',
      id:'11'
    },
    {
      label:'BLI_BLI_DS',
      id:'16'
    },
    {
      label:'SHOPEE_WIA',
      id:'9'
    },
   
   

    {
      label:'RETAILER',
      id:'31'
    },


    {
      label:'TIKTOK_WIA',
      id:'35'
    },
    {
      label:'TIKTOK_DM',
      id:'36'
    },
    {
      label:'B2B',
      id:'5'
    },
   
  ]

  months=
  [ 
      {
        value : 0,
        name : 'January'
      },
      {
        value : 1,
        name : 'February'
      },
      {
        value : 2,
        name : 'March'
      },
      {
        value : 3,
        name : 'April'
      },
      {
        value : 4,
        name : 'May'
      },
      {
        value : 5,
        name : 'June'
      },
      {
        value : 6,
        name : 'July'
      },
      {
        value : 7,
        name : 'August'
      },
      {
        value : 8,
        name : 'September'
      },
      {
        value : 9,
        name : 'October'
      },
      {
        value : 10,
        name : 'November'
      },
      {
        value : 11,
        name : 'December'
      },
      
  ]

  tab_menu_list = [
    
    {
      'id':'all',
      'label' : 'All Order'
    },
    {
      'id':'open_invoice',
      'label' : 'Open Invoice'
    },
    {
      'id':'payment_receive',
      'label' : 'Payment Receive'
    }
  
  ]
  ngOnInit(): void {
  
      var date = new Date();
    
      this.month_view = false
      this.route.queryParamMap.subscribe(queryParams => {

      this.status = queryParams.get("tab_view")
      
      this.table_view = queryParams.get("view")

      if(this.status == 'open_invoice'){
        this.total_title = 'Open Invoice'
      }
      else if(this.status == 'payment_receive'){
        this.total_title = 'Payment Receive'
      }
      else{
        this.total_title = 'All Order'
      }
      this.start_date = queryParams.get("start_date")
      this.end_date = queryParams.get("end_date")
      this.month = queryParams.get("month")
      this.asset = queryParams.get("asset")
      this.year = queryParams.get("year")

      
      this.current_asset = this.asset
      this.current_month = this.month
      this.current_year = this.year
      if(!this.table_view){
        this.table_view = 'order'
      }

      if(!this.current_asset){
        this.current_asset ='0'
      }

      // if(!this.current_month){
      //   this.current_month = date.getMonth()
      // }
     
      // if(!this.current_year){
      //   this.current_year = date.getFullYear()
      // }

      if(this.start_date && this.end_date){
        this.getSalesOrdersByInvoiceStatus(this.month,this.status,this.current_asset)

      }
     
      
    // this.getSalesOrdersByInvoiceStatus(this.month,this.status,this.current_asset)
      this.searchInput = null
     
    })
   
  }

 
  changeMonth(value){

    // this.router.navigate( ['/sales-order'], {
    //       queryParams: { month: value },
    //       queryParamsHandling:'merge',
    //     }
    //   );
    this.month = value
    
  }
  changeAsset(value){
  
    // this.router.navigate( ['/sales-order'], {
    //       queryParams: { asset: value },
    //       queryParamsHandling:'merge',
    //     }
    //   );
    this.asset = value
    
  }
  changeYear(value){
    
    // this.router.navigate( ['/sales-order'], {
    //       queryParams: { year: value },
    //       queryParamsHandling:'merge',
    //     }
    //   );
    this.year = value
  }

  search(){
    this.router.navigate( ['/sales-order'], {
          queryParams: { 
            start_date: this.start_date,
            end_date: this.end_date,
            asset: this.asset
           },
          queryParamsHandling:'merge',
        }
      );
    console.log(this.start_date)
    console.log(this.end_date)
    console.log(this.asset)
  }
  async getSalesOrdersByInvoiceStatus(month,status,asset) {
    this.sales_orders = []
    this.isShowProductView = false
   


   if( !this.start_date &&  !this.end_date){
    var date = new Date();
    var firstDay = new Date( parseInt(this.current_year) , parseInt(this.current_month) - 0,0);
    var lastDay = new Date( parseInt(this.current_year) , parseInt(this.current_month) + 1, 0);
    this.start_date = formatDate(firstDay, 'yyyy-MM-dd', 'en_US') 
    this.end_date = formatDate(lastDay, 'yyyy-MM-dd', 'en_US') 

   }
   


   

    try {
    this.spinner.show();
     let response = await this.financeService.getSalesOrdersByInvoiceStatus(status?status:'all',this.start_date,this.end_date,asset)
   console.log(response)
    this.sales_orders = response.sales_orders
    this.fetch_sales_orders =  response.sales_orders



    } catch (e) {
      console.log(e)
    } finally {
     
      this._buildSalesOrder(this.sales_orders)
      this.spinner.hide();
      
    }
  }

  _buildSalesOrder(sales_orders){
    this.total = 0
    this.total_transaction_fee = 0
    this.total_cogs = 0
    this.total_bonus = 0
    this.total_sales_discount = 0
    this.total_transaction_fee_income = 0
    this.total_trx = 0
    this.test = []
    let test = 0

    if(sales_orders){
      sales_orders.forEach(item => {
        this.total +=  parseFloat(item.subtotal) 
      
        this.test.push(parseFloat(item.shipping_cost))
        test +=parseFloat(item.shipping_cost)
        this.total_cogs +=  this._getProductCogs(item.products)
        this.total_transaction_fee += this._getProductTransactionFee(item.products)
        this.total_bonus += this._getProductBonus(item.products)
        this.total_trx += 0
      
        this.total_sales_discount += Math.abs( parseFloat(item.discount))
        item.transaction_fee = this._getProductTransactionFee(item.products)
        item.bonus = this._getProductBonus(item.products)
        item.cogs = this._getProductCogs(item.products)
       //. item.show= true
        if(item.cogs){
          item.gross_profit = (item.subtotal - item.discount - item.cogs - item.transaction_fee - item.bonus) + parseFloat(item.shipping_cost) + parseFloat(item.transaction_fee_income)

        }

        this.total_transaction_fee_income += parseFloat(item.transaction_fee_income)
      });
      
   
   
    this.shipping_cost = test
    this._getProfit()
    this._getLossProfit()
  
    if(!this.table_view){
      this._buildProductView()
    }
    
    }

   
   
    
  }

  _buildProductView(){
   
    this.isShowProductView = true
    this.product_view = []

    this.sales_orders.forEach(item => {
      item.products.forEach(product => {
        this.product_view.push({
        // 'item' : item,
          'date' :item.order_date,
          'id' : product.id,
          'order_id' :item.increment_id,
          'invoice_number' :item.invoice_number,
          'source' :item.customer_group,
          'customer' :item.shipping_address.firstname +' '+item.shipping_address.lastname,
          'payment_method' :this._getPaymentMethod(item.midtrans),
          'product_name' : product.name,
          'qty' : product.qty,
          'sell_price' : product.sell_price,
          'current_total_transaction_fee' : item.transaction_fee,
          'transaction_fee_percentage' : ' ',
          'transaction_fee' : item.transaction_fee,
          'bonus_value' :  item.bonus,
    
        })
      });
    });
 
    
   

    this.fetch_product_view = this.product_view
  }

  searchKeyword(e){
  
    this.sales_orders = this.fetch_sales_orders.filter(item =>
      
      (  this.getProductFilter(item, e) )
       
      )

      console.log(this.sales_orders)

     
  
    this._buildSalesOrder(this.sales_orders)
  }

  getProductFilter(item,name){
    var a = item.products.filter(product =>
      product.name.toLowerCase().includes(name.toLowerCase())
    )
    if(a.length>0){
      return true
    }
  }




  async updateOrder(item){
   

    try {
      this.spinner.show();

      let response = await this.financeService.updateSalesOrderTransactionFee(item)

      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
      }
  }

  _getPaymentMethod(item){
    if(item){
      var payment = JSON.parse(item)
      if(payment.payment_type =='credit_card'){
        return payment.payment_type + '-' + payment.bank + '-' + payment.installment_term
      }
      else{
        return payment.payment_type
      }
    
    }
   
  }

   syntaxHighlight(json) {
    if (typeof json != 'string') {
         json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return  match ;
    });
}

salesViewCallBack(e){
// this.selected_order = e
// this.selected_order.invoice_number = e.invoice_number
// this.selected_order.journal_entry = e.journal_entry
//   let indexToUpdate = this.sales_orders.findIndex(item => item.entity_id === e.entity_id);
//   this.sales_orders[indexToUpdate] = e;

//   console.log(this.sales_orders)
// this.selected_order = null

}

  del(item){
    var index = this.sales_orders.indexOf(item);
    this.sales_orders.splice(index, 1);
    this._buildSalesOrder(this.sales_orders)
  
  }
  _getLossProfit(){
    this.loss_profit = 0
    this.sales_orders.forEach(item => {
      if(item.gross_profit < 0){
        this.loss_profit += Math.abs(item.gross_profit)
      }
     
    })
  }
  _getProfit(){
    if( this.total){
      this.total_profit = 
      this.total - 
      this.total_cogs - 
      this.total_sales_discount - 
      this.total_transaction_fee - 
      this.total_bonus +
      this.shipping_cost + this.total_transaction_fee_income
    }
  }

  goToJournal(id){
    this.router.navigate(
      ['/chart-account/view/'+id],
      { queryParams: { year :this.current_year, month:this.current_month} }
    )
    
  }


  updateDate(){


  //   this.router.navigate( ['/sales-order'], {
  //     queryParams: { 
  //       start_date: this.start_date,
  //       end_date: this.end_date 
  //     },
  //     queryParamsHandling:'merge',
  //   }
  // );
  }
  async validateJournal(id){
    this.total_journal_result = null
    var date = new Date();
    var firstDay = new Date( parseInt(this.current_year) , parseInt(this.current_month) - 0,0);
    var lastDay = new Date( parseInt(this.current_year) , parseInt(this.current_month) + 1, 0);

    var from = formatDate(firstDay, 'yyyy-MM-dd', 'en_US') 
    var to = formatDate(lastDay, 'yyyy-MM-dd', 'en_US') 

    try {
    this.spinner.show();
     let response = await this.financeService.getChartAccoutsById(id,from,to)


     let res = 0
     this.sales_orders.forEach(item => {
      if(this._validateId(id,item)){
        res +=  this._checkData(response.transactions,item.entity_id)
       // var result = this._checkData(response.transactions,item.entity_id)
        
       
      }
    
      
      });

      this.total_journal_result = res

     
     
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      
    }
    
    
    
  }

  _validateId(id,item){
    let item_value = 0
    
    switch(id) {
      case 41:
        item_value = item.subtotal
        break;
      case 39:
        item_value = item.cogs
        break;
      case 122:
      item_value = item.bonus
      break;
      case 73:
      item_value = item.discount
      break;

      case 74:
      item_value = item.transaction_fee
      break;

      case 114:
        item_value = item.shipping_cost
        break;
      default:
        // code block
    }

    if(item_value >0 ){
      return true
    }
    else{
      return false
    }
  }
  

  _checkData(items,value){
     
    let obj  = items.filter(item => item.reference_id.toLowerCase().includes(value))
    if(obj.length>0){
      console.log(obj[0].debit)
      //return true

      
      var a = parseFloat(obj[0].credit) - parseFloat(obj[0].debit) 
      return Math.abs(a)
    }else{
      return 0
    }
   
  }

  _getProductTransactionFee(items){
    let inventory = 0
    let transaction_fee = 0
    let bonus_value = 0
    items.forEach(item => {
  
      //inventory += this._getInventoryItems(JSON.parse(item.purchase_order_item))
      transaction_fee += (parseFloat(item.transaction_fee) * item.qty)
      //bonus_value += (parseFloat(item.bonus_value) * item.qty)
    });

    return transaction_fee
 

 
  }

  viewOrder(item){
    this.selected_order = item
    item.show = !item.show
    //console.log(this.selected_order)
  }

  
  _getProductBonus(items){
    let inventory = 0
    let transaction_fee = 0
    let bonus_value = 0
    items.forEach(item => {
  
      //inventory += this._getInventoryItems(JSON.parse(item.purchase_order_item))
      //transaction_fee += (parseFloat(item.transaction_fee) * item.qty)
     if(item.bonus_value){
      bonus_value += (parseFloat(item.bonus_value) * item.qty)
     }
      
    });

    return bonus_value
 

 
  }

  _getProductCogs(items){
    let inventory = 0
    let transaction_fee = 0
    let bonus_value = 0
    items.forEach(item => {
  
      inventory += parseFloat(item.total_cogs?item.total_cogs:0)
      //transaction_fee += (parseFloat(item.transaction_fee) * item.qty)
      //bonus_value += (parseFloat(item.bonus_value) * item.qty)
    });

    return inventory
 

 
  }

  _getInventoryItems(items){
   
    let inventory = 0
    if(items){
      items.forEach(item => {
        if(item.order_price){
          inventory += parseFloat(item.order_price) / parseFloat(items.length)
        }
      
      });
    
    }
   
    return inventory
   
  }




}
