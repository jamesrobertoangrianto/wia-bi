import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { formatDate } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { faCaretSquareDown} from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-finance-report',
  templateUrl: './finance-report.component.html',
  styleUrls: ['./finance-report.component.scss']
})
export class FinanceReportComponent implements OnInit {
  finance_reports: any;
  stements: any;
  faChevronCircleDown = faCaretSquareDown  
  months=
  [ 
      {
        value : 0,
        name : 'January'
      },
      {
        value : 1,
        name : 'February'
      },
      {
        value : 2,
        name : 'March'
      },
      {
        value : 3,
        name : 'April'
      },
      {
        value : 4,
        name : 'May'
      },
      {
        value : 5,
        name : 'June'
      },
      {
        value : 6,
        name : 'July'
      },
      {
        value : 7,
        name : 'August'
      },
      {
        value : 8,
        name : 'September'
      },
      {
        value : 9,
        name : 'October'
      },
      {
        value : 10,
        name : 'November'
      },
      {
        value : 11,
        name : 'December'
      },
      
  ]

  quarter=
  [ 
      {
        value : 3,
        name : 'Quarter 1'
      },
      {
        value : 6,
        name : 'Quarter 2'
      },
      {
        value : 9,
        name : 'Quarter 3'
      },
      {
        value : 12,
        name : 'Quarter 4'
      },

      {
        value : 0,
        name : 'Quarter 1 - 4'
      },
      
      
  ]
  current_month: number;
  report: any
  accounts: any;
  total: number;
  cashAccount: any[];
  cashFlow: any
  profitLoss: any
  profitLossByMonth: any
  profitLossLifetime: any
  monthlyCashFlow: any
  profit_current_month: number;
  cashFlowCredit: number;
  cashFlowDebit: number;
  totalCashBank: any;
  purchase_order: any;
  inventory: Promise<any>;
  totalInventory: number;
  totalAccountPayable: number;
  totalAccountReceivable: number;
  totalReceivable: number;
  inventory_account: any;
  assets_account: any;
  payable_account: any;
  balanceStatment: any[];
  current_year: number;
  years: any[];
  IncomeStatement: any[];
  current_quarter: any;
  total_income: number;
  total_expenses: any;
  total_cogs: number;
  total_net_profit: number;
  active_year: number;
  

  constructor(
    private FinanceService : ManualOrderService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    private financeService : ManualOrderService,
  ) { }

  ngOnInit(): void {
    const d = new Date();
    this.current_quarter = 3
    this.current_month = d.getMonth()
    this.current_year = d.getFullYear()
    this.active_year = d.getFullYear()
    this.years = []
    var year = this.current_year
    for(let i=0;i<5;i++) {
   
      this.years.push({
        'year' : year
      })
      year = year - 1
    }
   
   

    this.cashAccount = []
      // this.report = {
      //   'cashbank' : this.getCashBank(),
      //   'due' : this.getDue(),
      //   'inventory' : this.getInventory()
      // }
      
      this.getCashBank()
      this.getUnPaidPurchaseOrders()
      this.getCashflowByDate()
      
      
    this.getInventoryAccount()
    this.getAccountPayableAccount()
    this.getAccountReceiveableAccount()
    this.getAssetsAccount()

    this.buildIncomeStatement()
    this.buildBalanceStatement()


   

  }
  changeYear(val){
    console.log(val)
    this.current_year = parseFloat(val)
    this.buildIncomeStatement()
    this.buildBalanceStatement()
    //this.getProfitLoss()
  }

  changeQuarter(val){
    console.log(val)
    this.current_quarter = parseFloat(val)
    this.buildIncomeStatement()
    this.buildBalanceStatement()
    //this.getProfitLoss()
  }
  _getMonthName(last?) {
    const d = new Date();
    
    const date = new Date();
    date.setMonth(d.getMonth() - last);
  
    return date.toLocaleString('en-US', {
      month: 'long',
    });
  }

  _getMonthNames(month) {
    
    
    const date = new Date();
    date.setMonth(month);
  
    return date.toLocaleString('en-US', {
      month: 'long',
    });
  }




  _getCashInByMonth(month){
    let credit = 0
    let debit = 0
    this.cashFlow.items.forEach(item => {
      if(this._checkMonth(item.create_date,month)){
        debit += parseFloat(item.debit)
      
      }

     
       
    });
    return debit
  }
  _getCashOutByMonth(month){
    let credit = 0
    let debit = 0
    this.cashFlow.items.forEach(item => {
      if(this._checkMonth(item.create_date,month)){
        credit += parseFloat(item.credit)
      
      }

      
       
    });
    return credit
  }

  async  getCashflowByDate() {
  
    try {
    this.spinner.show();
    let credit = 0
    let debit = 0
     let response = await this.financeService.getCashflowByDate(1,1)
      console.log(response)
      this.cashFlow = response
      console.log(this.cashFlow)
      response.items.forEach(item => {
        credit += parseFloat(item.credit)
        debit += parseFloat(item.debit)
      });

      

      this.cashFlowCredit = credit
      this.cashFlowDebit = debit
     
      
   
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  

  async buildIncomeStatement(){
    this.IncomeStatement = []
    let start_month = this.current_quarter-3
    let end_month = this.current_quarter
    if(this.current_quarter===0){
      start_month = 0
       end_month = 12
    }
    for(let i=start_month;i<end_month;i++) {
      this.IncomeStatement.push({
        'year' : this.current_year,
        'month' : i,
        'month_name' : this._getMonthNames(i),
        'income' : await this._getRevenueByMonth(i),
        'cogs' : await this._getCogsByMonth(i),
        'expense' : await this._getExpenseByMonth(i),
       
       // 'profit' : await (await this._getRevenueByMonth(i)).total - await (await this._getCogsByMonth(i)).total -  await (await this._getExpenseByMonth(i)).total 
        
      })
      
     // console.log(i)
    }

    this._calculateTotal()

  
   

  }

   async buildBalanceStatement(){
    this.balanceStatment = []
    let start_month = this.current_quarter-3
    let end_month = this.current_quarter
    if(this.current_quarter===0){
      start_month = 0
       end_month = 12
    }
    for(let i=start_month;i<end_month;i++) {
      this.balanceStatment.push({
        'year' : this.current_year,
        'month' : i,
        'month_name' : this._getMonthNames(i),
        'asset' : await this._getAssetsByMonth(i),
        'liabilities' : await this._getLiabilityByMonth(i),
        'capital' : await this._getCapitalByMonth(i),
    
       
        
      })
      
     // console.log(i)
    }

    this._calculateTotal()

  
   

  }


  _calculateTotal(){
    console.log(this.IncomeStatement)
    console.log('this')
    this.total_income = 0
    this.total_expenses = 0
    this.total_cogs = 0
    this.IncomeStatement.forEach(item => {
      this.total_income += item.income.total
      this.total_expenses += item.expense.total
      this.total_cogs += item.cogs.total
    });

    this.total_net_profit = this.total_income  - this.total_expenses - this.total_cogs 
  }

  async _getAssetsByMonth(month){
    
    var date = new Date();
    var firstDay = new Date(2022 , parseInt(month) - 0,0);
    var lastDay = new Date(this.current_year, parseInt(month) + 1, 0);
   // console.log(date.getFullYear())
    var from = formatDate(firstDay, 'yyyy-MM-dd', 'en_US') 
    var to = formatDate(lastDay, 'yyyy-MM-dd', 'en_US') 
    
    try {
      let current_assets = await this.financeService.getJournalTransactionByAccountType('current_assets',from,to)
      let cash_bank = await this.financeService.getJournalTransactionByAccountType('cash_bank',from,to)
      let inventory = await this.financeService.getJournalTransactionByAccountType('inventory',from,to)
      let account_payable = await this.financeService.getJournalTransactionByAccountType('account_payable',from,to)
      let equity = await this.financeService.getJournalTransactionByAccountType('equity',from,to)
    
      
     if( this.active_year <= this.current_year && this.current_month<month){
     
      let res = {
        'petty_cash' : '',
        'inventory_goods' : '',
        'inventory_bonus' : '',
        'inventory_cbd' : '',
        'prepaid_building' : '',
        'prepaid_outing' : '',
        
      }
      return res
     }else{
      

      let res = {
        'cash_bank' : Math.abs( this._getAccountBalanceById(cash_bank.items,28).credit - this._getAccountBalanceById(cash_bank.items,28).debit),
        'petty_cash' : Math.abs( this._getAccountBalanceById(cash_bank.items,76).credit - this._getAccountBalanceById(cash_bank.items,76).debit),
        'inventory_goods' : Math.abs( this._getAccountBalanceById(inventory.items,44).credit - this._getAccountBalanceById(inventory.items,44).debit),
        'inventory_bonus' : Math.abs( this._getAccountBalanceById(inventory.items,113).credit - this._getAccountBalanceById(inventory.items,113).debit),
        'inventory_cbd' : Math.abs( this._getAccountBalanceById(inventory.items,116).credit - this._getAccountBalanceById(inventory.items,116).debit),
        'prepaid_building' : Math.abs( this._getAccountBalanceById(current_assets.items,123).credit - this._getAccountBalanceById(current_assets.items,123).debit),
        'prepaid_outing' : Math.abs( this._getAccountBalanceById(current_assets.items,124).credit - this._getAccountBalanceById(current_assets.items,124).debit),
      
        'account_payable' : Math.abs( this._getAccountBalanceById(account_payable.items,45).credit - this._getAccountBalanceById(account_payable.items,45).debit),
        'holding_account' : Math.abs( this._getAccountBalanceById(cash_bank.items,131).credit - this._getAccountBalanceById(cash_bank.items,131).debit),
        'devidend' : Math.abs( this._getAccountBalanceById(equity.items,29).credit - this._getAccountBalanceById(equity.items,29).debit),
        'retained_earnings' : Math.abs( this._getAccountBalanceById(equity.items,132).credit - this._getAccountBalanceById(equity.items,132).debit),
        
      }
      return res
     
     }
      
     
    
    } catch (error) {
      
    }
    finally{
      this.spinner.hide()
    }
   
     
    
  }


  async _getLiabilityByMonth(month){

    var date = new Date();
    var firstDay = new Date(this.current_year , parseInt(month) - 0,0);
    var lastDay = new Date(this.current_year, parseInt(month) + 1, 0);
   // console.log(date.getFullYear())
    var from = formatDate(firstDay, 'yyyy-MM-dd', 'en_US') 
    var to = formatDate(lastDay, 'yyyy-MM-dd', 'en_US') 
    
    try {
      let response = await this.financeService.getJournalTransactionByAccountType('income',from,to)
      console.log(response)
      this.spinner.show()

      let res = {
        'total' : '123',
        'account_payable' : '123',
       
      }
  
      return res
    } catch (error) {
      
    }
    finally{
      this.spinner.hide()
    }
   
     
    
  }

  async _getCapitalByMonth(month){

    var date = new Date();
    var firstDay = new Date(this.current_year , parseInt(month) - 0,0);
    var lastDay = new Date(this.current_year, parseInt(month) + 1, 0);
   // console.log(date.getFullYear())
    var from = formatDate(firstDay, 'yyyy-MM-dd', 'en_US') 
    var to = formatDate(lastDay, 'yyyy-MM-dd', 'en_US') 
    
    try {
      let response = await this.financeService.getJournalTransactionByAccountType('income',from,to)
      console.log(response)
      this.spinner.show()

      let res = {
        'total' : '123',
        'holding' : '123',
       
      }
  
      return res
    } catch (error) {
      
    }
    finally{
      this.spinner.hide()
    }
   
     
    
  }


  async _getRevenueByMonth(month){

    var date = new Date();
    var firstDay = new Date(this.current_year , parseInt(month) - 0,0);
    var lastDay = new Date(this.current_year, parseInt(month) + 1, 0);
   // console.log(date.getFullYear())
    var from = formatDate(firstDay, 'yyyy-MM-dd', 'en_US') 
    var to = formatDate(lastDay, 'yyyy-MM-dd', 'en_US') 
    
    try {
      let response = await this.financeService.getJournalTransactionByAccountType('income',from,to)
      console.log(response)
      this.spinner.show()

      let res = {
        'total' : this._getAccountBalanceById(response.items).credit - this._getAccountBalanceById(response.items).debit,
        'sales_revenue' : this._getAccountBalanceById(response.items,41).credit - this._getAccountBalanceById(response.items,41).debit,
        'credit_note' : this._getAccountBalanceById(response.items,117).credit - this._getAccountBalanceById(response.items,117).debit,
        'sales_bonus' : this._getAccountBalanceById(response.items,55).credit - this._getAccountBalanceById(response.items,55).debit,
      
        'shipping_revenue' : this._getAccountBalanceById(response.items,114).credit - this._getAccountBalanceById(response.items,114).debit,
        'transaction_fee' : this._getAccountBalanceById(response.items,115).credit - this._getAccountBalanceById(response.items,115).debit,
        'split_payment' : this._getAccountBalanceById(response.items,119).credit - this._getAccountBalanceById(response.items,119).debit,
      
      }
  
      return res
    } catch (error) {
      
    }
    finally{
      this.spinner.hide()
    }
   
     
    
  }

  async _getCogsByMonth(month){

    var date = new Date();
    var firstDay = new Date(this.current_year , parseInt(month) - 0,0);
    var lastDay = new Date(this.current_year, parseInt(month) + 1, 0);
    console.log(date.getFullYear())
    var from = formatDate(firstDay, 'yyyy-MM-dd', 'en_US') 
    var to = formatDate(lastDay, 'yyyy-MM-dd', 'en_US') 
    
    try {
      this.spinner.show()
      let response = await this.financeService.getJournalTransactionByAccountType('cost_of_sales',from,to)
      //    console.log(response)
          
        let res = {
          'total' : Math.abs(this._getAccountBalanceById(response.items).credit - this._getAccountBalanceById(response.items).debit),
          'goods' : Math.abs(this._getAccountBalanceById(response.items,39).credit - this._getAccountBalanceById(response.items,39).debit),
          'bonus' : Math.abs(this._getAccountBalanceById(response.items,122).credit - this._getAccountBalanceById(response.items,122).debit),
        
        }
    
        return res
    } catch (error) {
      
    }finally{
      this.spinner.hide()
    }
    
  }

  async _getExpenseByMonth(month){

    var date = new Date();
    var firstDay = new Date(this.current_year , parseInt(month) - 0,0);
    var lastDay = new Date(this.current_year, parseInt(month) + 1, 0);
    console.log(date.getFullYear())
    var from = formatDate(firstDay, 'yyyy-MM-dd', 'en_US') 
    var to = formatDate(lastDay, 'yyyy-MM-dd', 'en_US') 
    try {
      this.spinner.show()
      let response = await this.financeService.getJournalTransactionByAccountType('expenses',from,to)
      console.log(response)
       
      let res = {
        'total' : Math.abs(this._getAccountBalanceById(response.items).credit - this._getAccountBalanceById(response.items).debit),
        'general_expense' : Math.abs(this._getAccountBalanceById(response.items,30).credit - this._getAccountBalanceById(response.items,30).debit),
        'marketing_expense' : Math.abs(this._getAccountBalanceById(response.items,31).credit - this._getAccountBalanceById(response.items,31).debit),
        'production_expense' : Math.abs(this._getAccountBalanceById(response.items,34).credit - this._getAccountBalanceById(response.items,34).debit),
        'operational_expense' : Math.abs(this._getAccountBalanceById(response.items,35).credit - this._getAccountBalanceById(response.items,35).debit),
       
        'building' : Math.abs(this._getAccountBalanceById(response.items,32).credit - this._getAccountBalanceById(response.items,32).debit),
        'tech' : Math.abs(this._getAccountBalanceById(response.items,33).credit - this._getAccountBalanceById(response.items,33).debit),
       

        'utility' : Math.abs(this._getAccountBalanceById(response.items,40).credit - this._getAccountBalanceById(response.items,40).debit),
       

        'sales_discount' : Math.abs(this._getAccountBalanceById(response.items,73).credit - this._getAccountBalanceById(response.items,73).debit),
        'transaction_fee' : Math.abs(this._getAccountBalanceById(response.items,74).credit - this._getAccountBalanceById(response.items,74).debit),
        'shipping_cost' : Math.abs(this._getAccountBalanceById(response.items,75).credit - this._getAccountBalanceById(response.items,75).debit),
        
        'cashback' : Math.abs(this._getAccountBalanceById(response.items,91).credit - this._getAccountBalanceById(response.items,91).debit),
        'employee' : Math.abs(this._getAccountBalanceById(response.items,36).credit - this._getAccountBalanceById(response.items,36).debit),
        'employee_bonus' : Math.abs(this._getAccountBalanceById(response.items,37).credit - this._getAccountBalanceById(response.items,37).debit),
       
      }
  
      return res
    } catch (error) {
      
    }finally{
      this.spinner.hide()
    }
  
   
  }

  _getAccountBalanceById(items,account_id?){
    let credit = 0
    let debit = 0
    if(items){
      if(account_id){
        items.forEach(item => {
          if(item.account_id == account_id){
            credit += parseFloat(item.credit)
            debit += parseFloat(item.debit)
          }
         
        });
      }
      else{
        items.forEach(item => {
         if(item.account_id){
            credit += parseFloat(item.credit)
            debit += parseFloat(item.debit)
         }
            
         
         
        });
      }
    }
    
    
    let a = {
      'credit' : credit,
      'debit' : debit,
     
    }
    return a
  }



  getCreditNoteByMonth(month){
   
    let credit = 0
    let debit = 0
    this.profitLoss.revenue.items.forEach(item => {
      if(this._checkMonth(item.create_date,month) && item.account_id == 117){
        credit += parseFloat(item.credit)
        debit += parseFloat(item.debit)
      }
    });

    let a = {
      'credit' : credit,
      'debit' : debit,
     
    }
    return a
  }

  getShippingRevenueByMonth(month){
   
    let credit = 0
    let debit = 0
    this.profitLoss.revenue.items.forEach(item => {
      if(this._checkMonth(item.create_date,month) && item.account_id == 114){
        credit += parseFloat(item.credit)
        debit += parseFloat(item.debit)
      }
    });

    let a = {
      'credit' : credit,
      'debit' : debit,
     
    }
    return a
  }


  goToDetails(id,month) {
    
    this.router.navigate(
      ['/chart-account/view/'+id],
      { queryParams: { year: this.current_year, 'month': month } }
    )
    console.log(this.current_month)
    console.log(this.current_year)
  }


  _checkMonth(date,month){
    const objDate = new Date(date);
    
     if( (objDate.getMonth() === month) && (objDate.getFullYear() === this.current_year) ){
      return true
     }else{
      return false
     }
  }

  _checkYear(date){
    const objDate = new Date(date);
    
     if( (objDate.getFullYear() === this.current_year) ){
      return true
     }else{
      return false
     }
  }



  async getUnPaidPurchaseOrders() {
   
    try {
    this.spinner.show();
     let response = await this.financeService.getPurchaseOrdersByPaymentStatus('UN_PAID')
    this.purchase_order = response.purchase_orders
     let total_payment = 0
    let overdue = 0

    this.purchase_order.forEach(item => {
      total_payment += parseFloat(item.purchase_amount)
      var today = new Date()
      var receive = new Date(item.due_date)
      if(receive.getTime() < today.getTime() ){
        overdue += parseFloat(item.purchase_amount)
      }
      
    });

    this.purchase_order.total_payment = total_payment
    this.purchase_order.overdue = overdue
   
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  async getCashBank(){
    try {
      this.spinner.show();
      let response = await this.financeService.getChartAccountByType('cash_bank')
        console.log(response)
        this.accounts = response.accounts
  
        // get total
        let total = 0
        this.accounts.forEach(item => {
          total += parseFloat(item.account_balance)
        });

        this.totalCashBank = total
     
      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
      }
    
  }


  async getInventoryAccount(){
    try {
      this.spinner.show();
       let response = await this.financeService.getChartAccountByType('inventory')
      console.log(response)
      this.inventory_account = response.accounts
      let total = 0
      response.accounts.forEach(item => {
        total += parseFloat(item.account_balance)
      });
      console.log(total)
      this.totalInventory=total
     
      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
      }
    
  }

  async getAssetsAccount(){
    try {
      this.spinner.show();
       let response = await this.financeService.getChartAccountByType('current_assets')
      console.log(response)
      this.assets_account = response.accounts
      let total = 0
      response.accounts.forEach(item => {
        total += parseFloat(item.account_balance)
      });
      console.log(total)
    
     
      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
      }
    
  }

  async getAccountPayableAccount(){
    try {
      this.spinner.show();
       let response = await this.financeService.getChartAccountByType('account_payable')
       this.payable_account = response.accounts
      console.log(response)
      let total = 0
      response.accounts.forEach(item => {
        total += parseFloat(item.account_balance)
      });
      console.log(total)
      this.totalAccountPayable=total
     
      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
      }
    
  }

  async getAccountReceiveableAccount(){
    try {
      this.spinner.show();
       let response = await this.financeService.getChartAccountByType('account_receiveable')
      console.log(response)
      let total = 0
      response.accounts.forEach(item => {
        total += parseFloat(item.account_balance)
      });
      console.log(total)
      this.totalAccountReceivable=total
     this.totalReceivable = total
      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
      }
    
  }

  getDue(){
    console.log('get due')
    return '1000'
  }




}
