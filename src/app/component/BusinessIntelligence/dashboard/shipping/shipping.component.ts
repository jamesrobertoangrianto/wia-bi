import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent implements OnInit {
  rates: any;
  selectedDistrict: any;
  districtList: any;
  cityList: any;
  show_city: boolean;
  shippingRates: any;

  constructor(
    private financeService : ManualOrderService,
    private manualOrderSvc: ManualOrderService,
  ) { }

  ngOnInit(): void {
  //   this.getShippingRates()

  }

  async getShippingRates(id){
    try {
   
       let response = await this.financeService.getShippingRates(id)
    // console.log(response)
       this.rates = response
      console.log(this.rates)
      } catch (e) {
        console.log(e)
      } finally {
       
     
      }
  }

  addMore(){
    this.rates.push({
      'district_code' : this.selectedDistrict.district_code,
      'price' : '',
      'service' : '',
      'method' : '',
      'method_code' : '',
      'service_code' : '',
      'is_edit' : true
    })
  }


  changeDistrict(){
    this.selectedDistrict=null

  }

  selectDistrict(item){
    
    console.log(item)
    console.log('ksks')
    this.getShippingRates(item.district_code)
     this.selectedDistrict = item
    
     this.districtList = null
     this.cityList = null

  }

  findDistrict(value){
    
    this.getDistrict(value)
      console.log(value)
        this.show_city = true
     
    }

    selectCity(item){
      console.log(item.city_name)
      // this.city_keyword = item.city_name
       this.getDistrict(item.city_name)
        this.show_city = false
    }
  
  

    async getDistrict(id){
      let res = await await this.manualOrderSvc.getDistrict(id)
      console.log(res)
      this.districtList = res.district
      this.cityList = res.city
    }

    editRate(item){
      console.log(item)
    }

   

    async saveRate(item){
      console.log(item)
      try {
     
         let response = await this.financeService.addShippingRates(item)
       console.log(response)
       item.id = response.id
    
        } catch (e) {
          console.log(e)
        } finally {
         item.is_edit = false
        
       
        }
    }
  

    async removeShippingRates(item){
      console.log(item)
      try {
     
         let response = await this.financeService.removeShippingRates(item.id)
       console.log(response)
        
    
        } catch (e) {
          console.log(e)
        } finally {
          var index = this.rates.indexOf(item);
          this.rates.splice(index, 1);
       
        }
    }
  
    
    

  

}
