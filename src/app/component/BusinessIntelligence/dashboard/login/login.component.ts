import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm={
    email: null,
    password: null
  }
  constructor(

    private toastr: ToastrService,
    private router : Router,
    private toast: ToastrService,

    public customerService : ManualOrderService,
    private spinner: NgxSpinnerService,

    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }




  async login(){

    try {
      this.spinner.show();
      let response = await this.customerService.login(this.loginForm)
      console.log(response)

      
      localStorage.setItem("customer", JSON.stringify(response.customer) );
      localStorage.setItem("session_token", response.customer.token);
      localStorage.setItem("account_name", response.customer.firstname);
      localStorage.setItem("account_role", response.customer.manager);


      if(response.customer.manager ){
        localStorage.setItem('role',response.customer.manager);
      }

      if(response.customer.email == 'warehouse@wia.id'){
        localStorage.setItem('isWarehouse', 'true');
      }
     
    
      if(response.customer.token){
            this.router.navigate( ['/'] );
        // window.location.reload();
        setTimeout(()=>{                           // <<<---using ()=> syntax
          window.location.reload();
      }, 1000);

      }

    } catch (e) {
      console.log()
    } finally {
      this.spinner.hide();
      
    }
  }


}
