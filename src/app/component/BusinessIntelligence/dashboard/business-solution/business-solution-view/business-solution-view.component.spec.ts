import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessSolutionViewComponent } from './business-solution-view.component';

describe('BusinessSolutionViewComponent', () => {
  let component: BusinessSolutionViewComponent;
  let fixture: ComponentFixture<BusinessSolutionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessSolutionViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessSolutionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
