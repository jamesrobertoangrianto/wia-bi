import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
  params: string;
  customer: any;

  constructor(
    private webService: ManualOrderService,

    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router: Router,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(queryParams => {


      this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams }))



    })
  }




}
