import { Component, Input, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-business-order-view',
  templateUrl: './business-order-view.component.html',
  styleUrls: ['./business-order-view.component.scss']
})
export class BusinessOrderViewComponent implements OnInit {
  showModal: any;
  @Input() order: any
  product: any;


  constructor(
    private customerService : ManualOrderService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
   console.log(this.order)
  }


  async getOrderById(id){
    
    try {
      this.spinner.show();
       let response = await this.customerService.getBusinessOrderById(id)
       this.order = response.data
       
        console.log(response)
  
      } catch (e) {
        console.log(e)
      } finally {
         this.spinner.hide();
      }
   

  }


  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }

  async updateOrder(e,id?){
    console.log(e)
    let form ={}
    form[e.id]=e.value
    console.log(id)
    try {
      this.spinner.show();
       let response = await this.customerService.updateBusinessOrder(form,id )
        this.product = response.products
       
        console.log(response)
  
      } catch (e) {
        console.log(e)
      } finally {
         this.spinner.hide();
      }
   

  }

  async updateProduct(e,item){
    console.log(item)
    let form ={}
    form[e.id]=e.value
    item[e.id]=e.value

    this.getFinalPrice(item)

    try {
      this.spinner.show();
       let response = await this.customerService.updateBusinessOrderProduct(item,item.businessorderproduct_id )
        this.product = response.products
        console.log(response)
  
      } catch (e) {
        console.log(e)
      } finally {
         this.spinner.hide();
      }
   

  }


  async searchProduct(e) {
    console.log(e)
    if(e.length >3){
     var size = 15;
    
     
     try {
       this.spinner.show();
        let response = await this.customerService.getProductList(e,size,1, 0 )
         this.product = response.products
         console.log(response)
   
       } catch (e) {
         console.log(e)
       } finally {
          this.spinner.hide();
       }
    
     }
 
 
    
   }
 
   getFinalPrice(order){
    let dis = (parseFloat(order.product?.final_price) * parseFloat(order?.order_discount_percentage))/100
    let price = parseFloat(order.product?.final_price)
   
    let total = (price - dis)*order.order_qty
    order.order_final_price =  total
    return total
   }
  
  selectProduct(e){
    console.log(e)
  }



  async addProduct(item) {
    item.isAdded = true
    let form = {
      'businessorder_id' : this.order.businessorder_id,
      'product_id':item.id
    }

    try {
      this.spinner.show();
      let response = await this.customerService.addBusinessOrderProduct(form)
      if(!this.order.products){
        this.order.products = []
      }
     this.order.products = response.data
     console.log(response)
     
       this.closeModal()
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
    
  }
}
