import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessOrderViewComponent } from './business-order-view.component';

describe('BusinessOrderViewComponent', () => {
  let component: BusinessOrderViewComponent;
  let fixture: ComponentFixture<BusinessOrderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessOrderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessOrderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
