import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronCircleDown, faChevronDown, faExpandAlt, faPhone, faTrash } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-business-order',
  templateUrl: './business-order.component.html',
  styleUrls: ['./business-order.component.scss']
})
export class BusinessOrderComponent implements OnInit {
  faExpandAlt=faExpandAlt
  faTrash=faTrash
  faChevronCircleDown=faChevronDown
  faPhone=faPhone
  orderForm = new FormGroup({
    customer_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),
    sales_name: new FormControl(null,{ 
      validators: [Validators.required, Validators.nullValidator]
    }),

  
  })

  showModal: any;
  order: any;

  status_label = [
    {
      'id' :'deal_stage',
      'label':'new_request',
      'value':'new_request'
    },
    {
      'id' :'deal_stage',
      'label':'initiate_contact',
      'value':'initiate_contact'
    },
    {
      'id' :'deal_stage',
      'label':'quote_sent',
      'value':'quote_sent'
    },
    {
      'id' :'deal_stage',
      'label':'deal',
      'value':'deal'
    },
    {
      'id' :'deal_stage',
      'label':'cancel',
      'value':'cancel'
    },
    {
      'id' :'deal_stage',
      'label':'loss',
      'value':'loss'
    }
  ]
  params: string;

  constructor(
    private customerService : ManualOrderService,
    private spinner: NgxSpinnerService,
    private serializer: UrlSerializer,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {

   



    this.route.queryParamMap.subscribe(queryParams => {
     
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))      
      this.getBusinessOrder()
    })
  }

  toggleitem(item){
    item.isEdit = !item.isEdit
    console.log(item)
  }
  async addOrder() {
    console.log(this.orderForm.value)
    try {
      this.spinner.show();
      let response = await this.customerService.addBusinessOrder(this.orderForm.value)
      response.data.isEdit = true
       this.order.unshift(response.data)
      
       this.orderForm.reset
       this.closeModal()
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
    
  }


  async getBusinessOrder() {
    try {
      this.spinner.show();
      let response = await this.customerService.getBusinessOrder(this.params)
      this.order = response.data
      console.log(response)
      this.order.forEach(item => {
        item.total_amount = this.getTotal(item)
       });
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
    
  }


  async deleteBusinessOrder(id) {
    if (confirm("Confirm?") == true) {
      try {
        this.spinner.show();
        let response = await this.customerService.deleteBusinessOrder(id)
        
      } catch (e) {
        console.log(e)
      } finally {
        this.ngOnInit()
        this.spinner.hide();
      }
    } 


   
    
  }


  async onSelect(e,id){
    console.log(e)
    let form ={}
    form[e.id]=e.value
  
    try {
      this.spinner.show();
      let response = await this.customerService.updateBusinessOrder(form,id )
       
       console.log(response)
  
      } catch (e) {
        console.log(e)
      } finally {
         this.spinner.hide();
      }
   
  }

  checkDate(item){
    if(item.deal_stage == 'deal'){
      return false
    }else if(item.deal_stage == 'loss'){
      return false
    }
    else if(item.deal_stage == 'cancel'){
      return false
    }
    else{
      return true
    }
  }
  getTotal(order){
    let total = 0
    order.products?.forEach(element => {
     
     total += parseFloat(element?.order_final_price)
    });

    return total
  }
  openModal(modal){
    this.showModal = modal
  }

  closeModal(){
    this.showModal = null
  }
  searchProduct(e){
    console.log(e)
  }
  selectProduct(e){
    console.log(e)
  }

}
