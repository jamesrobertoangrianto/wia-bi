export const testForm={
    "customer": {
      "firstname": "Tes",
      "lastname": "Tes",
      "email": "james@wia.id",
      "telephone": "12345678910"
    },
    "shipping_address": {
      "region": "Banten",
      "region_id": "487",
      "postcode": "15810",
      "city": "Kabupaten Lebak",
      "street": "Ruko Dalton Utara No 50",
      "location": "Wearinasia",
      "telephone": "12345678910",
    },
    "origin_address": {
      "region": "Banten",
      "region_id": "487",
      "postcode": "15810",
      "city": "Kabupaten Lebak",
      "street": "Ruko Dalton Utara No 50",
      "location": "Wearinasia",
      "telephone": "12345678910",
    },
    "quote": [
      {
        "item_id": null,
        "product_id": "1574",
        "name": "Fjallraven Kanken Classic - Burnt Ora...",
        "brand": null,
        "request_quantity": 1,
        "available_quantity": null,
        "url_path": null,
        "has_error": null,
        "price": "1349100.0000",
        "featured_image": {
          "image": "https://wia.id/media/catalog/product/cache/1/small_image/200x250/9df78eab33525d08d6e5fb8d27136e95/b/u/burnt_orange.jpg",
          "image_2x": "https://wia.id/media/catalog/product/cache/1/small_image/400x500/9df78eab33525d08d6e5fb8d27136e95/b/u/burnt_orange.jpg",
          "image_3x": "https://wia.id/media/catalog/product/cache/1/small_image/400x500/9df78eab33525d08d6e5fb8d27136e95/b/u/burnt_orange.jpg"
        },
        "installment_term": 12,
        "product_custom_options_list": null,
        "custom_options": null
      }
    ],
    "newcustomerflag": false,
    "customer_group_id": "5",
    "transactionFee": "10",
    "custom_note": null
  }