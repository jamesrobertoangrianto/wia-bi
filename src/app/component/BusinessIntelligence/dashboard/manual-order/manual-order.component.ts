import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators, FormBuilder } from '@angular/forms';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { ToastrService } from 'ngx-toastr';
import { testForm } from './manualOrderTest';
import { NgxSpinnerService } from 'ngx-spinner';
import { validPhoneNumber, phoneFormatter, formError } from 'src/app/forms/validation';
import { keyframes } from '@angular/animations';
import { faChevronLeft, faChevronRight, faCircle, faCircleNotch, faDotCircle, faMapMarked, faMapMarkedAlt, faMapMarker, faMapMarkerAlt, faMapPin, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-manual-order',
  templateUrl: './manual-order.component.html',
  styleUrls: ['./manual-order.component.scss']
})
export class ManualOrderComponent implements OnInit {
  @ViewChild('searchQuery',{static: true}) searchInput: ElementRef;
  isLoading: boolean;
  selected_product_id:any
  isExistingCusomer: boolean;
  isExistingCustomer: boolean;
  showModal: boolean;
  discount_value: any;
  subTotal: number;
  total_transaction_fee: number;
  selectedProductItem: any
  faChevronLeft=faChevronLeft
  faCircleNotch=faCircle
  final_quote: any;
  products: any;
  buffer_filter: any;
  last_payment_link: any;
  last_order_id: any;
  selectedShippingMethod: any;
  quote_id: any;
  districtList: any;
  cityList: any;
  city_keyword: any;
  show_city: boolean;
  selectedDistrict: any;
keyword: any;
faTrashAlt=faTrashAlt
faChevronRight=faChevronRight
faMapMarker=faMapMarkerAlt
  shipping_rates: any;
  is_custom_shipping: boolean;
  paymentLink: any;
  is_processing: any;
  

  constructor(
    private manualOrderSvc: ManualOrderService,
    private toast: ToastrService,
    private spinner: NgxSpinnerService,
  ) {
    
  }

  tab_option = [
   
    {
      'label' : 'Value',
      'name' : 'stock_level_value',
      'is_active' : true
      
    },
    {
      'label' : 'Stock',
      'name' : 'stock_level',
      
    }
  ]



  sourceList = [
    {
      label:'TEST',
      email:'jamesrobertoangrianto@gmail.com',
      
    },
    {
      label:'ONLINE_CHAT',
      email:'online_chat@wia.id'
    },
    {
      label:'OFFLINE_STORE',
      email:'offline_store@wia.id'
    },
    {
      label:'TOKPED_WIA',
      email:'tokopedia@wia.id'
    },
    {
      label:'TOKPED_DS',
      email:'tokopedia@dronestore.id'
    },
    {
      label:'TOKPED_KINTO',
      email:'tokped_kinto@wia.id'
    },
    {
      label:'BLI_BLI_WIA',
      email:'blibli@wearinasia.com'
    },
    {
      label:'BLI_BLI_DS',
      email:'blibli@dronestore.id'
    },
    {
      label:'SHOPEE_WIA',
      email:'shopee@wia.id'
    },
    {
      label:'ETHIX_KG',
      email:'ethix_kg@wia.id'
    },
    {
      label:'ETHIX_CP',
      email:'ethix_cp@wia.id'
    },
   
   

    {
      label:'RETAILER',
      email:'retailer@wia.id'
    },


    {
      label:'TIKTOK_WIA',
      email:'tiktok_wia@wia.id'
    },
    {
      label:'TIKTOK_DM',
      email:'tiktok_dm@wia.id'
    },
    {
      label:'B2B',
      email:'b2b@wia.id'
    },
   
  ]
  fError = formError

  oriRegionList: any
  oriSelectedRegion: any
  oriCityList: any
  oriSelectedCity: any
  

  destRegionList: any
  destSelectedRegion: any
  destCityList: any
  destSelectedCity: any

  shippingMethods: any

  dropshipMode: Boolean
  
  formStep: any

  state={
    destProvinceChanging: false
  }

  search={
    searchQuery: null,
    currentLoadedItem: null,
    maxItem: null,
    listOfSearchResult: null,
    pageNumber: 1,
    canLoadMore: () => this.search.currentLoadedItem <= this.search.maxItem
  }

  customerGroupSelection=[]

  transactionFeeSelection=[]

  manualOrderForm=new FormGroup({
    customer: new FormGroup({
      firstname: new FormControl(null,{
        validators: [Validators.required]
      }),
      
      email: new FormControl(null,{
        validators: [Validators.required, Validators.email],
      }),
      telephone: new FormControl(null,{
        validators: [Validators.required, validPhoneNumber]
      }),
     
    }),
    shipping_address: new FormGroup({
      region: new FormControl(null,{
        validators: [Validators.required]
      }),
      region_id: new FormControl(null,{
        validators: [Validators.required]
      }),
      postcode: new FormControl(null,{
      
      }),
      city: new FormControl(null,{
        validators: [Validators.required]
      }),
      city_id: new FormControl(null,{
        validators: [Validators.required]
      }),
      street: new FormControl(null,{
        validators: [Validators.required]
      }),
      location: new FormControl(null,{
      
      }),
      district: new FormControl(null,{
       
      }),
      district_code: new FormControl(null,{
       
      }),
    }),
    quote: new FormArray([],{
      validators: [Validators.required]
    }),


   
    customer_group_id: new FormControl(null,{
     
    }),
    transactionFee: new FormControl(null,{
     
    }),

 


    shipping_discount: new FormControl(0),
    profit: new FormControl(null),
    subtotal: new FormControl(null),
    discount: new FormControl(null),

    created_date: new FormControl(null),
    split_payment: new FormControl(null),
    
    discount_description: new FormControl(null),
    total_transaction_fee: new FormControl(null),
    grand_total: new FormControl(null),
    custom_note: new FormControl(null),
    quote_id: new FormControl(null),
  })


  async ngOnInit() {
   
    this.selectedShippingMethod ={
      'price' : null,
      'code' : null,
      'tracking_code' :null
    }

    for(let i = 0; i<= 60; i++){
      this.transactionFeeSelection.push(  {
        name: `${i*0.5}%`,
        value: `${i*0.5}`,
      },)
    }

    this.dropshipMode=false
    this.formStep=1




  }

  changeStockLevel(e){
    console.log(e)
  }

  getFormData(input){
    let res = this.manualOrderForm.get(input).value
    if(res) return res
    else return null
  }

  setCustomerGroup(input){
    this.manualOrderForm.controls.customer_group_id.setValue(input)
  }

  getCustomerGroup(){
    let res = this.customerGroupSelection.find(item => 
      item.customer_group_id == this.manualOrderForm.controls.customer_group_id.value)
    if(res) return res.customer_group_code
    else return null
  }

  enabledCustomerGroup(input){
    if(this.manualOrderForm.controls.customer_group_id.value == input){
      return true
    }
  }


  setTransactionFee(input){
    this.manualOrderForm.controls.transactionFee.setValue(input)
  }

  getTransactionFee(){
    let res = this.transactionFeeSelection.find(item => item.value == this.manualOrderForm.controls.transactionFee.value)
    if(res) return res.name
    else return ""
  }

  enabledTransactionFee(input){
    if(this.manualOrderForm.controls.transactionFee.value == input){
      return true
    }
  }
  
  enabledNewCustomerFlagClass(input){
    if(input == this.manualOrderForm.controls.newcustomerflag.value){
      return {
        'hollow': false,
      }
    }else{
      return {
        'hollow': true
      }
    }
  }



  isAllow(){
   if(this.manualOrderForm.get('customer.email').value=='online_chat@wia.id'){
    return false
   }else if(this.manualOrderForm.get('customer.email').value=='b2b@wia.id'){
    return false
   }else{
    return true
   }
  }

  selectCity(item){
    console.log(item.city_name)
    // this.city_keyword = item.city_name
     this.getDistrict(item.city_name)
      this.show_city = false
  }

  changeDistrict(){
    this.selectedDistrict=null

  }
  selectDistrict(item){
    
    console.log(item)
     this.selectedDistrict = item
    
     this.districtList = null
     this.cityList = null
    // console.log(this.selectedDistrict)
    // this.onClosePinpoint()

    this.manualOrderForm.controls.shipping_address.get('region').setValue(this.selectedDistrict.province)
    this.manualOrderForm.controls.shipping_address.get('region_id').setValue(this.selectedDistrict.province_id)
    this.manualOrderForm.controls.shipping_address.get('city_id').setValue(this.selectedDistrict.city_id)
    this.manualOrderForm.controls.shipping_address.get('city').setValue(this.selectedDistrict.city_name)
    this.manualOrderForm.controls.shipping_address.get('district').setValue(this.selectedDistrict.subdistrict_id)
    this.manualOrderForm.controls.shipping_address.get('district_code').setValue(this.selectedDistrict.district_code)
    this.manualOrderForm.controls.shipping_address.get('postcode').setValue(this.selectedDistrict.postal_code)




    // this.isLoadMap=true
  }



  findDistrict(value){
    if(value){
      this.getDistrict(value)
      console.log(value)
        this.show_city = true
     
    }

  }

  async getShippingMethod(){
    console.log(this.selectedDistrict)
    try{
       
      let res = await this.manualOrderSvc.getShippingRates(this.manualOrderForm.value)
      console.log(res)
      this.shipping_rates = res.items



    }catch(e){
      console.warn(e)
     
    } finally{
      return
    }

  }



  async getDistrict(id){
    let res = await await this.manualOrderSvc.getDistrict(id)
    console.log(res)
    this.districtList = res.district
    this.cityList = res.city
  }
  


  toggleCustomShipping(){
    this.is_custom_shipping = !this.is_custom_shipping
  }
  async addToOrder(){
    this.last_payment_link = null
    this.last_order_id = null
    let body = this.manualOrderForm.value

    body.shipping_address.region_id = parseInt(body.shipping_address.region_id)
    body.shipping_method = this.selectedShippingMethod?this.selectedShippingMethod:'CUSTOM',
    body.selected_shipping_code = this.selectedShippingMethod.code?this.selectedShippingMethod.code:'CUSTOM',
    
    body.payment_link = this.paymentLink
    body.is_processing = this.is_processing

    console.log(body)

    this.spinner.show()
  
   
    try{
       let res = await this.manualOrderSvc.createManualOrder(body)
      // console.log('msk')
       console.log(res)
       
       
       this.toast.success("Success")
       if(this.paymentLink){
        window.open( res.snap.redirect_url, '_blank');

       }

    }catch(e){
      
      console.warn(e)
    }finally{
      this.spinner.hide()

            //bersih bersih form
  
            let quoteForm=<FormArray> this.manualOrderForm.get('quote')
          
            this.quote_id=null
            this.selectedDistrict=null
            while (quoteForm.length !== 0) {
              quoteForm.removeAt(0)
            }
            this.manualOrderForm.reset()
            
          this.selectedShippingMethod ={
            'price' : null,
            'code' : null,
            'tracking_code' :null
          }
            this.shippingMethods = null
            this.paymentLink = false
            this.is_processing = false

            this.is_custom_shipping = false


    }
  }
 
  async searchChanges(e){
    
    this.search.searchQuery= e
   // console.log(this.search.searchQuery)
    if(this.search.searchQuery.length > 0){
      this.isLoading = true
      this.search.listOfSearchResult=null
      //begin search
     
      this.search.pageNumber=1
      let res = await this.manualOrderSvc.getProductList(this.search.searchQuery,15,this.search.pageNumber, this.buffer_filter)
      this.products = res.products
     
    }else{
      this.clearSearch()
    }
  }

  clearSearch(){
    this.keyword = null
    this.products = null
  }


   
  copyLink(item) {
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (item));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');
    this.toast.success('payment link copied')
  }


   selectShippingMethod(item){
    console.log(item)
    this.selectedShippingMethod = item
    console.log(this.selectedShippingMethod)
   
    this.shipping_rates = null
    this.is_custom_shipping = false
  }





  async loadMoreSearch(e){
    if(this.search.canLoadMore()){
      this.search.pageNumber+=1
      let res = await this.manualOrderSvc.getProductList(this.search.searchQuery,5,this.search.pageNumber, this.buffer_filter)
    console.log(res)

    this.products =  this.products.concat(res.products)
      // this.search.currentLoadedItem+=parseInt(res.items.length)
      // console.log(res.items)
      // console.log(this.search.listOfSearchResult)
      // this.search.listOfSearchResult = this.search.listOfSearchResult.concat(res.items)
      // console.log(this.search.listOfSearchResult)
    }
  }

  async addThisProduct(item){
    console.log(item)
    this.spinner.show();
    try{
     
    let quoteArray = <FormArray>this.manualOrderForm.get('quote');

    

    
    let res = await this.manualOrderSvc.getProductStock(item.id)
    item.qty=res.total_stock

    for(let i=0; i<quoteArray.value.length; i++){
      if(quoteArray.at(i).value.product_id == item.id){
        
        if(quoteArray.at(i).get('request_quantity').value < item.qty){
          quoteArray.at(i).get('request_quantity').setValue(quoteArray.at(i).value.request_quantity+1)
        }
        else{
          this.toast.warning(`There are no more stock available for ${item.name}`)
        }
        return
      }
    }
    
    quoteArray.push(new FormGroup({

      item_id: new FormControl(),
      product_id: new FormControl(item.id),
      name: new FormControl(item.name),
      brand: new FormControl(),
      request_quantity: new FormControl(1),
      available_quantity: new FormControl(res.total_stock),
      url_path: new FormControl(),  
      has_error: new FormControl(),
      margin: new FormControl(item.margin),
      bonus_value: new FormControl(0),
      transaction_fee: new FormControl('2'),
      price: new FormControl(item.price),
      promo_discount_amount: new FormControl(item.promo_discount_amount),
      promo_code: new FormControl(item.promo_code),

     
      custom_price: new FormControl(item.final_price),
      image: new FormControl(item.image),
      installment_term: new FormControl(item.installment),
      product_custom_options_list: new FormControl(),
      custom_options: new FormControl(),
    }))

    this.spinner.hide();
    this.toast.success('product added')



    }catch(e){
      console.warn(e)
     
    } finally{
      this.clearSearch()

    }

    

   
 
  }

  updateProductValue(e){
    console.log(e)
  }

  getQuote(){
    return this.manualOrderForm.get('quote').value
  }

  removeProduct(index){
    let fArray = <FormArray>this.manualOrderForm.get('quote')
    fArray.removeAt(index)
  }

  
  getSubTotal(){
    let fArray = this.manualOrderForm.get('quote').value
    let total=0
    let bonus_value=0
    fArray.forEach(item=>{
      total+=parseInt(item.custom_price) * item.request_quantity
    })
   
    this.subTotal = total 
    this.manualOrderForm.controls.subtotal.setValue(this.subTotal)
    return this.subTotal
  }

  getDiscount(){
    let fArray = this.manualOrderForm.get('quote').value
    let total=0
    let bonus_value=0
    fArray.forEach(item=>{
      total+=parseInt(item.promo_discount_amount) * item.request_quantity
    })
   
   
    this.manualOrderForm.controls.discount.setValue(total)
    return total
  }


  getTotalTransactionFee(){
    let fArray = this.manualOrderForm.get('quote').value
    let total=0
    fArray.forEach(item=>{
      total+= (item.transaction_fee/100) * item.custom_price * item.request_quantity
    })

    this.total_transaction_fee = total

    this.manualOrderForm.controls.total_transaction_fee.setValue(total)
    return total
  }




  getGrandTotal(){

    let grand_total = this.subTotal - parseFloat(this.discount_value?this.discount_value:0) - parseFloat(this.manualOrderForm.get('discount').value?this.manualOrderForm.get('discount').value:0) + this.selectedShippingMethod.price


    this.manualOrderForm.controls.grand_total.setValue(grand_total)
    return grand_total

    
  }


  





}
