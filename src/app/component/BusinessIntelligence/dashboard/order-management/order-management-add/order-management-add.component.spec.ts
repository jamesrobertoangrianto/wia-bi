import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderManagementAddComponent } from './order-management-add.component';

describe('OrderManagementAddComponent', () => {
  let component: OrderManagementAddComponent;
  let fixture: ComponentFixture<OrderManagementAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderManagementAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderManagementAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
