import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-order-management-add',
  templateUrl: './order-management-add.component.html',
  styleUrls: ['./order-management-add.component.scss']
})
export class OrderManagementAddComponent implements OnInit {
  @Output() onClose = new EventEmitter()

  orderForm = new FormGroup({

    sales_channel: new FormControl(null, { validators: [Validators.required] }),
    sales_channel_id: new FormControl(null),
    order_date: new FormControl(null),

    customer_email: new FormControl(null),
    status: new FormControl('NEW_ORDER'),
    payment_status: new FormControl(),

    payment_link: new FormControl(),


    shipping_phone: new FormControl(null),
    shipping_address: new FormControl(null),
    shipping_address_name: new FormControl(null),
    shipping_district: new FormControl(null),
    shipping_city: new FormControl(null),
    shipping_region: new FormControl(null),

    shipping_method: new FormControl(null),
    shipping_method_label: new FormControl(null),
    shipping_cost: new FormControl(null),
    shipping_number: new FormControl(null),


    payment_bank: new FormControl(),
    payment_method: new FormControl(),


    product: new FormArray([], {
      validators: [Validators.required]
    }),

  })


  tab_option = [


    // {
    //   'label' : 'WIA WEB',
    //   'name' : 'wia_web',
    //   'is_active' : true

    // },
    {
      'label': 'ONLINE CHAT',
      'name': 'online_chat',
    },
    {
      'label': 'WIA STORE',
      'name': 'wia_store',
    },
    {
      'label': 'TOKOPEDIA WIA',
      'name': 'tokopedia_wia',
    },
    {
      'label': 'TOKOPEDIA DRONESTORE',
      'name': 'tokopedia_ds',
    },
    {
      'label': 'TOKOPEDIA KINTO',
      'name': 'tokopedia_kinto',
    },
    {
      'label': 'BLI BLI WIA',
      'name': 'bli_bli_wia',
    },
    {
      'label': 'BLI BLI DRONESTORE',
      'name': 'bli_bli_ds',
    },
    {
      'label': 'SHOPEE WIA',
      'name': 'shopee_wia',
    },
    {
      'label': 'ETHIX KG',
      'name': 'ethix_kg',
    },
    {
      'label': 'ETHIX CP',
      'name': 'ethix_cp',
    },
    {
      'label': 'RETAILER',
      'name': 'retailer',
    },
    {
      'label': 'TIKTOK WIA',
      'name': 'tik_tok_wia',
    },
    {
      'label': 'B2B',
      'name': 'b2b',
    },


  ]
  product: any;
  is_custom_shipping: any;
  is_processing: any;
  is_payment_link: any;

  city_list: any;
  variant: any;
  isLoading: boolean;
  custom_date: any;
  keyword: any;
  page: number;
  constructor(
    private webService: ManualOrderService,
    private toast: ToastService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    let quoteForm = <FormArray>this.orderForm.get('product')
    while (quoteForm.length !== 0) {
      quoteForm.removeAt(0)
    }
    this.product = []
    this.is_processing = false
    this.orderForm.reset()


    let currentDate = new Date().toLocaleDateString()
    var today = new Date();
    console.log(currentDate)
    //  this.orderForm.get('order_date').setValue(currentDate)
  }



  async getProductSearch(e) {

    if (!e) {
      this.product = []
    }
    if (e.length > 2) {

      this.keyword = e
      try {

        let response = await this.webService.getProduct('?search=' + e)
        console.log(response)
        this.product = response.data
        this.page = 1
      } catch (e) {
        console.log(e)
      } finally {

      }
    }

  }



  async loadMoreSearch() {
    console.log('load more search')
    this.page += 1;
    try {

      let response = await this.webService.getProduct('?search=' + this.keyword + '&page=' + this.page)
      console.log(response)

      this.product = this.product.concat(response.data)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  toggleShipping() {
    this.is_custom_shipping = !this.is_custom_shipping
  }



  async getProductVariant(item) {
    try {

      let response = await this.webService.getProductVariants(item.id)
      this.variant = response.data



    } catch (e) {
      console.log(e)
    } finally {

    }

  }

  async addProduct(item) {
    console.log(item)
    try {

      let quoteArray = <FormArray>this.orderForm.get('product');


      for (let i = 0; i < quoteArray.value.length; i++) {
        if (quoteArray.at(i).value.entity_id == item.entity_id) {
          console.log('here')
          if (quoteArray.at(i).get('order_qty').value < item.qty) {
            quoteArray.at(i).get('order_qty').setValue(quoteArray.at(i).value.order_qty + 1)
          }
          else {
            //this.toast.warning(`There are no more stock available for ${item.name}`)
            this.toast.sendMessage(`There are no more stock available for ${item.name}`)

          }
          return
        }
      }


      quoteArray.push(new FormGroup({


        entity_id: new FormControl(item.entity_id),
        name: new FormControl(item.name ? item.name : item.variant_name),
        brand: new FormControl(item.brand?.name ? item.brand?.name : '-'),
        order_qty: new FormControl(1),
        weight: new FormControl(item.weight),
        price: new FormControl(item.price),
        image_url: new FormControl(item.feature_image),
        special_price: new FormControl(item.special_price),


        final_price: new FormControl(item.final_price),

        discount_amount: new FormControl(0),


      }))





    } catch (e) {
      console.warn(e)

    } finally {
      this.product = []


    }





  }

  getQuote() {
    return this.orderForm.get('product').value
  }

  removeProduct(index) {
    let fArray = <FormArray>this.orderForm.get('product')
    fArray.removeAt(index)
  }

  updateProductQty(state, product, i) {

    if (state) {
      product.value.order_qty = product.value.order_qty + 1

    } else {

      product.value.order_qty = product.value.order_qty - 1
      if (product.value.order_qty <= 0) {
        console.log('remove')
        this.removeProduct(i)
      }
    }
  }



  selectCity(e) {
    console.log(e)
    this.orderForm.get('shipping_region').setValue(e.province)
    this.orderForm.get('shipping_city').setValue(e.city)
    this.orderForm.get('shipping_district').setValue(e.district)

  }




  async getDistrict(id) {
    let res = await await this.webService.getDistrict(id)
    console.log(res)
    if (res.district) {
      this.city_list = res.district.map(item => {

        return {
          id: item.city_id,
          label: item.province + ',' + item.city_name + ',' + item.subdistrict_name,
          district: item.subdistrict_name,
          city: item.city_name,
          province: item.province
        };
      });

    }

  }

  convertNumber(num) {
    return parseFloat(num)
  }


  async addOrder() {
    if (this.is_processing) {
      this.orderForm.get('status').setValue('PROCESSING')
      this.orderForm.get('payment_status').setValue(true)

    } else {
      this.orderForm.get('status').setValue('NEW_ORDER')

    }

    if (this.custom_date) {
      this.orderForm.get('order_date').setValue(this.custom_date)

    }

    if (this.is_payment_link) {
      this.orderForm.get('payment_link').setValue(true)
    }

    try {
      console.log(this.orderForm.value)

      let response = await this.webService.addSalesOrder(this.orderForm.value)

      if (response.data) {
        console.log(response.data)
        this.toast.sendMessage('Order Created')

        this.custom_date = null

      }
    } catch (e) {
      console.log(e)
    } finally {
      this.ngOnInit()

    }
  }


  getSubtotal() {
    let fArray = this.orderForm.get('product').value
    let total = 0

    fArray.forEach(item => {
      total += parseInt(item.final_price) * item.order_qty
    })

    return total ? total : 0
  }
  getShippingCost() {
    let total = this.orderForm.get('shipping_cost').value
    return parseFloat(total ? total : 0)
  }
  getTotalDiscount() {
    let fArray = this.orderForm.get('product').value
    let total = 0

    fArray.forEach(item => {
      if (item.discount_amount) [
        total += parseInt(item.discount_amount) * item.order_qty

      ]
    })

    return total ? total : 0;
  }
  getTotal() {
    return this.getSubtotal() + this.getShippingCost() - this.getTotalDiscount()
  }


}
