import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderManagementViewComponent } from './order-management-view.component';

describe('OrderManagementViewComponent', () => {
  let component: OrderManagementViewComponent;
  let fixture: ComponentFixture<OrderManagementViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderManagementViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderManagementViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
