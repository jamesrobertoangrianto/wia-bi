


import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-referal-view',
  templateUrl: './referal-view.component.html',
  styleUrls: ['./referal-view.component.scss']
})
export class ReferalViewComponent implements OnInit {
  active_view: string;
  history: any;
  affiliate_id: string;

  tab_menu_list = [
    
    {
      'id':'transaction',
      'label' : 'Transaction'
    },
    {
      'id':'withdrawal',
      'label' : 'Withdrawal'
    }
  
  ]
tab_view: any;
  member_id: string;
  withdrawal: any;
  transaction: any;

  constructor(
    private affiliateService : ManualOrderService,
    
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router : Router,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.member_id = params.get("id")
    
    })


    this.route.queryParamMap.subscribe(queryParams => {
      this.getAffiliateMemberTransaction(this.member_id)
      this.getAffiliateMemberWithdrawal(this.member_id)
      


      this.tab_view = queryParams.get("tab_view")
     
    })

    


   

    

  }

  
  async getAffiliateMemberTransaction(id){
     
    try {
      this.spinner.show();
         let response = await this.affiliateService.getAffiliateMemberTransaction(id)
         this.transaction = response.data
         
          console.log(response)
         
      
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        }
  }


  
  async getAffiliateMemberWithdrawal(id){
     
    try {
      this.spinner.show();
         let response = await this.affiliateService.getAffiliateMemberWithdrawal(id)
         this.withdrawal = response.data
          console.log(response)
         
      
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        }
  }
  

}
