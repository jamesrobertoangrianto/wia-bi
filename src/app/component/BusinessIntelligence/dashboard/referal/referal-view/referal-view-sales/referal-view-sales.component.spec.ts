import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferalViewSalesComponent } from './referal-view-sales.component';

describe('ReferalViewSalesComponent', () => {
  let component: ReferalViewSalesComponent;
  let fixture: ComponentFixture<ReferalViewSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferalViewSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferalViewSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
