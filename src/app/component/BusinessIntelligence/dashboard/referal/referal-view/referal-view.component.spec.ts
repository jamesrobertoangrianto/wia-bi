import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferalViewComponent } from './referal-view.component';

describe('ReferalViewComponent', () => {
  let component: ReferalViewComponent;
  let fixture: ComponentFixture<ReferalViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferalViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferalViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
