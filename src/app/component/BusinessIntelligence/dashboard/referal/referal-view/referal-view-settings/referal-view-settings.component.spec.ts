import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferalViewSettingsComponent } from './referal-view-settings.component';

describe('ReferalViewSettingsComponent', () => {
  let component: ReferalViewSettingsComponent;
  let fixture: ComponentFixture<ReferalViewSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferalViewSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferalViewSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
