import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-referal-view-settings',
  templateUrl: './referal-view-settings.component.html',
  styleUrls: ['./referal-view-settings.component.scss']
})
export class ReferalViewSettingsComponent implements OnInit {
  affiliate_id: string;
  affiliate: any;

  constructor(
    private affiliateService : ManualOrderService,
    
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router : Router,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.affiliate_id = params.get("id")
     this.getSettingsById(this.affiliate_id)
    })

  }

  
  
  async getSettingsById(id){
     
    try {
      this.spinner.show();
         let response = await this.affiliateService.getAffiliateByAffiliateId(id)
        // this.promotions = response.promotions
          console.log(response)
          this.affiliate = response.item
         
          
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        }
  }

}
