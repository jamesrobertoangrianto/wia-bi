import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronDown, faExchangeAlt, faExpand, faExpandAlt, faExpandArrowsAlt } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-referal',
  templateUrl: './referal.component.html',
  styleUrls: ['./referal.component.scss']
})
export class ReferalComponent implements OnInit {
  faExpandAlt=faExpandAlt
  members: any;
  active_view: string;
  balances: any;
  nameKey: any;
  customer: any;
  faChevronDown=faChevronDown
  customers: any;

  sortListItems = [
    {
     
      'name' : 'status',
      'items' : [
        {
          'id' : 'status',
          'name' : 'PENDING',
          'label' : 'Pending',
    
        },
        {
          'id' : 'status',
          'name' : 'ON_PROCESS',
          'label' : 'on process',
    
        },
        {
          'id' : 'status',
          'name' : 'COMPLETE',
          'label' : 'complete',
    
        },
      ]

    },
    
  ]


  sortListItemss = [
    {
     
      'name' : 'is_filter',
      'items' : [
        
        {
          'id' : 'status',
          'name' : 'true',
          'label' : 'Affiliate Product',
    
        },
     
      ]

    },
    
  ]

  tab_menu_list = [
    {
      'id':'members',
      'label' : 'Members'
    },
   
    {
      'id':'withdrawal',
      'label' : 'Withdrawal'
    },
    {
      'id':'product_setting',
      'label' : 'Affiliate Product'
    },
  
  ]
  params: string;
  product: any;
  withdrawal: any;
  isShowReference: boolean;

  constructor(
    private affiliateService : ManualOrderService,
    
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router : Router,
    private serializer: UrlSerializer,
  ) { }

  ngOnInit(): void {
    
    this.getAffiliateMember()
   
    this.route.queryParamMap.subscribe(queryParams => {

      this.params = this.serializer.serialize(this.router.createUrlTree([''],
      { queryParams: this.route.snapshot.queryParams}))     


      this.active_view = queryParams.get("tab_view")
     
      

      if(this.active_view == 'withdrawal'){
        this.getAffiliateWithdrawal()
      }

      if(this.active_view == 'product_setting'){
        this.getAffiliateProductSetting()
      }
     
     
    })



  }


  async getAffiliateWithdrawal(){
    // console.log(this.params)
    try {
      this.spinner.show();
         let response = await this.affiliateService.getAffiliateWithdrawal(this.params)
        // this.promotions = response.promotions
          console.log(response)
          this.withdrawal = response.data
      
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        }
  }

  async getAffiliateProductSetting(){
    try {
      this.spinner.show();
         let response = await this.affiliateService.getAffiliateProductSetting(this.params)
        this.product = response.data
        console.log(response)
         
      
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        }
  }



  async updateProduct(item,e){
    
    item[e.id] = e.value

    if(e.value>20){
      if(confirm('you are setting to '+e.value+'% continue?') == true){
        try {
          this.spinner.show();
             let response = await this.affiliateService.editAffiliateProductSetting(item)
            // this.promotions = response.promotions
              console.log(response)
             
            } catch (e) {
              console.log(e)
            } finally {
              item.edit = false
                this.spinner.hide();
            }
      }else{
      this.ngOnInit()
        
      }
    }
    else{

      try {
        this.spinner.show();
           let response = await this.affiliateService.editAffiliateProductSetting(item)
          // this.promotions = response.promotions
            console.log(response)
           
          } catch (e) {
            console.log(e)
          } finally {
            item.edit = false
              this.spinner.hide();
          }
    }
   
  }

  async confirm(item){
    console.log(item)
    
    item.status = 'complete'
    try {
      this.spinner.show();
         let response = await this.affiliateService.updateAffiliateWithdrawal(item,item.id)
        // this.promotions = response.promotions
          console.log(response)
         
        } catch (e) {
          console.log(e)
        } finally {
          item.edit = false
            this.spinner.hide();
        }
  }

  async getAffiliateMember(){
     
    try {
      this.spinner.show();
         let response = await this.affiliateService.getAffiliateMember()
         this.members = response.data
          console.log(response)
         
      
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        }
  }
  setCustomer(item,customer){
    item.customer_email = customer.email
    item.customer_id = customer.id
    console.log(item)
    console.log(customer)
 
  }

  async getAffiliateMembersByName(item){
    
    try {
      this.spinner.show();
         let response = await this.affiliateService.getAffiliateMembersByName(item.nameKey)
        this.customers = response.employees
          console.log(response)
         
      
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        }
  }


  async AddNewMember(){
    var value
    try {
      this.spinner.show();
         let response = await this.affiliateService.addAffiliateMember(value)
   
          console.log(response)
        this.members.push(response.item)
      
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        }
  }

  editMember(item){
    item.edit = true
  }
  async updateMember(item,value){
    item.action = value
    try {
      this.spinner.show();
         let response = await this.affiliateService.updateAffiliateMember(item)
         var index = this.members.indexOf(item);
         this.members.splice(index, 1);
          console.log(response)
     
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
        }
  }

  removeEmail(item){
    item.customer_email = null
    item.nameKey = null
   
  }

  async saveMember(item){
    item.action = 'SAVE'
    try {
      this.spinner.show();
         let response = await this.affiliateService.updateAffiliateMember(item)
        
          console.log(response)
     
        } catch (e) {
          console.log(e)
        } finally {
            this.spinner.hide();
            item.edit = false
            this.ngOnInit()
        }
  }


}



