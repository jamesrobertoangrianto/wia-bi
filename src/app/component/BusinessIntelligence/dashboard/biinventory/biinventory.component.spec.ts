import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BIInventoryComponent } from './biinventory.component';

describe('BIInventoryComponent', () => {
  let component: BIInventoryComponent;
  let fixture: ComponentFixture<BIInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BIInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BIInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
