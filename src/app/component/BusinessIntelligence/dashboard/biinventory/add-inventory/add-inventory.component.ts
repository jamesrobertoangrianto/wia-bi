import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators, FormBuilder } from '@angular/forms';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { validPhoneNumber, phoneFormatter, formError } from 'src/app/forms/validation';
import { faTimesCircle, faChevronLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-add-inventory',
  templateUrl: './add-inventory.component.html',
  styleUrls: ['./add-inventory.component.scss']
})
export class AddInventoryComponent implements OnInit {

 
  @ViewChild('searchQuery',{static: true}) searchInput: ElementRef;
  isLoading: boolean;
  isExistingCusomer: boolean;
  isExistingCustomer: boolean;
  showModal: boolean;
  discount_value: any;
  subTotal: number;
  total_transaction_fee: number;
  selectedProductItem: any
 
  faChevronLeft=faChevronLeft
  faTimesCircle=faTimesCircle
  final_quote: any;
  shipping_fee: any;
  purchase_order: any;
  products: any;
  buffer_filter: any;

  constructor(
    private manualOrderSvc: ManualOrderService,
    private toast: ToastrService,
    private spinner: NgxSpinnerService,
  ) {
    
  }


  search={
    searchQuery: null,
    currentLoadedItem: null,
    maxItem: null,
    listOfSearchResult: null,
    pageNumber: 1,
    canLoadMore: () => this.search.currentLoadedItem <= this.search.maxItem
  }

  customerGroupSelection=[]

  async ngOnInit() {
    this.preparePo()
  }


  preparePo(){
    this.purchase_order = {
      shipping_fee : null,
      subtotal : null,
      vendor_name : null,
      inventory_type : null,
      total_product : null,
      term_of_payment : null,
      items:[],
      ids:[]
    }
  }
 




  async addPurchaseOrder(){
  //   let body = this.manualOrderForm.value
   console.log(this.purchase_order)
   if(this.purchase_order){
    this.isLoading = true
    this.spinner.show()
    try{
      let response = await this.manualOrderSvc.addInventory(this.purchase_order)
      console.log(response)   
      
         
      this.isLoading = false
    }catch(e){
     
      console.warn(e)
    }finally{
      this.spinner.hide()
      this.toast.success('success')
      this.preparePo()
      
    }

   
   }
  
  }

  

  async searchChanges(e){
    
    this.search.searchQuery= e.target.value
   // console.log(this.search.searchQuery)
    if(this.search.searchQuery.length >= 3){
      this.isLoading = true
      this.search.listOfSearchResult=null
      //begin search
     
      this.search.pageNumber=1
      let res = await this.manualOrderSvc.getProductList(this.search.searchQuery,5,this.search.pageNumber, this.buffer_filter)
      this.products = res.products
      console.log(res)
      // if(res.items){
      //   this.isLoading = false
      //   this.search.maxItem=res.count
      //   this.search.currentLoadedItem = parseInt(res.items.length)
      //   this.search.listOfSearchResult=res.items
      // }
    }else{
      this.search.listOfSearchResult=null
    }
  }




  clearSearch(){
    this.searchInput.nativeElement.value=""
    this.search.listOfSearchResult=null
  }

  async loadMoreSearch(e){
    this.search.pageNumber +=1
      let res = await this.manualOrderSvc.getProductList(this.search.searchQuery,5,this.search.pageNumber, this.buffer_filter)
      console.log(res)
     // this.search.currentLoadedItem+=parseInt(res.items.length)
      // console.log(res.items)
      // console.log(this.search.listOfSearchResult)
   this.products= this.products.concat(res.products)
  }

  addThisProduct(item){
   
    

    if(!this.purchase_order.ids.includes(item.id)){
      this.purchase_order.ids.unshift(item.id)
      this.purchase_order.items.unshift({
           
            product_id: item.id,
            name: item.name,
            brand: item.brand,
            request_quantity: 1,
            stock_buffer: item.stock_buffer?item.stock_buffer:'-',
            qty : item.qty,
            inventory_stock : this.getTotalStockLeft(item.inventory),
            price: parseFloat(item.price),
            order_price: parseFloat(item.price)  - (item.price * (item.margin * 1/100))
           
      })
    }

   
    console.log(this.purchase_order)

 
  }

  getMargin(item,i){
    var a = Math.round ( ((item.price - item.order_price) / item.price) * 100)
    this.purchase_order.items[i].margin=a
    return a
  }

  
  getSubtotal(){
    if(this.purchase_order.items){
      let total = 0
      this.purchase_order.items.forEach(item => {
        total += parseFloat(item.order_price) * item.request_quantity
      });
      this.purchase_order.subtotal = total
      return total
    }
    
  }

  getTotalStockLeft(items){
    let total = 0
    if(items){
     
      items.forEach(item => {
        total += parseFloat(item.stock_left)
      });
      return total
      
    }
    return total
   
   
    
  }

  getTotal(){
    return this.getSubtotal() + this.purchase_order.shipping_fee
    
    
  }
  getTotalProduct(){
    if(this.purchase_order.items){
      let total = 0
      this.purchase_order.items.forEach(item => {
        total += parseFloat(item.request_quantity) 
      });
      this.purchase_order.total_product = total
      return total
    }
    
    
  }


  remove(index){
    this.purchase_order.items.splice(index,1);
    this.purchase_order.ids.splice(index,1);
  }



}
