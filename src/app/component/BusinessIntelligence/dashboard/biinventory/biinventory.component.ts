import { Component, OnInit, ViewChild } from '@angular/core';
import { BusinessIntelligenceService } from 'src/app/services/business-intelligence/business-intelligence.service';
import {Stock} from 'src/app/model/stock'
import { MatPaginator } from '@angular/material/paginator';
import { NgxSpinnerService } from 'ngx-spinner';

import {faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-biinventory',
  templateUrl: './biinventory.component.html',
  styleUrls: ['./biinventory.component.scss']
})
export class BIInventoryComponent implements OnInit {
  @ViewChild('paginator') paginator: MatPaginator;



  shownEntries: any[]
  inventory: any;
  inventorys: any;
  inventory_amount: any;
  total_inventory_product: any;
  total_inventory_sold_product: number;
  selectedItem: any;
  selectedItemIndex: any;
  faChevronRight=faChevronRight
  inventory_list_container: string;
  display_inventorys: any;
  stock_item: any;
  consignment: any;
  inventory_age: string;
  start_date: string;
  end_date: string;
  total_product: number;
  total_product_left: number;
  total_order_price: number;
  query: any;
  total_consignment_amount: number;
  total_stock_amount: number;
  
  constructor(
    private biSvc: BusinessIntelligenceService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
  ) {
   
  }

  async ngOnInit() {
    // this.inventory_list_container = 'medium-12 columns'
    this.getInventoryList()

    
    this.route.queryParamMap.subscribe(queryParams => {

      this.start_date = queryParams.get("start")
      this.end_date = queryParams.get("end")
      this.inventory_age = queryParams.get("inventory_age")
      console.log( this.start_date)
      console.log( this.end_date)
      if(this.start_date){
        this.filterInventory(this.start_date,this.end_date)
      }else{
        this.getInventoryList()
      }
     
     
    })




    
   
   
  }

  filterInventory(start,end){
  console.log(end)
  console.log(start)
    var today = new Date()
    var priorDate = new Date(new Date().setDate(today.getDate() - start));
    var endDate = new Date(new Date().setDate(today.getDate() - end));
    console.log(priorDate.getTime())
    console.log(today.getTime())

    if(this.inventorys){
      this.display_inventorys  = this.inventorys.filter(item =>
        this.todate(item.receive_date) > endDate.getTime()  && this.todate(item.receive_date) < priorDate.getTime() 
       
      )
  
      this.updateInventorySummary(this.display_inventorys)
    }
   
      
    
 
  }

  updateInventorySummary(items){
    this.total_product = 0

    this.total_consignment_amount = 0
    this.total_stock_amount = 0

    this.total_product_left = 0
    this.total_order_price = 0
    items.forEach(item => {
      this.total_product += parseFloat(item.order_qty)
      this.total_product_left += parseFloat(item.stock_left)
      this.total_order_price += parseFloat(item.stock_left) * parseFloat(item.order_price)

      if(item.inventory_type == 'CONSIGNMENT'){
        this.total_consignment_amount += parseFloat(item.stock_left) * parseFloat(item.order_price)
      
      }
      if(item.inventory_type == 'STOCK_ITEM'){
        this.total_stock_amount += parseFloat(item.stock_left) * parseFloat(item.order_price)
      }
     
    });
  }

  todate(date){
  
    return new Date(date).getTime()
  }

  getCriteria(name){
  
   var a = parseFloat(name)
   if(a>=0){
    if(  a < 0.45  ){
      return 'Non moving'
    }

    if(  a < 0.9  ){
      return 'Slow moving'
    }
    else{
      return 'Fast Moving'
    }

   }

  }

  
  getCriteriaCSS(name){
//   console.log(name)
  
  
    var a = parseFloat(name)
    if(a>=0){
      if( a < 0.45  ){
        return 'profit-65'
      }
      if( a < 0.9  ){
        return 'profit-90'
      }
      else{
        return 'profit-100'
      }
    

    }

  
 
  
  }

  getProgressPersentage(from, to){
   
    var a = 100 - ( ((to - from) / to) * 100)

     return Math.round(a) 

   }


   fiterBy(name){
     console.log(name)
     this.display_inventorys  = this.inventorys.filter(item =>
      //  item.vendor.toLowerCase().includes(name.toLowerCase()) ||
      //   item.purchase_order_id.toString().includes(name.toLowerCase()) 
        this.getProductFilter(item,name)
       )
      
      this.getInventorySummary(this.display_inventorys,name)
  }

  getProductFilter(item,name){
    var a = item.items.filter(product =>
      product.name.toLowerCase().includes(name.toLowerCase())
    )
    if(a.length>0){
      return true
    }
  }

  getInventoryAge(date){
    var today = new Date()
    var receive = new Date(date)
  
   return Math.round((today.getTime() - receive.getTime())/(1000*60*60*24));
  }
  
  getInventoryAgeClass(date){
    var today = new Date()
    var receive = new Date(date)
    var age = Math.round((today.getTime() - receive.getTime())/(1000*60*60*24));
    if(age > 30){
      return 'profit-90'
    }
    if(age > 60){
      return 'profit-80'
    }
    if(age > 90){
      return 'profit-70'
    }
    if(age > 120){
      return 'profit-65'
    }
   
  }
  toggleEdit(item){
    item.isEdit = !item.isEdit 
  }
  async updateInventory(item){
    console.log(item)
    item.isEdit = false

    try {
      this.spinner.show();
      let response = await this.biSvc.updateInventoryByItem(item)
      console.log(response)
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    
    }
  }

 
  async search() {
    let isAdmin = 1
    console.log(this.query)
    try {
      this.spinner.show();
      let response = await this.biSvc.getInventoryByName(this.query)
      console.log(response)
       this.inventorys = response.inventorys
      this.display_inventorys =  this.inventorys 
  
    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.updateInventorySummary(this.display_inventorys)
      if(this.start_date){
        this.filterInventory(this.start_date,this.end_date)
      }
    }
  }

  async getInventoryList() {
  let isAdmin = 1
  try {
    this.spinner.show();
    let response = await this.biSvc.getInventory()
    console.log(response)
     this.inventorys = response.inventorys
    this.display_inventorys =  this.inventorys 

  } catch (e) {
    console.log(e)
  } finally {
    this.spinner.hide();
    this.updateInventorySummary(this.display_inventorys)
    if(this.start_date){
      this.filterInventory(this.start_date,this.end_date)
    }
  }
}



getInventorySummary(items,name){
  this.stock_item = {
   
    buyying_rate :  this.getAverageBuyingRates(items,'STOCK_ITEM',name),//this.getTotalProductOrderQty(items,'STOCK_ITEM',name) -  this.getTotalProductStockLeft(items,'STOCK_ITEM',name),
    total_product : this.getTotalProductStockLeft(items,'STOCK_ITEM',name)?this.getTotalProductStockLeft(items,'STOCK_ITEM',name):0,
    total_amount : this.getTotalInventoryAmount(items,'STOCK_ITEM',name)?this.getTotalInventoryAmount(items,'STOCK_ITEM',name):0,
    total_order : this.getTotalProductOrderQty(items,'STOCK_ITEM',name)?this.getTotalProductOrderQty(items,'STOCK_ITEM',name):0,
    avarage_age : this.getAvarageinventoryAge(items,'STOCK_ITEM',name)?this.getAvarageinventoryAge(items,'STOCK_ITEM',name):0,// Math.round(this.getTotalProductStockLeftxAge(items,'STOCK_ITEM',name) / this.getTotalProductStockLeft(items,'STOCK_ITEM',name)),
    avarage_stock_level : this.getAverageStockLevel(items,'STOCK_ITEM',name)
    
  }

  this.consignment = {
    
    total_product : this.getTotalProductStockLeft(items,'CONSIGNMENT',name)?this.getTotalProductStockLeft(items,'CONSIGNMENT',name):0,
    buyying_rate : this.getAverageBuyingRates(items,'CONSIGNMENT',name)?this.getAverageBuyingRates(items,'CONSIGNMENT',name):0,
    total_amount : this.getTotalInventoryAmount(items,'CONSIGNMENT',name)?this.getTotalInventoryAmount(items,'CONSIGNMENT',name):0,
    total_order : this.getTotalProductOrderQty(items,'CONSIGNMENT',name)?this.getTotalProductOrderQty(items,'CONSIGNMENT',name):0,
    avarage_age : this.getAvarageinventoryAge(items,'CONSIGNMENT',name)?this.getAvarageinventoryAge(items,'CONSIGNMENT',name):0,//Math.round(this.getTotalProductStockLeftxAge(items,'CONSIGNMENT',name) / this.getTotalProductStockLeft(items,'CONSIGNMENT',name)),
    avarage_stock_level :  this.getAverageStockLevel(items,'CONSIGNMENT',name)?this.getAverageStockLevel(items,'CONSIGNMENT',name):0// Math.round (100 - ( ((this.getTotalProductOrderQty(items,'CONSIGNMENT',name) -  this.getTotalProductStockLeft(items,'CONSIGNMENT',name)) / this.getTotalProductOrderQty(items,'CONSIGNMENT',name)) * 100) )
  }
 
} 


getAverageStockLevel(items,type,name){
  return Math.round (100 - ( ((this.getTotalProductOrderQty(items,type,name) -  this.getTotalProductStockLeft(items,type,name)) / this.getTotalProductOrderQty(items,type,name)) * 100) )
}

getAverageBuyingRates(items,type,name){
  return ( ( this.getTotalProductOrderQty(items,type,name) -  this.getTotalProductStockLeft(items,type,name)) / this.getAvarageinventoryAge(items,type,name) ).toFixed(2)
 }
 


getAvarageinventoryAge(items,type,name){
  return Math.round(this.getTotalProductStockLeftxAge(items,type,name) / this.getTotalProductStockLeft(items,type,name))
 }
 



getTotalProductStockLeftxAge(items,type,name){
  let total_order_item = 0
  items.forEach(item=>{
    if(item.inventory_type === type  ){
      total_order_item += this.getProductStockLeft(item.items,name) * this.getInventoryAge(item.receive_date)
    
    }
  
  })
  return total_order_item
}




getTotalProductStockLeft(items,type,name){
  let total_order_item = 0
  items.forEach(item=>{
    if(item.inventory_type === type  ){
      total_order_item += this.getProductStockLeft(item.items,name)
    
    }
  
  })
  return total_order_item
}

getTotalProductOrderQty(items,type,name){
  let total_order_item = 0
  items.forEach(item=>{
    if(item.inventory_type === type  ){
      total_order_item += this.getProductOrderQty(item.items,name)
    
    }
  
  })
  return total_order_item
}



getProductStockLeft(items,name?){
  if(items){
    let total_order_item = 0
      items.forEach(item=>{
        if( item.name.toLowerCase().includes(name.toLowerCase()) ){
          total_order_item += parseFloat(item.stock_left)
        }
        
      })
    return total_order_item;
  }
  
}

getProductOrderQty(items,name?){
  if(items){
    let total_order_item = 0
      items.forEach(item=>{
        if( item.name.toLowerCase().includes(name.toLowerCase()) ){
          total_order_item += parseFloat(item.order_qty)
        }
        
      })
    return total_order_item;
  }
  
}



getTotalInventoryAmount(items,type,name){
  let total_order_item = 0
  items.forEach(item=>{
    if(item.inventory_type === type  ){
      total_order_item += this.getInventoryAmount(item.items,name)
    
    }
  
  })

  return total_order_item
}

getInventoryAmount(items,name){
  if(items){
    let total_order_item = 0
      items.forEach(item=>{
        if( item.name.toLowerCase().includes(name.toLowerCase()) ){
          
          total_order_item += parseFloat(item.stock_left) * item.order_price
        }
        
      })
    return total_order_item;
  }
  
}





getTotalStockItemAmount(){
  let sold = 0
  let order = 0

  if(this.inventorys){
    this.inventorys.forEach(item=>{
      if(item.inventory_type === 'STOCK_ITEM'){
       
        order += this.getStockLeftAmount(item.items)
      
      }
    
    })
  
    return order
  }
}

getTotalConsignmentAmount(){
  let sold = 0
  let order = 0

  if(this.inventorys){
    this.inventorys.forEach(item=>{
      if(item.inventory_type === 'CONSIGNMENT'){
        order += this.getStockLeftAmount(item.items)
      
      }
    
    })
  
    return order
  }
}



closeDetails(){
  this.inventory_list_container = 'medium-12 columns'
  this.selectedItem = null
 
}





  


  calcPercent(items){
   if(items){
    return (( this.getStockLeft(items) /  this.getOrderQty(items))*100).toFixed(2)
   }
 
  }


  getOrderQty(items){
    if(items){
      let total_order_item = 0
    items.forEach(item=>{
      total_order_item += parseFloat(item.order_qty)
    })
   return total_order_item;
    }
    
  }



  getStockLeft(items){
    if(items){
      let total_order_item = 0
      items.forEach(item=>{
        total_order_item += parseFloat(item.stock_left)
      })
  
      return total_order_item;
    }
   
    

  }

  getStockLeftAmount(items){
    if(items){
      let total_order_item = 0
      items.forEach(item=>{
        
        total_order_item += parseFloat(item.stock_left) * item.order_price
      })
      return total_order_item;
    }
   
  }

  getSoldAmount(items){
    if(items){
      let total_order_item = 0
      items.forEach(item=>{
        total_order_item += (parseFloat(item.order_qty) - parseFloat(item.stock_left)) * item.order_price
      })
      return total_order_item;
    }
   
  }



  viewDetails(item,i){
    this.selectedItem = item
   this.selectedItemIndex = i
   this.inventory_list_container = 'medium-6 columns'
   console.log(item)
 
 
  }

    
  updatePage(e){
    this.shownEntries=this.inventory.slice(
      (this.paginator.pageIndex)*this.paginator.pageSize,
      (this.paginator.pageIndex+1)*this.paginator.pageSize,
    )
  }
}
