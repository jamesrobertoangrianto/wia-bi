import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

import {faCheckCircle,faCircle,faPrint, faShippingFast, faRecycle, faSync, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  faCheckCircle=faCheckCircle
  faCircle=faCircle
  faPrint=faPrint
  faShippingFast=faShippingFast
  faSync=faSync
  faChevronDown=faChevronDown
  search: any;
  page_view: string;
  keyword: any;
  type: string;
  constructor(
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router : Router,
    private salesService : ManualOrderService,
  ) { }

  ngOnInit(): void {

    //console.log(this.array)

    // this.array.forEach(item => {
    //   console.log(item.district_code)
    // });

    this.route.queryParamMap.subscribe(queryParams => {

      this.page_view = queryParams.get("page_view")
      
     
      this.getSearchItem(null,this.page_view)
      
    })

    
  }

  async getSearchItem(query?,type?) {
    try {
      this.spinner.show();
      let response = await this.salesService.getSearchItem(query?query:'all',type)
      this.search = response
      console.log(response)



    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }

  }

  findSearch(){
    console.log(this.keyword)
    if(this.keyword.length>=3){
      
      this.getSearchItem(this.keyword,this.page_view)
    }
    else if(!this.keyword){
      this.getSearchItem(null,this.page_view)
    }
  
    
  }


  async updateSearch(item) {

   // var item= 'new'

    try {
      this.spinner.show();
      let response = await this.salesService.updateSearch(item)
      this.search = response
      console.log(response)



    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }

  }

  async updateSearchById(item){
    console.log(item)
    
    try {
      this.spinner.show();
      let response = await this.salesService.updateSearchItemById(item)
     // this.search = response
      console.log(response)



    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }

  }


}
