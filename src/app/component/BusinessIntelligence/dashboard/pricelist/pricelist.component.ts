import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { formatNumber, formatDate } from '@angular/common';
pdfMake.vfs = pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-pricelist',
  templateUrl: './pricelist.component.html',
  styleUrls: ['./pricelist.component.scss']
})
export class PricelistComponent implements OnInit {
  products: any;
  display_products: any;
  priceListForm: any;
  buffer_filter: any;

  constructor(
    private salesService : ManualOrderService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
   
    this.preparePo()
    this.priceListForm = {
      brand : null,
      company : null,
      pic :null,
      margin : null,
    }

    //this.getProductList()
  }


  preparePo(){
    this.display_products = []
  }
 

  async getProductList() {
    var size = 1000;
     var page = 1;
     var name = this.priceListForm.brand
     this.display_products = []
     console.log(this.priceListForm)
    
     if(name){
      try {
        this.spinner.show();
         let response = await this.salesService.getProductList(name,size,page, this.buffer_filter )
         this.products = response.products
          console.log(response)
 
          this.products.forEach(item => {
           this.generateForm(item)
          });
        
     
    
        } catch (e) {
          console.log(e)
        } finally {
           this.spinner.hide();
        }
     }
     
    
   }


  generateForm(item){
    var margin = this.priceListForm.margin
    this.display_products.unshift({
           
      id: item.id,
      name: item.name,
      stock: this._getStock(item),
      msrp: item.price,
      bonus_price: this._getBonusPrice(item.price,margin),
      margin: null,
     
    })  
  }

  download(){
    console.log(this.display_products)
    this.generatePdf();
  }

  generatePdf() {

    var today = new Date()
    const documentDefinition = { 
      
      content: [
        {text: 'Stock Update & Pricelist' ,  margin: [ 0, 10, 0, 5 ],	fontSize: 16,
          bold: true,},
        
          {text: 'Create By : Sinta'  , 	fontSize: 10},
          {text: 'Prepare for : ' + this.priceListForm.company + '(' +this.priceListForm.pic + ')' , 	fontSize: 10},
          {text: 'Brand : ' + this.priceListForm.brand  , 	fontSize: 10},
          {text: 'Date : ' + today.toDateString()   ,	margin: [ 0, 10, 0, 5 ], fontSize: 10},


          this.getEducationObject(),
        
         {text: 'Catatan : \n' + '', margin: [ 0, 20,0, 0 ] , 	fontSize: 12},
        
      
      
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 5, 5]
        },
        tableExample: {
          margin: [0, 0, 0, 0]
        },
        tableHeader: {
          bold: true,
          fontSize: 12,
          color: 'black'
        }
      },
      defaultStyle: {
        // alignment: 'justify'
      }

     };
   
    pdfMake.createPdf(documentDefinition).download()
  }


  getEducationObject() {
    return {
      table: {
       
        widths: [50,"*", 50, 100,100, 50],
       
        body: [
          [{
            text: 'Id',
            style: 'tableHeader'
          },
          {
            text: 'Name',
            style: 'tableHeader'
          },
          {
            text: 'Stock',
            style: 'tableHeader'
          },
          {
            text: 'MSRP ( Rp )',
            style: 'tableHeader'
          },
          {
            text: 'Bonus Price ( Rp )',
            style: 'tableHeader'
          },
          {
            text: 'Margin ( % )',
            style: 'tableHeader'
          },
        
        
          ],
          ...this.display_products.map(item => {
            return [item.id,item.name,item.stock,item.msrp,item.bonus_price,item.margin];
          })
        ]
      }
    };
  }


  _getMargin(i,msrp,bonus){
    var a = Math.round ( ((msrp - bonus) / msrp) * 100)
    this.display_products[i].margin = a
    return a
  }
  _getBonusPrice(price,margin){
    var bonus_price = price - ( price * (margin / 100) )
    return bonus_price
  }
  _getStock(item){
    let total = 0
    if(item.inventory){
        item.inventory.forEach(item => {
          total += parseFloat(item.stock_left)
      });
      
    }
    return total
   
  }

}
