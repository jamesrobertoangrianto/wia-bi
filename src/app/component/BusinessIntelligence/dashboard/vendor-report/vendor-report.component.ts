import { Component, OnInit } from '@angular/core';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { NgxSpinnerService } from 'ngx-spinner';

import { matRangeDatepickerRangeValue } from 'mat-range-datepicker';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { formatNumber, formatDate } from '@angular/common';
import { AngularCsv } from 'angular-csv-ext/dist/Angular-csv';
pdfMake.vfs = pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-vendor-report',
  templateUrl: './vendor-report.component.html',
  styleUrls: ['./vendor-report.component.scss']
})
export class VendorReportComponent implements OnInit {
  sales_items: any;
  productName: any;
  fromDate: any;
  toDate: any;

  
  range: matRangeDatepickerRangeValue<any>
  from: any;
  to: any;
  shipping_fee: () => number;
  vendor_name: string;
  grand_total: number;
  start_date: any;
  end_date: any;

  constructor(
    private salesService : ManualOrderService,
    private spinner: NgxSpinnerService

  ) { 
    this.range={
      'begin': null,
      'end': null
    }
  }

  ngOnInit(): void {
   // this.getSalesByProduct()
  
   
  }

  download(){
    
    var data =  this.sales_items.map(item => {
      return [item.source,item.order_id, formatDate(item.order_date, 'yyyy/MM/dd', 'en_US'), item.name, parseInt(item.qty),parseInt(item.order_price)];
    })

    var options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true, 
      showTitle: true,
      headers: ["Platform","Order ID", "Date", "Product", "Qty","Sell Price / Item"],
      useHeader: false,
      nullToEmptyString: true,
    };
  
    new AngularCsv(data, 'filename', options);

  //  new AngularCsv(data, 'My Report');
  }

  
  async getSalesByProduct(start?, end?) {
    let fromDate = this.start_date
    let toDate = this.end_date


    if(this.productName){
      this.spinner.show();
      try {
      
        let response = await this.salesService.getSalesByProduct(this.productName,fromDate,toDate)
        this.sales_items = response.sales_items
        this.from = formatDate(response.from, 'yyyy/MM/dd', 'en_US') 
        this.to = formatDate(response.to, 'yyyy/MM/dd', 'en_US')
  
        console.log(this.sales_items)
        this.calculateGrandTotal()

        console.log(this.start_date)
      // console.log(response)
  
      } catch (e) {
        console.log(e)
      } finally {
        this.spinner.hide();
      }
    }
   
  }

  calculateGrandTotal(){
    if(this.sales_items){
      let subtotal = 0
      this.sales_items.forEach(item => {
          subtotal += (item.order_price * item.qty) - (item.cashback * item.qty)
      });
    //  console.log(subtotal)
     this.grand_total = subtotal
    }
  }

  getSubtotal(){
    var total = 0
    this.sales_items.forEach(item => {
      total += item.order_price * item.qty
    })

    return total
  }
  getTotalProduct(){
    var total = 0
    this.sales_items.forEach(item => {
      total += parseInt(item.qty)
    })

    return total
  }
  getTotal(){
    return this.getSubtotal 
  }

  getEducationObject() {
    return {
      table: {
       
        widths: [50,"*", 100, "*", "*","*","*"],
       
        body: [
          [{
            text: 'Order Id',
            style: 'tableHeader'
          },
          {
            text: 'Order Date',
            style: 'tableHeader'
          },
          {
            text: 'Product',
            style: 'tableHeader'
          },
          {
            text: 'Qty',
            style: 'tableHeader'
          },
          {
            text: 'Price',
            style: 'tableHeader'
          },
          {
            text: 'Cashback',
            style: 'tableHeader'
          },
          {
            text: 'Subtotal',
            style: 'tableHeader'
          },
          ],
          ...this.sales_items.map(item => {
            return [item.order_id, formatDate(item.order_date, 'yyyy/MM/dd', 'en_US'), item.name, parseInt(item.qty), parseInt(item.order_price),parseInt(item.cashback), (item.order_price*item.qty)-item.cashback*item.qty];
          })
        ]
      }
    };
  }


  generatePdf() {

      
    // var orderList[] = new Array();
    // this.selectedItem.items.forEach(item => {
    //   orderList.push('',item.name,item.order_price,item.order_qty,item.order_qty)
    // });

    // console.log(this.selectedItem.items)

    const documentDefinition = { 
      
      content: [
        {text: 'Sales Report' ,  margin: [ 0, 10, 0, 5 ],	fontSize: 16,
          bold: true,},
          
          {text: 'Company : Wearinasia (WIA.ID)' , 	fontSize: 10},
          {text: 'Date :' + this.from + ' - ' + this.to , 	fontSize: 10},
          
          {text: 'Create By : Sinta'  , 	fontSize: 10},
          {text: 'To : ' + this.vendor_name    ,  margin: [ 0, 10, 0, 5 ] ,	fontSize: 10},

          
      
        
          this.getEducationObject(),
        
        
          {text: 'Order Summary', margin: [ 0, 10, 0, 5 ] , 	fontSize: 10,
          bold: true,},
          {
            columns: [
                  
                    {
                      // auto-sized columns have their widths based on their content
                      width: '20%',
                      text: 'Total Product',
                      
                    
                    },
                    
                    {
                      // star-sized columns fill the remaining space
                      // if there's more than one star-column, available width is divided equally
                      width: '50%',
                      text: this.getTotalProduct(),
                    
                    },
                    
                    
               
              ],
            
          // optional space between columns
          columnGap: 10
        },


            {
                columns: [
                      
                        {
                          // auto-sized columns have their widths based on their content
                          width: '20%',
                          text: 'Subtotal',
                          
                        
                        },
                        
                        {
                          // star-sized columns fill the remaining space
                          // if there's more than one star-column, available width is divided equally
                          width: '50%',
                          text: this.getSubtotal(),
                        
                        },
                        
                        
                   
                  ],
                
              // optional space between columns
              columnGap: 10
            },
            {
                columns: [
                      
                        {
                          // auto-sized columns have their widths based on their content
                          width: '20%',
                          text: 'Shipping',
                        },
                        
                        {
                          // star-sized columns fill the remaining space
                          // if there's more than one star-column, available width is divided equally
                          width: '50%',
                          text: '-',
                        
                        },
                        
                        
                   
                  ],
                
              // optional space between columns
              columnGap: 10
            },
            
            {
                columns: [
                      
                        {
                          // auto-sized columns have their widths based on their content
                          width: '20%',
                          text: 'Total',
                        },
                        
                        {
                          // star-sized columns fill the remaining space
                          // if there's more than one star-column, available width is divided equally
                          width: '50%',
                          text: this.grand_total,
                        },
                        
                        
                   
                  ],
                
              // optional space between columns
              columnGap: 10
            },
            
           
          
        
        
          {text: 'Catatan : \n' + '', margin: [ 0, 20,0, 0 ] , 	fontSize: 12},
        
      
      
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 5, 5]
        },
        tableExample: {
          margin: [0, 0, 0, 0]
        },
        tableHeader: {
          bold: true,
          fontSize: 12,
          color: 'black'
        }
      },
      defaultStyle: {
        // alignment: 'justify'
      }

     };
   
    pdfMake.createPdf(documentDefinition).download()
  }




}
