import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoucherDiscountComponent } from './voucher-discount.component';

describe('VoucherDiscountComponent', () => {
  let component: VoucherDiscountComponent;
  let fixture: ComponentFixture<VoucherDiscountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoucherDiscountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoucherDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
