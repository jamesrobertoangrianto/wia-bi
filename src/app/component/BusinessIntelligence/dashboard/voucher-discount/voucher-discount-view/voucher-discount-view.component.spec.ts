import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoucherDiscountViewComponent } from './voucher-discount-view.component';

describe('VoucherDiscountViewComponent', () => {
  let component: VoucherDiscountViewComponent;
  let fixture: ComponentFixture<VoucherDiscountViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoucherDiscountViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoucherDiscountViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
