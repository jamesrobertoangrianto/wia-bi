import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { faChevronLeft, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-voucher-discount-view',
  templateUrl: './voucher-discount-view.component.html',
  styleUrls: ['./voucher-discount-view.component.scss']
})
export class VoucherDiscountViewComponent implements OnInit {





  promo: any;
  faTrashAlt = faTrashAlt
  faChevronLeft = faChevronLeft
  tab_view: string;
  promo_id: string;
  product: any;
  showModal: any;
  display_product: any;
  search_product: any;
  selected_product = []
  selected_added_product = []
  is_select_all: boolean;
  is_select_all_search: boolean;
  params: string;
  brand_list: any;
  sortListItems: ({ name: string; items: { id: string; name: string; label: string; }[]; label?: undefined; } | { name: string; label: string; items: any; })[];
  tab_menu_list: { id: string; label: string; }[];
  is_show_collection: any;
  collection: any;

  constructor(
    private webService: ManualOrderService,

    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastService,
    private router: Router,
    private serializer: UrlSerializer,
  ) {
    this.sortListItems = []
    this.getPromoBrand()
  }



  ngOnInit(): void {
    this.getCategoryCollection('')


    this.selected_added_product = []
    this.selected_product = []
    this.search_product = []


    this.route.paramMap.subscribe(params => {
      this.tab_view = params.get("tab_view")

      this.promo_id = params.get("id")
      this.getPromoRuleById(this.promo_id)

    })


    this.route.queryParamMap.subscribe(queryParams => {
      this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams }))

      this.tab_view = queryParams.get("tab_view")
      let search = queryParams.get("search")
      if (this.tab_view == 'setting') {
        this.getPromoRuleById(this.promo_id)

      }
      if (this.tab_view == 'product') {
        // this.getPromoProduct(this.promo_id)
        if (this.showModal == 'addProductModal') {
          this.searchPromoProduct()

        } else {
          this.getPromoProduct(this.promo_id)
        }
      }

      // if(search){
      //   this.searchPromoProduct(search)
      // }
    })



  }


  async getCategoryCollection(key) {

    try {
      let response = await this.webService.getCategoryCollection('');
      this.collection = response.data
      const categories = {
        BRAND: { name: 'brand_ids', label: 'Brand' },
        CATEGORY: { name: 'category_ids', label: 'Category' },
        ACTIVITY: { name: 'activity_ids', label: 'Activity' }
      };

      Object.entries(categories).forEach(([key, { name, label }]) => {
        if (response.data[key]) {
          this.sortListItems.push({
            name,
            label,
            items: response.data[key].map(({ id, name }) => ({ id, label: name, name: id }))
          });
        }
      });

    } catch (e) {
      console.error(e);
    } finally {
      this.spinner.hide();
    }
  }







  async fetch() {
    try {

      this.spinner.show();
      let response = await this.webService.fetch(this.promo_id)
      if (response.data) {
        this.toast.sendMessage('Berhasil')
      }
      console.log(response)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

    }
  }



  async removeSpecialPrice(item) {
    try {

      this.spinner.show();
      let response = await this.webService.removeSpecialPrice(item.entity_id)
      if (response.status_code == 200) {
        this.deletePromoProduct(item.id)
      }
      console.log(response)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

    }
  }




  async addCategoryProduct(item) {
    let form = [
      {
        'entity_id': item.id,
        'name': item.name,
        'brand': item.id,
        'promo_rule_id': this.promo.id,
        'type': item.type,
      }
    ]

    console.log(item)
    console.log(form)

    try {
      this.spinner.show();
      let response = await this.webService.addPromoProduct(form)

      console.log(response)

      this.getPromoProduct(this.promo_id)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

      // this.selected_product = []
      // this.showModal = null
      // this.selectAllSearch()
      // this.ngOnInit()

    }
  }


  async getPromoRuleById(id) {
    try {
      this.spinner.show();

      const response = await this.webService.getPromoRuleById(id);
      if (!response?.data) {
        throw new Error("Invalid response data");
      }

      this.promo = response.data;

      if (this.promo.type === 'special_price') {
        this.getPromoProduct(this.promo_id)

      }


      console.log(response);

      const baseMenu = [{ id: 'setting', label: 'Settings' }];

      this.tab_menu_list = this.promo.type === 'product_discount'
        ? [...baseMenu, { id: 'product', label: 'Product' }, { id: 'claim', label: 'Claimed Customer' }]
        : [...baseMenu];

    } catch (e) {
      console.error("Error fetching promo rule:", e);
    } finally {
      this.spinner.hide();
    }
  }


  updateStatus(key, value) {

    var a = {
      'id': key,
      'value': value
    }

    this.updatePromo(a)
  }

  selectAllSelected() {

    this.is_select_all = !this.is_select_all

    this.product.forEach(item => {
      item.checked = this.is_select_all
      this.selectAddedProduct(this.is_select_all, item.id)
    });
  }


  selectAllSearch() {

    this.is_select_all_search = !this.is_select_all_search

    this.search_product.forEach(item => {
      if (!item.isAdded) {
        item.checked = this.is_select_all_search
        this.selectProduct(this.is_select_all_search, item)
      }

    });
  }

  async updatePromo(e) {

    var form = {}
    form[e.id] = e.value
    try {
      this.spinner.show();
      let response = await this.webService.updatePromoRule(this.promo_id, form)

      console.log(response)
      this.toast.sendMessage('Success')

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.ngOnInit()
    }
  }


  async onChange(event: any, item) {
    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      formData.append('file', file);
      this.spinner.show();
      try {

        let res = await this.webService.uploadPhoto(formData, 'voucher_cover')
        console.log(res)
        item.photo = res.data
        if (res.data) {
          let form = {
            'image_url': item.photo,
            'id': item.id
          }
          let response = await this.webService.updatePromoRule(this.promo_id, form)


        }

      } catch (error) {
        console.log(error)
      }
      finally {

        this.spinner.hide()

      }

    }
  }




  async searchCollection(e) {
    console.log(e)
    if (e) {
      try {
        let response = await this.webService.getCategoryCollection('?name=' + e);
        this.collection = response.data


      } catch (e) {
        console.error(e);
      } finally {
        this.spinner.hide();
      }
    }



  }


  async getPromoProduct(id) {
    // console.log(this.params)
    try {
      this.spinner.show();
      let response = await this.webService.getPromoProduct(id, this.params)
      this.product = response.data

      console.log(response)
      console.log('dsini')

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  async getPromoBrand() {

    try {
      this.spinner.show();
      let response = await this.webService.searchPromoBrand()

      this.brand_list = response.data.map(item => {
        return {
          id: item.entity_id,
          label: item.name,
          name: item.name
        };
      });

      this.sortListItems.push(
        {

          'name': 'brand',
          'items': this.brand_list
        },
      )




    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }


  async searchPromoProduct() {

    try {
      this.spinner.show();
      let response = await this.webService.searchPromoProduct(this.params)
      this.search_product = response.data
      console.log(response)
      this.search_product.forEach(item => {
        item.isAdded = this.isAdded(item.entity_id)
      });

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

  async addProduct() {




    try {
      this.spinner.show();
      let response = await this.webService.addPromoProduct(this.selected_product)





    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();

      this.selected_product = []
      this.showModal = null
      this.selectAllSearch()
      this.ngOnInit()

    }

  }

  deleteItem() {
    this.selected_added_product.forEach(id => {
      this.deletePromoProduct(id)
    });


  }



  async deletePromoProduct(id) {


    try {
      this.spinner.show();
      let response = await this.webService.deletePromoProduct(id)



    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.ngOnInit()
    }

  }

  isAdded(id) {

    if (this.product.some(product => product.entity_id === id)) {
      return true
    } else {
      return false
    }

  }
  selectProduct(check, item) {
    console.log(item)

    if (check) {
      this.selected_product.push(
        {
          'entity_id': item.entity_id,
          'name': item.name,
          'brand': item.brand,
          'promo_rule_id': this.promo.id,
          'promo_code': this.promo.promo_code,
          'type': 'product',

        }
      )
    }
    else {
      this.selected_product = this.selected_product.filter(arrayItem => arrayItem.entity_id !== item.entity_id);
    }

    console.log(this.selected_product)

  }



  selectAddedProduct(check, id) {


    if (check) {
      this.selected_added_product.push(id)
    }
    else {
      this.selected_added_product = this.selected_added_product.filter(arrayItem => arrayItem !== id);
    }



  }



}
