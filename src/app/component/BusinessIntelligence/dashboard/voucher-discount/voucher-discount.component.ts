import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ManualOrderService } from 'src/app/services/manual-order/manual-order.service';

@Component({
  selector: 'app-voucher-discount',
  templateUrl: './voucher-discount.component.html',
  styleUrls: ['./voucher-discount.component.scss']
})
export class VoucherDiscountComponent implements OnInit {



  params: string;
  promo: any;

  promoForm = new FormGroup({
    name: new FormControl(null, { validators: [Validators.required] }),
    promo_code: new FormControl(null),
    status: new FormControl(false),
    type: new FormControl(null, { validators: [Validators.required] }),


  })
  showModal: any;
  tab_view: string;

  constructor(
    private webService: ManualOrderService,

    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toast: ToastrService,
    private router: Router,
    private serializer: UrlSerializer,

  ) {

  }

  ngOnInit(): void {

    this.route.queryParamMap.subscribe(queryParams => {

      this.params = this.serializer.serialize(this.router.createUrlTree([''],
        { queryParams: this.route.snapshot.queryParams }))
      this.tab_view = queryParams.get("tab_view")
      this.getPromoRule()



    })
  }


  setPromoType(id) {

    this.promoForm.get('type').setValue(id)



  }


  async addPromoRule() {
    console.log('special here')
    try {
      this.spinner.show();
      let response = await this.webService.addPromoRule(this.promoForm.value)

      console.log(response)
      console.log('special here')

      if (response.data.id) {
        this._navigateTo(response.data.id)

      }



    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
      this.showModal = null
      this.ngOnInit()

    }
  }



  _navigateTo(id) {
    this.router.navigate(
      ['/voucher-discount/view/' + id]
    );
  }

  async getPromoRule() {
    // console.log(this.params)
    try {
      this.spinner.show();
      let response = await this.webService.getPromoRule(this.params)
      this.promo = response.data
      console.log(response)

    } catch (e) {
      console.log(e)
    } finally {
      this.spinner.hide();
    }
  }

}
