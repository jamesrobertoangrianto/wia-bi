export interface Stock{
    id: string,
    brand: string,
    in_stock: any,
    out_of_stock: any,
    total: any
} 