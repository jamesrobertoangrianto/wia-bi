import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { faBell, faThLarge } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { ManualOrderService } from './services/manual-order/manual-order.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'wia-goods';
  userSubs: Subscription
  authenticated: boolean;

  faThLarge=faThLarge
  loginForm={
    email: null,
    password: null
  }
  resetForm={
    email: null,
    password: null,
    password_confirmation: null,
    token: null,

  }
  isLoading: boolean;
  dateStr: string;
  isCreative: boolean;
  faBell=faBell
  notification_count: number;
  tasks: any;
  operational_task: number;
  creative_task: number;
  isEnableResetPassword : boolean = false;
  isEnableLogin : boolean = true;
  isEnableSendResetPassword : boolean = false;
  appVersion: any;
  login_page: boolean;
  isactive: boolean;



  constructor(
 
    private toastr: ToastrService,
    private router : Router,
    private marketingService : ManualOrderService,
    private toast: ToastrService,

    public customerService : ManualOrderService,
    private spinner: NgxSpinnerService,

    private route: ActivatedRoute
    
  ) {
    this.appVersion = '0.70';
  }






  async ngOnInit() {
    let customer = JSON.parse(localStorage.getItem("customer"))
    
    if(customer.email == 'warehouse@wia.id'){
      this.router.navigate( ['warehouse/view/1'] );
    }
  }

 
  
}
