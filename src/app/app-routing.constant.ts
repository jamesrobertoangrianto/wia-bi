export type PAGE_PERMISSION_TYPE = |
    'financeReport' | //1
    'cashBank' | //2
    'purchaseOrder' |
    'salesOrder' | //4
    'productReport' |
    'chartOfAccount' |
    'orderManagement' |
    'product' | //8
    'warehouse' |
    'salesPipeline' |
    'vendorReport' |
    'customer' | //12
    'analytics' |
    'affiliate' |
    'revenuePlanning' |
    'promotion' | //16
    //17 empty
    'placement' | //18
    'journalAccount' | //19
    'journalTransaction' //20