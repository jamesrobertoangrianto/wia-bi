import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { DeviceDetectorModule } from "ngx-device-detector";
import { ServiceWorkerModule } from "@angular/service-worker";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { BIDashboardComponent } from "./component/BusinessIntelligence/dashboard/dashboard.component";
import { BISidebarComponent } from "./component/BusinessIntelligence/dashboard/bisidebar/bisidebar.component";
import { BIInventoryComponent } from "./component/BusinessIntelligence/dashboard/biinventory/biinventory.component";

import { ToastrModule } from "ngx-toastr";
import { NgxSpinnerModule } from "ngx-spinner";
import { ManualOrderComponent } from "./component/BusinessIntelligence/dashboard/manual-order/manual-order.component";
import { VendorReportComponent } from "./component/BusinessIntelligence/dashboard/vendor-report/vendor-report.component";
import { ProductComponent } from "./component/BusinessIntelligence/dashboard/product/product.component";
import { RevenuePlanningComponent } from "./component/BusinessIntelligence/dashboard/revenue-planning/revenue-planning.component";
import { PricelistComponent } from "./component/BusinessIntelligence/dashboard/pricelist/pricelist.component";
import { SalesOrderComponent } from "./component/BusinessIntelligence/dashboard/sales-order/sales-order.component";
import { SalesOrderViewComponent } from "./component/BusinessIntelligence/dashboard/sales-order/sales-order-view/sales-order-view.component";
import { CustomerComponent } from "./component/BusinessIntelligence/dashboard/customer/customer.component";
import { SearchComponent } from "./component/BusinessIntelligence/dashboard/search/search.component";
import { BarChartComponent } from "./component/BusinessIntelligence/dashboard/shared/bar-chart/bar-chart.component";
import { PlacementComponent } from "./component/BusinessIntelligence/dashboard/placement/placement.component";
import { PurchaseOrderListComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/purchase-order-list/purchase-order-list.component";
import { FinanceReportComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/finance-report/finance-report.component";
import { ExpenseComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/expense/expense.component";
import { CashBankComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/cash-bank/cash-bank.component";
import { AssetsManagementComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/assets-management/assets-management.component";
import { ChartAccountComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/chart-account/chart-account.component";
import { AccountTransactionComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/chart-account/account-transaction/account-transaction.component";
import { PurchaseOrderViewComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/purchase-order-list/purchase-order-view/purchase-order-view.component";
import { PurchaseOrderComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/purchase-order-list/add-purchase-order/purchase-order.component";
import { SalesorderComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/salesorder/salesorder.component";
import { SalesComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/salesreport/sales.component";
import { SalesorderViewComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/salesorder/salesorder-view/salesorder-view.component";
import { AddInventoryComponent } from "./component/BusinessIntelligence/dashboard/biinventory/add-inventory/add-inventory.component";
import { AddTransactionComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/cash-bank/add-transaction/add-transaction.component";
import { VoucherDiscountComponent } from "./component/BusinessIntelligence/dashboard/voucher-discount/voucher-discount.component";
import { VoucherDiscountViewComponent } from "./component/BusinessIntelligence/dashboard/voucher-discount/voucher-discount-view/voucher-discount-view.component";
import { SalesorderProductListComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/salesorder/salesorder-product-list/salesorder-product-list.component";
import { ShippingComponent } from "./component/BusinessIntelligence/dashboard/shipping/shipping.component";
import { ReferalComponent } from "./component/BusinessIntelligence/dashboard/referal/referal.component";
import { ReferalViewComponent } from "./component/BusinessIntelligence/dashboard/referal/referal-view/referal-view.component";
import { ReferalViewSalesComponent } from "./component/BusinessIntelligence/dashboard/referal/referal-view/referal-view-sales/referal-view-sales.component";
import { ReferalViewSettingsComponent } from "./component/BusinessIntelligence/dashboard/referal/referal-view/referal-view-settings/referal-view-settings.component";
import { BusinessOrderComponent } from "./component/BusinessIntelligence/dashboard/business-order/business-order.component";
import { BusinessOrderViewComponent } from "./component/BusinessIntelligence/dashboard/business-order/business-order-view/business-order-view.component";
import { ModalComponent } from "./component/BusinessIntelligence/dashboard/shared/modal/modal.component";
import { FormInputComponent } from "./component/BusinessIntelligence/dashboard/shared/form-input/form-input.component";
import { FormSearchComponent } from "./component/BusinessIntelligence/dashboard/shared/form-search/form-search.component";
import { StatusLabelComponent } from "./component/BusinessIntelligence/dashboard/shared/status-label/status-label.component";
import { TopNavigationComponent } from "./component/BusinessIntelligence/dashboard/shared/top-navigation/top-navigation.component";
import { TabMenuComponent } from "./component/BusinessIntelligence/dashboard/shared/tab-menu/tab-menu.component";
import { TableActionComponent } from "./component/BusinessIntelligence/dashboard/shared/table-action/table-action.component";
import { ModalFullComponent } from "./component/BusinessIntelligence/dashboard/shared/modal-full/modal-full.component";
import { AnalyticsComponent } from "./component/BusinessIntelligence/dashboard/analytics/analytics.component";
import { RemoveButtonComponent } from "./component/BusinessIntelligence/dashboard/shared/remove-button/remove-button.component";
import { WarehouseComponent } from "./component/BusinessIntelligence/dashboard/warehouse/warehouse.component";
import { WarehouseViewComponent } from "./component/BusinessIntelligence/dashboard/warehouse/warehouse-view/warehouse-view.component";
import { RackingOrderComponent } from "./component/BusinessIntelligence/dashboard/warehouse/warehouse-view/racking-order/racking-order.component";
import { PackingOrderComponent } from "./component/BusinessIntelligence/dashboard/warehouse/warehouse-view/packing-order/packing-order.component";
import { OpnameOrderComponent } from "./component/BusinessIntelligence/dashboard/warehouse/warehouse-view/opname-order/opname-order.component";
import { LoginComponent } from "./component/BusinessIntelligence/dashboard/login/login.component";
import { CardComponent } from "./component/BusinessIntelligence/dashboard/shared/card/card.component";
import { TabOptionComponent } from "./component/BusinessIntelligence/dashboard/shared/tab-option/tab-option.component";
import { environment } from "src/environments/environment.prod";
import { ProgressComponentComponent } from "./component/BusinessIntelligence/dashboard/shared/progress-component/progress-component.component";
import { FormSelectComponent } from "./component/BusinessIntelligence/dashboard/shared/form-select/form-select.component";
import { ViewPurchaseOrderComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/purchaseorder/view-purchase-order/view-purchase-order.component";
import { PurchaseorderComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/purchaseorder/purchaseorder.component";
import { ProductPerformanceComponent } from "./component/BusinessIntelligence/dashboard/shared/product-performance/product-performance.component";
import { OrderManagementComponent } from "./component/BusinessIntelligence/dashboard/order-management/order-management.component";
import { OrderManagementViewComponent } from "./component/BusinessIntelligence/dashboard/order-management/order-management-view/order-management-view.component";
import { OrderManagementAddComponent } from "./component/BusinessIntelligence/dashboard/order-management/order-management-add/order-management-add.component";
import { LazyLoadDirective } from "./component/BusinessIntelligence/dashboard/shared/lazy-load.directive";
import { SalesOrderFinanceComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/sales-order-finance/sales-order-finance.component";
import { SalesOrderProductComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/sales-order-product/sales-order-product.component";
import { ProductViewComponent } from "./component/BusinessIntelligence/dashboard/product/product-view/product-view.component";
import { FormMultipleSelectComponent } from "./component/BusinessIntelligence/dashboard/shared/form-multiple-select/form-multiple-select.component";
import { CollectionComponent } from "./component/BusinessIntelligence/dashboard/product/collection/collection.component";
import { ToastComponent } from "./component/BusinessIntelligence/dashboard/shared/toast/toast.component";
import { JournalAccountComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/journal-account/journal-account.component";
import { FormErrorDisplayComponent } from "./component/shared/form-error-display/form-error-display.component";
import { JournalTransactionComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/journal-transaction/journal-transaction.component";
import { JournalAccountTransactionComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/journal-account/journal-account-transaction/journal-account-transaction.component";
import { ReviewComponent } from "./component/BusinessIntelligence/dashboard/product/review/review.component";
import { BundlingComponent } from "./component/BusinessIntelligence/dashboard/product/bundling/bundling.component";
import { AngularEditorModule } from "@kolkov/angular-editor";
import { AddCashBackTransactionComponent } from './component/BusinessIntelligence/dashboard/FInanceModule/cash-bank/add-cash-back-transaction/add-cash-back-transaction.component';
import { BusinessSolutionComponent } from './component/BusinessIntelligence/dashboard/business-solution/business-solution.component';
import { BusinessSolutionViewComponent } from './component/BusinessIntelligence/dashboard/business-solution/business-solution-view/business-solution-view.component';
import { BundlingViewComponent } from './component/BusinessIntelligence/dashboard/product/bundling/bundling-view/bundling-view.component';

@NgModule({
  declarations: [
    AppComponent,
    BIDashboardComponent,
    BIInventoryComponent,
    BISidebarComponent,
    SalesComponent,

    ManualOrderComponent,
    PurchaseOrderComponent,
    PurchaseOrderListComponent,
    VendorReportComponent,
    ProductComponent,
    ExpenseComponent,
    FinanceReportComponent,
    RevenuePlanningComponent,
    PricelistComponent,
    SalesOrderComponent,
    SalesOrderViewComponent,
    CustomerComponent,
    SearchComponent,
    BarChartComponent,
    PlacementComponent,
    CashBankComponent,
    AssetsManagementComponent,
    ChartAccountComponent,
    AccountTransactionComponent,
    PurchaseOrderViewComponent,
    SalesorderComponent,
    SalesorderViewComponent,
    AddInventoryComponent,
    AddTransactionComponent,
    VoucherDiscountComponent,
    VoucherDiscountViewComponent,
    SalesorderProductListComponent,
    ShippingComponent,
    ReferalComponent,
    ReferalViewComponent,
    ReferalViewSalesComponent,
    ReferalViewSettingsComponent,
    BusinessOrderComponent,
    BusinessOrderViewComponent,
    ModalComponent,
    FormInputComponent,
    FormSearchComponent,
    StatusLabelComponent,
    TopNavigationComponent,
    TabMenuComponent,
    TableActionComponent,
    ModalFullComponent,
    AnalyticsComponent,
    RemoveButtonComponent,
    WarehouseComponent,
    WarehouseViewComponent,
    RackingOrderComponent,
    PackingOrderComponent,
    OpnameOrderComponent,
    LoginComponent,
    CardComponent,
    TabOptionComponent,
    ProgressComponentComponent,
    FormSelectComponent,
    ViewPurchaseOrderComponent,
    PurchaseorderComponent,
    ProductPerformanceComponent,
    OrderManagementComponent,
    OrderManagementViewComponent,
    OrderManagementAddComponent,
    LazyLoadDirective,
    SalesOrderFinanceComponent,
    SalesOrderProductComponent,
    JournalAccountComponent,
    ProductViewComponent,
    FormMultipleSelectComponent,
    CollectionComponent,
    ToastComponent,
    BundlingComponent,
    ReviewComponent,
    FormErrorDisplayComponent,
    JournalTransactionComponent,
    JournalAccountTransactionComponent,
    AddCashBackTransactionComponent,
    BusinessSolutionComponent,
    BusinessSolutionViewComponent,
    BundlingViewComponent,
  ],
  imports: [
    AngularEditorModule,
    BrowserModule,
    AppRoutingModule,
    DeviceDetectorModule.forRoot(),

    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production,
    }),
    BrowserAnimationsModule,
    HttpClientModule,

    FormsModule,
    ReactiveFormsModule,

    FontAwesomeModule,

    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    NgxSpinnerModule,
  ],

  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
