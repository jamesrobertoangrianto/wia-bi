import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BIDashboardComponent } from "./component/BusinessIntelligence/dashboard/dashboard.component";
import { VendorReportComponent } from "./component/BusinessIntelligence/dashboard/vendor-report/vendor-report.component";
import { ProductComponent } from "./component/BusinessIntelligence/dashboard/product/product.component";
import { ManualOrderComponent } from "./component/BusinessIntelligence/dashboard/manual-order/manual-order.component";
import { RevenuePlanningComponent } from "./component/BusinessIntelligence/dashboard/revenue-planning/revenue-planning.component";
import { PricelistComponent } from "./component/BusinessIntelligence/dashboard/pricelist/pricelist.component";
import { SalesOrderComponent } from "./component/BusinessIntelligence/dashboard/sales-order/sales-order.component";
import { SalesOrderViewComponent } from "./component/BusinessIntelligence/dashboard/sales-order/sales-order-view/sales-order-view.component";
import { CustomerComponent } from "./component/BusinessIntelligence/dashboard/customer/customer.component";
import { SearchComponent } from "./component/BusinessIntelligence/dashboard/search/search.component";
import { PlacementComponent } from "./component/BusinessIntelligence/dashboard/placement/placement.component";
import { FinanceReportComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/finance-report/finance-report.component";
import { PurchaseOrderListComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/purchase-order-list/purchase-order-list.component";
import { SalesComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/salesreport/sales.component";
import { CashBankComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/cash-bank/cash-bank.component";
import { ChartAccountComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/chart-account/chart-account.component";
import { AccountTransactionComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/chart-account/account-transaction/account-transaction.component";
import { PurchaseOrderViewComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/purchase-order-list/purchase-order-view/purchase-order-view.component";
import { PurchaseOrderComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/purchase-order-list/add-purchase-order/purchase-order.component";
import { SalesorderComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/salesorder/salesorder.component";
import { SalesorderViewComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/salesorder/salesorder-view/salesorder-view.component";
import { AddTransactionComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/cash-bank/add-transaction/add-transaction.component";
import { VoucherDiscountViewComponent } from "./component/BusinessIntelligence/dashboard/voucher-discount/voucher-discount-view/voucher-discount-view.component";
import { VoucherDiscountComponent } from "./component/BusinessIntelligence/dashboard/voucher-discount/voucher-discount.component";
import { SalesorderProductListComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/salesorder/salesorder-product-list/salesorder-product-list.component";
import { ShippingComponent } from "./component/BusinessIntelligence/dashboard/shipping/shipping.component";
import { ReferalComponent } from "./component/BusinessIntelligence/dashboard/referal/referal.component";
import { ReferalViewComponent } from "./component/BusinessIntelligence/dashboard/referal/referal-view/referal-view.component";
import { BusinessOrderComponent } from "./component/BusinessIntelligence/dashboard/business-order/business-order.component";
import { AnalyticsComponent } from "./component/BusinessIntelligence/dashboard/analytics/analytics.component";
import { WarehouseComponent } from "./component/BusinessIntelligence/dashboard/warehouse/warehouse.component";
import { WarehouseViewComponent } from "./component/BusinessIntelligence/dashboard/warehouse/warehouse-view/warehouse-view.component";
import { AuthGuard } from "./guard/auth.guard";
import { LoginComponent } from "./component/BusinessIntelligence/dashboard/login/login.component";
import { ViewPurchaseOrderComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/purchaseorder/view-purchase-order/view-purchase-order.component";
import { PurchaseorderComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/purchaseorder/purchaseorder.component";
import { OrderManagementComponent } from "./component/BusinessIntelligence/dashboard/order-management/order-management.component";
import { OrderManagementViewComponent } from "./component/BusinessIntelligence/dashboard/order-management/order-management-view/order-management-view.component";
import { SalesOrderFinanceComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/sales-order-finance/sales-order-finance.component";
import { SalesOrderProductComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/sales-order-product/sales-order-product.component";
import { JournalAccountComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/journal-account/journal-account.component";
import { JournalAccountTransactionComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/journal-account/journal-account-transaction/journal-account-transaction.component";
import { JournalTransactionComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/journal-transaction/journal-transaction.component";
import { AddCashBackTransactionComponent } from "./component/BusinessIntelligence/dashboard/FInanceModule/cash-bank/add-cash-back-transaction/add-cash-back-transaction.component";
import { BusinessSolutionComponent } from "./component/BusinessIntelligence/dashboard/business-solution/business-solution.component";

const routes: Routes = [
  {
    path: "",
    component: BIDashboardComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "warehouse",
    children: [
      {
        path: "",
        component: WarehouseComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "view/:id",
        component: WarehouseViewComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "analytics",
    children: [
      {
        path: "",
        component: AnalyticsComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "journal-account",
    children: [
      {
        path: "",
        component: JournalAccountComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "view/:id",
        component: JournalAccountTransactionComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  {
    path: "journal-transaction",
    children: [
      {
        path: "",
        component: JournalTransactionComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  {
    path: "chart-account",
    children: [
      {
        path: "",
        component: ChartAccountComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "view/:id",
        component: AccountTransactionComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "cash-bank",
    children: [
      {
        path: "",
        component: CashBankComponent,
        canActivate: [AuthGuard],
      },

      {
        path: "view/:id",
        component: AccountTransactionComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "add/v2/:id",
        component: AddCashBackTransactionComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "add/:id",
        component: AddTransactionComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "voucher-discount",
    children: [
      {
        path: "",
        component: VoucherDiscountComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "add/:id",
        component: VoucherDiscountViewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "view/:id",
        component: VoucherDiscountViewComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "referal",
    children: [
      {
        path: "",
        component: ReferalComponent,
        canActivate: [AuthGuard],
      },

      {
        path: "view/:id",
        component: ReferalViewComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "business-order",
    children: [
      {
        path: "",
        component: BusinessSolutionComponent,
        canActivate: [AuthGuard],
      },

      {
        path: "old",
        component: BusinessOrderComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  {
    path: "pricelist",
    component: PricelistComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "vendor-report",
    component: VendorReportComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "product",
    component: ProductComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "shipping",
    children: [
      {
        path: "",
        component: ShippingComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  {
    path: "revenue-planning",
    component: RevenuePlanningComponent,
    canActivate: [AuthGuard],
  },
  // {
  //   path: 'payroll',
  //   component: PayrollComponent,
  //   canActivate: [AuthGuard]

  // },

  {
    path: "finance-report",
    component: FinanceReportComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "purchase-order",
    children: [
      {
        path: "",
        component: PurchaseOrderListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "view/:id",
        component: PurchaseOrderViewComponent,
        canActivate: [AuthGuard],
      },

      {
        path: "add",
        component: PurchaseOrderComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "purchase",
    children: [
      {
        path: "",
        component: PurchaseorderComponent,
        canActivate: [AuthGuard],
      },

      {
        path: "id/:id",
        component: ViewPurchaseOrderComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "sales-order",
    children: [
      {
        path: "",
        component: SalesorderComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "view/:id",
        component: SalesorderViewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "product-list",
        component: SalesorderProductListComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "sales-order-report",
    children: [
      {
        path: "",
        component: SalesOrderFinanceComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "sales-order-report",
    children: [
      {
        path: "",
        component: SalesOrderFinanceComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "sales-order-product-report",
    children: [
      {
        path: "",
        component: SalesOrderProductComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "sales-report",
    component: SalesComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "placement",
    children: [
      {
        path: "",
        component: PlacementComponent,
        canActivate: [AuthGuard],
      },

    ],
  },

  {
    path: "customer",
    children: [
      {
        path: "",
        component: CustomerComponent,
        canActivate: [AuthGuard],
      },

    ],
  },
  {
    path: "search",
    children: [
      {
        path: "",
        component: SearchComponent,
        canActivate: [AuthGuard],
      },
    ],
  },


  {
    path: "order-management",
    children: [
      {
        path: "",
        component: SalesOrderComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "view/:id",
        component: SalesOrderViewComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  {
    path: "order-management-v2",
    children: [
      {
        path: "",
        component: OrderManagementComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "view/:id",
        component: OrderManagementViewComponent,
        canActivate: [AuthGuard],
      },
    ],
  },

  {
    path: "promotions",
    component: BIDashboardComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "manual-order",
    component: ManualOrderComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "**",
    redirectTo: "",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
