import { TestBed } from "@angular/core/testing";

import { JournalAccountService } from "../journal-account.service";

describe("JournalAccountService", () => {
  let service: JournalAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JournalAccountService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
