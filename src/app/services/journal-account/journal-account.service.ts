import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class JournalAccountService {
  private apiURL = "https://api.wia.id/api";

  constructor() {}

  private _headers = {
    "Content-Type": "application/json",
    Authorization: "Bearer xxxWIA2025xxx",
  };
  async updateJournalAccount(
    id: number,
    spec: Record<string, any>
  ): Promise<any> {
    const response = await fetch(
      this.apiURL + `/bi/journal/account/${id}/update`,
      {
        method: "PATCH",
        headers: this._headers,
        body: JSON.stringify(spec),
      }
    );
    const result = await response.json();
    return result;
  }

  async getJournalAccount(): Promise<any> {
    const response = await fetch(this.apiURL + "/bi/journal/account", {
      method: "GET",
      headers: this._headers,
    });
    const result = await response.json();
    return result;
  }

  async addJournalAccount(spec: Record<string, any>): Promise<any> {
    const response = await fetch(this.apiURL + "/bi/journal/account/add", {
      method: "POST",
      body: JSON.stringify(spec),
      headers: this._headers,
    });
    const result = await response.json();
    return result;
  }

  async getJournalAccountTransaction(
    id,
    spec: Record<string, any>
  ): Promise<any> {
    const queryParams = new URLSearchParams(spec).toString();

    const response = await fetch(
      this.apiURL + `/bi/journal/account/${id}/transaction` + "?" + queryParams,
      {
        method: "GET",
        headers: this._headers,
      }
    );

    const result = await response.json();
    return result;
  }

  async getJournalTransaction(spec = {}): Promise<any> {
    const queryParams = new URLSearchParams(spec).toString();

    const response = await fetch(
      this.apiURL + "/bi/journal/transaction" + "?" + queryParams,
      {
        method: "GET",
        headers: this._headers,
      }
    );

    const result = await response.json();
    return result;
  }

  async addBankTransaction(cashBackAccountId, spec, type): Promise<any> {
    const response = await fetch(
      this.apiURL + `/bi/journal/account/${cashBackAccountId}/type/${type}`,
      {
        method: "POST",
        headers: this._headers,
        body: JSON.stringify(spec),
      }
    );

    const result = await response.json();
    return result;
  }
}
