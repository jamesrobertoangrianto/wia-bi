import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Notification } from 'src/app/model/notification';
import { MatDialogConfig } from '@angular/material';
import {Stock} from 'src/app/model/stock'
import { Transaction } from 'src/app/model/transaction';

type action = 'Quote'|'Checkout' | ""
export type trans={
  "total_transactions": any,
  "transactions": Transaction[]
}

@Injectable({
  providedIn: 'root'
})

export class BusinessIntelligenceService {
  private notificationLink = "https://wia.id/wia-module/notification"
  private firebaseNotif = "https://fcm.googleapis.com/fcm/send"
  private authorization = "key=AIzaSyDPLB-HSYQmu_PeRqII0Mxwy6DDrlCLWsc"
  private inventory = "https://wia.id/wia-module/BI/inventory?category=19"
  private sales = "https://wia.id/wia-module/BI/salesv2"

  BaseURL = "https://wia.id/wia-module/BI/"
  private header={
    "Content-Type":"application/json",
    "Authorization": this.authorization
  }

  constructor(
    private http: HttpClient
  ) { }

  getAllFCMs(){
    return this.http.get<Notification[]>(this.notificationLink)
  }


    
  async getInventory(){
    
    let res = await fetch(this.BaseURL+'inventory',{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

      
  async getInventoryByName(name){
    
    let res = await fetch(this.BaseURL+'getInventoryByName/name/'+name,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async updateInventoryByItem(item){
    


    let res = await fetch(this.BaseURL+'updateInventory',{
      method: "POST",
      body: JSON.stringify(item)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      return data
    }
  }





  updateFCM(data: Notification){
    console.log("updating fcm action to "+data.behavior)
    return this.http.put(this.notificationLink+`?fcm_id=${data.fcm_id}`,data)
  }

  
  async getSales(start,end){
    
    let res = await fetch(this.BaseURL+'salesv2/from/'+start+'/to/'+end,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async salesOrders(start,end){
    
    let res = await fetch(this.BaseURL+'salesOrders/from/'+start+'/to/'+end,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  
  checkSingleFCM(fcm_id){
    return this.http.get("https://wia.id/wia-module/notification?fcm_id="+fcm_id)
  }

  registerFCMToFirebaseTopic(fcm, topic){
    fetch("https://iid.googleapis.com/iid/v1/"+fcm+"/rel/topics/"+topic,{
      method: "POST",
      headers: this.header
    }).then(
      (res)=>{
        console.log("register topic all success")
      },
    ).catch((err)=>{
      console.log("register topic all fail")
    })
  }

  sendNotification(body){
    return fetch(this.firebaseNotif,{
      headers: this.header,
      body: JSON.stringify(body),
      method: 'POST',
    })
  }

  // getInventory(){
  //   return this.http.get<Stock[]>(this.inventory)
  // }

  dialogConfig(data?: {}){
    let _dialogConfig = new MatDialogConfig()

    _dialogConfig.disableClose = true
    _dialogConfig.autoFocus = false

    _dialogConfig.height = '80vh'
    _dialogConfig.minWidth='60vw'
    _dialogConfig.maxWidth='60vw'
    _dialogConfig.panelClass=['mat-dialog-mobile']
  
    if(data != null) _dialogConfig.data=data
    return _dialogConfig
  }

}
