import { TestBed } from '@angular/core/testing';

import { BusinessIntelligenceService } from './business-intelligence.service';

describe('BusinessIntelligenceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BusinessIntelligenceService = TestBed.get(BusinessIntelligenceService);
    expect(service).toBeTruthy();
  });
});
