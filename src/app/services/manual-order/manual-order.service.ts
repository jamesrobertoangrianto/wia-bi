import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class ManualOrderService {
  public userSessionSubject = new BehaviorSubject(null)
  BaseURL = "https://wia.id/wia-module/BI/"
  BaseURLCatalog = "https://wia.id/wia-module/catalog/"
  BaseURLCustomer = "https://wia.id/wia-module/customer/"
  apiURL = "https://api.wia.id/api"
  customer: any
  userToken: any;
  constructor(
    private http: HttpClient
  ) { }



  async getSalesOrdersByInvoiceStatus(invoice_status, from, to, asset) {

    let res = await fetch(this.BaseURL + 'getSalesOrdersByInvoiceStatus/status/' + invoice_status + '/asset/' + asset + '/from/' + from + '/to/' + to, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  getAccountName() {
    return localStorage.getItem("account_name")
  }

  getAccountRole() {
    return localStorage.getItem("account_role")
  }


  async getProductVariant(id) {

    let res = await fetch(environment.APIBaseURL + '/catalog/getProductVariant/entity_id/' + id, {
      method: "GET",

    })
    let data = await res.json()



    return data
  }

  async removeShippingRates(id) {

    let res = await fetch(this.BaseURL + 'removeShippingRates/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async addShippingRates(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addShippingRates', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async getSalesOrderById(id) {

    let res = await fetch(this.BaseURL + 'getSalesOrderById/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getSalesOrders(status, page, count, query?) {

    let res = await fetch(this.BaseURL + 'salesOrders/status/' + status + '/page/' + page + '/count/' + count + '/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getSearchItem(query, type) {

    let res = await fetch(this.BaseURLCatalog + 'getSearchItem/query/' + query + '/type/' + type, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async updateSearch(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURLCatalog + 'updateSearch', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async updateSearchItemById(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURLCatalog + 'updateSearchItemById', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    return data
  }

  async updateSalesOrderTransactionFee(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updateSalesOrderTransactionFee', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    return data
  }


  async getOrderById(id) {

    let res = await fetch(this.BaseURL + 'getOrderById/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getTrackingOrder(id) {

    let res = await fetch(this.BaseURL + 'getTrackingOrder/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getCityList(page, name) {

    let res = await fetch('https://wia.id/wia-module/api/getcity/page/' + page + '/id/' + name, {
      method: "GET"
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_code
    else {
      return data
    }

  }

  async getNotification(customer_id) {

    let res = await fetch(this.BaseURL + 'getNotification/id/' + customer_id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getTopNotif(customer_id) {

    let res = await fetch(this.BaseURL + 'getTopNotif/id/' + customer_id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async updateNotif(id) {

    let res = await fetch(this.BaseURL + 'updateNotif/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }





  async getAffiliateMember() {

    let res = await fetch(this.apiURL + '/feature/affiliate/member', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getAffiliateWithdrawal(query?) {

    let res = await fetch(this.apiURL + '/feature/affiliate/withdrawal/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async getPromoRule(query?) {

    let res = await fetch(this.apiURL + '/feature/promo/rule/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getWarehouse(query?) {

    let res = await fetch(this.apiURL + '/bi/warehouse/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async getDashboard(id, query?) {

    let res = await fetch(this.apiURL + '/bi/dashboard/' + id + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getProductPerformance(id) {

    let res = await fetch(this.apiURL + '/catalog/product/' + id + '/performance', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getSalesOrderSummary(query) {

    let res = await fetch(this.BaseURL + 'getSalesOrderSummary/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }





  async getWarehouseId(id) {

    let res = await fetch(this.apiURL + '/bi/warehouse/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }




  async getPurchaseId(id) {

    let res = await fetch(this.apiURL + '/bi/purchase/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getPurchaseCount(id) {

    let res = await fetch(this.apiURL + '/bi/purchase/' + id + '/count', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async getPurchaseItem(id) {

    let res = await fetch(this.apiURL + '/bi/purchase/' + id + '/item', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getInventory(id, query?) {

    let res = await fetch(this.apiURL + '/bi/warehouse/' + id + '/inventory/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getPackingOrder(id) {

    let res = await fetch(this.apiURL + '/bi/warehouse/inventory/' + id + '/packing/', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getCheckinOrder(id) {

    let res = await fetch(this.apiURL + '/bi/warehouse/inventory/' + id + '/checkin/', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }





  async getInventoryByOrder(order_id, query?) {

    let res = await fetch(this.apiURL + '/bi/warehouse/' + order_id + '/inventory/order' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async getWarehouseRack(id, query?) {

    let res = await fetch(this.apiURL + '/bi/warehouse/' + id + '/rack/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }







  async getOpnameOrder(id) {

    let res = await fetch(this.apiURL + '/bi/warehouse/inventory/' + id + '/opname/', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getWarehouseBuffer(id, query?) {

    let res = await fetch(this.apiURL + '/bi/warehouse/' + id + '/buffer/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async addPromoRule(form) {


    let res = await fetch(this.apiURL + '/feature/promo/rule/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async addWarehouse(form) {


    let res = await fetch(this.apiURL + '/bi/warehouse/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async uploadPhoto(formvalue, dir_name) {




    let res = await fetch(this.apiURL + '/bi/upload/' + dir_name, {

      method: "POST",
      // headers: { 'Content-Type': 'application/json' },
      body: formvalue //JSON.stringify(formvalue)

    })

    let data = await res.json()
    return data

  }




  async addInventory(form) {


    let res = await fetch(this.apiURL + '/bi/warehouse/inventory/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async addCheckinOrder(form) {


    let res = await fetch(this.apiURL + '/bi/warehouse/inventory/checkin/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async addOpnameOrder(form) {


    let res = await fetch(this.apiURL + '/bi/warehouse/inventory/opname/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async updateOpnameOrder(form) {


    let res = await fetch(this.apiURL + '/bi/warehouse/inventory/opname/update', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async updateInventory(id, form) {


    let res = await fetch(this.apiURL + '/bi/warehouse/inventory/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async deleteInventory(id) {


    let res = await fetch(this.apiURL + '/bi/warehouse/inventory/' + id + '/delete', {

      method: "DELETE",
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async allocateInventory(qty, form) {


    let res = await fetch(this.apiURL + '/bi/warehouse/inventory/qty/' + qty + '/allocate', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }
  async addWarehouseRack(form) {


    let res = await fetch(this.apiURL + '/bi/warehouse/rack/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async getPurchase(query?) {


    let res = await fetch(this.apiURL + '/bi/purchase' + query, {

      method: "GET",

      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async getSalesOrderV2(query?) {


    let res = await fetch(this.apiURL + '/sales/order' + query, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async getProduct(query?) {


    let res = await fetch(this.apiURL + '/catalog/product' + query, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async getProductReviews(query?) {


    let res = await fetch(this.apiURL + '/catalog/product/review' + query, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async getProductVariants(id) {


    let res = await fetch(this.apiURL + '/catalog/product/' + id + '/variant', {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async getCategory(query?) {


    let res = await fetch(this.apiURL + '/catalog/category' + query, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async getCategoryCollection(query?) {


    let res = await fetch(this.apiURL + '/catalog/category/collection' + query, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }





  async fetchSpecialPriceByid(id) {


    let res = await fetch(this.apiURL + 'feature/promo/' + id + '/special_price/fetch', {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }









  async getProductId(id) {


    let res = await fetch(this.apiURL + '/catalog/product/id/' + id, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async getCategoryProduct(id, query?) {


    let res = await fetch(this.apiURL + '/catalog/category/' + id + '/product' + query, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async getBundlingitem(id) {


    let res = await fetch(this.apiURL + '/catalog/bundling/' + id + '/item', {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async getCategoryChild(id) {


    let res = await fetch(this.apiURL + '/catalog/category/' + id + '/child', {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addCategoryProduct(cat_id, form) {

    let res = await fetch(this.apiURL + '/catalog/category/' + cat_id + '/product/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async addProductPromotion(form) {

    let res = await fetch(this.apiURL + '/catalog/product/promotion/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async addBundlingItem(id, form) {

    let res = await fetch(this.apiURL + '/catalog/bundling/' + id + '/item/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addCategory(form) {

    let res = await fetch(this.apiURL + '/catalog/category/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async deleteCategoryProduct(cat_id, form) {

    let res = await fetch(this.apiURL + '/catalog/category/' + cat_id + '/product/delete', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async deleteBundlingItem(id) {

    let res = await fetch(this.apiURL + '/catalog/bundling/item/' + id + '/delete', {

      method: "DELETE",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async updateProductID(id, form) {

    let res = await fetch(this.apiURL + '/catalog/product/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async updateProductReview(id, form) {

    let res = await fetch(this.apiURL + '/catalog/product/review/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async updateCategory(id, form) {

    let res = await fetch(this.apiURL + '/catalog/category/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async deleteCategory(id) {

    let res = await fetch(this.apiURL + '/catalog/category/' + id + '/delete', {

      method: "DELETE",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async updateProductAttribute(id, form) {

    let res = await fetch(this.apiURL + '/catalog/product/attribute/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async deleteProductAttribute(id) {

    let res = await fetch(this.apiURL + '/catalog/product/attribute/' + id + '/delete', {

      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async deleteProductID(id) {

    let res = await fetch(this.apiURL + '/catalog/product/' + id + '/delete', {

      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async addProduct(form) {

    let res = await fetch(this.apiURL + '/catalog/product/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }





  async getPlacement(query?) {

    let res = await fetch(this.apiURL + '/catalog/placement' + query, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async getBundling(query?) {

    let res = await fetch(this.apiURL + '/catalog/bundling' + query, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addPlacement(form) {

    let res = await fetch(this.apiURL + '/catalog/placement/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addBundling(form) {

    let res = await fetch(this.apiURL + '/catalog/bundling/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async updatePlacement(id, form) {

    let res = await fetch(this.apiURL + '/catalog/placement/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async updateBundling(id, form) {

    let res = await fetch(this.apiURL + '/catalog/bundling/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async updateBundlingItem(id, form) {

    let res = await fetch(this.apiURL + '/catalog/bundling/item/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }





  async fetch(id) {

    let res = await fetch(this.apiURL + '/feature/promo/' + id + '/special_price/fetch', {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async removeSpecialPrice(id) {

    let res = await fetch(this.apiURL + '/feature/promo/collection/' + id + '/special_price/remove', {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async addProductAttribute(id, form) {

    let res = await fetch(this.apiURL + '/catalog/product/' + id + '/attribute/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addCategoryAttribute(id, form) {

    let res = await fetch(this.apiURL + '/catalog/category/' + id + '/attribute/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async duplicateProduct(id) {

    let res = await fetch(this.apiURL + '/catalog/product/' + id + '/duplicate', {

      method: "POST",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async addProductGallery(id, form) {

    let res = await fetch(this.apiURL + '/catalog/product/' + id + '/gallery/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async addCategoryGallery(id, form) {

    let res = await fetch(this.apiURL + '/catalog/category/' + id + '/gallery/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async deleteProductGallery(id) {

    let res = await fetch(this.apiURL + '/catalog/product/gallery/' + id + '/delete', {

      method: "DELETE",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async getSalesOrderReport(query?) {


    let res = await fetch(this.apiURL + '/sales/order/report' + query, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async getSalesOrderProductReport(query?) {


    let res = await fetch(this.apiURL + '/sales/order/product/report' + query, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async getSalesOrderId(id) {


    let res = await fetch(this.apiURL + '/sales/order/id/' + id, {

      method: "GET",

      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async updateSalesOrder(id, form) {


    let res = await fetch(this.apiURL + '/sales/order/' + id + '/update', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async addSalesOrderItem(form) {


    let res = await fetch(this.apiURL + '/sales/order/item/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addShippingOrder(id, form) {


    let res = await fetch(this.apiURL + '/sales/order/' + id + '/shipping/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async updateSalesOrderItem(id, form) {

    let res = await fetch(this.apiURL + '/sales/order/item/' + id + '/update', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async addSalesOrder(form) {


    let res = await fetch(this.apiURL + '/sales/order/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer xxxWIA2025xxx',
      },

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async getPurchaseVendor(query?) {


    let res = await fetch(this.apiURL + '/bi/purchase/vendor' + query, {

      method: "GET",

      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addPurchase(form) {


    let res = await fetch(this.apiURL + '/bi/purchase/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async addPurchaseItem(form) {


    let res = await fetch(this.apiURL + '/bi/purchase/item/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async updatePurchase(form, id) {


    let res = await fetch(this.apiURL + '/bi/purchase/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async updatePurchaseItem(form, id) {


    let res = await fetch(this.apiURL + '/bi/purchase/item/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async deletePurchaseItem(id) {


    let res = await fetch(this.apiURL + '/bi/purchase/item/' + id + '/delete', {

      method: "DELETE",

      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async addPurchaseVendor(form) {


    let res = await fetch(this.apiURL + '/bi/purchase/vendor/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async addWarehouseBuffer(form) {


    let res = await fetch(this.apiURL + '/bi/warehouse/buffer/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async updateWarehouseBuffer(form, id) {


    let res = await fetch(this.apiURL + '/bi/warehouse/buffer/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async updatePromoRule(id, form) {


    let res = await fetch(this.apiURL + '/feature/promo/rule/' + id + '/update', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addPromoProduct(form) {


    let res = await fetch(this.apiURL + '/feature/promo/product/add', {

      method: "POST",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async deletePromoProduct(id) {


    let res = await fetch(this.apiURL + '/feature/promo/product/' + id + '/delete', {

      method: "DELETE",
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async getPromoRuleById(id, query?) {

    let res = await fetch(this.apiURL + '/feature/promo/rule/id/' + id + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getPromoProduct(id, query) {

    let res = await fetch(this.apiURL + '/feature/promo/rule/' + id + '/product/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async searchPromoBrand() {

    let res = await fetch(this.apiURL + '/feature/promo/search/brand', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async searchPromoProduct(search) {

    let res = await fetch(this.apiURL + '/feature/promo/search/product' + search, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }




  async getAffiliateMemberTransaction(member_id) {

    let res = await fetch(this.apiURL + '/feature/affiliate/member/' + member_id + '/transaction', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }




  async getAffiliateMemberWithdrawal(member_id) {

    let res = await fetch(this.apiURL + '/feature/affiliate/member/' + member_id + '/withdrawal', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async addAffiliateMember(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addAffiliateMember', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async getAffiliateMembersByName(value) {
    let res = await fetch(this.BaseURL + 'getAffiliateMembersByName/name/' + value, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async updateAffiliateMember(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updateAffiliateMember', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async getAffiliateProductByAffiliateId(id) {

    let res = await fetch(this.BaseURL + 'getAffiliateProductByAffiliateId/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async editAffiliateProductByAffiliateId(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'editAffiliateProductByAffiliateId', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async updateAffiliateWithdrawal(form, id) {

    let res = await fetch(this.apiURL + '/feature/affiliate/withdrawal/' + id + '/update/', {

      method: "PATCH",
      body: JSON.stringify(form),
      headers: { 'Content-Type': 'application/json' }

    })
    let data = await res.json()
    return data
  }


  async editAffiliateProductSetting(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'editAffiliateProductSetting', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async getAffiliateProductSetting(query?) {

    let res = await fetch(this.BaseURL + 'getAffiliateProductSetting/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }





  async addAffiliateProductByAffiliateId(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addffiliateProductByAffiliateId', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async addSingleProductByAffiliateId(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addSingleProductByAffiliateId', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async getAffiliateHistoryByAffiliateId(id) {

    let res = await fetch(this.BaseURL + 'getAffiliateHistoryByAffiliateId/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getAffiliateSalesByAffiliateId(id) {

    let res = await fetch(this.BaseURL + 'getAffiliateSalesByAffiliateId/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getAffiliateByAffiliateId(id) {

    let res = await fetch(this.BaseURL + 'getAffiliateByAffiliateId/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async updataStatusById(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updataStatusById', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async selectShipment(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'selectShipment', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async cancelShipment(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'cancelShipment', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async getDistrict(city_id: string) {
    let link = environment.APIBaseURL + '/checkoutv3/districtlist?city_id=' + city_id
    let data = await this.http.get<any>(link).toPromise()


    return data
  }




  async searchProduct(searchQuery: string, page?) {
    let link = environment.APIBaseURL + "/catalog/getproducst?q=" + searchQuery
    if (page) link += "&p=" + page

    let res = await fetch(link)
    let data = await res.json()
    if (data == null) data = []
    return data
  }

  async getCustomerGroup() {
    let link = environment.APIBaseURL + "/checkoutv3/getCustomerGroup"
    let res = await fetch(link)
    let data = await res.json()
    if (data == null) data = []
    return data
  }



  async createManualOrder(formvalue) {
    //console.log(formvalue)
    let res = await fetch(environment.APIBaseURL + '/BI/createBIQuote', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async getShippingRates(formvalue) {
    //console.log(formvalue)
    let res = await fetch(environment.APIBaseURL + '/BI/getShippingRates', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async getManualShippingList(quote_id) {

    let res = await fetch(environment.APIBaseURL + '/checkoutv3/getBIQuoteShippingList/id/' + quote_id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async createManualOrderStep2(formvalue) {
    //console.log(formvalue)
    let res = await fetch(environment.APIBaseURL + '/BI/createOrderFromBIQuote', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async getPurchaseOrderById(id) {

    let res = await fetch(this.BaseURL + 'getPurchaseOrderById/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getBusinessOrder(query?) {

    let res = await fetch(this.BaseURL + 'getBusinessOrder/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async getConversion(query?) {

    console.log('here')
    let res = await fetch(this.apiURL + '/catalog/event/conversion/' + query, {
      method: "GET"
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_code
    else {
      return data
    }

  }



  async getLinkbuilder(query?) {

    let res = await fetch(this.apiURL + '/catalog/linkbuilder/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async deleteLinkbuilder(id) {

    let res = await fetch(this.apiURL + '/catalog/linkbuilder/' + id + '/delete', {
      method: "DELETE"
    })
    let data = await res.json()
    return data
  }


  async addLinkBuilder(formvalue) {

    //console.log(formvalue)
    let res = await fetch(this.apiURL + '/catalog/linkbuilder/add', {
      method: "POST",
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(formvalue)

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async getProductByConversionType(type, campaign, source) {

    let res = await fetch(this.BaseURL + 'getProductByConversionType/type/' + type + '/campaign_id/' + campaign + '/source/' + source, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getCheckoutConversionType(type, campaign, source) {

    let res = await fetch(this.BaseURL + 'getCheckoutConversionType/type/' + type + '/campaign_id/' + campaign + '/source/' + source, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async deleteBusinessOrder(id?) {

    let res = await fetch(this.BaseURL + 'deleteBusinessOrder/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getBusinessOrderById(id) {

    let res = await fetch(this.BaseURL + 'getBusinessOrderById/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async addBusinessOrder(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addBusinessOrder', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async updateBusinessOrder(formvalue, order_id) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updateBusinessOrder/id/' + order_id, {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async updateBusinessOrderProduct(formvalue, order_id) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updateBusinessOrderProduct/id/' + order_id, {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async addBusinessOrderProduct(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addBusinessOrderProduct', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }







  async getLeaves(id) {

    let res = await fetch(this.BaseURL + 'getLeaves/customer_id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async addLeave(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addLeave', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async editLeave(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'editLeave', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async getExpense(from, to) {

    let res = await fetch(this.BaseURL + 'getExpenses/from/' + from + '/to/' + to, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async addPayout(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addPayout', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }






  async addstatement(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addstatement', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addChartAccount(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addChartAccount', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async addPurchasePayment(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addPurchasePayment', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async addReceivePayment(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addReceivePayment', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async addTransferFund(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addTransferFund', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addReconcile(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addReconcile', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addPurchaseReceive(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addPurchaseReceive', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async updatePurchaseItemById(item) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updatePurchaseItemById', {
      method: "POST",
      body: JSON.stringify(item)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }
  async getChartAccounts() {

    let res = await fetch(this.BaseURL + 'getChartAccounts', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getChartAccoutsByType() {

    let res = await fetch(this.BaseURL + 'getChartAccoutsByType', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getChartAccountByType(type) {

    let res = await fetch(this.BaseURL + 'getChartAccountByType/type/' + type, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getChartAccoutsByExpenseType() {

    let res = await fetch(this.BaseURL + 'getChartAccoutsByExpenseType', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getPurchaseOrdersByStatus(status) {

    let res = await fetch(this.BaseURL + 'getPurchaseOrdersByStatus/status/' + status, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getPurchaseOrdersByQuery(query) {

    let res = await fetch(this.BaseURL + 'getPurchaseOrdersByQuery/query/' + query, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getPurchaseOrdersByPaymentStatus(status) {

    let res = await fetch(this.BaseURL + 'getPurchaseOrdersByPaymentStatus/status/' + status, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async getChartAccoutsById(id, from, to) {

    let res = await fetch(this.BaseURL + 'getChartAccoutsById/id/' + id + '/from/' + from + '/to/' + to, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getCashflowByDate(from_date, end_date) {

    let res = await fetch(this.BaseURL + 'getCashflowByDate/from_date/' + from_date + '/end_date/' + end_date, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getJournalTransactionByAccountType(type, from, to) {

    let res = await fetch(this.BaseURL + 'getJournalTransactionByAccountType/type/' + type + '/from/' + from + '/to/' + to, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async deletJournalTransactionById(id) {

    let res = await fetch(this.BaseURL + 'deletJournalTransactionById/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }




  async getStatement(year, month) {

    let res = await fetch(this.BaseURL + 'getstatement/year/' + year + '/month/' + month, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getBrands(name) {

    let res = await fetch(this.BaseURL + 'getBrands/name/' + name, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getInventoryItem(name) {

    let res = await fetch(this.BaseURL + 'getinventoryitemsbybrand/name/' + name, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getRevenueId(name, asset) {

    let res = await fetch(this.BaseURL + 'getrevenueId/name/' + name + '/asset/' + asset, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getProductList(name, size, page, buffer_filter, include_inventory?) {

    let res = await fetch(this.BaseURL + 'getproducts/name/' + name + '/size/' + size + '/page/' + page + '/buffer_filter/' + buffer_filter + '/include_inventory/' + include_inventory, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async getProductStock(id) {

    let res = await fetch(this.BaseURL + 'getProductStock/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getProductList2(name, size, page, buffer_filter) {

    let res = await fetch(this.BaseURL + 'getproducts/name/' + name + '/size/' + size + '/page/' + page + '/buffer_filter/' + buffer_filter + '/parrent/1', {
      method: "GET"
    })
    let data = await res.json()
    return data
  }



  async getSalesByProduct(name, start, end) {

    let res = await fetch(this.BaseURL + 'getsalesbyproduct/product/' + name + '/from/' + start + '/to/' + end, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async login(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURLCustomer + 'login', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })


    let data = await res.json()
    if (data.message_code != 200) throw data.message_dialog
    else {
      this.customer = true
      this.userSessionSubject.next(data)
      return data
    }

  }


  async getSession(key) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURLCustomer + 'getSession/session/' + key, {
      method: "GET",

    })
    let data = await res.json()
    this.userSessionSubject.next(data.customer)



    return data
  }



  async sendResetPassword(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURLCustomer + 'sendResetPassword', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async resetPassword(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURLCustomer + 'resetPassword', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async updateInventoryItems(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updateInventoryItems', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async updateProduct(id, formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updateProduct/id/' + id, {
      method: "POST",
      body: JSON.stringify(formvalue),

    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  //marketing

  async getSalesOrder(from, to) {

    let res = await fetch(this.BaseURL + 'salesv2/from/' + from + '/to/' + to, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getRevenuePlanning(month, year) {

    let res = await fetch(this.BaseURL + 'getrevenueplanning/month/' + month + '/year/' + year, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async deleteRevenuePlanning(id) {

    let res = await fetch(this.BaseURL + 'deleteRevenue/id/' + id, {
      method: "POST"
    })
    let data = await res.json()
    return data
  }



  async getLandingPageById(id) {

    let res = await fetch(this.BaseURL + 'getLandingPage/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async addLandingPage(id, type) {
    let res = await fetch(this.BaseURL + 'addlandingpage/campaign_id/' + id + '/type/' + type, {
      method: "POST"
    })
    let data = await res.json()
    return data
  }

  async editLandingPage(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'editlandingpage', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




  async getComments(id) {

    let res = await fetch(this.BaseURL + 'getComments/id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async addTask(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addtask', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async updateTask(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updateTask', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }



  async addComment(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addcomment', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async getCampaignById(id) {

    let res = await fetch(this.BaseURL + 'getcampaignByid/campaign_id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getCampaignProductById(id) {

    let res = await fetch(this.BaseURL + 'getcampaignByid/campaign_id/' + id, {
      method: "GET"
    })
    let data = await res.json()
    return data
  }


  async addCampaign(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addcampaign', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }
  async editCampaign(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'editcampaign', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }

  async addCampaignAsset(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addcampaignasset', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async addRevenue(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'addrevenue', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }
  async updateRevenue(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updaterevenue', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async updateCampaignStatus(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'updateCampaignStatus', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }


  async duplicateCampaign(formvalue) {
    //console.log(formvalue)
    let res = await fetch(this.BaseURL + 'duplicateCampaign', {
      method: "POST",
      body: JSON.stringify(formvalue)
    })
    let data = await res.json()
    if (data.status_code != 200) throw data.message_dialog
    else {
      return data
    }
  }




}
