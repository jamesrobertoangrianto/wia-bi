import { Injectable,EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  isFull: boolean;
  messages: string;
  actionName: string;
  actionFn: VoidFunction;

  launchEvent: EventEmitter<string> = new EventEmitter()

  constructor() { }

  sendMessage(msg:string, actionName?: string, callbackFn?: VoidFunction, isFull?:boolean){
    console.log('woi')
    this.messages = msg;
    this.isFull = isFull;
    this.actionName = actionName;
    this.actionFn = callbackFn
    this.launchEvent.emit("launch")
  }

  destroy(){
    this.messages = null
    this.actionFn = null
    this.actionName = null
  }
}
