import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ManualOrderService } from '../services/manual-order/manual-order.service';



@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
  session_id: any;
  constructor(
    private webService : ManualOrderService,
    private router: Router
  ) {}

  async canActivate() {
    
     let data = localStorage.getItem("session_token")
   
     if(!data){
        this.router.navigate( ['/login'] );
        return false
     }else{
      // let response = await this.webService.getSession(data)
      let customer = JSON.parse(localStorage.getItem("customer"))

    
      


  
        if(customer){
          return true
        }else{
          this.router.navigate( ['/login'] );
          return false
        }
      
       
     }

  }








  encript(data){
    return data.split("").reduce(function(a, b) {
      a = ((a << 5) - a) + b.charCodeAt(0);
      return a & a;
    }, 0);
  }

  
}
