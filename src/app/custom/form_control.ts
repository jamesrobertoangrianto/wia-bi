import { FormControl } from '@angular/forms';
import { isNumber } from 'util';

export function noWhiteSpace(e: FormControl){
    try{
        if(e.value.includes(' ')) return {'whitespace':true}
        else return null
    }catch(e){
        return null
    }
}


export const formError={
    'whitespace': "This field should not contain spaces",
}